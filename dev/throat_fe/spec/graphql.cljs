;; spec/graphql.cljs -- Check specs for the graphql queries
;; Copyright (C) 2022 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.spec.graphql
  (:require
   [clojure.spec.alpha :as spec]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [expound.alpha :as expound]
   [re-frame.core :as re-frame]
   [throat-fe.config :as config]
   [throat-fe.errors :as errors]
   [throat-fe.log :as log]))

(defn- spec-name [typ query-key]
  (keyword 'throat-fe.content.graphql-spec
           (str (name typ) "." (name query-key))))

(re-frame/reg-event-db ::check-graphql-spec
  ;; :doc Check the result of a graphql query or mutation against its spec.
  ;; :doc Does nothing if the query returns an error, assuming that will
  ;; :doc be handled elseware.
  (fn-traced [db [_ {:keys [type query-key response]}]]
    (if-not (or config/debug? config/testing?)
      db
      (let [{:keys [data errors]} response
            failed? (when (and data (nil? errors))
                      (let [spec (spec-name type query-key)]
                        (when (not (spec/valid? spec data))
                          (log/warn {:spec spec
                                     :expound (expound/expound-str spec data)}
                                    "Query result does not match spec")
                          true)))]
        (errors/assoc-errors db {:event query-key
                                 :errors
                                 (when failed?
                                   ["Query result does not match spec"])
                                 :describe-error-fn (fn [msg] {:msg msg})})))))
