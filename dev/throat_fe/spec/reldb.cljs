;; spec/reldb.cljs -- Validate reldb spec for throat-fe
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.spec.reldb
  (:require
   [clojure.spec.alpha :as spec]
   [expound.alpha :as expound]
   [throat-fe.content.graphql-spec :as gspec]
   [throat-fe.content.loader :as loader]
   [throat-fe.content.spec :as content]
   [throat-fe.log :as log]
   [throat-fe.ui.forms.core :as forms]))

(defn valid? [spec data]
  (if-let [error (spec/explain-data spec data)]
    (do
      (log/error {:expound (expound/expound-str spec data)
                  :data data} "reldb spec error")
      (throw (ex-info "reldb spec error" error)))
    true))

;;; Specs for relational data

(spec/def :reldb.Comment.content-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Comment/content-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Comment.content-history/content]))))
(spec/def :reldb.Comment/distinguish (spec/nilable #{:ADMIN :MOD}))
(spec/def :reldb.Comment/edited (spec/nilable ::content/timestamp))
(spec/def :reldb.Comment/status
  #{:ACTIVE :DELETED :DELETED-BY-ADMIN :DELETED-BY-MOD :DELETED-BY-USER})
(spec/def :reldb.Comment/time (spec/nilable ::content/timestamp))
(def CommentSchema
  [[:from :Comment]
   [:constrain
    [:req
     :cid
     :sticky
     :score
     :status
     :time
     :uid]
    [:check
     [spec/valid? [:_ :type.Comment/cid]              :cid]
     [spec/valid? [:_ :type.Comment/content]          :content]
     [spec/valid? [:_ :reldb.Comment/edited]          :edited]
     [spec/valid? [:_ :type.Comment/sticky]           :sticky]
     [spec/valid? [:_ :type.Comment/score]            :score]
     [spec/valid? [:_ :type.Comment/upvotes]          :upvotes]
     [spec/valid? [:_ :type.Comment/downvotes]        :downvotes]
     [spec/valid? [:_ :reldb.Comment/status]          :status]
     [spec/valid? [:_ :reldb.Comment/time]            :time]
     [spec/valid? [:_ :type.User/uid]                 :uid]
     [spec/valid? [:_ :type.Comment/author_flair]     :author-flair]
     [spec/valid? [:_ :reldb.Comment/distinguish]     :distinguish]
     [spec/valid? [:_ :type.Post/pid]                 :pid]
     [spec/valid? [:_ :reldb.Comment/content-history] :content-history]]]])

(def ContentStatusSchema
  [[:from :ContentStatus]
   [:constrain
    [:req
     :content-type
     :status]
    [:check [#(valid? ::loader/content-status %)]]]])

(def FormStateSchema
  [[:from :FormState]
   [:constrain
    [:req
     :id
     :form-type]
    [:check [#(valid? ::forms/typed-form %)]]]])

(spec/def :reldb.Post.content-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Post/content-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.content-history/content]))))
(spec/def :reldb.Post/default-sort #{:BEST :NEW :TOP})
(spec/def :reldb.Post/distinguish (spec/nilable #{:MOD :ADMIN}))
(spec/def :reldb.Post/edited (spec/nilable ::content/timestamp))
(spec/def :reldb.Post/hide-results (spec/nilable boolean?))
(spec/def :reldb.Post.open-reports/id ::gspec/non-empty-string)
(spec/def :reldb.Post/open-reports
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.open-reports/id]))))
(spec/def :reldb.Post/poll-closes-time (spec/nilable ::content/timestamp))
(spec/def :reldb.Post/poll-open (spec/nilable boolean?))
(spec/def :reldb.Post.poll-options/id string?)
(spec/def :reldb.Post.poll-options/text string?)
(spec/def :reldb.Post.poll-options/votes (spec/nilable nat-int?))
(spec/def :reldb.Post/poll-options
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.poll-options/id
                        :reldb.Post.poll-options/text
                        :reldb.Post.poll-options/votes]))))
(spec/def :reldb.Post/poll-votes (spec/nilable nat-int?))
(spec/def :reldb.Post/posted ::content/timestamp)
(spec/def :reldb.Post/status #{:ACTIVE
                               :DELETED
                               :DELETED-BY-USER
                               :DELETED-BY-MOD
                               :DELETED-BY-ADMIN})
(spec/def :reldb.Post.title-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Post/title-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.title-history/content]))))
(spec/def :reldb.Post/type #{:LINK :TEXT :POLL :UPLOAD})
(def PostSchema
  [[:from :Post]
   [:constrain
    [:req
     :pid
     :uid
     :status
     :nsfw
     :posted
     :is-archived
     :locked
     :sticky
     :type
     :score
     :comment-count
     :best-sort-enabled
     :default-sort]
    [:check
     [spec/valid? [:_ :type.Post/pid]               :pid]
     [spec/valid? [:_ :type.Post/content]           :content]
     [spec/valid? [:_ :reldb.Post/edited]           :edited]
     [spec/valid? [:_ :type.Post/link]              :link]
     [spec/valid? [:_ :type.Post/nsfw]              :nsfw]
     [spec/valid? [:_ :reldb.Post/posted]           :posted]
     [spec/valid? [:_ :type.Post/is_archived]       :is-archived]
     [spec/valid? [:_ :type.Post/locked]            :locked]
     [spec/valid? [:_ :type.Post/sticky]            :sticky]
     [spec/valid? [:_ :reldb.Post/status]           :status]
     [spec/valid? [:_ :reldb.Post/type]             :type]
     [spec/valid? [:_ :type.Post/score]             :score]
     [spec/valid? [:_ :type.Post/upvotes]           :upvotes]
     [spec/valid? [:_ :type.Post/downvotes]         :downvotes]
     [spec/valid? [:_ :type.Post/thumbnail]         :thumbnail]
     [spec/valid? [:_ :type.Post/title]             :title]
     [spec/valid? [:_ :type.Post/slug]              :slug]
     [spec/valid? [:_ :type.Post/flair]             :flair]
     [spec/valid? [:_ :reldb.Post/distinguish]      :distinguish]
     [spec/valid? [:_ :type.Post/comment_count]     :comment-count]
     [spec/valid? [:_ :type.Post/best_sort_enabled] :best-sort-enabled]
     [spec/valid? [:_ :reldb.Post/default-sort]     :default-sort]
     [spec/valid? [:_ :type.User/uid]               :uid]
     [spec/valid? [:_ :reldb.Post/content-history]  :content-history]
     [spec/valid? [:_ :reldb.Post/title-history]    :title-history]
     [spec/valid? [:_ :reldb.Post/open-reports]     :open-reports]
     [spec/valid? [:_ :reldb.Post/poll-options]     :poll-options]
     [spec/valid? [:_ :reldb.Post/poll-open]        :poll-open]
     [spec/valid? [:_ :reldb.Post/poll-closes-time] :poll-closes-time]
     [spec/valid? [:_ :reldb.Post/poll-votes]       :poll-votes]
     [spec/valid? [:_ :reldb.Post/hide-results]     :hide-results]]]])

(spec/def :reldb.PostUserAttributes/viewed (spec/nilable ::content/timestamp))
(spec/def :reldb.PostUserAttributes/vote (spec/nilable #{:UP :DOWN}))
(def PostUserAttributesSchema
  [[:from :PostUserAttributes]
   [:constrain
    [:req :pid :is-saved]
    [:check
     [spec/valid? [:_ :type.Post/pid]                     :pid]
     [spec/valid? [:_ :type.PostUserAttributes/is_saved]  :is-saved]
     [spec/valid? [:_ :type.PostUserAttributes/poll_vote] :poll-vote]
     [spec/valid? [:_ :reldb.PostUserAttributes/viewed]   :viewed]
     [spec/valid? [:_ :reldb.PostUserAttributes/vote]     :vote]]]])

(spec/def :reldb.Sub/creation ::content/timestamp)
(def SubSchema
  [[:from :Sub]
   [:constrain
    [:req
     :sid
     :creation
     #_:creator-uid
     :freeform-user-flairs
     :nsfw
     :post-count
     :restricted
     :sub-banned-users-private
     :sublog-private
     :subscriber-count
     :title
     :user-can-flair
     :user-can-flair-self
     :user-must-flair]
    [:check
     [spec/valid? [:_ :type.Sub/sid]                      :sid]
     [spec/valid? [:_ :reldb.Sub/creation]                :creation]
     #_[spec/valid? [:_ :type.User/uid] :creator-uid]
     [spec/valid? [:_ :type.Sub/freeform_user_flairs]     :freeform-user-flairs]
     [spec/valid? [:_ :type.Sub/nsfw]                     :nsfw]
     [spec/valid? [:_ :type.Sub/post_count]               :post-count]
     [spec/valid? [:_ :type.Sub/restricted]               :restricted]
     [spec/valid? [:_ :type.Sub/sidebar]                  :sidebar]
     [spec/valid? [:_ :type.Sub/sub_banned_users_private] :sub-banned-users-private]
     [spec/valid? [:_ :type.Sub/sublog_private]           :sublog-private]
     [spec/valid? [:_ :type.Sub/subscriber_count]         :subscriber-count]
     [spec/valid? [:_ :type.Sub/title]                    :title]
     [spec/valid? [:_ :type.Sub/user_can_flair]           :user-can-flair]
     [spec/valid? [:_ :type.Sub/user_can_flair_self]      :user-can-flair-self]
     [spec/valid? [:_ :type.Sub/user_flair_choices]       :user-flair-choices]
     [spec/valid? [:_ :type.Sub/user_must_flair]          :user-must-flair]]]])

(def SubNameSchema
  [[:from :SubName]
   [:constrain
    [:req :sid :name]
    [:check
     [spec/valid? [:_ :type.Sub/sid]  :sid]
     [spec/valid? [:_ :type.Sub/name] :name]]]])

(spec/def :reldb.SubPostFlair/order nat-int?)
(spec/def :reldb.SubPostFlair/post-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(def SubPostFlairSchema
  [[:from :SubPostFlair]
   [:constrain
    [:req :id :text :sid :mods-only :post-types :order]
    [:check
     [spec/valid? [:_ :type.SubPostFlair/id]          :id]
     [spec/valid? [:_ :type.SubPostFlair/text]        :text]
     [spec/valid? [:_ :type.SubPostFlair/mods_only]   :mods-only]
     [spec/valid? [:_ :reldb.SubPostFlair/post-types] :post-types]
     [spec/valid? [:_ :type.Sub/sid]                  :sid]
     [spec/valid? [:_ :reldb.SubPostFlair/order]      :order]]]])

(spec/def :reldb.SubPostFlairState/options-open? boolean?)
(def SubPostFlairStateSchema
  "UI state for post flairs on the mod edit post flairs page."
  [[:from :SubPostFlairState]
   [:constrain
    [:req :id :options-open?]
    [:check
     [spec/valid? [:_ :type.SubPostFlair/id] :id]
     [spec/valid? [:_ :reldb.SubPostFlairState/options-open?]
      :options-open?]]]])

(def SubPostTypeConfigSchema
  [[:from :SubPostTypeConfig]
   [:constrain
    [:req :name :post-type :mods-only]
    [:check
     [spec/valid? [:_ :type.Sub/name]                    :name]
     [spec/valid? [:_ :type.SubPostTypeConfig/mods_only] :mods-only]
     [spec/valid? [:_ :reldb.Post/type]                  :post-type]
     [spec/valid? [:_ :type.SubPostTypeConfig/rules]     :rules]]]])

(spec/def :reldb.SubRule/order nat-int?)
(def SubRuleSchema
  [[:from :SubRule]
   [:constrain
    [:req :id :text :sid :order]
    [:check
     [spec/valid? [:_ :type.SubRule/id]     :id]
     [spec/valid? [:_ :type.SubRule/text]   :text]
     [spec/valid? [:_ :type.Sub/sid]        :sid]
     [spec/valid? [:_ :reldb.SubRule/order] :order]]]])

(spec/def :reldb.SubUserFlair/text ::gspec/non-empty-string)
(def SubUserFlairSchema
  [[:from :SubUserFlair]
   [:constrain
    [:req :text :sid :uid]
    [:check
     [spec/valid? [:_ :type.Sub/sid]            :sid]
     [spec/valid? [:_ :type.User/uid]           :uid]
     [spec/valid? [:_ :reldb.SubUserFlair/text] :text]]]])

(spec/def :reldb.User/status #{:ACTIVE :BANNED :DELETED})
(def UserSchema
  [[:from :User]
   [:constrain
    [:req :uid :status]
    [:check
     [spec/valid? [:_ :type.User/uid]     :uid]
     [spec/valid? [:_ :type.User/name]    :name]
     [spec/valid? [:_ :reldb.User/status] :status]]]])

(spec/def :reldb.UserNameHistory/changed (spec/nilable nat-int?))
(def UserNameHistorySchema
  [[:from :UserNameHistory]
   [:constrain
    [:req :uid :name]
    [:check
     [spec/valid? [:_ :type.User/uid]                :uid]
     [spec/valid? [:_ :type.User/name]               :name]
     [spec/valid? [:_ :reldb.UserNameHistory/changed] :changed]]]])
