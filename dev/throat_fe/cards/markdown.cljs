;; cards/markdown.cljs -- Markdown devcards for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.cards.markdown
  (:require
   [cljsjs.react]
   [cljsjs.react.dom]
   [devcards.core :refer [defcard-rg]]
   [re-frame.core :refer [dispatch-sync]]
   [throat-fe.util :as util]
   [throat-fe.views.common :refer [dangerous-html]]))

(defn init
  "Initialize any devcard states here."
  []
  (dispatch-sync [::util/start-markdown-renderer {:sub-prefix "s"}]))

(defn show-markdown
  [text]
  [:div
   [:span text]
   [:hr]
   [dangerous-html {} (util/markdown-to-html text)]
   [:hr]
   [:span (util/markdown-to-html text)]])

(defcard-rg basic-markdown [show-markdown "**Bold**

*Italic*

~~Strikethrough~~

- first item
- second item

1. number one
2. number two

> Quote

inline `code`

```
multiline code
```


# Title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum."])

(defcard-rg links-markdown [show-markdown "[Some text](http://example.com)

   http://example.com

   /u/user

   /s/sub
"])

(defcard-rg xss-markdown [show-markdown "
   <script src=\"javascript:alert('XSS');\">
"])

(defcard-rg spoiler-markdown [show-markdown "
 This should be hidden: >! Spoiler !<

 This should be hidden: [Text](#spoiler)"])
