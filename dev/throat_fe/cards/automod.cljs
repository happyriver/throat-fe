;; cards/automod.cljs -- Automod prototype for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.cards.automod
  (:require
   [devcards.core :refer [defcard-rg]]
   [re-frame.core :as re-frame :refer [dispatch subscribe]]
   [throat-fe.views.common :refer [helper-text]]
   [throat-fe.ui.window :as window]
   [throat-fe.views.inputs :refer [button radio-button text-input textarea-input
                                   markdown-editor datepicker
                                   dropdown-menu date-selector]]
   [throat-fe.views.util :refer [cls]]))

(defn init
  "Initialize any devcard states here."
  [])

(re-frame/reg-sub
  ::edit-automod-value
  (fn [db _]
    (get-in db [:devcards ::automod :value])))

(re-frame/reg-sub
  ::automod-state
  (fn [db _]
    (get-in db [:devcards ::automod :state])))

(re-frame/reg-event-db
  ::set-automod-value
  (fn [db [_ val]]
    (-> db
        (assoc-in [:devcards ::automod :value] val)
        (assoc-in [:devcards ::automod :state] :updated))))

(re-frame/reg-event-db
  ::reset-automod-state
  (fn [db [_ _]]
    (-> db
        (assoc-in [:devcards ::automod :value] "")
        (assoc-in [:devcards ::automod :state] :reset))))

(defn- clear-on-reset!
  "Helper function for the text input devcards."
  [input-state props]
  (if (= (:form-state props) :reset)
    (when-not (:already-updated? @input-state)
      (swap! input-state assoc
             :value ""
             :already-updated? true))
    (swap! input-state assoc :already-updated? false))
  (dissoc props :form-state))

(re-frame/reg-event-db ::reset-action-picker
  (fn [db [_ _]]
    (assoc-in db [:devcards ::action-picker] :nothing-selected)))

(re-frame/reg-event-db ::set-action-picker
  (fn [db [_ value]]
    (assoc-in db [:devcards ::action-picker] value)))

(re-frame/reg-sub ::action-picker
  (fn [db _]
    (get-in db [:devcards ::action-picker])))



(defn create-moderation-rule
  []
  (let [content (fn [] @(subscribe [::edit-automod-value]))
        form-state @(subscribe [::automod-state])
        reset #(dispatch [::reset-automod-state])
        day? @(subscribe [::window/day?])
        form-colors (if day? :bg-white :bg-black)
        action (fn [] @(subscribe [::action-picker]))
        names ["Delete"
               "Do nothing"
               "Report"
               "Hide and Report"
               "Discuss"
               "Hide and Discuss"]]
    [:div.pa4.bg-gray
     [:div {:class (cls :w-80.h-auto.pa2.br3 form-colors)}
      [:div.pa4
       [:div
        [:h2 "Create a new moderation rule"]
        [:p "Rule name"]
        [text-input
         {:id "text"
          :class :w-100
          :placeholder "Placeholder"
          :value content
          :form-state form-state
          :update-value! clear-on-reset!
          :event [::set-automod-value]}]
        [:p "Words or phrases that will activate this rule"]
        [textarea-input {:value content
                         :class :w-100.mb2.h6
                         :required true
                         :form-state form-state
                         :update-value! clear-on-reset!
                         :event [::set-automod-value]}]
        [:p "Select an action"]
        [:div.pure-form.pure-control-group.w-50
         [dropdown-menu {:label "Choose... "
                         :id "m-id"
                         :value action
                         :choices (into {:nothing-selected "Choose..."}
                                        (zipmap names names))
                         :event [::set-action-picker]}]]
        [helper-text "This action will be applied to posts and comments."]

        ]]]]
    ))

(defcard-rg create-moderation-rule [create-moderation-rule])
