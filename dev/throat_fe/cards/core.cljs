;; cards/core.cljs -- Start the devcard ui for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.cards.core
  (:require
   [cljsjs.react]
   [cljsjs.react.dom]
   [devcards.core :as devcards]
   [lambdaisland.glogi :as glogi]
   [lambdaisland.glogi.console :as glogi-console]
   [re-frame.core :as re-frame]
   [throat-fe.cards.automod :as automod]
   [throat-fe.cards.inputs :as inputs]
   [throat-fe.cards.markdown :as markdown]
   [throat-fe.cards.modmail :as modmail]
   [throat-fe.cards.movable-list :as movable-list]
   [throat-fe.cards.sub-bar :as sub-bar]
   [throat-fe.cards.forms.user-flair :as user-flair-form]
   [throat-fe.config :as config]
   [throat-fe.db :as db]
   [throat-fe.log :as log]
   [throat-fe.ui.window :as window]))

(defn dev-setup []
  (when config/debug?
    (glogi-console/install!)
    (glogi/set-levels
     ;; Set a root logger level, this will be inherited by all loggers
     {:glogi/root :info})))

(defn init
  "Do some minimum app-db setup and launch the devcards UI"
  []
  (dev-setup)
  (re-frame/dispatch-sync [::db/initialize-db])
  (re-frame/dispatch [::window/clock-tick])
  (re-frame/dispatch [::window/start-resize-watcher])
  (re-frame/dispatch [::window/start-focus-watcher])

  (log/info {} "Starting devcards")
  (automod/init)
  (inputs/init)
  (markdown/init)
  (modmail/init)
  (movable-list/init)
  (sub-bar/init)
  (user-flair-form/init)
  (devcards/start-devcard-ui!))

