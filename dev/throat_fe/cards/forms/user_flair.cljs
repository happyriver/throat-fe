;; cards/forms/user_flair.cljs -- Devcards for user flair form variations
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.cards.forms.user-flair
  (:require
   [cljsjs.react]
   [cljsjs.react.dom]
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [clojure.test.check.generators :as gen]
   [com.wotbrew.relic :as rel]
   [devcards.core :refer [defcard-rg]]
   [re-frame.core :as re-frame :refer [dispatch subscribe]]
   [throat-fe.log :as log]
   [throat-fe.routes]
   [throat-fe.ui.forms.user-flair :refer [initialize-user-flair-form]]
   [throat-fe.ui.window :as window]
   [throat-fe.user.spec :as user]
   [throat-fe.views.inputs :refer [button]]
   [throat-fe.views.sidebar :refer [user-flair-form-content]]
   [throat-fe.views.util :refer [cls]]))

(defn init
  "Initialize any devcard states here."
  []
  (log/info {} "Starting user flair devcards")
  (dispatch [::initialize-custom])
  (dispatch [::initialize-presets])
  (dispatch [::initialize-custom-and-presets]))

(defn day-night []
  [button
   {:button-class :primary
    :class :dib
    :id "day-night"
    :on-click #(dispatch [::window/toggle-daynight])} "Toggle Night Mode"])

(defcard-rg day-night [day-night])

(def current-user
  "A current-user object for the db that passes the specs."
  (-> ::user/current-user
      spec/gen
      (as-> $ (gen/fmap #(assoc % :name "username") $))
      gen/generate))

(def custom-form-path [::user-flair :custom])
(def presets-form-path [::user-flair :presets])
(def custom-and-presets-form-path [::user-flair :custom-and-presets])

(defn init-form
  [db path {:keys [sid name] :as sub}]
  (let [db-with-form (-> db
                         (assoc-in [:view :options :sub] name)
                         (update :reldb rel/transact [:insert :Sub sub])
                         (update :reldb rel/transact
                                 [:insert :SubName
                                  {:sid sid
                                   :name name
                                   :lc-name (str/lower-case name)}])
                         (initialize-user-flair-form path)
                         (assoc-in (conj path :actions) {}))]
    (log/info {:path path
               :form (get-in db-with-form path)} "initialized form")
    (-> db
        (assoc :current-user current-user)
        (assoc-in path (get-in db-with-form path)))))

(defn reset-button [event]
  [button {:button-class :secondary
           :class :dib.mv2
           :on-click #(dispatch event)}
   "Reset"])

(re-frame/reg-event-db ::initialize-custom
  (fn [db [_ _]]
    (init-form db custom-form-path {:name "Custom"
                                    :sid "custom"
                                    :freeform-user-flairs true
                                    :user-flair "Flair"
                                    :user-flair-choices nil})))

(defn- form-wrapper
  [form]
  (let [day? @(subscribe [::window/day?])
        form-colors (if day? :bg-white :bg-black)]
    [:div {:class (cls :w-80.h-auto.pa2.br3 form-colors)}
     [:div.pa4
      form]]))

(defn user-flair-form-custom
  []
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form custom-form-path}]]
   [reset-button [::initialize-custom]]])

(re-frame/reg-event-db ::initialize-presets
  (fn [db [_ _]]
    (init-form db presets-form-path
               {:name "Presets"
                :sid "presets"
                :user-flair "Flair"
                :user-flair-choices ["Choice 1"
                                     "Choice 2"
                                     "Some flair"
                                     "Some other flair"
                                     "More flair"
                                     "Flair"
                                     "aaaaaaaaaaaaaaaaaaaaaaaa"]
                :user-can-flair-self true
                :freeform-user-flairs false})))

(defn user-flair-form-presets
  []
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form presets-form-path}]]
   [reset-button [::initialize-presets]]])

(re-frame/reg-event-db ::initialize-custom-and-presets
  (fn [db [_ _]]
    (init-form db custom-and-presets-form-path
               {:name "PresetsAndCustom"
                :sid "presetsandcustom"
                :user-flair nil
                :user-flair-choices ["Flair"
                                     "More Flair"
                                     "Some flair"
                                     "Some other flair"
                                     "This flair"
                                     "Flair 😼"
                                     "aaaaaaaaaaaaaaaaaaaaaaaa"]
                :freeform-user-flairs true})))

(defn user-flair-form-custom-and-presets
  []
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form custom-and-presets-form-path}]]
   [reset-button [::initialize-custom-and-presets]]])

(defcard-rg user-flair-form-custom [user-flair-form-custom])
(defcard-rg user-flair-form-presets [user-flair-form-presets])
(defcard-rg user-flair-form-custom-and-presets [user-flair-form-custom-and-presets])
