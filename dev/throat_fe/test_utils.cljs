;; test_utils.cljs -- Shared testing utitilies for throat-fe
;; Copyright (C) 2022 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.test-utils
  (:require
   [cljs.test :as test]
   [clojure.string :as str]
   [lambdaisland.glogi :as glogi]
   [lambdaisland.glogi.console :as glogi-console]
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]
   [throat-fe.graphql :as graphql]
   [throat-fe.log :as log]
   [throat-fe.spec.db]))

(glogi-console/install!)
(glogi/set-levels
 ;; Set a root logger level, this will be inherited by all loggers
 {:glogi/root :info
  ;; Customize levels here.
  're-frame.core :error})

;; Stop re-frame from complaining about event handler redefs.
(re-frame.core/set-loggers!  {:warn (constantly nil)})

(defn test-name
  "Return the name(s) of the currently running tests."
  []
  (-> (test/testing-vars-str {})
      (str/split #"\(|\)")
      second))

(defn setup-regraph-subscribe-logging
  "Register a graphql subscription handler that just logs names."
  []
  (re-frame/reg-event-fx ::re-graph/subscribe
    (fn [_ [_ {:keys [query _]}]]
      (let [gql-query (graphql/query-name-for-test query)]
        (log/info {:subscription gql-query} "subscribe")))))
