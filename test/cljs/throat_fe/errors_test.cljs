;; errors.cljs -- Test errors for throat-fe
;; Copyright (C) 2020-2022 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.errors-test
  (:require
   [cljs.test :refer-macros [deftest is testing]]
   [day8.re-frame.test :as rf-test]
   [re-frame.core :refer [dispatch subscribe reg-event-db]]
   [throat-fe.db :as db]
   [throat-fe.errors :as errors]
   [throat-fe.log :as log]
   [throat-fe.test-utils :as test-utils]))

(def error-502
  [{:message "The HTTP call failed."
    :extensions {:status 502}}])

(def example-errors
  "An error returned by re-graph."
  [{:message "Sender and recipient are the same"
    :locations [{:line 2, :column 3}]
    :path ["create_modmail_message"]
    :extensions {:arguments {:subject "$subject", :content "$content",
                             :sid "$sid", :username "$username",
                             :show_mod_username "$show_mod_username"}}}])

(defn test-fixtures
  []
  (log/info {:test (test-utils/test-name)} "Starting test")
  (reg-event-db
   ;; Set up a test event to hand off errors.
   ::receive-errors
   (fn [db [_ errors]]
     (errors/assoc-errors db {:event ::receive-errors
                              :errors errors}))))

(deftest test-error-processing
  (rf-test/run-test-sync
   (test-fixtures)
   (testing "Processing errors returned from graphql"
     (dispatch [::db/initialize-db])
     (dispatch [::receive-errors error-502])
     (let [errors @(subscribe [::errors/errors])
           msgs (map :msg errors)]
       (is (= msgs ["Could not contact server"]))))))
