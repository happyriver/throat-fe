;; content/user_test.cljs -- Testing user info for throat-fe
;; Copyright (C) 2022 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.user-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [com.rpl.specter :as s]
            [day8.re-frame.test :as rf-test]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]
            [throat-fe.content.graphql-spec :as graphql-spec]
            [throat-fe.content.settings :as settings]
            [throat-fe.db :as db]
            [throat-fe.graphql :as graphql]
            [throat-fe.log :as log]
            [throat-fe.routes :as routes]
            [throat-fe.test-utils :as test-utils]
            [throat-fe.ui.page-title :as page-title]
            [throat-fe.user :as user]
            [throat-fe.util :as util]))

(spec/check-asserts true)

(def seed #_999 (rand-int 1000))
(def size 5)

(def get-all-subs-result
  "A non-empty generated list of subs."
  (-> ::graphql-spec/query.get-all-subs
      (spec/and (fn [data]
                  (> (count (get-in data [:all_subs :edges])) 3)))
      spec/gen
      (as-> $ (gen/fmap
               #(assoc-in % [:all_subs :pageInfo :hasNextPage] false) $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/query.get-all-subs $))))

(def all-subs
  (map #(select-keys (:node %) [:sid :name])
       (get-in get-all-subs-result [:all_subs :edges])))

(def index-query-result
  "Startup query with an authenticated user with no subscriptions."
  (-> ::graphql-spec/query.index-query
      (spec/and #(map? (:current_user %)))
      spec/gen
      (as-> $
          (gen/fmap #(-> %
                         (assoc-in [:current_user :subscriptions] nil)
                         (assoc-in [:site_configuration :enable_totp] false))
                    $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/query.index-query $))
      util/kebab-case-keys))

(defn register-test-graphql-queries
  "Define the queries to return the generated data above."
  []
  (test-utils/setup-regraph-subscribe-logging)
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query callback]}]]
      (let [gql-query (graphql/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       "get_all_subs" get-all-subs-result
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result
                                                  :errors nil})]]}))))

(defn startup
  "Start up the app with the test data."
  []
  (log/info {:seed seed
             :test (test-utils/test-name)} "Starting test")
  (register-test-graphql-queries)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  (log/info {:iq index-query-result} "startup")
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))
  (dispatch [::db/initialize-db])
  (dispatch [::settings/load-index-query]))

(deftest test-subscribe-block-and-unsubscribe
  (rf-test/run-test-sync
   (let [mutation-vars (atom nil)
         [sub1 sub2 sub3] all-subs]
     (startup)
     (testing "user subscriptions start out empty"
       (is (empty? @(subscribe [::user/topbar-sub-names]))))
     (testing "subscribing"
       (re-frame/reg-event-fx ::re-graph/mutate
         (fn [_ [_ {:keys [query variables callback]}]]
           (testing "expected change_subscription mutation"
             (is (= "change_subscription" (graphql/query-name-for-test query)))
             (reset! mutation-vars variables)
             (let [{:keys [sid]} variables
                   sub-name (s/select [s/ALL #(= (:sid %) sid) :name] all-subs)]
               {:fx [[:dispatch (update callback 1
                                        assoc-in [:response :data
                                                  :change_subscription_status]
                                        (assoc variables
                                               :name sub-name
                                               :order nil))]]}))))

       (dispatch [::user/subscribe sub1])
       (testing "adds the sub to the top bar"
         (is (= {:sid (:sid sub1) :change :SUBSCRIBE} @mutation-vars))
         (is (= [(:name sub1)] @(subscribe [::user/topbar-sub-names])))
         (dispatch [::routes/show-contact-mods {:sub (:name sub1)}])
         (is @(subscribe [::user/subscribed?])))
       (testing "twice does not duplicate the sub in the top bar"
         (dispatch [::user/subscribe sub1])
         (is (= {:sid (:sid sub1) :change :SUBSCRIBE} @mutation-vars))
         (is (= [(:name sub1)] @(subscribe [::user/topbar-sub-names])))
         (is @(subscribe [::user/subscribed?]))))
     (testing "to two subs adds them both to the top bar"
       (dispatch [::user/subscribe sub2])
       (is (= {:sid (:sid sub2) :change :SUBSCRIBE} @mutation-vars))
       (is (= [(:name sub1) (:name sub2)] @(subscribe
                                            [::user/topbar-sub-names])))
       (is  @(subscribe [::user/subscribed?]))
       (dispatch [::routes/show-contact-mods {:sub (:name sub1)}])
       (is @(subscribe [::user/subscribed?])))
     (testing "and then blocking does not add the blocked one to the top bar"
       (dispatch [::user/block sub3])
       (is (= {:sid (:sid sub3) :change :BLOCK} @mutation-vars))
       (is (= [(:name sub1) (:name sub2)] @(subscribe
                                            [::user/topbar-sub-names])))
       (dispatch [::routes/show-contact-mods {:sub (:name sub3)}])
       (is (not @(subscribe [::user/subscribed?])))
       (is @(subscribe [::user/blocked?])))
     (testing "and then blocking removes the sub from the top bar"
       (dispatch [::user/block sub1])
       (is (= {:sid (:sid sub1) :change :BLOCK} @mutation-vars))
       (dispatch [::routes/show-contact-mods {:sub (:name sub1)}])
       (is @(subscribe [::user/blocked?]))
       (is (= [(:name sub2)] @(subscribe [::user/topbar-sub-names]))))
     (testing "and then unsubscribing removes the sub from the top bar"
       (dispatch [::user/unsubscribe sub2])
       (is (= {:sid (:sid sub2) :change :UNSUBSCRIBE} @mutation-vars))
       (is (= [] @(subscribe [::user/topbar-sub-names])))
       (dispatch [::routes/show-contact-mods {:sub (:name sub2)}])
       (is (not @(subscribe [::user/subscribed?])))
       (is (not @(subscribe [::user/blocked?])))))))
