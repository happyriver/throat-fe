let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.nodejs
    pkgs.jdk17 pkgs.clojure pkgs.clojure-lsp
    pkgs.clj-kondo
  ];
}
