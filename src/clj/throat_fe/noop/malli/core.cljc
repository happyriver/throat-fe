(ns throat-fe.noop.malli.core
  #?(:cljs (:require-macros throat-fe.noop.malli.core)))

#?(:clj (defmacro validate [_ _val] true))
#?(:clj (defmacro decode [_ val & _forms] val))
#?(:clj (defmacro explain [& _forms]))
