;; content/post/db.cljs -- Post reldb access for throat-fe
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.post.db
  (:require [com.wotbrew.relic :as rel]))

(defn single-post
  "Return the current single post object."
  [{:keys [reldb] :as db}]
  (let [pid (get-in db [:view :options :pid])]
    (-> (rel/q reldb [[:from :Post] [:where [= :pid pid]]])
        first)))

(defn assoc-post
  "Assoc a field in a post object in the db."
  [db pid kw val]
  (update db :reldb rel/transact
          [:update :Post #(assoc % kw val) [= :pid pid]]))

(defn update-post
  "Update a post object in the db."
  [db pid func]
  (update db :reldb
          rel/transact [:update :Post func [= :pid pid]]))

(defn single-post-user-attributes
  "Return the current single post object's user attributes."
  [{:keys [reldb] :as db}]
  (let [pid (get-in db [:view :options :pid])]
    (-> (rel/q reldb [[:from :PostUserAttributes] [:where [= :pid pid]]])
        first)))

(defn assoc-post-user-attributes
  "Assoc a field in a post's user attributes in the db."
  [db pid kw val]
  (update db :reldb rel/transact
          [:update :PostUserAttributes #(assoc % kw val) [= :pid pid]]))
