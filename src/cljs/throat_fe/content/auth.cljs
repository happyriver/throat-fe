;; content/auth.clj -- Registration and login for throat-fe
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.auth
  (:require [clojure.string :as str]
            [com.wotbrew.relic :as rel]
            [day8.re-frame.tracing :refer-macros [fn-traced]]
            [re-frame.core :as re-frame]
            [throat-fe.content.internal :as internal]
            [throat-fe.content.loader :as loader]
            [throat-fe.content.settings :as settings]
            [throat-fe.errors :as errors]
            [throat-fe.graphql :as graphql]
            [throat-fe.routes :as-alias routes]
            [throat-fe.routes.util :refer [url-for]]
            [throat-fe.tr :refer [trx trmx-html]]
            [throat-fe.ui.forms.auth :as auth-forms]
            [throat-fe.ui.window :as window]
            [throat-fe.user :as user]
            [throat-fe.util :as util]))

;; Shared settings and form initialization

(re-frame/reg-event-fx ::prepare-auth-forms
  ;; :doc Fetch any needed settings and initialize the forms needed.
  (fn-traced [_ [_ form-type]]
    {:fx [[:dispatch [::load-registration-settings form-type]]
          (when (= :edit-account form-type)
            [:dispatch [::get-user-email]])]}))

(util/reg-ajax-event-chain ::get-user-email
  ;; :doc Get the user's email from the auth server.
  {:call
   (fn-traced [{:keys [db]} [_ _]]
     (when (and (some? (get-in db [:current-user :uid]))
                (not= :loaded (get-in db [:status :user-email])))
       {:db (assoc-in db [:status :user-email] :loading)
        :fx [[::util/ajax-get {:url "/ovarit_auth/info"}]]}))

   :success
   (fn-traced [{:keys [db]} [_ {:keys [response]}]]
     {:db (-> db
              (assoc-in [:status :user-email] :loaded)
              (assoc-in [:current-user :email] (:email response)))
      :fx [[:dispatch [::init-auth-form :edit-account]]]})

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [error-response]}]]
     {:db
      (-> db
          (errors/assoc-errors {:event ::get-user-email
                                :errors (errors/errors-from-ajax-error-response
                                         error-response)})
          (assoc-in [:status :user-email] :error))})})

(re-frame/reg-event-fx ::load-registration-settings
  ;; :doc Initialize the appropriate form for the route.
  ;; :doc Load app registration config if that has not yet been done.
  ;; :doc Fetch the user's email, if we don't have that.
  (fn-traced [{:keys [db]} [_ form-type]]
    (if (#{:loading :loaded} (get-in db [:status :registration-settings]))
      {:fx [[:dispatch [::init-auth-form form-type]]]}
      {:db (assoc-in db [:status :registration-settings] :loading)
       :fx [[:dispatch
             [::graphql/query
              {:graphql settings/graphql
               :name :registration-settings
               :id :registration-settings
               :variables {}
               :handler [::set-registration-settings {:form-type
                                                      form-type}]}]]]})))

(re-frame/reg-event-fx ::set-registration-settings
  ;; :doc Record received registration settings in the database.
  ;; :doc If response was successful, initialize current form.
  (fn-traced [{:keys [db]} [event {:keys [form-type response]}]]
    (let [{:keys [data errors]} response
          invite-code-required (get-in data [:invite-code-settings :required])
          settings (some-> data
                           :site-configuration
                           (assoc :invite-code-required invite-code-required))]
      {:db (-> db
               (errors/assoc-errors {:event event :errors errors})
               (assoc-in [:status :registration-settings]
                         (if errors :error :loaded))
               (assoc-in [:settings :registration] settings))
       :fx [(when (nil? errors)
              [:dispatch [::init-auth-form form-type]])]})))

(re-frame/reg-event-db ::init-auth-form
  ;; :doc If everything needed has been loaded, initialize the form(s).
  (fn-traced [db [_ form-type]]
    (if (or (not= :loaded (get-in db [:status :registration-settings]))
            (and (= :edit-account form-type)
                 (not= :loaded (get-in db [:status :user-email]))))
      db
      (let [init-edit-acc #(-> %
                               auth-forms/initialize-change-email-form
                               auth-forms/initialize-change-password-form)
            init (case form-type
                   :registration auth-forms/initialize-registration-form
                   :complete-reset-password auth-forms/initialize-reset-form
                   :edit-account init-edit-acc
                   identity)]
        (init db)))))

(re-frame/reg-sub ::registration-settings
  ;; :doc Extract the registration settings.
  :<- [::settings/site-config]
  (fn [site-config _]
    (:registration site-config)))

(re-frame/reg-sub ::registration-settings-status
  ;; :doc Extract the status of loading the registration settings.
  :<- [::window/statuses]
  (fn [statuses _]
    (:registration-settings statuses)))

(re-frame/reg-sub ::registration-settings-ready?
  ;; :doc Return true if ready to show something.
  ;; :doc Use for content that depends on the registration settings.  If
  ;; :doc loading them hasn't yet started or is in progress, return false.
  :<- [::registration-settings-status]
  (fn [status _]
    (nil? (#{:not-loaded :loading} status))))

(re-frame/reg-sub ::edit-account-status
  ;; :doc Extract the status of fetches for the edit account form.
  :<- [::window/statuses]
  (fn [{:keys [registration-settings user-email]} _]
    (cond (or (= :error registration-settings) (= :error user-email))
          :error

          (and (= :loaded registration-settings) (= :loaded user-email))
          :loaded

          (or (= :loading registration-settings) (= :loading user-email))
          :loading

          :else :not-loaded)))

(util/reg-ajax-event-chain ::start-confirmation
  ;; :doc Ask the server to confirm a registration or an email.
  {:call
   (fn-traced [{:keys [db]} [_ confirmation-type]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/create/validate"
             :payload (-> db
                          (get-in [:view :options])
                          (select-keys [:code])
                          (update :code #(subs % 0 36)))
             :confirmation-type confirmation-type}]]})

   :success
   (fn-traced [{:keys [db]} [_ {:keys [confirmation-type]}]]
     (if (= :email confirmation-type)
       ;; Users confirming emails get an acknowledgement on this page.
       {:db (assoc-in db [:view :options :state] :confirmed)}
       ;; Users confirming registrations get sent to the login page.
       (let [welcome-url (url-for db :wiki/welcome)
             login-url (url-for db :auth/login
                                :query-args {:next welcome-url
                                             :notify "confirm"})]
         {:fx [[::routes/set-url login-url]]})))

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [error-response]}]]
     (let [status (:status error-response)
           xmit-error? (or (>= status 500) (= status 429))]
       {:db (if xmit-error?
              (-> db
                  (assoc-in [:view :options :state] :xmit-error)
                  (cond-> xmit-error?
                    (errors/assoc-errors
                     {:event ::start-confirmation
                      :errors (errors/errors-from-ajax-error-response
                               error-response)})))
              (assoc-in db [:view :options :state] :invalid-link))}))})

(re-frame/reg-event-fx ::retry-confirm
  ;; :doc
  (fn-traced [{:keys [db]} [_ route]]
    (let [code (get-in db [:view :options :code])]
      {:fx [[::routes/redirect (url-for db route :query-args {:code code})]]})))

;; Support for forms and events

(defn- highlight-banned-string
  [banned username]
  (let [pos (str/index-of (str/lower-case username)
                          (str/lower-case banned))
        end (+ pos (count banned))]
    (if pos
      [:span
       [:span (subs username 0 pos)]
       [:span.black (subs username pos end)]
       [:span (subs username end)]]
      banned)))

(defn- registration-error-reason
  [db {:keys [username]} {:keys [reason banned]}]
  (if banned
    (trmx-html db "Username contains forbidden text: %{banned}"
               {:banned (highlight-banned-string banned username)})
    reason))

(defn- describe-registration-error
  "Translate ovarit_auth error codes into messages."
  [db args {:keys [status status-text response]}]
  (if (and (>= status 400) (< status 500))
    {:msg (registration-error-reason db args response)
     :status status}
    (errors/describe-error (trx db "Error: ")
                           {:message status-text
                            :extensions {:status status}})))

(util/reg-ajax-event-chain ::register
  ;; :doc Register a new user and redirect to the confirmation page.
  {:call
   (fn-traced [_ [_ form update-event]]
     (let [{:keys [id invite-code username email password]} form]
       {:fx [[::util/ajax-post
              {:url "/ovarit_auth/create"
               :payload {:invite_code (str/trim invite-code)
                         :email (str/trim email)
                         :username (str/trim username)
                         :password password}
               :form-id id
               :update-event update-event}]]}))

   :success
   (fn-traced [{:keys [db]} [_ {:keys [payload update-event response]}]]
     (let [{:keys [code uid]} response
           confirm-url (url-for db :auth/verify-registration
                                :query-args {:delete_code code :uid uid})]
       {:db (assoc-in db [:content :verify-registration uid]
                      (select-keys payload [:username :email]))
        :fx [[:dispatch update-event]
             [::routes/set-url confirm-url]]}))

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form-id payload update-event
                                       error-response]}]]
     (let [describe-error #(describe-registration-error db payload %)]
       {:db (errors/assoc-errors db
                                 {:event form-id
                                  :errors [error-response]
                                  :describe-error-fn describe-error})
        :fx [[:dispatch update-event]]}))})

(util/reg-ajax-event-chain ::start-over
  ;; :doc Delete a newly created user and redirect to /register.
  {:call
   (fn-traced [{:keys [db]} _]
     (let [{:keys [delete-code uid]} (get-in db [:view :options])]
       {:fx [[::util/ajax-post {:url "/ovarit_auth/delete"
                                :payload {:code delete-code
                                          :uid uid}}]]}))

   :success
   (fn-traced [{:keys [db]} [_ _]]
     {:fx [[::routes/set-url (url-for db :auth/register)]]})

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [error-response]}]]
     (let [status (:status error-response)]
       (cond
         ;; The email verify link has already been used.  Stay on the
         ;; page and show a login button.
         (= 403 status)
         {:db (assoc-in db [:view :options :already-verified?] true)}

         ;; For transmission errors, stay on the page and show a toast.
         (#{500 502 503 429} status)
         {:db (errors/assoc-errors
               db
               {:event ::start-over
                :errors (errors/errors-from-ajax-error-response
                         error-response)})}
         ;; We'll get a 404 if the user has already been deleted,
         ;; in which case we want to show the registration form.
         ;; Any other error can go there too.
         :else
         {:fx [[::routes/set-url (url-for db :auth/register)]]})))})

(re-frame/reg-event-fx ::login-then-welcome
  ;; :doc Redirect a user to the login page, and then the welcome wiki.
  (fn-traced [{:keys [db]} [_ _]]
    (let [welcome (url-for db :wiki/welcome)]
      {:fx [[::routes/redirect (url-for db :auth/login
                                        :query-args {:next welcome})]]})))

(defn- describe-auth-error
  "Translate ovarit_auth error codes into messages."
  [db {:keys [status status-text response]}]
  (if (and (>= status 400) (< status 500)
           (not= status 429))
    {:msg (or (:reason response)
              (trx db "An error occurred"))
     :status status}
    (errors/describe-error (trx db "Error: ")
                           {:message status-text
                            :extensions {:status status}})))

(defn reg-auth-form-chain [kw {:keys [call success failure]}]
  ;; :doc Register `kw`, `kw`-success and `kw`-failure events.
  ;; :doc Provide defaults for success and failure that dispatch
  ;; :doc the update form event and record any errors.
  (util/reg-ajax-event-chain kw
    {:call call
     :success (or success
                  (fn-traced [_ [_ {:keys [update-event]}]]
                    {:fx [[:dispatch update-event]]}))
     :failure (or failure
                  (fn-traced [{:keys [db]} [_ {:keys [update-event
                                                      error-response
                                                      form-id]}]]
                    {:db
                     (->> {:event form-id
                           :errors [error-response]
                           :describe-error-fn #(describe-auth-error db %)}
                          (errors/assoc-errors db))
                     :fx [[:dispatch update-event]]}))}))

(reg-auth-form-chain ::login
  ;; :doc Log in and redirect on success.
  {:call
   (fn-traced [_ [_ form update-event]]
     (let [{:keys [id username password remember-me? next-url]} form]
       {:fx [[::util/ajax-post
              {:url "/ovarit_auth/authenticate"
               :payload {:username (str/trim username)
                         :password password
                         :remember_me remember-me?}
               :next-url next-url
               :form-id id
               :update-event update-event}]]}))
   :success
   (fn-traced [_ [_ {:keys [next-url update-event]}]]
     {:fx [[:dispatch update-event]
           [::routes/redirect next-url]]})})

(reg-auth-form-chain ::resend
  ;; :doc Request that the initial verification email be resent.
  {:call
   (fn-traced [{:keys [_db]} [_ {:keys [id username]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/create/resend"
             :payload {:username (str/trim username)}
             :form-id id
             :update-event update-event}]]})})

(reg-auth-form-chain ::reset
  ;; :doc Request a password reset email.
  {:call
   (fn-traced [{:keys [_db]} [_ {:keys [id username email]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/reset"
             :payload {:username (str/trim username)
                       :email (str/trim email)}
             :form-id id
             :update-event update-event}]]})})

(util/reg-ajax-event-chain ::complete-reset
  ;; :doc Send the new password to complete a password reset.
  {:call
   (fn-traced [{:keys [db]} [_ {:keys [id password]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/reset/complete"
             :payload {:code (get-in db [:view :options :code])
                       :new_password password}
             :form-id id
             :update-event update-event}]]})

   :success
   (fn-traced [{:keys [db]} [_ _]]
     ;; Use redirect to make the index query reload, because if we had
     ;; a cookie, it's no longer good.  If we were logged in as a different
     ;; user than the one we just changed the password of, the back
     ;; end will redirect to the home page.
     {:fx [[::routes/redirect (url-for db :auth/login
                                       :query-args {:notify "reset"})]]})

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form-id update-event error-response]}]]
     ;; 403 means the link was expired, already used or bogus.
     (if (= 403 (:status error-response))
       {:db (assoc-in db [:view :options :bad-code?] true)
        :fx [[:dispatch update-event]]}
       ;; Otherwise some kind of transmission error, show it in the form.
       {:db
        (errors/assoc-errors db {:event form-id
                                 :errors (errors/errors-from-ajax-error-response
                                          error-response)})
        :fx [[:dispatch update-event]]}))})

(reg-auth-form-chain ::change-email
  ;; :doc Update the user's email address on the server.
  {:call
   (fn-traced [_ [_ {:keys [id current-password email]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/update"
             :payload {:password current-password
                       :new_email email}
             :form-id id
             :update-event update-event}]]})})

(reg-auth-form-chain ::change-password
  ;; :doc Update the user's password on the server.
  {:call
   (fn-traced [_ [_ {:keys [id current-password password]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/update"
             :payload {:password current-password
                       :new_password password}
             :form-id id
             :update-event update-event}]]})})

(reg-auth-form-chain ::delete-account
  ;; :doc Delete the user account on the server.
  {:call
   (fn-traced [_ [_ {:keys [id current-password]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/retire"
             :payload {:password current-password}
             :form-id id
             :update-event update-event}]]})})

(loader/reg-loader ::rename-settings
  ;; :doc Load the settings relevant to renaming a user account.
  {:make-content-status
   (fn [_]
     {:content-type ::rename-settings})

   :load
   (fn-traced [_ _]
     {:fx [[:dispatch [::graphql/query
                       {:graphql settings/graphql
                        :name :rename-settings
                        :variables {}
                        :handler [::receive-rename-settings]}]]]})

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           settings (:site-configuration data)
           uid (get-in db [:current-user :uid])
           hist (map #(-> %
                          (assoc :uid uid)
                          (update :changed js/parseInt))
                     (get-in data [:current-user :username-history]))]
       {:db (-> db
                (errors/assoc-errors {:event event :errors errors})
                (update :settings merge settings)
                (update :reldb rel/transact
                        [:delete :UserNameHistory [= uid :uid]])
                (update :reldb rel/transact
                        (into [:insert :UserNameHistory] hist)))
        :fx [[:dispatch [::rename-settings-completed
                         (if (seq errors) :failure :success)]]
             (when (zero? (:username-change-limit settings))
               [:dispatch [::routes/show-error :unmatched-route]])]}))})

(defmethod loader/content-status-id ::rename-settings [_]
  ::rename-settings)
(defmethod loader/content-status ::rename-settings [_]
  ::loader/content-status-fields)

(re-frame/reg-sub ::recent-rename-history-count
  ;; :doc Return the number of username changes within the limit period.
  :<- [::user/current-user]
  :<- [::internal/reldb]
  :<- [::settings/site-config]
  :<- [::window/now]
  (fn [[{:keys [uid]} reldb {days :username-change-limit-days} now] _]
    (let [cutoff (- now (* days 24 60 60 1000))
          where-clause (cond-> [:where
                                [= uid :uid]]
                         (pos? days) (conj [> :changed cutoff]))]
      (-> reldb
          (rel/q [[:from :UserNameHistory]
                  where-clause])
          count))))

(re-frame/reg-sub ::permitted-change-count
  ;; :doc Return the number of username changes currently permitted.
  :<- [::recent-rename-history-count]
  :<- [::settings/site-config]
  (fn [[recent-count {limit :username-change-limit}] _]
    (max 0 (- limit recent-count))))

(reg-auth-form-chain ::change-username
  ;; :doc Change the username on the server.
  {:call
   (fn-traced [_ [_ {:keys [id username password]} update-event]]
     {:fx [[::util/ajax-post
            {:url "/ovarit_auth/change_username"
             :payload {:new_username username
                       :password password}
             :new-username username
             :form-id id
             :update-event update-event}]]})
   :success
   (fn-traced [{:keys [db]} [_ {:keys [new-username update-event]}]]
     (let [{:keys [uid]} (:current-user db)]
       {:db (-> db
                (assoc-in [:current-user :name] new-username)
                (update :reldb rel/transact
                        [:update :User {:name new-username}
                         [= uid :uid]]))
        :fx [[:dispatch update-event]]}))

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form-id new-username update-event
                                       error-response]}]]
     (let [describe-error #(describe-registration-error
                            db {:username new-username} %)]
       {:db (errors/assoc-errors db
                                 {:event form-id
                                  :errors [error-response]
                                  :describe-error-fn describe-error})
        :fx [[:dispatch update-event]]}))})
