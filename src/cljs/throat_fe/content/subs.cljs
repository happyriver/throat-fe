;; content/subs.cljs -- Subs for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.subs
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [throat-fe.content.internal :refer [sub-info-by-name] :as internal]
   [throat-fe.content.loader :as loader]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.routes :as-alias routes]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :as tr]
   [throat-fe.ui.window :as window]
   [throat-fe.util :as util]))

;; ::info is used for the sub details in the get_single_post
;; and sub_info queries.
(defmethod loader/content-status-id ::info [{:keys [name]}]
  (util/add-suffix ::info name))
(defmethod loader/content-status ::info [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::post-flairs is used for the sub's post flairs in the
;; sub_post_flairs query.
(defmethod loader/content-status-id ::post-flairs [{:keys [name]}]
  (util/add-suffix ::post-flairs name))
(defmethod loader/content-status ::post-flairs [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::post-types is used for the list of post types configurations
;; in the post_type_config and submit_post_config queries.
(defmethod loader/content-status-id ::post-types [{:keys [name]}]
  (util/add-suffix ::post-types name))
(defmethod loader/content-status ::post-types [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::sub-names is used for the list of sub names in the
;; get_all_subs query.
(defmethod loader/content-status-id ::sub-names [_]
  ::sub-names)
(defmethod loader/content-status ::sub-names [_]
  ::loader/content-status-fields)

;; ::sub-rules is used for the list of sub rules in the sub_rules
;; query.
(defmethod loader/content-status-id ::sub-rules [{:keys [name]}]
  (util/add-suffix ::sub-rules name))
(defmethod loader/content-status ::sub-rules [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::submit-post-settings is used for some fields in the
;; db :settings map pertaining to the submit post form.
(defmethod loader/content-status-id ::submit-post-settings [_]
  ::submit-post-settings)
(defmethod loader/content-status ::submit-post-settings [_]
  ::loader/content-status-fields)

(loader/reg-loader ::sub-names
  ;; :doc Load all sub names from the server.
  {:make-content-status
   (fn [_] {:content-type ::sub-names})

   :load
   (fn-traced [{:keys [db]} _]
     {:db (update db :reldb rel/transact [:delete :SubName])
      :fx [[:dispatch [::graphql/query
                       {:graphql graphql/subs-graphql
                        :name :get-all-subs
                        :variables {:first 100}
                        :handler [::receive-sub-names]}]]]})

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           next-page? (get-in data [:all-subs :page-info :has-next-page])
           end-cursor (get-in data [:all-subs :page-info :end-cursor])
           subs (get-in data [:all-subs :edges])]
       {:db (-> db
                (update :reldb rel/transact
                        (into [:insert-or-replace :SubName]
                              (map :node subs)))
                (errors/assoc-errors {:event event :errors errors}))
        :fx  [(if next-page?
                [:dispatch [::graphql/query
                            {:graphql graphql/subs-graphql
                             :name :get-all-subs
                             :variables {:first 500 :after end-cursor}
                             :handler [::receive-sub-names]}]]
                [:dispatch [::sub-names-completed
                            (if (some? errors) :failure :success)]])]}))})

(re-frame/reg-event-fx ::check-sub
  ;; :doc Check that the sub name in [:view :options] exists.
  ;; :doc Fix the case of the name if it is incorrect, or set it to
  ;; :doc nil if no such sub exists..
  ;; :doc Load all sub names from the server before dispatching this event.
  (fn-traced [{:keys [db]} [event _]]
    (let [sub-name (get-in db [:view :options :sub])
          sub (when sub-name
                (rel/row (:reldb db)
                         [[:from :SubName]
                          [:where [= [str/lower-case :name]
                                   (str/lower-case sub-name)]]]))]
      {:db (assoc-in db [:view :options :sub] (:name sub))
       :fx [[:dispatch [::loader/update-on-completion [event] :success]]]})))

(re-frame/reg-event-fx ::load-all-subs
  ;; :doc Start loading all subs.
  (fn-traced [{:keys [db]} [_ on-load]]
    {:db (-> db
             (update :reldb rel/transact [:delete :SubName])
             (assoc-in [:status :subs] :loading))
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql/subs-graphql
                       :name :get-all-subs
                       :variables {:first 100}
                       :handler [::receive-all-subs {:on-load on-load}]}]]]}))

(re-frame/reg-event-fx ::receive-all-subs
  ;; :doc Collect all the subs using multiple queries if necessary.
  ;; :doc Probably want to do this lazily if the number of subs
  ;; :doc gets large.
  (fn-traced [{:keys [db]} [event {:keys [on-load response]}]]
    (let [{:keys [data errors]} response
          next-page? (get-in data [:all-subs :page-info :has-next-page])
          end-cursor (get-in data [:all-subs :page-info :end-cursor])
          subs (get-in data [:all-subs :edges])
          dbx (-> db
                  (update :reldb rel/transact
                          (into [:insert :SubName] (map :node subs)))
                  (errors/assoc-errors {:event event
                                        :errors errors}))]
      {:db (if next-page?
             dbx
             (assoc-in dbx [:status :subs] :loaded))
       :fx  [(if next-page?
               [:dispatch [::graphql/query
                           {:graphql graphql/subs-graphql
                            :name :get-all-subs
                            :variables {:first 500 :after end-cursor}
                            :handler [::receive-all-subs {:on-load on-load}]}]]
               (when on-load
                 [:dispatch on-load]))]})))

(re-frame/reg-sub ::all-subs
  ;; :doc Extract the list of subs.
  :<- [::internal/reldb]
  (fn [reldb _]
    (rel/q reldb [[:from :Sub]])))

(re-frame/reg-sub ::all-sub-names
  ;; :doc Extract the list of sub names.
  :<- [::internal/reldb]
  (fn [reldb _]
    (->> (rel/q reldb [[:from :SubName]])
         (map :name)
         (sort-by str/lower-case))))

(re-frame/reg-sub ::sub-load-status
  ;; :doc Extract the status of loading subs from the server.
  (fn [db _]
    (get-in db [:status :subs])))

(re-frame/reg-event-fx ::load-current-user-mod-stats
  ;; :doc Fetch statistics on the subs moderated by the current user.
  (fn-traced [_ [_ _]]
    {:dispatch [::graphql/query
                {:graphql graphql/subs-graphql
                 :name :current-user-mod-stats
                 :variables {}
                 :handler [::receive-current-user-mod-stats]}]}))

(re-frame/reg-event-db ::receive-current-user-mod-stats
  ;; :doc Receive statistics on moderated subs from the server.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          subs (get-in data [:current-user :subs-moderated])
          compare-by-name (fn [a b]
                            (compare (str/lower-case (:name a))
                                     (str/lower-case (:name b))))
          sorted-subs (->> subs
                           (map :sub)
                           (sort compare-by-name))]
      (-> db
          (errors/assoc-errors {:event event
                                :errors errors})
          (assoc-in [:content :mod-stats :subs-moderated] sorted-subs)))))

(re-frame/reg-event-fx ::load-admin-mod-stats
  ;; :doc Fetch moderation statistics on all subs for the admin.
  (fn-traced [{:keys [db]} [_ {:keys [cursor] :as options}]]
    (let [page-size (* 2 (get-in db [:settings :page-size]))
          args (if cursor
                 {:first page-size :after cursor}
                 {:first page-size})]
      {:db (assoc-in db [:status :content] :loading)
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql/subs-graphql
                         :name :admin-mod-stats
                         :variables args
                         :handler [::receive-admin-mod-stats options]}]]]})))

(re-frame/reg-event-db ::receive-admin-mod-stats
  ;; :doc Receive moderation statistics on all subs for the admin
  (fn-traced [db [event {:keys [cursor response]}]]
    (let [{:keys [data errors]} response
          page-info (get-in data [:all-subs :page-info])
          results (get-in data [:all-subs :edges])
          new-subs (map :node results)
          path [:content :mod-stats :all-sub-stats]
          existing-subs (if (= cursor (get-in db (conj path :cursor)))
                          (get-in db (conj path :subs))
                          [])
          subs (concat existing-subs new-subs)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:status :content] :loaded)
          (assoc-in [:content :mod-stats :all-sub-stats]
                    {:subs subs
                     :more-items-available? (:has-next-page page-info)
                     :cursor (:end-cursor page-info)})))))

(re-frame/reg-sub ::mod-stats
  ;; :doc Extract the sub moderation data.
  (fn [db _]
    (get-in db [:content :mod-stats])))

(re-frame/reg-sub ::moderated-sub-stats
  ;; :doc Extract the list of moderated subs with statistics.
  :<- [::mod-stats]
  :<- [:view/options]
  :<- [:throat-fe.user/subs-not-moderated-names]
  (fn [[mod-stats {:keys [show-other-circles?]} subs-not-modded-names] _]
    (let [not-modded? (set subs-not-modded-names)]
      (if show-other-circles?
        (filter #(not-modded? (:name %))
                (get-in mod-stats [:all-sub-stats :subs]))
        (get mod-stats :subs-moderated)))))

(re-frame/reg-sub ::more-moderated-sub-stats-available?
  ;; :doc Are there more subs with moderation statistics that could be fetched?
  :<- [::mod-stats]
  :<- [:view/options]
  (fn [[mod-stats {:keys [show-other-circles?]}] _]
    (when show-other-circles?
      (get-in mod-stats [:all-sub-stats :more-items-available?]))))

(re-frame/reg-sub ::moderated-sub-stats-cursor
  ;; :doc Extract the cursor for fetching more sub stats.
  :<- [::mod-stats]
  :<- [:view/options]
  (fn [[mod-stats {:keys [show-other-circles?]}] _]
    (when show-other-circles?
      (get-in mod-stats [:all-sub-stats :cursor]))))

(re-frame/reg-sub ::viewed-sub-info
  ;; :doc Return info on the sub targeted by the view options.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [sub]}] _]
    (when sub
      (-> reldb
          (rel/q [[:from :SubName]
                  [:left-join :Sub {:sid :sid}]
                  [:where [= [str/lower-case :name] (str/lower-case sub)]]])
          first))))

(re-frame/reg-sub ::sub-banned?
  ;; :doc Extract whether the user is banned in the current sub.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (get-in sub [:user-attributes :banned])))

(re-frame/reg-sub ::normalized-sub-name
  ;; :doc Return the sub name routed to with the correct capitalization.
  ;; :doc Return the empty string if it's not a valid sub name.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (or (:name sub) "")))

(re-frame/reg-sub ::raw-sidebar
  ;; :doc Extract the sub sidebar pre-markdown text.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (:sidebar sub)))

(re-frame/reg-sub ::sidebar
  ;; :doc Format the sub's sidebar text with markdown.
  :<- [::raw-sidebar]
  (fn [text _]
    (if (empty? text)
      ""
      (util/markdown-to-html text))))

(re-frame/reg-sub ::creation-timeago
  ;; :doc Get the timeago for the sub's creation.
  :<- [::viewed-sub-info]
  :<- [::window/now]
  :<- [::tr/lang]
  (fn [[{:keys [creation]} now lang] _]
    (cond-> creation
      js/parseInt
      (util/time-ago now lang))))

(re-frame/reg-sub ::moderators
  ;; :doc Extract the list of sub moderators and their levels
  :<- [::viewed-sub-info]
  (fn [sub _]
    (:moderators sub)))

(re-frame/reg-sub ::active-moderators
  ;; :doc Filter out deleted moderators from the moderator list.
  :<- [::moderators]
  (fn [mods _]
    (filter #(= :ACTIVE (get-in % [:mod :status])) mods)))

(re-frame/reg-sub ::mod-names
  ;; :doc Get names of mods of the current sub.
  ;; :doc Levels can be a set of moderation level keywords
  ;; :doc or nil for all.
  :<- [::active-moderators]
  (fn [mods [_ levels]]
    (let [level? (or levels #{:OWNER :MODERATOR :JANITOR})]
      (->> mods
           (filter #(level? (:moderation-level %)))
           (map #(get-in % [:mod :name]))
           sort))))

(re-frame/reg-sub ::sub-user-flair
  ;; :doc Get a user's flair in a sub.
  :<- [::internal/reldb]
  (fn [reldb [_ sid uid]]
    (-> reldb
        (rel/q [[:from :SubUserFlair]
                [:where [:and [= :sid sid] [= :uid uid]]]])
        first
        :text)))

(loader/reg-loader ::sub-rules
  ;; :doc Load the post flairs for a sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::sub-rules
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql/subs-graphql
                                            :name :sub-rules
                                            :variables {:name sub-name}
                                            :handler [::receive-sub-rules]}]]
               [:dispatch [::sub-rules-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           sub-rules (:sub-by-name data)
           {:keys [rules sid]} sub-rules]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              rules  (internal/update-rules sid rules))
        :fx [[:dispatch [::sub-rules-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::rules
  ;; :doc Extract the rules for the currently viewed sub.
  :<- [::internal/reldb]
  :<- [::viewed-sub-info]
  (fn [[reldb {:keys [sid]}] _]
    (rel/q reldb [[:from :SubRule]
                  [:where [= :sid sid]]
                  [:sort [:order :asc]]])))

(loader/reg-loader ::post-flairs
  ;; :doc Load the post flairs for a sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::post-flairs
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql/subs-graphql
                                            :name :sub-post-flairs
                                            :variables {:name sub-name}
                                            :handler [::receive-post-flairs]}]]
               [:dispatch [::post-flairs-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           {:keys [post-flairs sid]} (:sub-by-name data)]
       {:db (cond-> db
              errors      (errors/assoc-errors {:event event :errors errors})
              post-flairs (internal/update-flairs sid post-flairs))
        :fx [[:dispatch [::post-flairs-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::post-flairs
  ;; :doc Extract the post flairs for the currently viewed sub.
  :<- [::internal/reldb]
  :<- [::viewed-sub-info]
  (fn [[reldb {:keys [sid]}] _]
    (rel/q reldb [[:from :SubPostFlair]
                  [:where [= :sid sid]]
                  [:sort [:order :asc]]])))


(defn permitted-flairs
  "Get the permitted flair objects for the selected post type."
  [reldb {:keys [sub-name is-mod-or-admin? post-type]}]
  (let [usable? (fn [{:keys [mods-only post-types]}]
                  (or is-mod-or-admin?
                      (and (not mods-only)
                           (post-types post-type))))]
    (rel/q reldb [[:from :SubPostFlair]
                  [:join :SubName {:sid :sid}]
                  [:where [= :name sub-name]]
                  [:where [usable?]]
                  [:sort [:order :asc]]])))

(defn- fixup-post-type-config
  [name config]
  (-> config
      (assoc :name name)
      (update :post-type keyword)))

(loader/reg-loader ::post-types
  ;; :doc Load the post types configuration for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::post-types
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql/subs-graphql
                                            :name :post-type-config
                                            :variables {:name sub-name}
                                            :handler [::receive-post-types]}]]
               [:dispatch [::post-types-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           name (get-in data [:sub-by-name :name])
           configs (->> (get-in data [:sub-by-name :post-type-config])
                        (map #(fixup-post-type-config name %)))]
       {:db (-> db
                (errors/assoc-errors {:event event :errors errors})
                (cond-> data
                  (update :reldb rel/transact
                          (into [:insert-or-replace :SubPostTypeConfig]
                                configs))))
        :fx [[:dispatch [::post-types-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::post-type-config
  ;; :doc Extract a post type config for the current sub.
  :<- [::internal/reldb]
  :<- [::normalized-sub-name]
  (fn [[reldb sub-name] [_ post-type]]
    (internal/get-post-type-config reldb sub-name post-type)))

(defn- fixup-sub
  "Make sub details fields more clojure-friendly."
  [sub]
  (let [keywordize-mod #(-> %
                            (update :moderation-level keyword)
                            (update-in [:mod :status] keyword))]
    (-> sub
        (update-in [:creator :status] keyword)
        (update :moderators #(map keywordize-mod %))
        (update :creation util/parse-int)
        (dissoc :name :post-flairs))))

(defn update-sub
  "Update or add a sub to the subs in the db."
  [db {:keys [sid post-flairs user-attributes] :as new-sub}]
  (let [sub (fixup-sub new-sub)
        uid (get-in db [:current-user :uid])]
    (-> db
        (update :reldb rel/transact
                [:insert-or-replace :SubName
                 (select-keys new-sub [:sid :name])])
        (update :reldb rel/transact [:insert-or-replace :Sub sub])
        (update :reldb rel/transact [:insert-or-replace :ContentStatus
                                     {:content-type ::info
                                      :name (:name new-sub)
                                      :status :success
                                      :epoch (:epoch db)}])
        (internal/update-flairs sid post-flairs)
        (cond-> user-attributes
          (update :reldb rel/transact
                  [:insert-or-replace :SubUserAttributes
                   {:sid sid
                    :banned (:banned user-attributes)}]))
        (cond-> (and user-attributes (:user-flair user-attributes))
          (update :reldb rel/transact
                  [:insert-or-replace :SubUserFlair
                   {:sid sid
                    :uid uid
                    :text (:user-flair user-attributes)}])))))

(defn update-sub-user-flair
  "Update a user's flair in the subs in the db."
  [db sid uid new-flair]
  (if (nil? new-flair)
    (update db :reldb rel/transact
            [:delete :SubUserFlair
             [= :sid sid] [= :uid uid]])
    (update db :reldb rel/transact
            [:insert-or-replace :SubUserFlair
             {:sid sid :uid uid :text new-flair}])))

(loader/reg-loader ::info
  ;; :doc Load the sidebar info, flairs etc. for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::info
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])
           authenticated? (some? (get-in db [:current-user :uid]))
           variables {:name sub-name
                      :is_authenticated authenticated?}]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql/subs-graphql
                                            :name :sub-info
                                            :variables variables
                                            :handler [::receive-info]}]]
               [:dispatch [::info-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           sub (:sub-by-name data)]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              data (update-sub sub))
        :fx [[:dispatch [::info-completed
                         (if (some? errors) :failure :success)]]]}))})

(loader/reg-loader ::submit-post-settings
  ;; :doc Load the sidebar info, flairs etc. for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [_]
     {:content-type ::submit-post-settings})

   :load
   (fn-traced [_ _]
     {:fx [[:dispatch [::graphql/query
                       {:graphql graphql/subs-graphql
                        :name :submit-post-settings
                        :variables {}
                        :handler [::receive-submit-post-settings]}]]]})

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           settings (:site-configuration data)]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              data (update :settings merge settings))
        :fx [[:dispatch [::submit-post-settings-completed
                         (if (some? errors) :failure :success)]]]}))})

(defn- describe-submit-post-error
  "Pass through an already translated error message from Python."
  [message]
  {:msg message})

(util/reg-ajax-form-event-chain ::submit-post
  ;; :doc Send a submit post form to the server and handle the response.
  {:call
   (fn-traced [{:keys [db]} [_ form update-form-event]]
     (let [{:keys [id sub title content link file selected-post-type
                   selected-flair nsfw? poll-options hide-results?
                   close-date close-poll?]} form
           ptype (-> selected-post-type name str/lower-case)
           valid-options (filter seq poll-options)
           append-options #(doall (map-indexed
                                   (fn [num opt]
                                     (.append % (str "options-" num) opt))
                                   valid-options))
           payload (doto (js/FormData.)
                     (.append "sub" sub)
                     (.append "title" title)
                     (.append "content" content)
                     (.append "link" link)
                     (.append "ptype" ptype)
                     (.append "flair" (or selected-flair ""))
                     (.append "nsfw" (str nsfw?))
                     (.append "hideresults" (str hide-results?))
                     (.append "closetime" (if close-poll?
                                            (.toISOString close-date) ""))
                     (.append "files" (or file ""))
                     append-options)]
       {:fx [[:dispatch
              [::util/ajax-csrf-formdata-post
               {:url (route-util/url-for db :subs/submit :ptype ptype)
                :form-id id
                :sub sub
                :payload payload
                :update-form-event update-form-event}]]]}))
   :success
   (fn-traced [{:keys [db]} [_ {:keys [update-form-event sub response]}]]
     (let [pid (:pid response)]
       {:fx [[:dispatch update-form-event]
             (when pid
               [::routes/set-url (route-util/url-for db :sub/view-single-post
                                                     :sub sub :pid pid)])]}))
   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form-id update-form-event error-response]}]]
     {:db (-> db
              (errors/assoc-errors
               {:event form-id
                :errors [(get-in error-response [:response :message])]
                :describe-error-fn describe-submit-post-error}))
      :fx [[:dispatch update-form-event]]})})
