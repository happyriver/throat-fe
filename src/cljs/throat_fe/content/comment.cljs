;; content/comment.cljs -- Comment content for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.comment
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [clojure.walk :as walk]
   [com.rpl.specter :as s]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame :refer [subscribe]]
   [re-frame.std-interceptors :refer [path]]
   [statecharts.core :as fsm]
   [throat-fe.content.comment.db :refer [assoc-comment
                                         update-comment]]
   [throat-fe.content.internal :refer [sid-from-name sub-rules] :as internal]
   [throat-fe.content.post :as-alias post]
   [throat-fe.content.post.db :refer [single-post]]
   [throat-fe.content.settings :as-alias settings]
   [throat-fe.content.subs :as subs]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :refer [trx] :as tr]
   [throat-fe.ui :as ui]
   [throat-fe.ui.forms.compose-comment :as comment-form]
   [throat-fe.ui.forms.mod-action :as mod-action-form]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as-alias user]
   [throat-fe.util :as util]))

;; Use COMMENTS to navigate to all comments in the tree.
(s/declarepath COMMENTS)
(s/declarepath COMMENT)
(s/providepath COMMENTS [s/ALL COMMENT])
(s/providepath COMMENT (s/continue-then-stay [:children COMMENTS]))

(defn comment-tree-path
  "Return the path in the db to the comment tree for a post."
  [db pid]
  (let [sort-path (get-in db [:ui-state :post :sort-path])]
    (-> (concat [:content :comments pid] sort-path [:tree])
        vec)))

(defn comment-loaded-path
  "Return the path in the db to the set of loaded cids for a post."
  [db pid]
  (let [sort-path (get-in db [:ui-state :post :sort-path])]
    (-> (concat [:content :comments pid] sort-path [:loaded])
        vec)))

(defn comment-levels-path
  "Return the path to the comment levels map."
  [pid]
  [:content :comments pid :levels])

(defn comment-level
  "Return a comment's depth in the tree"
  [db pid cid]
  (get-in db (conj (comment-levels-path pid) cid)))

(defn- keywordize-comment
  "Make keywords out of the enums in a queried comment."
  [comment]
  (-> comment
      (update :distinguish keyword)
      (update :status util/screaming-kebab-case)
      (update :user-attributes #(when %
                                  (update % :vote keyword)))))

(defn- extract-levels
  "Extract the level field from the comment tree nodes into a flat map."
  [tree]
  (apply merge
         (walk/postwalk #(if (map? %)
                           (-> (apply merge (:children %))
                               (assoc (:cid %) (:level %)))
                           %) tree)))

(defn- prep-comment
  [{:keys [author checkoff] :as comment} pid]
  (-> comment
      (assoc :uid (:uid author))
      (dissoc :author)
      (assoc :pid pid)
      (update :time util/parse-int)
      (update :edited util/parse-int)
      (cond->
          checkoff (update-in [:checkoff :time] util/parse-int))
      keywordize-comment))

(defn- extract-authors
  [comments]
  (map (fn [{:keys [author]}]
         (update author :status keyword)) comments))

(defn- extract-author-flairs
  [comments sid]
  (->> comments
       (map (fn [{:keys [author author-flair]}]
              {:uid (:uid author)
               :sid sid
               :text author-flair}))
       (filter :text)))

(defn- unpack-comment-data
  "Make the received comment data easier to use.
  Parse the comment tree from JSON and convert the list of comments
  into a map from cid to comment info. Return a map containing
  the tree, the list of comments and a map from cids to comment levels."
  [comment-tree pid sid]
  (let [{:keys [tree-json comments]} comment-tree
        tree (->> tree-json
                  util/parse-json
                  (walk/postwalk
                   #(if (map? %)
                      (update % :status keyword)
                      %)))]
    {:tree tree
     :comments (map #(prep-comment % pid) comments)
     :users (extract-authors comments)
     :flairs (extract-author-flairs comments sid)
     :levels (extract-levels tree)}))

(defn update-comments
  "Update the comments section with the newly loaded comments."
  [db comment-tree pid sort-path]
  (let [sub (get-in db [:view :options :sub])
        sid (sid-from-name db sub)
        comment-data (unpack-comment-data comment-tree pid sid)
        {:keys [tree comments users flairs levels]} comment-data]
    (-> db
        (update :reldb rel/transact
                (into [:insert-or-replace :Comment] comments))
        (update :reldb rel/transact
                (into [:insert-or-replace :User] users))
        (update :reldb rel/transact
                (into [:insert-or-replace :SubUserFlair] flairs))
        (assoc-in (concat [:content :comments pid] sort-path)
                  {:tree tree
                   :loaded (->> comments (map :cid) set)})
        (update-in [:content :comments pid :levels] merge levels))))

(re-frame/reg-sub ::comment-data
  ;; :doc Extract the comment data for the currently-viewed post.
  (fn [db _]
    (let [pid (get-in db [:view :options :pid])]
      (get-in db [:content :comments pid]))))

(re-frame/reg-sub ::comment-tree
  ;; :doc Extract the comment tree for the current post.
  :<- [::comment-data]
  :<- [::post/single-post-sort-path]
  (fn [[data sort-path] _]
    (get-in data (conj sort-path :tree))))

(re-frame/reg-sub ::root-level
  ;; :doc Get the root level of the tree.
  :<- [::comment-tree]
  (fn [tree _]
    (if (empty? tree)
      0
      (-> tree first :level))))

(re-frame/reg-sub ::loaded-cids
  ;; :doc Extract the set of loaded cids for the current sort method.
  :<- [::comment-data]
  :<- [::post/single-post-sort-path]
  (fn [[data sort-path] _]
    (get-in data (conj sort-path :loaded))))

(defn extract-children
  ;; :doc Get a list of child comments from the tree.
  ;; :doc `path` is a list of parent cids to navigate to.
  [tree path]
  (if (empty? path)
    tree
    (extract-children (->> tree
                           (some #(when (= (:cid %) (last path)) %))
                           :children)
                      (butlast path))))

(re-frame/reg-sub ::child-comment-branches
  :<- [::comment-tree]
  ;; :doc Extract child comments from the comment tree.
  (fn [tree [_ path]]
    (extract-children tree path)))

(defn- loaded-children
  [branches loaded-cids]
  (filter (fn [{:keys [cid]}]
            (loaded-cids cid)) branches))

(re-frame/reg-sub ::child-comments
  ;; :doc Return a list of the child comments with content loaded.
  (fn [[_ path]]
    [(subscribe [::child-comment-branches path])
     (subscribe [::loaded-cids])])
  (fn [[branches loaded-cids] [_ _]]
    (loaded-children branches loaded-cids)))

(defn- count-children
  [{:keys [children] :as node} new?]
  (if (map? node)
    (-> node
        (assoc :count
               (+ (count children)
                  (apply + (map :count children))))
        (assoc :unchecked-count
               (+ (count (filter #(not (:checked %)) children))
                  (apply + (map :unchecked-count children))))
        (assoc :new-count
               (+ (count (filter new? children))
                  (apply + (map :new-count children)))))
    node))

(defn- unloaded-children
  "Return the child nodes in the tree which are not loaded."
  [all-children loaded-children]
  (let [loaded? (-> (map :cid loaded-children)
                    set)
        unloaded (filter (fn [{:keys [cid]}]
                           (not (loaded? cid)))
                         all-children)]
    (vec unloaded)))

(re-frame/reg-sub ::unloaded-child-counts
  ;; :doc Count the child comments which are not loaded.
  ;; :doc Also count the child comments which are not loaded and not
  ;; :doc yet checked off.  Include all descendants.
  (fn [[_ path]]
    [(subscribe [::child-comment-branches path])
     (subscribe [::child-comments path])
     (subscribe [::post/user-attributes])
     (subscribe [::user/current-user])])
  (fn [[all loaded {:keys [viewed]} current-user] _]
    (let [new? (fn [{:keys [uid time status]}]
                 (and (some? viewed)
                      (> time viewed)
                      (= :ACTIVE status)
                      (not= uid (:uid current-user))))
          counted (->> {:children (unloaded-children all loaded)}
                       (walk/postwalk #(count-children % new?)))]
      (select-keys counted [:count :unchecked-count :new-count]))))

(re-frame/reg-sub ::unloaded-child-count
  ;; :doc Produce the count of child comments which are not loaded.
  (fn [[_ path]]
    (subscribe [::unloaded-child-counts path]))
  (fn [counts _]
    (:count counts)))

(re-frame/reg-sub ::unchecked-child-count
  ;; :doc Count the child comments which are not loaded and not checked off.
  (fn [[_ path]]
    [(subscribe [::user/is-mod-or-admin?])
     (subscribe [::unloaded-child-counts path])])
  (fn [[is-mod-or-admin? counts] _]
    (if is-mod-or-admin?
      (:unchecked-count counts)
      0)))

(re-frame/reg-sub ::new-child-count
  ;; :doc Count the child comments which are not loaded and new.
  (fn [[_ path]]
    [(subscribe [::post/single-post])
     (subscribe [::unloaded-child-counts path])])
  (fn [[{:keys [is-archived]} counts] _]
    (if is-archived
      0
      (:new-count counts))))

(defn extract-unloaded-children
  ;; :doc Return nodes from the comment tree whose cids are unloaded.
  [db path]
  (let [pid (get-in db [:view :options :pid])
        tree (get-in db (comment-tree-path db pid))
        loaded-cids (get-in db (comment-loaded-path db pid))
        all (extract-children tree path)
        loaded (loaded-children all loaded-cids)
        not-yet-loaded (unloaded-children all loaded)]
    not-yet-loaded))

(defn- assoc-child-count
  "Add the count of a node's descendants to the node.
  Only count descendants up to and including `level-limit`"
  [{:keys [level children] :as node} level-limit]
  (assoc node :count
         (if (< level level-limit)
           (+ (count children) (apply + (map :count children)))
           0)))

(defn- count-children-up-to-level
  "Add a :count field to each node with the count of the node's descendants.
  Only count descendants whose level is at or below `level-limit`."
  [comment-tree level-limit]
  (walk/postwalk #(if (map? %)
                    (assoc-child-count % level-limit)
                    %)
                 comment-tree))

(defn- list-cids-in-tree
  "Return a flattened list of the cids in a comment tree.
  Do not include cids of any comments below `level` deep."
  [tree level]
  (->> tree
       (walk/postwalk
        #(if (map? %)
           (cond
             (< (:level %) level) (conj (:children %) (:cid %))
             (= (:level %) level) [(:cid %)]
             :else [])
           %))
       flatten))

(defn- collect-cids
  "Collect cids from `tree`.
  Comment ids for top level comments and their descendants, up to
  `level-limit` deep, will be added to the list of comment ids, until
  the length of the list exceeds the argument `num-to-fetch`, or there
  are no more top-level comments."
  [tree num-to-fetch level-limit]
  (loop [remaining (count-children-up-to-level tree level-limit)
         num 0
         collected []]
    (let [node (first remaining)]
      (if (or (empty? remaining) (>= num num-to-fetch))
        (list-cids-in-tree collected level-limit)
        (recur (rest remaining)
               (+ num 1 (:count node))  ; Add 1 to include self.
               (conj collected node))))))

(defn- cids-to-load
  [db path]
  (let [not-yet-loaded (extract-unloaded-children db path)
        {:keys [num level]} (get-in db [:settings :comment-tree])
        current-level (-> not-yet-loaded first :level)]
    (collect-cids not-yet-loaded num (+ current-level level))))

(re-frame/reg-event-fx ::load-more
  ;; :doc Request more child comments from the server.
  (fn-traced [{:keys [db]} [_ path]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          sort-path (get-in db [:ui-state :post :sort-path])
          cids (cids-to-load db path)
          authenticated? (some? (get-in db [:current-user :uid]))
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin]))
                            boolean)
          state {:sort-path sort-path :pid pid :path path
                 :sid (sid-from-name db sub)}]
      {:db (assoc-in db [:status :more-comments (str path)] :loading)
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql/subs-graphql
                         :name :comments-by-cid
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :sid (internal/sid-from-name db sub)
                                     :cids cids}
                         :handler [::receive-more-comments state]}]]]})))

(re-frame/reg-sub ::loading-more-map
  ;; :doc Extract the status of the load more links
  :<- [::window/statuses]
  (fn [status [_ _]]
    (:more-comments status)))

(re-frame/reg-sub ::load-more-status
  ;; :doc Extract the status of the load more link for a path.
  :<- [::loading-more-map]
  (fn [loading-more [_ path]]
    (get loading-more (str path))))

(re-frame/reg-event-db ::receive-more-comments
  ;; :doc Handle the response to a request for more comments.
  (fn-traced [db [event {:keys [sort-path sid pid path response]}]]
    (let [{:keys [data errors]} response
          users (extract-authors (:comments-by-cid data))
          flairs (extract-author-flairs (:comments-by-cid data) sid)
          comments (->> (:comments-by-cid data)
                        (map #(prep-comment % pid)))
          cids (set (map :cid comments))
          loaded-path (concat [:content :comments pid] sort-path [:loaded])]
      (-> db
          (update-in [:status :more-comments] dissoc (str path))
          (errors/assoc-errors {:event event :errors errors})
          (update :reldb rel/transact
                  (into [:insert-or-replace :Comment] comments))
          (update :reldb rel/transact
                  (into [:insert-or-replace :User] users))
          (update :reldb rel/transact
                  (into [:insert-or-replace :SubUserFlair] flairs))
          (update-in loaded-path set/union cids)))))

(re-frame/reg-sub ::comment-count
  ;; :doc Calculate the number of comments in the tree.
  :<- [::comment-tree]
  (fn [tree _]
    (apply + (walk/postwalk #(if (map? %)
                               (inc (apply + (:children %)))
                               %)
                            tree))))

(re-frame/reg-sub ::details
  ;; :doc Get the details for a single comment by cid.
  :<- [::internal/reldb]
  (fn [reldb [_ cid]]
    (rel/row reldb :Comment [= :cid cid])))

(re-frame/reg-sub ::author
  ;; :doc Get a comment's author.
  (fn [[_ cid]]
    [(subscribe [::internal/reldb])
     (subscribe [::details cid])
     (subscribe [::user/is-mod-or-admin?])])
  (fn [[reldb {:keys [uid status]} is-mod-or-admin?] ]
    (let [user (rel/row reldb :User [= :uid uid])]
      (if (or (= :ACTIVE status) is-mod-or-admin?)
        user
        (dissoc user :name)))))

(re-frame/reg-sub ::is-author?
  ;; :doc Determine whether the current user is the author of the comment.
  (fn [[_ cid]]
    [(subscribe [::user/is-authenticated?])
     (subscribe [::user/current-user])
     (subscribe [::author cid])])
  (fn [[is-authenticated? {:keys [name]} author]]
    (and is-authenticated? (= name (:name author)))))

(re-frame/reg-sub ::user-attributes
  ;; :doc Extract the user-specific parts of the comment.
  (fn [[_ cid]]
    (subscribe [::details cid]))
  (fn [comment _]
    (:user-attributes comment)))

(re-frame/reg-sub ::vote
  ;; :doc Extract the user's vote on this comment.
  (fn [[_ cid]]
    (subscribe [::user-attributes cid]))
  (fn [attr _]
    (:vote attr)))

(re-frame/reg-sub ::edit-history
  ;; :doc Extract the comment's edit history.
  ;; :doc Edit history won't come from the server for non-mods, but it
  ;; :doc gets added if they make an edit here, so don't show it to them.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::user/is-mod-or-admin?])])
  (fn [[details is-mod-or-admin?] _]
    (when is-mod-or-admin?
      (:content-history details))))

(re-frame/reg-sub ::levels
  ;; :doc Get the map of comment levels.
  :<- [::comment-data]
  (fn [comment-data _]
    (:levels comment-data)))

(re-frame/reg-sub ::level
  ;; :doc Get the level of a comment.
  :<- [::levels]
  (fn [levels [_ cid]]
    (get levels cid)))

(re-frame/reg-sub ::edit-history-index
  ;; :doc Get the edit history index for a comment.
  (fn [[_ cid]]
    (subscribe [::ui-state cid]))
  (fn [state _]
    (:content-history-index state)))

(re-frame/reg-sub ::edit-history-state
  ;; :doc Construct a map with state for the edit history widget.
  (fn [[_ cid]]
    [(subscribe [::edit-history cid])
     (subscribe [::edit-history-index cid])])
  (fn [[edit-history index] _]
    {:show-history? (seq edit-history)
     :num-edits (inc (count edit-history))
     :index index
     :oldest? (zero? index)
     :newest? (nil? index)}))

(defn- comment-content-history-count
  "Return the count of items in a comment's edit history."
  [{:keys [reldb]} cid]
  (-> (rel/row reldb :Comment [= :cid cid])
      :content-history
      count))

(defn- inc-index [{:keys [content-history-index] :as state} num]
  (assoc state :content-history-index
         (when (and content-history-index (< (inc content-history-index) num))
           (inc content-history-index))))

(defn- dec-index [{:keys [content-history-index] :as state} num]
  (assoc state :content-history-index
         (cond
           (nil? content-history-index) (dec num)
           (zero? content-history-index) 0
           :else (dec content-history-index))))

(re-frame/reg-event-db ::see-newer-version
  ;; :doc Look at a newer version of the comment history.
  (fn-traced [db [_ cid]]
    (let [num (comment-content-history-count db cid)]
      (update-in db [:ui-state :comment cid] inc-index num))))

(re-frame/reg-event-db ::see-older-version
  ;; :doc Look at an older version of the comment history.
  (fn-traced [db [_ cid]]
    (let [num (comment-content-history-count db cid)]
      (update-in db [:ui-state :comment cid] dec-index num))))

(re-frame/reg-sub ::content
  ;; :doc Extract the content of a single comment by cid.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::edit-history-index cid])])
  (fn [[{:keys [content-history] :as comment} index] _]
    (:content (if index
                (nth content-history index)
                comment))))

(re-frame/reg-sub ::html-content
  ;; :doc Produce the html version of a comment's content.
  (fn [[_ cid]]
    (subscribe [::content cid]))
  (fn [content _]
    (when content
      (util/markdown-to-html content))))

(re-frame/reg-event-db ::record-html-content-height
  ;; :doc Save the height of the html content element in the db.
  (fn-traced [db [_ cid val]]
    (assoc-in db [:ui-state :comment cid :content-height] val)))

(re-frame/reg-event-db ::record-content-dom-node
  ;; :doc Save the html content's dom node in the db.
  (fn-traced [db [_ cid val]]
    (assoc-in db [:ui-state :comment cid :content-dom-node] val)))

(re-frame/reg-cofx ::comment-content-bottom-map
  ;; :doc Get current positions of bottom of comment html content.
  ;; :doc Return a map indexed by cid.  Value may be a number or nil.
  (fn [{:keys [db] :as cofx} _]
    (let [get-bottom (fn [{:keys [content-dom-node]}]
                       (some-> content-dom-node
                               .getBoundingClientRect
                               .-bottom))
          result (->> (get-in db [:ui-state :comment])
                      (s/transform [s/MAP-VALS] get-bottom))]
      (assoc cofx :comment-bottoms result))))

(re-frame/reg-cofx ::comment-content-top
  ;; :doc Get the current position of the top of a comment's content.
  ;; :doc Value may be a number or nil.  Get the cid to use from the
  ;; :doc event vector.
  (fn [{:keys [db event] :as cofx} _]
    (let [[_ cid] event
          dom-node (get-in db [:ui-state :comment cid :content-dom-node])
          top (some-> dom-node
                      .getBoundingClientRect
                      .-top)]
      (assoc cofx :comment-content-top top))))

(re-frame/reg-event-fx ::start-scroll-watcher
  ;; Start an event watcher to check viewed or updated comments on scroll.
  (fn-traced [_ [_ _]]
    {:window/on-scroll {:event [::visibility-check]
                        :debounce-ms 100
                        :id ::scroll-watcher}}))

(re-frame/reg-event-fx ::stop-scroll-watcher
  ;; Stop the scroll event watcher.
  (fn-traced [_ [_ _]]
    {:window/stop-on-scroll {:id ::scroll-watcher}}))

(re-frame/reg-event-fx ::start-resize-watcher
  ;; Start an event watcher to check viewed or updated comments on resize.
  (fn-traced [_ [_ _]]
    {:window/on-resize {:event [::visibility-check]
                        :debounce-ms 100
                        :id ::resize-watcher}}))

(re-frame/reg-event-fx ::stop-resize-watcher
  ;; Stop the resize event watcher.
  (fn-traced [_ [_ _]]
    {:window/stop-on-resize {:id ::resize-watcher}}))

(re-frame/reg-event-fx ::visibility-check
  ;; :doc Update after possible changes to comment visibility.
  (fn-traced [_ [_ _]]
    {:fx [[:dispatch [::mark-viewed]]
          [:dispatch [::process-updates]]]}))

(defn- visible-comment-cids
  [db comment-bottoms window-height]
  (let [not-viewed? #(not (get-in % [:user-attributes :viewed]))
        visible? (fn [{:keys [cid]}]
                   (let [bottom (get comment-bottoms cid)]
                     (and bottom (pos? bottom)
                          (< bottom window-height))))
        marked-viewed? #(get-in db [:ui-state :comment (:cid %)
                                    :marked-viewed?])]
    (->> (rel/q (:reldb db) [[:from :Comment]])
         (filter #(and (not-viewed? %)
                       (visible? %)
                       (not (marked-viewed? %))))
         (map :cid))))

(re-frame/reg-event-fx ::mark-viewed
  ;; :doc Mark the visible comments as viewed.
  [(re-frame/inject-cofx ::comment-content-bottom-map)
   (re-frame/inject-cofx ::window/height)]
  (fn-traced [{:keys [comment-bottoms db window-height]} _]
    (let [is-archived? (:is-archived (single-post db))
          authenticated? (some? (get-in db [:current-user :uid]))
          menu-state (get-in db [:ui-state :menu])]
      ;; If the mobile menu is open, it might be hiding the comments.
      (when (and authenticated? (not is-archived?) (not= menu-state :open))
        (let [cids (visible-comment-cids db comment-bottoms window-height)]
          {:db (reduce (fn [dbx cid]
                         (assoc-in dbx [:ui-state :comment cid :marked-viewed?]
                                   true))
                       db cids)
           :fx [(when (seq cids)
                  [:dispatch [::util/ajax-form-post
                              [(route-util/url-for db :do/mark-comments-viewed)
                               {:cids (-> cids
                                          clj->js
                                          js/JSON.stringify)}
                               nil
                               [::errors/receive-ajax-error ::mark-viewed
                                identity]]]])]})))))

(defn- remove-updates-by-cid
  [updates remove?]
  (filter (fn [{:keys [cid]}]
            (not (remove? cid)))
          updates))

(defn- apply-update
  [db {:keys [cid comment]}]
  (let [fields (select-keys comment [:checkoff :distinguish :status])]
    (update-comment db cid #(merge % fields))))

(defn- apply-updates
  [db updates]
  (reduce apply-update db updates))

(re-frame/reg-event-fx ::process-updates
  ;; :doc Update comments which have scrolled into view.
  [(re-frame/inject-cofx ::comment-content-bottom-map)]
  (fn-traced [{:keys [comment-bottoms db]} [_ _]]
    (let [pid (get-in db [:view :options :pid])
          update-path [:content :comments pid :updates]
          updates (get-in db update-path)
          to-apply (filter (fn [{:keys [cid]}]
                             (let [bottom (get comment-bottoms cid)]
                               (and bottom (pos? bottom))))
                           updates)
          applied-cid? (->> to-apply
                            (map :cid)
                            set)]
      {:db (-> db
               (apply-updates to-apply)
               (update-in update-path remove-updates-by-cid applied-cid?))})))

(re-frame/reg-sub ::ui-state
  ;; :doc Extract the comment UI state
  :<- [::ui/state]
  (fn [state [_ cid]]
    (get-in state [:comment cid])))

(re-frame/reg-sub ::highlight
  ;; :doc Get the highlight state of a comment.
  ;; :doc Possiblities are :new if just inserted, and :direct if we are
  ;; :doc on that comment's direct link page.
  (fn [[_ cid]]
    [(subscribe [::ui-state cid])
     (subscribe [:view/options])])
  (fn [[ui-state {highlight-cid :cid}] [_ cid]]
    (or (:highlight ui-state)
        (when (= cid highlight-cid)
          :direct))))

(re-frame/reg-sub ::new?
  ;; :doc Determine whether the comment should be highlighted as new.
  ;; :doc Set if the comment is new since the last time the user
  ;; :doc looked at this post, isn't authored by the user, and
  ;; :doc hasn't just been checked off by a mod.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::ui-state cid])
     (subscribe [::post/user-attributes])
     (subscribe [::is-author? cid])])
  (fn [[{:keys [time status]} ui-state {:keys [viewed]} is-author?] _]
    (and (some? viewed) (> time viewed)
         (= :ACTIVE status)
         (not is-author?)
         (not (:checked-off? ui-state)))))

(re-frame/reg-sub ::background
  ;; :doc Get the background class to apply to a comment.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::highlight cid])
     (subscribe [::new? cid])
     (subscribe [::can-check-off? cid])
     (subscribe [::user/is-mod-or-admin?])
     (subscribe [::window/day?])])
  (fn [[{:keys [status]} highlight new? can-check-off? is-mod-or-admin? day?] _]
    (let [highlight-delete? (and is-mod-or-admin? (not= :ACTIVE status))]
      (cond
        highlight-delete?   (if day? :bg-washed-pink :bg-dark-reddish-brown)
        can-check-off?      (if day? :bg-washed-green :bg-dark-bluish-green)
        (or highlight new?) (if day? :bg-washed-yellow :bg-dark-gray)
        :else               (if day? :bg-white :bg-black)))))

(re-frame/reg-sub ::times
  ;; :doc Extract the posting and last edit times of a single comment by cid.
  (fn [[_ cid]]
    (subscribe [::details cid]))
  ;; TODO why don't these get parsed when the comment arrives?
  (fn [{:keys [time edited checkoff]} _]
    {:time (js/parseInt time)
     :edited (when edited (js/parseInt edited))
     :checkoff (when checkoff (js/parseInt (:time checkoff)))}))

(re-frame/reg-sub ::timeagos
  ;; :doc Compute the timeagos of a single comment by cid.
  ;; :doc Produce a map with :time and :edited keys.
  (fn [[_ cid]]
    [(subscribe [::tr/lang])
     (subscribe [::times cid])
     (subscribe [::window/now])])
  (fn [[lang times now] _]
    (s/transform [s/MAP-VALS] #(when %
                                 (util/time-ago % now lang)) times)))

(re-frame/reg-event-fx ::toggle-collapse
  ;; :doc Toggle the collapse state of a comment.
  ;; :doc If the top of the comment is above the top of the visible
  ;; :doc area, scroll so that its top is about 20% down from the top
  ;; :doc in the client (window) area
  [(re-frame/inject-cofx ::window/height)
   (re-frame/inject-cofx ::comment-content-top)]
  (fn-traced [{:keys [db window-height comment-content-top]} [_ cid]]
    (let [top-above-visible-area? (and comment-content-top
                                       (neg? comment-content-top))
          dom-node (get-in db [:ui-state :comment cid :content-dom-node])]
      {:db (-> db
               (update-in [:ui-state :comment cid :toggle-collapse] not)
               (assoc-in [:ui-state :comments :last-toggled-cid] cid))
       :fx [[:dispatch [::set-collapse-hover cid false]]
            (when top-above-visible-area?
              [::window/scroll-elem-to {:elem dom-node
                                        :y (* 0.2 window-height)}])]})))

(re-frame/reg-sub ::collapse-toggled?
  ;; :doc Get whether the collapse state of a comment has been toggled.
  (fn [[_ cid]]
    (subscribe [::ui-state cid]))
  (fn [ui-state _]
    (:toggle-collapse ui-state)))

(re-frame/reg-event-db ::set-collapse-hover
  ;; :doc Set whether a collapse control or bar is hovered over.
  ;; :doc debounce?
  (fn-traced [db [_ cid val]]
    (assoc-in db [:ui-state :comment cid :hover-collapse] val)))

(re-frame/reg-sub ::comments-ui-state
  ;; :doc Extract the state that applies to the entire comment listing.
  :<- [::ui/state]
  (fn [state _]
    (:comments state)))

(re-frame/reg-sub ::collapse-hover
  ;; :doc Get the collapse hover value for a comment.
  (fn [[_ cid]]
    [(subscribe [::ui-state cid])
     (subscribe [::comments-ui-state])])
  (fn [[{:keys [hover-collapse]} {:keys [last-toggled-cid]}] [_ cid]]
    (or hover-collapse (= cid last-toggled-cid))))

(re-frame/reg-sub ::content-block
  ;; :doc Determine what kind of content blocking, if any, applies to a comment.
  (fn [[_ cid]]
    [(subscribe [::user/is-mod-or-admin?])
     (subscribe [::subs/mod-names nil])
     (subscribe [::author cid])
     (subscribe [::user/content-blocks])])
  (fn [[is-mod-or-admin? mods author content-blocks] _]
    (let [{:keys [name uid]} author
          author-is-mod? ((set mods) name)]
      (when (and (not is-mod-or-admin?) (not author-is-mod?))
        (get content-blocks uid)))))

(re-frame/reg-sub ::initially-collapsed?
  ;; :doc Determine whether the comment starts out collapsed.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::content-block cid])])
  (fn [[{:keys [content]} content-block] _]
    (or (nil? content)
        (some? content-block))))

(re-frame/reg-sub ::collapsed?
  ;; :doc Get whether a comment display is set to the collapsed state.
  (fn [[_ cid]]
    [(subscribe [::collapse-toggled? cid])
     (subscribe [::initially-collapsed? cid])])
  (fn [[toggled? initially-collapsed?] _]
    (if toggled?
      (not initially-collapsed?)
      initially-collapsed?)))

(re-frame/reg-sub ::can-vote?
  ;; :doc Determine whether to enable the voting arrows.
  ;; :doc They are enabled for anons because they bring up the login
  ;; :doc modal.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::subs/sub-banned?])
     (subscribe [::post/single-post-is-archived?])
     (subscribe [::details cid])
     (subscribe [::author cid])
     (subscribe [::content-block cid])
     (subscribe [::settings/site-config])])
  (fn [[is-author? banned? is-archived? {:keys [status]} author
        content-block {:keys [self-vote-comments]}] _]
    (and (or self-vote-comments (not is-author?))
         (not is-archived?)
         (not banned?)
         (= :ACTIVE status)
         (not= :DELETED (:status author))
         (not= :HIDE content-block))))

(defn- can-check-off?
  [{:keys [status checkoff]} is-archived? is-mod?]
  (and is-mod?
       (= :ACTIVE status)
       (not is-archived?)
       (nil? checkoff)))

(re-frame/reg-event-fx ::vote
  ;; :doc Send a comment vote request to the Python server.
  ;; :doc If the user is a mod, the vote is an upvote and the
  ;; :doc comment is not checked off, send a checkoff request
  ;; :doc as well.
  (fn-traced [{:keys [db]} [_ cid direction]]
    (let [post (single-post db)
          comment (rel/row (:reldb db) :Comment [= :cid cid])
          in-flight? (get-in db [:status :comment-vote-in-flight? cid])
          existing-vote (get-in comment [:user-attributes :vote])
          new-vote (when (not= existing-vote direction)
                     direction)
          vote-value (-> direction name str/lower-case)
          restore (fn [db] (-> db
                               (update :reldb rel/transact
                                       [:update :Comment
                                        #(assoc-in % [:user-attributes :vote]
                                                   existing-vote)
                                        [= :cid cid]])
                               (update-in [:status :comment-vote-in-flight?]
                                          dissoc cid)))]
      (when-not in-flight?
        {:db (-> db
                 (update :reldb rel/transact
                         [:update :Comment
                          #(assoc-in % [:user-attributes :vote]
                                     new-vote)
                          [= :cid cid]])
                 (assoc-in [:status :comment-vote-in-flight? cid] true))
         :fx [[:dispatch [::util/ajax-form-post
                          [(route-util/url-for db :do/upvotecomment :cid cid
                                               :value vote-value)
                           {}
                           [::receive-vote-comment-response cid direction]
                           [::errors/receive-ajax-error
                            ::vote-comment restore]]]]
              (when (and (= direction :UP)
                         (can-check-off? comment (:is-archived post)
                                         (internal/is-mod-of-sub? db)))
                [:dispatch [::check-off cid]])]}))))

(re-frame/reg-event-db ::receive-vote-comment-response
  ;; :doc Receive the response to a comment vote request.
  (fn-traced [db [_ cid direction {:keys [rm score]}]]
    (-> db
        (update-in [:status :comment-vote-in-flight?] dissoc cid)
        (update :reldb rel/transact
                [:update :Comment
                 #(-> %
                      (assoc :score score)
                      (assoc-in [:user-attributes :vote]
                                (when-not rm direction)))
                 [= :cid cid]]))))

;;; Comment bottombar links menu

(re-frame/reg-sub ::show-blocked-users-link?
  ;; :doc Show the blocked users page link if relevant to the comment.
  (fn [[_ cid]]
    [(subscribe [::author cid])
     (subscribe [::content-block cid])])
  (fn [[{:keys [status]} content-block] _]
    (and content-block (not= :DELETED status))))

(re-frame/reg-sub ::can-reply?
  ;; :doc Determine whether the user can reply to a comment.
  (fn [[_ cid]]
    [(subscribe [::subs/viewed-sub-info])
     (subscribe [::post/single-post])
     (subscribe [::details cid])
     (subscribe [::author cid])
     (subscribe [::level cid])
     (subscribe [::content-block cid])
     (subscribe [::user/is-authenticated?])
     (subscribe [::user/is-mod-or-admin?])])
  (fn [[{:keys [banned]} post comment author level
        content-block is-authenticated? is-mod-or-admin?] _]
    (and is-authenticated?
         (not banned)
         (not= :DELETED (:status author))
         (not= :HIDE content-block)
         (not (:is-archived post))
         (< (inc level) 25)
         (or is-mod-or-admin? (and (not (:locked post))
                                   (not= :DELETED (:status comment))
                                   (not= :DELETED (:status post)))))))

(re-frame/reg-sub ::can-report?
  ;; :doc Determine whether the comment is reportable.
  (fn [[_ cid]]
    [(subscribe [::user/is-authenticated?])
     (subscribe [::is-author? cid])
     (subscribe [::details cid])
     (subscribe [::content-block cid])])
  (fn [[is-authenticated? is-author? {:keys [status]} content-block] _]
    (and is-authenticated?
         (not is-author?)
         (= :ACTIVE status)
         (not= :HIDE content-block))))

(re-frame/reg-sub ::can-show-source?
  ;; :doc Determine whether to show the source link on the comment bottombar.
  (fn [[_ cid]]
    [(subscribe [::content cid])
     (subscribe [::content-block cid])])
  (fn [[content content-block] _]
    (and (some? content) (not= :HIDE content-block))))

(re-frame/reg-sub ::can-edit?
  ;; :doc Determine whether the comment is editable.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::details cid])])
  (fn [[is-author? {:keys [status]}] [_ _cid]]
    (and is-author? (= :ACTIVE status))))

(re-frame/reg-sub ::can-delete?
  ;; :doc Determine whether the user can delete the comment.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::user/is-mod-or-admin?])
     (subscribe [::details cid])])
  (fn [[is-author? is-mod-or-admin? {:keys [status]} ] _]
    (and (= :ACTIVE status)
         (or is-author? is-mod-or-admin?))))

(re-frame/reg-event-fx ::delete-comment
  ;; :doc Delete a comment.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [comment (rel/row (:reldb db) :Comment [= :cid cid])
          fields (select-keys comment [:status :content])
          current-user-uid (get-in db [:current-user :uid])
          restore (fn [db]
                    (update db :reldb rel/transact
                            [:update :Comment
                             #(merge % fields)
                             [= :cid cid]]))]
      (if (= current-user-uid (:uid comment))
        {:db (update db :reldb rel/transact
                     [:update :Comment
                      #(assoc % :status :DELETED-BY-USER
                              :content nil)
                      [= :cid cid]])
         :fx [[:dispatch [::util/ajax-form-post
                          [(route-util/url-for db :do/delete-comment)
                           {:cid cid}
                           nil
                           [::errors/receive-ajax-error
                            ::delete-comment restore]]]]]}
        {:fx [[:dispatch [::toggle-mod-delete-comment-form cid]]]}))))

(re-frame/reg-event-fx ::mod-delete-comment
  ;; :doc Send a mod comment delete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [cid reason]} update-form-event]]
    (let [restore (fn [db]
                    (update db :reldb rel/transact
                            [:update :Comment
                             #(assoc % :status :ACTIVE)
                             [= :cid cid]]))
          state {:update-form-event update-form-event
                 :restore restore}]
      {:db (update db :reldb rel/transact
                   [:update :Comment
                    #(assoc % :status
                            (if (internal/is-mod-of-sub? db)
                              :DELETED-BY-MOD
                              :DELETED-BY-ADMIN))
                    [= :cid cid]])
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/delete-comment)
               {:cid cid
                :reason reason}
               [::receive-mod-delete-comment-response state]
               [::receive-mod-delete-comment-error-response state]]]]]})))

(re-frame/reg-event-fx ::receive-mod-delete-comment-response
  ;; :doc Handle a server response to a mod delete comment request.
  (fn-traced [_ [_ {:keys [update-form-event]} _]]
    {:fx [[:dispatch update-form-event]
          [:dispatch [::hide-mod-delete-comment-form]]]}))

(re-frame/reg-event-fx ::receive-mod-delete-comment-error-response
  ;; :doc Handle an error response to a mod delete comment request.
  (fn-traced [{:keys [db]}
              [_ {:keys [restore update-form-event]} error-response]]
    {:db (-> db
             restore
             (errors/assoc-errors
              {:event ::receive-mod-delete-comment-response
               :errors (errors/errors-from-ajax-error-response
                        error-response)}))
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-event-db ::toggle-mod-delete-comment-form
  ;; :doc Toggle the display of the moderator delete comment reason form.
  (fn-traced [db [_ cid]]
    (-> db
        (mod-action-form/initialize-mod-action-form :delete-comment cid)
        (update-in [:ui-state :comment-modals :show-mod-delete-comment-form?]
                   not))))

(re-frame/reg-event-db ::hide-mod-delete-comment-form
  ;; :doc Hide the moderator delete comment reason form.
  (fn-traced [db [_ _]]
    (update-in db [:ui-state :comment-modals]
               dissoc :show-mod-delete-comment-form?)))

(re-frame/reg-sub ::show-mod-delete-comment-form?
  ;; :doc Extract whether the moderator delete comment reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:comment-modals :show-mod-delete-comment-form?])))

(re-frame/reg-sub ::can-undelete?
  ;; :doc Determine whether the user can undelete the comment.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::user/is-admin?])
     (subscribe [::user/is-mod-or-admin?])])
  (fn [[{:keys [status]} is-admin? is-mod-or-admin?] _]
    (or (and (= :DELETED-BY-MOD status) is-mod-or-admin?)
        (and (= :DELETED-BY-ADMIN status) is-admin?))))

(re-frame/reg-event-db ::toggle-mod-undelete-comment-form
  ;; :doc Toggle the display of the moderator undelete comment reason form.
  (fn-traced [db [_ cid]]
    (-> db
        (mod-action-form/initialize-mod-action-form :undelete-comment cid)
        (update-in [:ui-state :comment-modals :show-mod-undelete-comment-form?]
                   not))))

(re-frame/reg-event-db ::hide-mod-undelete-comment-form
  ;; :doc Hide the moderator undelete comment reason form.
  (fn-traced [db [_ _]]
    (update-in db [:ui-state :comment-modals]
               dissoc :show-mod-undelete-comment-form?)))

(re-frame/reg-sub ::show-mod-undelete-comment-form?
  ;; :doc Extract whether the moderator undelete comment reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:comment-modals :show-mod-undelete-comment-form?])))

(re-frame/reg-event-fx ::mod-undelete-comment
  ;; :doc Send a mod comment undelete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [cid reason]} update-form-event]]
    (let [status (:status (rel/row (:reldb db) :Comment [= :cid cid]))
          set-status (fn [db val]
                       (assoc-comment db cid :status val))
          restore #(set-status % status)
          state {:update-form-event update-form-event
                 :restore restore}]
      {:db (set-status db :ACTIVE)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/undelete-comment)
               {:cid cid
                :reason reason}
               [::receive-mod-undelete-comment-response state]
               [::receive-mod-undelete-comment-error-response state]]]]]})))

(re-frame/reg-event-fx ::receive-mod-undelete-comment-response
  ;; :doc Handle a server response to a mod undelete comment request.
  (fn-traced [_ [_ {:keys [update-form-event]} _]]
    {:fx [[:dispatch update-form-event]
          [:dispatch [::hide-mod-undelete-comment-form]]]}))

(re-frame/reg-event-fx ::receive-mod-undelete-comment-error-response
  ;; :doc Handle an error response to a mod undelete comment request.
  (fn-traced [{:keys [db]}
              [_ {:keys [restore update-form-event]} error-response]]
    {:db (-> db
             restore
             (errors/assoc-errors
              {:event ::receive-mod-undelete-comment-response
               :errors (errors/errors-from-ajax-error-response
                        error-response)}))
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-sub ::can-distinguish?
  ;; :doc Determine whether the user can distinguish the comment.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::user/is-mod-or-admin?])])
  (fn [[is-author? is-mod-or-admin?] _]
    (and is-author? is-mod-or-admin?)))

(re-frame/reg-sub ::can-distinguish-author-oneshot?
  ;; :doc Determine whether the user can distinguish the comment author.
  ;; :doc Admins get an option of how to distinguish if they also mod
  ;; :doc the sub.
  ;; :doc If the post is already distinguished, no need to give the
  ;; :doc admin the option.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::user/is-mod-of-sub?])
     (subscribe [::user/is-admin?])
     (subscribe [::details cid])])
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? (= :ACTIVE status)
         (or is-admin? is-mod?)
         (or (not is-admin?) (not is-mod?) (some? distinguish)))))

(re-frame/reg-sub ::can-distinguish-author-choice?
  ;; :doc Determine whether the user has a choice of how to distinguish.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::user/is-mod-of-sub?])
     (subscribe [::user/is-admin?])
     (subscribe [::details cid])])
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? is-mod? is-admin? (nil? distinguish) (= :ACTIVE status))))

(re-frame/reg-event-fx ::edit-distinguish
  ;; :doc Send a comment author distinguish request to the server.
  ;; :doc If distinguish is set, it will be cleared, otherwise
  ;; :doc it will be set according to the user's mod or admin status.
  ;; :doc If the user is both mod and admin, supply
  ;; :doc `distinguish-as` with the user's choice.
  (fn-traced [{:keys [db]} [event cid distinguish-as]]
    (let [{:keys [sub]} (get-in db [:view :options])
          is-mod? (internal/is-mod-of-sub? db)
          distinguish (:distinguish (rel/row (:reldb db) :Comment [= :cid cid]))
          new-distinguish (cond
                            (some? distinguish) nil
                            (= :admin distinguish-as) :ADMIN
                            (= :mod distinguish-as) :MOD
                            is-mod? :MOD
                            :else :ADMIN)
          set-distinguish (fn [db val]
                            (assoc-comment db cid :distinguish val))
          restore #(set-distinguish % distinguish)]
      {:db (set-distinguish db new-distinguish)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :distinguish-comment
                         :id (str :distinguish-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)
                                     :distinguish new-distinguish}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-sub ::can-sticky?
  ;; :doc Determine whether the user can make the comment sticky.
  (fn [[_ cid]]
    [(subscribe [::is-author? cid])
     (subscribe [::user/is-mod-or-admin?])
     (subscribe [::level cid])])
  (fn [[is-author? is-mod-or-admin? level] _]
    (and is-author? is-mod-or-admin? (zero? level))))

(defn- update-sticky-flags
  "Set a new sticky comment and unset the old one.
  Either can be nil."
  [db existing-sticky-cid new-sticky-cid]
  (if (= new-sticky-cid existing-sticky-cid)
    (update-comment db new-sticky-cid
                    #(update % :sticky not))
    (cond-> db
      existing-sticky-cid (assoc-comment existing-sticky-cid :sticky false)
      new-sticky-cid      (assoc-comment new-sticky-cid :sticky true))))

(re-frame/reg-event-fx ::toggle-sticky
  ;; :doc Toggle the stickyness of a comment.
  ;; :doc If another comment is set sticky, unsticky it.
  ;; :doc Does not move the new sticky comment to the top of the list.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [pid (get-in db [:view :options :pid])
          existing-sticky-cid
          (-> (rel/row (:reldb db) :Comment [:and [= :pid pid] :sticky])
              :cid)

          restore #(update-sticky-flags % cid existing-sticky-cid)]
      {:db (update-sticky-flags db existing-sticky-cid cid)
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/set-sticky-comment
                                             :comment cid)
                         {}
                         nil
                         [::errors/receive-ajax-error ::toggle-sticky
                          restore]]]]]})))

(re-frame/reg-sub ::checkoff
  ;; :doc Return a comment's mod checkoff.
  (fn [[_ cid]]
    (subscribe [::details cid]))
  (fn [comment _]
    (:checkoff comment)))

(re-frame/reg-sub ::can-check-off?
  ;; :doc Determine whether to show the 'check off' link in the bottombar.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::post/single-post-is-archived?])
     (subscribe [::user/is-mod-of-sub?])])
  (fn [[comment is-archived? is-mod?] _]
    (can-check-off? comment is-archived? is-mod?)))

(defn set-checkoff
  "Set the checkoff field of a commment."
  [db cid checkoff]
  (-> db
      (update :reldb rel/transact
              [:update :Comment #(assoc % :checkoff checkoff)
               [= :cid cid]])
      (assoc-in [:ui-state :comment cid :checked-off?] true)))

(re-frame/reg-event-fx ::check-off
  ;; :doc Check off a comment.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          username (get-in db [:current-user :name])
          restore #(set-checkoff db cid nil)
          state {:pid pid :cid cid :restore restore
                 :mutation-key :checkoff-comment}]
      {:db (set-checkoff db cid {:user {:name username}
                                 :time (:now db)})
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :checkoff-comment
                         :id (str :checkoff-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)}
                         :handler [::receive-checkoff state]}]]]})))

(re-frame/reg-sub ::can-uncheck?
  ;; :doc Determine whether to show the 'uncheck' link in the bottombar.
  (fn [[_ cid]]
    [(subscribe [::details cid])
     (subscribe [::post/single-post-is-archived?])
     (subscribe [::user/is-mod-of-sub?])
     (subscribe [::is-author? cid])
     (subscribe [::user/current-user])])
  (fn [[{:keys [status checkoff]} is-archived? is-mod? is-author?
        {:keys [name]}] _]
    (and is-mod?
         (= :ACTIVE status)
         (not is-archived?)
         (not is-author?)
         (= name (get-in checkoff [:user :name])))))

(re-frame/reg-event-fx ::uncheck
  ;; :doc Un-check a comment.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          checkoff (:checkoff (rel/row (:reldb db) :Comment [= :cid cid]))
          restore #(set-checkoff db cid checkoff)
          state {:pid pid :cid cid :restore restore
                 :mutation-key :un-checkoff-comment}]
      {:db (set-checkoff db cid nil)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :un-checkoff-comment
                         :id (str :un-checkoff-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)}
                         :handler [::receive-checkoff state]}]]]})))

(re-frame/reg-event-db ::receive-checkoff
  ;; :doc Receive the response from the checkoff or uncheckoff mutation.
  (fn-traced [db [event {:keys [cid restore mutation-key response]}]]
    (let [{:keys [data errors]} response
          result (get data mutation-key)
          checkoff (when result
                     (update result :time util/parse-int))]
      (if errors
        (-> db
            (errors/assoc-errors {:event event :errors errors})
            restore)
        (set-checkoff db cid checkoff)))))

;;; Display of comment content.

;; Possibilities are as HTML, as markdown source, and (for the author)
;; an edit form.

(defn- init-form-fx [state _event]
  [:dispatch [::init-comment-compose-form :edit (:cid state)]])

(def comment-content-view-machine
  "A state machine to hold the possible views of comment content."
  {:id :comment-edits
   :initial :html
   :context nil
   :states
   {:html {:on {:toggle-source [{:target :source}]
                :toggle-editor [{:target :editor}]}}
    :source {:on {:toggle-editor [{:target :editor}]
                  :toggle-source [{:target :html}]}}
    :editor {:on {:toggle-source [{:target :source}]
                  :toggle-editor [{:target :html}]
                  :hide-editor [{:target :html}]}
             :entry [(util/queue-fx init-form-fx)]}}})
(assert (= comment-content-view-machine
           (fsm/machine comment-content-view-machine)))

(defn- transition-content-view-fx [db cid event-type]
  (let [state (or (get-in db [:ui-state :comment-content-view cid])
                  (fsm/initialize comment-content-view-machine
                                  {:context {:cid cid}}))
        new-state (fsm/transition comment-content-view-machine state
                                  {:type event-type})]
    {:db (assoc-in db [:ui-state :comment-content-view cid]
                   (dissoc new-state :fx))
     :fx (vec (:fx new-state))}))

(re-frame/reg-sub ::content-views
  ;; :doc Extract the comment view state objects for all the comments.
  :<- [::ui/state]
  (fn [state _]
    (:comment-content-view state)))

(re-frame/reg-sub ::content-view
  ;; :doc Extract the comment view state object for a comment.
  :<- [::content-views]
  (fn [views [_ cid]]
    (get views cid)))

(re-frame/reg-event-fx ::toggle-source
  ;; :doc Toggle whether comment markdown source is shown.
  (fn-traced [{:keys [db]} [_ cid]]
    (transition-content-view-fx db cid :toggle-source)))

(re-frame/reg-sub ::show-source?
  ;; :doc Get whether the content is being shown as markdown source.
  (fn [[_ cid]]
    (subscribe [::content-view cid]))
  (fn [content-view _]
    (= :source (:_state content-view))))

(defn- initialize-compose-form
  [db form-type cid]
  (cond
    (= :edit form-type)
    (comment-form/initialize-comment-edit-form db cid)

    (= :reply form-type)
    (comment-form/initialize-comment-reply-form db cid)))

(re-frame/reg-event-db ::init-comment-compose-form
  ;; :doc Initialize the form for a comment reply or edit.
  (fn-traced [db [_ form-type cid]]
    (initialize-compose-form db form-type cid)))

(re-frame/reg-event-fx ::toggle-editor
  ;; :doc Toggle the visibility of a comment's content editor.
  (fn-traced [{:keys [db]} [_ cid]]
    (transition-content-view-fx db cid :toggle-editor)))

(re-frame/reg-event-fx ::hide-editor
  ;; :doc Hide the comment content editor.
  (fn-traced [{:keys [db]} [_ cid]]
    (transition-content-view-fx db cid :hide-editor)))

(re-frame/reg-sub ::show-editor?
  ;; :doc Get whether to show the comment content editor.
  (fn [[_ cid]]
    (subscribe [::content-view cid]))
  (fn [content-view _]
    (= :editor (:_state content-view))))

(re-frame/reg-event-db ::toggle-reply-editor
  ;; :doc Toggle the visibility of the reply editor for a comment.
  ;; :doc Initialize the related form if that has not yet been done.
  (fn-traced [db [_ cid]]
    (-> db
        (update-in [:ui-state :comment cid :show-reply-editor?] not)
        (initialize-compose-form :reply cid))))

(re-frame/reg-sub ::show-reply-editor?
  ;; :doc Get whether to show the comment reply editor.
  ;; :doc If `cid` is nil, then determine whether to show
  ;; :doc the top-level post reply box.
  (fn [[_ cid]]
    [(subscribe [::ui-state cid])
     (subscribe [::post/post-can-reply?])])
  (fn [[state post-can-reply?] [_ cid]]
    (if cid
      (:show-reply-editor? state)
      post-can-reply?)))

;;; Comment bottombar actions

(re-frame/reg-event-fx ::send-reply
  ;; :doc Send a reply to a comment.
  (fn-traced [{:keys [db]} [_ compose update-form]]
    (let [pid (get-in db [:view :options :pid])
          {:keys [parent-cid content]} compose]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :create-comment
                         :id :create-comment
                         :variables {:pid pid
                                     :parent_cid parent-cid
                                     :content content}
                         :handler [::comment-reply-response
                                   {:pid pid
                                    :parent-cid parent-cid
                                    :update-form update-form}]}]]]})))

(defn describe-reply-error
  "Describe errors specific to comment replies."
  [db {:keys [message status] :as error}]
  (cond
    (= message "Post deleted")
    {:msg (trx db "This post has been deleted.")
     :status status}

    (= message "Post locked")
    {:msg (trx db "The moderators have locked this post.")
     :status status}

    :else
    (or (errors/describe-error (trx db "Error: ") error)
        {:msg (trx db "Something went wrong")
         :status status})))

(defn- insert-new-tree-node
  "Add a comment to the tree as the first child of the parent."
  [tree parent-cid new-node]
  (if (nil? parent-cid)
    (cons new-node tree)
    (s/transform [COMMENTS #(= (:cid %) parent-cid) :children]
                 #(cons new-node %) tree)))

(defn- add-checkoff
  "If the user is a mod, add a checkoff to their new comment."
  [comment db]
  (if (internal/is-mod-of-sub? db)
    (assoc comment :checkoff {:user {:name (get-in db [:current-user :name])}
                              :time (:time comment)})
    comment))

(re-frame/reg-event-fx ::comment-reply-response
  ;; :doc Process the server's response to a comment reply.
  (fn-traced [{:keys [db]} [_ {:keys [pid parent-cid update-form response]}]]
    (let [{:keys [data errors]} response
          comment (some-> (:create-comment data)
                          (assoc-in [:user-attributes :viewed] true)
                          (add-checkoff db))
          uid (get-in db [:current-user :uid])
          {:keys [cid time]} comment
          tree-path (comment-tree-path db pid)
          levels-path (comment-levels-path pid)
          loaded-path (comment-loaded-path db pid)
          level (if (nil? parent-cid)
                  0
                  (inc (comment-level db pid parent-cid)))
          ;; Ensure that if we didn't get a comment we report some kind
          ;; of error.  This will get turned into "Something went wrong."
          errors (if (and (empty? errors) (nil? comment))
                   [{:message "Failed" :status 400}]
                   errors)
          db' (errors/assoc-errors db {:event ::comment-reply-response
                                       :errors errors
                                       :describe-error-fn
                                       (partial describe-reply-error db)})]
      {:db (if (seq errors)
             db'
             (-> db
                 (update-in tree-path insert-new-tree-node parent-cid
                            {:cid cid :children [] :level level
                             :status :ACTIVE :uid uid :time time})
                 (update-in levels-path assoc cid level)
                 (update-in loaded-path conj cid)
                 (update :reldb rel/transact
                         [:insert :Comment (prep-comment comment pid)])
                 (assoc-in [:ui-state :comment cid :highlight] :new)
                 (cond->
                     parent-cid (assoc-in [:ui-state :comment parent-cid
                                           :show-reply-editor?] false))))
       :fx [[:dispatch update-form]]})))

(re-frame/reg-event-fx ::send-comment-edit
  ;; :doc Update a comment's content on the server
  (fn-traced [{:keys [db]} [_ compose update-form-event]]
    (let [{:keys [cid content]} compose
          existing-content (-> (rel/row (:reldb db) :Comment [= :cid cid])
                               :content)
          unchanged? (= (str/trim content) (str/trim existing-content))]
      {:fx (if unchanged?
             [[:dispatch [::hide-editor cid]]
              [:dispatch update-form-event]]
             [[:dispatch
               [::graphql/mutate
                {:graphql graphql/subs-graphql
                 :name :update-comment-content
                 :variables {:cid cid
                             :content content}
                 :handler [::receive-comment-edit-response
                           {:compose-form compose
                            :update-form-event update-form-event}]}]]])})))

(defn update-edited-comment
  "Update the content and content history of a comment."
  ;; author is nil, need to check current user name
  [reldb cid author-name new-content now]
  (-> reldb
      (rel/transact
       [:update :Comment
        ;; TODO checkoffs should be their own tables.
        ;; And we don't use the time anyway
        (fn [{:keys [content checkoff] :as comment}]
          (let [updated-checkoff (if (and checkoff
                                          (= author-name
                                             (get-in checkoff [:user :name])))
                                   (assoc checkoff :time now)
                                   checkoff)]
            (-> comment
                (update :content-history concat [{:content content}])
                (assoc :content new-content
                       :edited now
                       :checkoff updated-checkoff))))
        [= :cid cid]])))

(re-frame/reg-event-fx ::receive-comment-edit-response
  ;; :doc Receive the response to a comment edit from the server.
  ;; :doc On success, update the comment's content and edit history
  ;; :doc and hide the form. On failure, record the errors and leave
  ;; :doc the form visible.
  [(re-frame/inject-cofx ::window/now)]
  (fn-traced [{:keys [db now]}
              [event {:keys [compose-form update-form-event response]}]]
    (let [{:keys [data errors]} response
          {:keys [cid content]} compose-form
          changed? (:update-comment-content data)
          author-name (get-in db [:current-user :name])]
      {:db (if (or errors (not changed?))
             (errors/assoc-errors db {:errors errors :event event})
             (update db :reldb
                     update-edited-comment cid author-name content now))
       :fx [(when (nil? errors)
              [:dispatch [::hide-editor cid]])
            [:dispatch update-form-event]]})))

(defn- describe-report-comment-error
  "Pass through an already translated error message from Python."
  [{:keys [message]}]
  {:msg message})

(util/reg-ajax-form-event-chain ::report-comment
  {:call
   (fn-traced [{:keys [db]} [_ {:keys [cid reason rule explanation]}
                             update-form-event]]
     (let [sid (->> (get-in db [:view :options :sub])
                    (sid-from-name db))
           rule-text (->> (sub-rules db sid)
                          (filter #(= rule (:id %)))
                          first
                          :text)
           reason-text (case reason
                         :sub-rule (str (trx db "Circle Rule: ")
                                        (if (= :other rule)
                                          explanation
                                          rule-text))
                         :site-rule (trx db "Sitewide Rule violation")
                         :other explanation)]
       {:fx [[:dispatch
              [::util/ajax-csrf-form-post
               {:url (route-util/url-for db :do/report-comment)
                :payload {:post cid
                          :send_to_admin (not= reason :sub-rule)
                          :reason reason-text}
                :update-form-event update-form-event}]]]}))

   :success
   (fn-traced [{:keys [db]} [_ {:keys [update-form-event response]}]]
     (let [error (:error response)]
       {:db (-> db
                (errors/assoc-errors
                 {:event ::report-comment
                  :errors (errors/errors-from-do-api error)
                  :describe-error-fn describe-report-comment-error}))
        :fx [[:dispatch update-form-event]]}))

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [update-form-event error-response]}]]
     {:db (-> db
              (errors/assoc-errors
               {:event ::report-comment
                :errors (errors/errors-from-ajax-error-response
                         error-response)}))
      :fx [[:dispatch update-form-event]]})})

(re-frame/reg-event-fx ::check-scroll
  ;; :doc Scroll to `elem` if `cid` matches the one in the db.
  [(path [:view :options])]
  (fn-traced [{:keys [db]} [_ cid elem]]
    (when (= cid (:scroll-to-cid db))
      {:db (assoc db :scroll-to-cid nil)
       :fx [[::window/scroll-to-elem elem]]})))

(defn- update-leaf
  [leaf {:keys [status checkoff]}]
  (assoc leaf
         :checked (some? checkoff)
         :status status))

(re-frame/reg-event-fx ::receive-comment-update
  ;; :doc Handle a response from the comment-update subscription.
  ;; :doc Update statuses in the tree, but defer updating
  ;; :doc the content until we're sure that at least the bottom of the
  ;; :doc comment is visible, to avoid content-shifting.
  (fn-traced [{:keys [db]} [event {:keys [pid response]}]]
    (let [{:keys [data errors]} response
          {:keys [cid] :as update} (-> data
                                       util/kebab-case-keys
                                       :comment-update)
          sort-path (-> [:content :comments pid]
                        (concat (get-in db [:ui-state :post :sort-path])))
          tree-path (concat sort-path [:tree COMMENTS #(= (:cid %) cid)])
          comment (prep-comment update pid)
          existing (rel/row (:reldb db) :Comment [= :cid cid])
          update-path [:content :comments pid :updates]]
      {:db (if errors
             (errors/assoc-errors db {:event event :errors errors})
             (cond->> db
               true (s/transform tree-path #(update-leaf % comment))
               existing (s/transform update-path
                                     #(conj % {:cid cid :comment comment}))))
       :fx [[:dispatch [::process-updates]]]})))
