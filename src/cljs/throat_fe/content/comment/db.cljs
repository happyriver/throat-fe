;; content/comment/db.cljs -- Comment reldb access for throat-fe
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.comment.db
  (:require
   [com.wotbrew.relic :as rel]))

(defn assoc-comment
  "Assoc a field in a comment object in the db."
  [db cid kw val]
  (update db :reldb rel/transact
          [:update :Comment #(assoc % kw val) [= :cid cid]]))

(defn update-comment
  "Update a comment object in the db."
  [db cid func]
  (update db :reldb
          rel/transact [:update :Comment func [= :cid cid]]))
