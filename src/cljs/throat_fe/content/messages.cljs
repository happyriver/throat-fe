;; messages.cljs -- Private message content for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.messages
  (:require
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [graphql-builder.parser :refer-macros [defgraphql]]
   [re-frame.core :as re-frame]
   [throat-fe.content.internal :as internal]
   [throat-fe.errors :refer [assoc-errors] :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.routes :as-alias routes]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :refer [tr] :as tr]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.util :as util]))

;; Import graphql queries.
(defgraphql graphql
  "src/cljs/throat_fe/content/messages.graphql")

;; Helper functions
(defn precalc-message-attributes
  "Precalculate message attributes needed by the view functions."
  [{:keys [mtype sender receiver unread] :as message} sub]
  (let [sid (:sid sub)
        get-sids-modded (fn [submods]
                          (map #(get-in % [:sub :sid]) submods))
        sender-subs (get-sids-modded (:subs-moderated sender))
        receiver-subs (get-sids-modded (:subs-moderated receiver))]
    (-> message
        (assoc :sender-is-mod? (some? ((set sender-subs) sid))
               :receiver-is-mod? (some? ((set receiver-subs) sid))
               :sent-with-name-hidden? (some? (#{:MOD_TO_USER_AS_MOD
                                                 :USER_NOTIFICATION}
                                               mtype))
               :unread? unread)
        (update :receiver dissoc :subs_moderated)
        (update :sender dissoc :subs_moderated)
        (dissoc :unread))))

(defn prepare-message
  "Clean up a message received from a query."
  [message sub]
  (-> message
      (update :mtype keyword)
      (precalc-message-attributes sub)))

(defn prepare-thread
  "Clean up a message thread received from a query."
  [{:keys [sub] :as thread}]
  (-> thread
      (update :mailbox keyword)
      (update :first-message #(prepare-message % sub))
      (update :latest-message #(prepare-message % sub))
      (update :report #(when % (update % :rtype keyword)))))

(defn linkable-to-report?
  "Determine if a composed message is linked to a report."
  [form {:keys [user sub report-type]}]
  (and (some? report-type)
       (= (:username form) user)
       (= (:sub form) sub)
       (:send-to-user? form)))

(re-frame/reg-event-fx ::send-message
  ;; :doc Send a modmail message.  Or maybe any message.
  (fn-traced [{:keys [db]} [_ compose update-form]]
    (let [{:keys [sub mid thread-id send-to-user? show-username? username
                  subject content report-type report-id]} compose
          msg-type (:type compose)]
      (case msg-type
        :modmail
        (let [options (get-in db [:view :options])
              linked-report? (linkable-to-report? compose options)
              args (merge {:subject subject
                           :content content
                           :sid (internal/sid-from-moderated-sub db sub)}
                          (when send-to-user?
                            {:username username
                             :show_mod_username show-username?})
                          (when linked-report?
                            {:report_type (str/upper-case report-type)
                             :report_id report-id}))]
          {:fx [[:dispatch [::graphql/mutate
                            {:graphql graphql
                             :name :create-modmail-message
                             :variables args
                             :handler [::compose-response
                                       {:update-form update-form}]}]]]})

        :modmail-reply
        (let [args (merge {:thread_id thread-id
                           :content content
                           :send_to_user send-to-user?}
                          (when send-to-user?
                            {:show_mod_username show-username?}))]
          {:fx [[:dispatch [::graphql/mutate
                            {:graphql graphql
                             :name :create-modmail-reply
                             :variables args
                             :handler [::add-reply-to-thread
                                       {:mid mid
                                        :update-form update-form}]}]]]})

        :contact-mods
        {:fx [[:dispatch [::graphql/mutate
                          {:graphql graphql
                           :name :create-message-to-mods
                           :variables {:subject subject
                                       :content content
                                       :sid (internal/sid-from-name db sub)}
                           :handler [::compose-response
                                     {:update-form update-form}]}]]]}))))

(defn describe-error
  "Describe errors specific to message sending."
  [{:keys [message] :as error}]
  (let [description (tr "Could not send message:")]
    (cond
      (= message "Recipient not found")
      {:msg (str description " " (tr "recipient does not exist"))
       :status 400}

      (= message "Sender and recipient are the same")
      {:msg (str description " " (tr "you can't send a message to yourself"))
       :status 400}

      :else
      (errors/describe-error description error))))

(re-frame/reg-event-fx ::compose-response
  ;; :doc Process the server's response to sending a new message.
  (fn-traced [{:keys [db]} [_ {:keys [update-form response]}]]
    {:db (errors/assoc-errors db {:event ::compose-response
                                  :errors (:errors response)
                                  :describe-error-fn #(describe-error %)})
     :fx [[:dispatch update-form]]}))

(re-frame/reg-event-fx ::add-reply-to-thread
  ;; :doc Handle the server response from a mod mail reply.
  ;; :doc If the reply was successful, add the message to the thread.
  ;; :doc The reply count will be out of date but it's not shown in
  ;; :doc thread view.  Pass the response along to the form's response
  ;; :doc handler for the display of error messages, if any, and updates
  ;; :doc to the form state.
  (fn-traced [{:keys [db]} [_ {:keys [mid update-form response]}]]
    (let [{:keys [data errors]} response
          sub (get-in db [:content :modmail :thread mid])
          message (when-let [reply (:create-modmail-reply data)]
                    (prepare-message reply sub))
          db' (errors/assoc-errors db {:event ::compose-response
                                       :errors errors
                                       :describe-error-fn #(describe-error %)})]
      {:db (if (or errors (nil? message))
             db'
             (s/setval [:content :modmail :thread mid :items s/END]
                       [message] db'))
       :fx [[:dispatch update-form]]})))

(defn sid-list
  "Return a list of sids that should be fetched, from the modmail sub option,
  which should be either :all-subs or a sub name."
  [db sub]
  (let [sid (when (not= sub :all-subs) (internal/sid-from-name db sub))
        user-subs (set (internal/subs-moderated-names db))
        is-admin? (get-in db [:current-user :attributes :is-admin])]
    (cond
      (= sub :all-subs) (internal/subs-moderated-sids db)
      (and sid (or is-admin? (user-subs sub))) [sid]
      :else nil)))

(re-frame/reg-event-fx ::load-modmails
  ;; :doc Fetch modmails from the server, from the thread identified by id.
  (fn-traced [{:keys [db]} [_ {:keys [id sub cursor] :as options} on-load]]
    (let [page-size (get-in db [:settings :page-size])
          sids (sid-list db sub)
          args (merge {:first page-size
                       :thread_id id}
                      (when cursor
                        {:after cursor}))]
      {:db (assoc-in db [:status :content] :loading)
       :fx [(if (seq sids)
              [:dispatch [::graphql/query
                          {:graphql graphql
                           :name :get-modmail-thread-by-id
                           :id (str :get-modmail-thread-by-id id)
                           :variables args
                           :handler [::receive-threaded-modmails
                                     {:options options
                                      :on-load on-load}]}]]
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::receive-threaded-modmails
  ;; :doc Receive a modmail thread with messages from the server.
  (fn-traced [{:keys [db]} [event {:keys [options on-load response]}]]
    (let [{:keys [cursor]} options
          {:keys [data errors]} response
          page-info (get-in data [:modmail-thread :page-info])
          sub (get-in data [:message-thread-by-id :sub])
          received-messages (map #(-> (:node %)
                                      (prepare-message sub))
                                 (get-in data [:modmail-thread :edges]))
          thread (prepare-thread (:message-thread-by-id data))
          {:keys [id first-message mailbox report subject sub]} thread
          path [:content :modmail :thread (:mid first-message)]
          existing-messages (if (= cursor
                                   (get-in db (conj path :cursor)))
                              (get-in db (conj path :items))
                              [])
          replies (if (= (:mid first-message) (:mid (last received-messages)))
                    (butlast received-messages)
                    received-messages)
          messages (concat (reverse replies) existing-messages)]
      {:db (-> db
               (assoc-errors {:event event :errors errors})
               (assoc-in [:status :content] :loaded)
               (assoc-in [:view :options :mid] (:mid first-message))
               (assoc-in [:view :options :thread-id] (:id thread))
               (assoc-in path {:items messages
                               :thread-id id
                               :report report
                               :subject subject
                               :sub sub
                               :mailbox mailbox
                               :more-items-available? (:has-next-page page-info)
                               :cursor (:end-cursor page-info)
                               :parent first-message}))
       :fx [(when on-load
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::load-modmail-category
  ;; :doc Fetch a modmail thread category from the server.
  (fn-traced [{:keys [db]} [_ {:keys [sub mailbox cursor] :as options}
                            on-load]]
    (let [page-size (get-in db [:settings :page-size])
          sids (sid-list db sub)
          base-args {:first page-size
                     :sids sids
                     :category (case mailbox
                                 :all :ALL
                                 :new :NEW
                                 :in-progress :IN_PROGRESS
                                 :archived :ARCHIVED
                                 :discussions :MOD_DISCUSSIONS)
                     :unread_only false}
          args (if cursor
                 (assoc base-args :after cursor)
                 base-args)]
      {:db (assoc-in db [:status :content] :loading)
       :fx [(if (seq sids)
              [:dispatch [::graphql/query
                          {:graphql graphql
                           :name :get-modmail-category
                           :variables args
                           :handler [::receive-modmail-category
                                     {:options options
                                      :on-load on-load}]}]]
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::receive-modmail-category
  ;; :doc Receive messages from a modmail mailbox from the server.
  (fn-traced [{:keys [db]} [event {:keys [options on-load response]}]]
    (let [{:keys [mailbox sub cursor]} options
          {:keys [data errors]} response
          page-info (get-in data [:modmail-threads :page-info])
          received-threads (map #(prepare-thread (:node %))
                                (get-in data [:modmail-threads :edges]))
          path [:content :modmail :subs sub mailbox]
          existing-threads (if (= cursor (get-in db (conj path :cursor)))
                             (get-in db (conj path :items))
                             [])
          threads (concat existing-threads received-threads)]
      {:db (-> db
               (assoc-errors {:event event :errors errors})
               (assoc-in [:status :content] :loaded)
               (assoc-in path {:items threads
                               :more-items-available? (:has-next-page page-info)
                               :cursor (:end-cursor page-info)}))
       :fx [(when on-load
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::set-modmail-sub-selection
  ;; :doc Select a sub for the modmail mailboxes view.
  (fn-traced [{:keys [db]} [_ value]]
    (let [mailbox (get-in db [:view :options :mailbox])
          route [:modmail/mailbox :mailbox mailbox]
          route-args (if (= value :all-subs)
                       route
                       (into route [:query-args {:sub value}]))
          url (apply (partial route-util/url-for db) route-args)]
      {:fx [[::routes/set-url url]]})))

(re-frame/reg-sub ::modmail-sub-selection
  ;; :doc Extract the sub for the modmail mailboxes view.
  (fn [db _]
    (or (get-in db [:view :options :sub]) :all-subs)))

(re-frame/reg-sub ::modmail-all-mailboxes
  ;; :doc Extract modmail mailboxes for all subs.
  (fn [db _]
    (get-in db [:content :modmail :subs])))

(re-frame/reg-sub ::modmail-mailboxes
  ;; :doc Extract lists of threads by category for current sub selection.
  :<- [::modmail-all-mailboxes]
  :<- [::modmail-sub-selection]
  (fn [[category sub] _]
    (get category sub)))

(re-frame/reg-sub ::supervising-admin?
  ;; :doc Determine whether an admin is supervising.
  ;; :doc (Looking at messages for a sub they don't mod.)
  :<- [::modmail-sub-selection]
  :<- [::user/subs-moderated-names]
  :<- [::user/is-admin?]
  (fn [[sub sub-names is-admin?] _]
    (and
     is-admin?
     (not (= sub :all-subs))
     (not ((set sub-names) sub)))))

(re-frame/reg-sub ::more-modmail-messages-available?
  ;; :doc Extract whether a mailbox has more messages that can be fetched.
  :<- [::modmail-mailboxes]
  :<- [:view/options]
  (fn [[mailboxes {:keys [mailbox]}] _]
    (get-in mailboxes [mailbox :more-items-available?])))

(re-frame/reg-sub ::modmail-mailbox-cursor
  ;; :doc Extract the cursor for fetching more messages for a mailbox.
  :<- [::modmail-mailboxes]
  :<- [:view/options]
  (fn [[mailboxes {:keys [mailbox]}] _]
    (get-in mailboxes [mailbox :cursor])))

(defn get-sid-class
  "Return a CSS class name for consistent colorization of subs."
  [sid]
  ;; Use the first character of the sid to pick a color.
  (get ["fill-dark-red" "fill-red" "fill-light-red" "fill-orange"
        "fill-gold" "fill-yellow" "fill-light-green" "fill-green"
        "fill-dark-green" "fill-light-teal" "fill-light-blue"
        "fill-dark-blue" "fill-blue" "fill-light-purple" "fill-purple"
        "fill-dark-pink"]
       ;; If sids are ever not hex, the max 0 protects against
       ;; ##NaN.
       (max 0 (js/parseInt (first sid) 16))))

(defn- add-ui-info
  "Add timeagos and html to a message."
  [{:keys [time mailbox content] :as message} now lang]
  (-> message
      (assoc :timeago (util/time-ago (js/parseInt time) now lang)
             :archived? (= mailbox :ARCHIVED)
             :html-content (util/markdown-to-html content))))

(defn- add-thread-ui-info
  "Precalculate values useful for displaying a modmail thread."
  [{:keys [sub mailbox latest-message] :as thread} now lang]
  (let [timeago (-> (:time latest-message)
                    js/parseInt
                    (util/time-ago now lang))
        markdown (util/markdown-to-html (:content latest-message))]
    (-> thread
        (assoc-in [:latest-message :timeago] timeago)
        (assoc-in [:latest-message :html-content] markdown)
        (assoc :sub-color-class (get-sid-class (:sid sub))
               :archived? (= mailbox :ARCHIVED)))))

(re-frame/reg-sub ::modmail-mailbox-threads
  ;; :doc Provide list of threads in a mailbox with timeagos and sub colors.
  :<- [::modmail-mailboxes]
  :<- [:view/options]
  :<- [::window/now]
  :<- [::tr/lang]
  (fn [[categories {:keys [mailbox]} now lang] _]
    (let [threads (get-in categories [mailbox :items])]
      (map #(add-thread-ui-info % now lang) threads))))

(re-frame/reg-sub ::modmail-threads
  ;; :doc Extract all messages in all threads.
  (fn [db _]
    (get-in db [:content :modmail :thread])))

(re-frame/reg-sub ::modmail-thread-info
  ;; :doc Extract info on the thread currently being viewed.
  :<- [::modmail-threads]
  :<- [:view/options]
  (fn [[threads {:keys [mid]}] _]
    (get threads mid)))

(re-frame/reg-sub ::modmail-thread-sub-color-class
  ;; :doc Calculate the color of the sub icon for the current thread.
  :<- [::modmail-thread-info]
  (fn [thread _]
    (get-sid-class (get-in thread [:sub :sid]))))

(re-frame/reg-sub ::modmail-thread-raw-messages
  ;; :doc Extract the messages in the current thread.
  :<- [::modmail-thread-info]
  (fn [thread _]
    (:items thread)))

(re-frame/reg-sub ::modmail-thread-raw-parent
  ;; :doc Extract the original message of the current thread.
  :<- [::modmail-thread-info]
  (fn [thread _]
    (:parent thread)))

(re-frame/reg-sub ::modmail-thread
  ;; :doc Provide list of messages in a thread with timeagos and sub colors.
  :<- [::modmail-thread-raw-messages]
  :<- [::window/now]
  :<- [::tr/lang]
  (fn [[messages now lang] _]
    (map #(add-ui-info % now lang) messages)))

(re-frame/reg-sub ::modmail-thread-parent
  ;; :doc Provide the parent message of the thread with timeagos and sub colors.
  :<- [::modmail-thread-raw-parent]
  :<- [::window/now]
  :<- [::tr/lang]
  (fn [[parent now lang] _]
    (add-ui-info parent now lang)))

(re-frame/reg-sub ::modmail-thread-any-unread
  ;; :doc Determine whether any messages in the thread are unread.
  :<- [::modmail-thread-raw-parent]
  :<- [::modmail-thread-raw-messages]
  (fn [[parent messages] _]
    (or (:unread? parent)
        (some :unread? messages))))

(defn update-message
  "Update a message in mailboxes and threads."
  [db mid func]
  (let [mid-match? #(= (:mid %) mid)]
    (->> db
         (s/transform [:content :modmail :subs
                       s/MAP-VALS s/MAP-VALS :items s/ALL
                       :latest-message mid-match?]
                      func)
         (s/transform [:content :modmail :thread
                       s/MAP-VALS :items s/ALL mid-match?] func)
         (s/transform [:content :modmail :thread
                       s/MAP-VALS :parent mid-match?]
                      func))))

(re-frame/reg-event-fx ::set-unread
  ;; :doc Set the unread flag on a message by message id.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ mid value]]
    {:db (update-message db mid #(assoc % :unread? value))
     :fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :update-message-unread
                       :variables {:mid mid :unread value}
                       :handler [::handle-message-unread-update
                                 {:event ::set-unread}]}]]]}))

(re-frame/reg-event-db ::handle-message-unread-update
  ;; :doc Handle the response from updating unread or mailbox on the server.
  (fn-traced [db [_ {:keys [event response]}]]
    (assoc-errors db {:event event :errors (:errors response)})))

(defn update-thread-messages
  "Update all messages in a thread in mailboxes and the thread."
  [db thread-id func]
  (->> db
       (s/transform [:content :modmail :subs
                     s/MAP-VALS s/MAP-VALS :items s/ALL #(= (:id %) thread-id)]
                    func)
       (s/transform [:content :modmail :thread
                     s/MAP-VALS #(= (:thread-id %) thread-id) :items s/ALL]
                    func)
       (s/transform [:content :modmail :thread
                     s/MAP-VALS #(= (:thread-id %) thread-id) :parent]
                    func)))

(re-frame/reg-event-fx ::set-thread-unread
  ;; :doc Set the unread flag on a message thread by thread id.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ thread-id value]]
    (let [set-unread #(assoc % :unread? value)]
      {:db (update-thread-messages db thread-id set-unread)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :update-thread-unread
                         :variables {:thread_id thread-id :unread value}
                         :handler [::handle-message-update
                                   {:event ::set-unread}]}]]]})))

(defn update-thread
  "Update a modmail thread by its id."
  [db thread-id func]
  (->> db
       (s/transform [:content :modmail :subs
                     s/MAP-VALS s/MAP-VALS :items s/ALL #(= (:id %) thread-id)]
                    func)
       (s/transform [:content :modmail :thread
                     s/MAP-VALS #(= (:thread-id %) thread-id)] func)))

(re-frame/reg-event-fx ::set-archived
  ;; :doc Archive or unarchive a thread.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ thread-id archived?]]
    (let [value (if archived? :ARCHIVED :INBOX)
          set-mailbox #(assoc % :mailbox value)]
      {:db (update-thread db thread-id set-mailbox)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :update-modmail-mailbox
                         :variables {:thread_id thread-id
                                     :mailbox value}
                         :handler [::handle-message-update
                                   {:event ::set-archived}]}]]]})))

(re-frame/reg-event-db ::handle-message-update
  ;; :doc Handle the response from updating unread or mailbox on the server.
  (fn-traced [db [_ {:keys [event response]}]]
    (assoc-errors db {:event event :errors (:errors response)})))
