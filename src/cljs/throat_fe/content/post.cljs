;; content/post.cljs -- Single post content for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.post
  (:require
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [throat-fe.content.comment :as comment]
   [throat-fe.content.internal :refer [sub-info-by-name] :as internal]
   [throat-fe.content.post.db :refer [single-post assoc-post update-post
                                      assoc-post-user-attributes
                                      single-post-user-attributes]]
   [throat-fe.content.settings :as-alias settings]
   [throat-fe.content.subs :as subs]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.log :as log]
   [throat-fe.routes :as-alias routes]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :refer [trx] :as tr]
   [throat-fe.ui :as ui]
   [throat-fe.ui.forms.mod-action :as mod-action-form]
   [throat-fe.ui.forms.post :as post-forms]
   [throat-fe.ui.forms.post-edit :as post-edit-form]
   [throat-fe.ui.forms.post-edit-title :as post-edit-title-form]
   [throat-fe.ui.forms.report :as report-form]
   [throat-fe.ui.page-title :as-alias page-title]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as-alias user]
   [throat-fe.util :as util])
  (:import [goog Uri]))

(re-frame/reg-event-fx ::load-single-post
  ;; :doc Load a single post with author and up to date sub info.
  (fn-traced [{:keys [db]} [_ {:keys [cid pid]}]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin]))
                            boolean)
          {:keys [num level]} (get-in db [:settings :comment-tree])]
      (log/debug {:pid pid} "Dispatching single post queries")
      {:db (update db :ui-state dissoc :post :comment :comments
                   :comment-content-view :comment-modals)
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql/subs-graphql
                         :name :get-single-post
                         :id (str :get-single-post "-" pid)
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid}
                         :handler [::receive-single-post]}]]
            [:dispatch [::graphql/query
                        {:graphql graphql/subs-graphql
                         :name :get-comment-tree
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid :cid cid
                                     :first num :level level
                                     :sort_by val}
                         :handler [::receive-comment-tree {:cid cid :pid pid}]}]]]})))

(defn- post-sort
  "Determine the comment sort on the post from the post and view options."
  [db {:keys [best-sort-enabled default-sort]}]
  (let [{:keys [sort-choice]} (get-in db [:view :options])]
    ;; If we have a sort from routing and it is valid, use that.
    (if (and (#{"top" "best" "new"} sort-choice)
             (or best-sort-enabled
                 (not= "best" sort-choice)))
      (-> sort-choice
          str/upper-case
          keyword)
      ;; Otherwise use default for the post.
      default-sort)))

(defn- fixup-user-attributes
  [{:keys [pid user-attributes]}]
  (when user-attributes
    (-> user-attributes
        (update :viewed util/parse-int)
        (update :vote keyword)
        (assoc :pid pid))))

(defn- prepare-post
  "Make post fields more clojure-friendly."
  [{:keys [sub] :as post}]
  (-> post
      (update :status util/screaming-kebab-case)
      (update :type keyword)
      (update :default-sort keyword)
      (assoc :uid (get-in post [:author :uid]))
      (dissoc :author)
      (update :distinguish keyword)
      (assoc :sid (:sid sub))
      (update :posted util/parse-int)
      (update :edited util/parse-int)
      (update :poll-closes-time util/parse-int)
      (dissoc :sub :user-attributes)))

(defn- post-404?
  [{:keys [sub]} post errors]
  (let [sub-name (get-in post [:sub :name])
        sub-match? (and (:pid post) sub-name (= (str/lower-case sub)
                                                (str/lower-case sub-name)))
        not-found-error? (some #(= "Not found" (:message %)) errors)]
    (or not-found-error? (and sub-name (not sub-match?)))))

(defn- fix-slugs-of-deleted-posts
  [{:keys [status] :as post} db]
  (if (= "ACTIVE" status)
    post
    (assoc post :slug (trx db "deleted"))))

(defn- slug-mismatch? [options post]
  (not= (:slug options) (:slug post)))

(defn post-url
  "Create the url to view a post."
  [db {:keys [sub pid slug cid]}]
  (if cid
    (route-util/url-for db :sub/view-direct-link
                        :sub sub :pid pid :slug slug :cid cid)
    ;; bidi is not getting this one right
    (let [base (route-util/url-for db :sub/view-single-post
                                   :sub sub :pid pid)]
      (if slug
        (str base "/" slug)
        base))))

(re-frame/reg-event-fx ::receive-single-post
  ;; :doc "Receive the results of the single post query."
  (fn-traced [{:keys [db]} [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          options (get-in db [:view :options])
          post (-> (:post-by-pid data)
                   (fix-slugs-of-deleted-posts db))
          sub (:sub post)
          filtered-errors (filter #(not= "Not found" (:message %)) errors)
          db' (errors/assoc-errors db {:event event
                                       :errors filtered-errors})]
      (log/debug {:post-404 (post-404? options post errors)
                  :post-missing (nil? post)
                  :slug-missing (slug-mismatch? options post)}
                 "Checking single-post url")
      (cond
        ;; Post does not exist or sub does not match.
        (post-404? options post errors)
        {:db db'
         :fx [[:dispatch [::routes/show-error :unmatched-route]]]}

        ;; Error, post not loaded.
        (nil? post)
        {:db db'
         :fx [[:dispatch [::routes/show-error :server-error]]]}

        ;; Slug in URL does not match, so fix it.
        (slug-mismatch? options post)
        {:db db'
         :fx [[::routes/replace-url
               (post-url db (assoc options
                                   :slug (:slug post)
                                   :sub (:name sub)))]]}

        ;; All good.
        :else (let [user-attributes (fixup-user-attributes post)
                    user (-> (:author post)
                             (update :status keyword))
                    author-flair (:author-flair post)
                    post (prepare-post post)
                    {:keys [cid]} options
                    sort-path (if cid
                                [:direct cid]
                                [(post-sort db post)])]
                {:db (-> db'
                         (assoc-in [:status :single-post] :loaded)
                         (update :reldb rel/transact
                                 [:insert-or-replace :Post post])
                         (update :reldb rel/transact
                                 [:insert-or-replace :SubName
                                  (select-keys sub [:name :sid])])
                         (cond-> user-attributes
                           (update :reldb rel/transact
                                   [:insert-or-replace :PostUserAttributes
                                    user-attributes]))
                         (update :reldb rel/transact
                                 [:insert-or-replace :User user])
                         (assoc-in [:ui-state :post :sort-path] sort-path)
                         (subs/update-sub sub)
                         (subs/update-sub-user-flair (:sid sub) (:uid user)
                                                     author-flair))
                 :fx [[:dispatch [::update-page-title]]
                      [:dispatch [::update-post-viewed post]]]})))))

(re-frame/reg-sub ::loaded?
  :<- [::window/statuses]
  (fn [statuses _]
    (= :loaded (:single-post statuses))))

(re-frame/reg-event-fx ::update-post-viewed
  ;; :doc Tell the server that we're reading a post.
  (fn-traced [{:keys [db]} [event {:keys [is-archived pid]}]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))]
      {:fx [(when (and authenticated? (not is-archived))
              [:dispatch [::graphql/mutate
                          {:graphql graphql/subs-graphql
                           :name :update-post-viewed
                           :id (str :update-post-viewed pid)
                           :variables {:pid pid}
                           :handler [::graphql/receive-mutation-handler
                                     {:event event}]}]])]})))

(defn- make-id [kw pid]
  (str (symbol kw) "-" pid))

(re-frame/reg-event-fx ::subscribe-new-comments
  ;; :doc Subscribe to the stream of new comments on the post.
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::new-comment pid)]
      {:db (update-in db [:subscriptions :stop-on-routing] conj id)
       :fx [[:dispatch [::graphql/subscribe
                        {:graphql graphql/subs-graphql
                         :name :new-comment
                         :id id
                         :variables {:pid pid}
                         :handler [::receive-new-comment pid]}]]]})))

(re-frame/reg-event-fx ::subscribe-post-update
  ;; :doc Subscribe to updates on the post.
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::post-update pid)]
      {:db (update-in db [:subscriptions :stop-on-routing] conj id)
       :fx [[:dispatch [::graphql/subscribe
                        {:graphql graphql/subs-graphql
                         :name :post-update
                         :id id
                         :variables {:pid pid}
                         :handler [::receive-post-update pid]}]]]})))

(re-frame/reg-event-fx ::subscribe-comment-updates
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::comment-update pid)
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin])))]
      (when mod-or-admin?
        {:db (update-in db [:subscriptions :stop-on-routing] conj id)
         :fx [[:dispatch [::graphql/subscribe
                          {:graphql graphql/subs-graphql
                           :name :comment-update
                           :id id
                           :variables {:pid pid :is_mod true}
                           :handler [::comment/receive-comment-update
                                     {:pid pid}]}]]]}))))

(re-frame/reg-event-fx ::subscribe-post
  ;; :doc Subscribe to post object updates and new comments.
  (fn-traced [_ [_ pid]]
    {:fx [#_[:dispatch [::subscribe-post-update pid]]
          [:dispatch [::subscribe-comment-updates pid]]
          #_[:dispatch [::subscribe-new-comments pid]]]}))

(re-frame/reg-event-db ::receive-post-update
  (fn-traced [db [_ _pid {:keys [_response]}]]
    db))

(re-frame/reg-event-db ::receive-new-comment
  (fn-traced [db [_ _pid {:keys [_response]}]]
    db))

(re-frame/reg-sub ::single-post
  ;; :doc Extract the single post being viewed.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [pid]}] _]
    (-> (rel/q reldb [[:from :Post] [:where [= :pid pid]]])
        first)))

(re-frame/reg-sub ::user-attributes
  ;; :doc Extract the user attributes of the single post being viewed.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [pid]}] _]
    (-> (rel/q reldb [[:from :PostUserAttributes] [:where [= :pid pid]]])
        first)))

(re-frame/reg-sub ::single-post-sort-path
  ;; :doc Extract the comment sort path for the current single post.
  ;; :doc May be [:BEST], [:NEW], [:TOP] or [:direct cid] when viewing a
  ;; :doc comment direct link.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :sort-path])))

(re-frame/reg-sub ::single-post-status
  ;; :doc Get the status of the post being viewed.
  :<- [::single-post]
  (fn [{:keys [status]} _]
    status))

(re-frame/reg-sub ::show-single-comment-thread?
  ;; :doc Determine whether a single comment's thread is shown.
  :<- [:view/options]
  (fn [{:keys [cid]}]
    (some? cid)))

(re-frame/reg-event-fx ::update-page-title
  ;; :doc Update the page title on the single post page.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [status title]} (single-post db)
          sub (get-in db [:view :options :sub])
          {:keys [name]} (internal/sub-info-by-name db sub)
          page-title (str (if (= :ACTIVE status)
                            title
                            (trx db "[Deleted Post]"))
                          " | "
                          name)]
      {:fx [[:dispatch [::page-title/update page-title]]]})))

(re-frame/reg-sub ::single-post-author
  ;; :doc Get author info for a post.
  :<- [::internal/reldb]
  :<- [::single-post]
  (fn [[reldb {:keys [uid]}] _]
    (-> reldb
        (rel/q [[:from :User] [:where [= :uid uid]]])
        first)))

(re-frame/reg-sub ::single-post-author-flair
  ;; :doc Get an author's flair for a post, if any.
  :<- [::internal/reldb]
  :<- [::single-post]
  (fn [[reldb {:keys [sid uid]}] _]
    (-> reldb
        (rel/q [[:from :SubUserFlair]
                [:where [= :uid uid] [= :sid sid]]])
        first
        :text)))

(re-frame/reg-sub ::is-post-author?
  ;; :doc Determine whether the current user is the author of the post.
  :<- [::user/is-authenticated?]
  :<- [::user/current-user]
  :<- [::single-post-author]
  (fn [[is-authenticated? current-user author]]
    (and is-authenticated? (= (:name current-user)
                              (:name author)))))

(re-frame/reg-sub ::single-post-when-posted
  ;; :doc Get the timeago when the post was made.
  :<- [::single-post]
  :<- [:throat-fe.ui.window/now]
  :<- [::tr/lang]
  (fn [[post now lang] _]
    (-> post
        :posted
        (js/parseInt)
        (util/time-ago now lang))))

;; TODO archived could be calculated here not on server
(re-frame/reg-sub ::single-post-is-archived?
  ;; :doc Get whether a post is archived.
  :<- [::single-post]
  (fn [post _]
    (get-in post [:is-archived])))

(re-frame/reg-sub ::single-post-nsfw?
  ;; :doc Return whether the currently viewed post contains NSFW content.
  :<- [::single-post]
  :<- [::subs/viewed-sub-info]
  (fn [[post sub] _]
    (or (:nsfw post) (:nsfw sub))))

(re-frame/reg-sub ::single-post-blur
  ;; :doc Return blur if any for the currently viewed post.
  :<- [::single-post-nsfw?]
  :<- [::user/nsfw-settings]
  (fn [[nsfw? {:keys [show? blur?]}] _]
    (when (and nsfw? show? blur?)
      :nsfw-blur)))

(re-frame/reg-sub ::single-post-when-edited
  ;; :doc Get the timeago when the post was edited, if any.
  :<- [::single-post]
  :<- [:throat-fe.ui.window/now]
  :<- [::tr/lang]
  (fn [[post now lang] _]
    (some-> post
            :edited
            (js/parseInt)
            (util/time-ago now lang))))

(re-frame/reg-sub ::single-post-when-title-edited
  ;; :doc Get the timeago when the post title was edited, if any.
  :<- [::single-post]
  :<- [:throat-fe.ui.window/now]
  :<- [::tr/lang]
  (fn [[post now lang] _]
    (some-> post
            :title-edited
            (js/parseInt)
            (util/time-ago now lang))))

(re-frame/reg-sub ::single-post-domain
  ;; :doc Get the domain of a link post.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (when link
      (.getDomain (Uri. link)))))

(re-frame/reg-sub ::post-title-edit-history
  ;; :doc Get a post's title edit history.
  :<- [::single-post]
  :<- [::user/is-mod-or-admin?]
  (fn [[post is-mod-or-admin?] _]
    (when is-mod-or-admin?
      (:title-history post))))

(re-frame/reg-sub ::post-title-edit-history-index
  ;; :doc Get the edit history index for the post title.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :title-history-index])))

(re-frame/reg-event-db ::post-title-see-newer-version
  ;; :doc Look at a newer version of the post title.
  (fn-traced [db _]
    (let [num (count (:title-history (single-post db)))]
      (update-in db [:ui-state :post :title-history-index]
                 (fn [index]
                   (when (and index (< (inc index) num))
                     (inc index)))))))

(re-frame/reg-event-db ::post-title-see-older-version
  ;; :doc Look at an older version of the post title.
  (fn-traced [db _]
    (let [num (count (:title-history (single-post db)))]
      (update-in db [:ui-state :post :title-history-index]
                 (fn [index]
                   (cond
                     (nil? index) (dec num)
                     (zero? index) 0
                     :else (dec index)))))))

(re-frame/reg-sub ::post-title-edit-history-state
  ;; :doc Construct a map with state for the edit history widget.
  :<- [::post-title-edit-history]
  :<- [::post-title-edit-history-index]
  :<- [::single-post-author]
  (fn [[edit-history index author] _]
    (let [older-index (cond
                        (nil? index) (dec (count edit-history))
                        (zero? index) nil
                        :else (dec index))
          edited-by (when older-index
                      (-> edit-history
                          (nth older-index)
                          :user))]
      {:show-history? (seq edit-history)
       :num-edits (inc (count edit-history))
       :index index
       :oldest? (zero? index)
       :newest? (nil? index)
       :by-mod? (not= (:uid edited-by) (:uid author))
       :author (:name edited-by)})))

(re-frame/reg-sub ::post-title
  ;; :doc Get the currently viewed version of the post title
  :<- [::single-post]
  :<- [::post-title-edit-history-index]
  (fn [[{:keys [title-history] :as post} index] _]
    (if index
      (-> title-history
          (nth index)
          :content)
      (:title post))))

(re-frame/reg-sub ::post-content-edit-history
  ;; :doc Get a post's content edit history.
  :<- [::single-post]
  :<- [::user/is-mod-or-admin?]
  (fn [[post is-mod-or-admin?] _]
    (when is-mod-or-admin?
      (:content-history post))))

(re-frame/reg-sub ::post-content-edit-history-index
  ;; :doc Get the edit history index for the post content.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :content-history-index])))

(re-frame/reg-sub ::post-content-edit-history-state
  ;; :doc Construct a map with state for the edit history widget.
  :<- [::post-content-edit-history]
  :<- [::post-content-edit-history-index]
  (fn [[edit-history index] _]
    {:show-history? (seq edit-history)
     :num-edits (inc (count edit-history))
     :index index
     :oldest? (zero? index)
     :newest? (nil? index)}))

(re-frame/reg-event-db ::post-content-see-newer-version
  ;; :doc Look at a newer version of the post content history.
  (fn-traced [db _]
    (let [num (count (:content-history (single-post db)))]
      (update-in db [:ui-state :post :content-history-index]
                 (fn [index]
                   (when (and index (< (inc index) num))
                     (inc index)))))))

(re-frame/reg-event-db ::post-content-see-older-version
  ;; :doc Look at an older version of the post content history.
  (fn-traced [db _]
    (let [num (count (:content-history (single-post db)))]
      (update-in db [:ui-state :post :content-history-index]
                 (fn [index]
                   (cond
                     (nil? index) (dec num)
                     (zero? index) 0
                     :else (dec index)))))))

(re-frame/reg-sub ::post-content
  ;; :doc Get the currently viewed version of the post content.
  :<- [::single-post]
  :<- [::post-content-edit-history-index]
  (fn [[{:keys [content-history] :as post} index] _]
    (if index
      (-> content-history
          (nth index)
          :content)
      (:content post))))

(re-frame/reg-sub ::post-html-content
  ;; :doc Produce the html version of the post content.
  :<- [::post-content]
  (fn [content _]
    (when content
      (util/markdown-to-html content))))

(re-frame/reg-event-db ::record-post-html-content-height
  ;; :doc Save the height of the html content element in the db.
  (fn-traced [db [_ val]]
    (assoc-in db [:ui-state :post :content-height] val)))

(re-frame/reg-sub ::post-html-height
  ;; :doc Extract the height of the post html content element from the db.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :content-height])))

(re-frame/reg-sub ::post-editor-height
  ;; :doc Calculate the height for the post content editor.
  :<- [::post-html-height]
  (fn [height _]
    (when height
      (max 100 (+ 28 height)))))

(re-frame/reg-sub ::embeddable-image?
  ;; :doc Determine if a post is a link to an image.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (and link
         (re-find #"\.(png|jpg|gif|tiff|svg|bmp|jpeg)$" link))))

(re-frame/reg-sub ::embeddable-video?
  ;; :doc Determine if a post is a link to a video.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (and link
         (re-find #"\.(mp4|webm)$" link))))

(defn- gfycat-iframe-url
  [^Uri uri]
  (when (= "gfycat.com" (.getDomain uri))
    (str "https://gfycat.com/ifr/" (subs (.getPath uri) 1))))

(defn- streamable-iframe-url
  [^Uri uri]
  (when (= "streamable.com" (.getDomain uri))
    (str "https://streamable.com/o/" (subs (.getPath uri) 1))))

(defn- streamja-iframe-url
  [^Uri uri]
  (when (= "streamja.com" (.getDomain uri))
    (str "https://streamja.com/embed/" (subs (.getPath uri) 1))))

(defn- youtube-id
  [^Uri uri]
  (let [domain (.getDomain uri)
        path (.getPath uri)]
    (cond
      (and (#{"www.youtube.com" "youtube.com" "m.youtube.com"} domain)
           (= "/watch" path))
      (.getParameterValue uri "v")

      (= "youtu.be" domain)
      (subs path 1))))

(defn- youtube-iframe-url
  [uri]
  (when-let [id (youtube-id uri)]
    (str "https://www.youtube.com/embed/" id)))

(defn- vimeo-iframe-url
  [^Uri uri]
  (when (= "vimeo.com" (.getDomain uri))
    (str "https://player.vimeo.com/video/" (subs (.getPath uri) 1))))

(def iframe-video-support-functions
  [youtube-iframe-url
   vimeo-iframe-url
   gfycat-iframe-url
   streamable-iframe-url
   streamja-iframe-url])

(re-frame/reg-sub ::iframe-video-link
  ;; :doc Construct the link to embed a video in an iframe.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (when link
      (let [uri (Uri. link)]
        (some (fn [iframe-func]
                (iframe-func uri)) iframe-video-support-functions)))))

;;; Post and comment bottom bar menu element styling

(re-frame/reg-sub ::bottombar-colors
  ;; :doc Get colors for a bottombar menu item.
  ;; :doc Pass it :normal, :subdued or :alert.
  :<- [::window/day?]
  (fn [day? [_ typ]]
    (case typ
      :subdued (if day?
                 :gray.hover-medium-blue
                 :mid-gray.hover-medium-blue)
      :alert   (if day?
                 :dark-red.hover-red
                 :reddish-brown:hover-dark-red)
      (if day?
        :blue.hover-medium-blue
        :dark-blue.hover-medium-blue))))

(re-frame/reg-sub ::post-comment-sort
  ;; :doc Get the currently selected comment sort method.
  :<- [::ui/state]
  (fn [state _]
    (first (get-in state [:post :sort-path]))))

(re-frame/reg-event-fx ::set-comment-sort
  ;; :doc Set the comment sort method.
  (fn-traced [{:keys [db]} [_ val]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin]))
                            boolean)
          pid (get-in db [:view :options :pid])
          {:keys [num level]} (get-in db [:settings :comment-tree])
          first-fetch? (nil? (get-in db [:content :comments pid val]))]
      {:db (cond-> db
             first-fetch? (assoc-in [:status :comment-tree] :loading)
             true (assoc-in [:ui-state :post :sort-path] [val]))
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql/subs-graphql
                         :name :get-comment-tree
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid :first num :level level
                                     :sort_by val}
                         :handler [::receive-comment-tree {:pid pid
                                                           :sort-by val}]}]]]})))

(re-frame/reg-event-db ::receive-comment-tree
  ;; :doc "Receive the results of the comment tree query."
  (fn-traced [db [event {:keys [cid pid sort-by response]}]]
    (let [{:keys [data errors]} response
          result (:post-by-pid data)
          sort-path (or (and sort-by [sort-by])
                        (and cid [:direct cid])
                        [(-> result :default-sort keyword)])]
      (-> db
          (assoc-in [:status :comment-tree] :loaded)
          (assoc-in [:ui-state :post :sort-path] sort-path)
          (errors/assoc-errors {:event event :errors errors})
          (cond-> (nil? errors)
            (comment/update-comments (:comment-tree result) pid sort-path))))))

(re-frame/reg-sub ::comment-tree-loaded?
  ;; :doc Extract the load status of the comment tree.
  :<- [::window/statuses]
  (fn [statuses _]
    (= :loaded (:comment-tree statuses))))

;;; Post bottombar menu

(re-frame/reg-sub ::post-can-reply?
  ;; :doc Determine whether the user can reply to a post.
  :<- [::subs/sub-banned?]
  :<- [::single-post]
  :<- [::single-post-author]
  :<- [::user/is-authenticated?]
  :<- [::user/is-mod-or-admin?]
  (fn [[banned? {:keys [is-archived locked status]} author
        is-authenticated? is-mod-or-admin?] _]
    (and is-authenticated?
         (not banned?)
         (not= :DELETED (:status author))
         (not= :DELETED status)
         (not is-archived)
         (or is-mod-or-admin? (not locked)))))

(re-frame/reg-sub ::can-show-post-source?
  ;; :doc Determine whether to show the source link for a post.
  :<- [::single-post-status]
  :<- [::post-content]
  (fn [[status content] _]
    (and (not= :DELETED status) (seq content))))

(re-frame/reg-event-fx ::toggle-post-source
  ;; :doc Toggle the display of the post source.
  ;; :doc If the post content editor is open, close it.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (update-in db [:ui-state :post :show-source?] not)
     :fx [[:dispatch [::hide-edit-post-content]]]}))

(re-frame/reg-event-db ::hide-post-source
  ;; :doc Hide the post content source.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-source?] false)))

(re-frame/reg-sub ::show-post-source?
  ;; :doc Extract whether the post source should be shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-source?])))

(re-frame/reg-sub ::can-save-post?
  :<- [::single-post-status]
  (fn [status _]
    (= :ACTIVE status)))

(re-frame/reg-sub ::single-post-saved?
  ;; :doc Extract whether the current single post is saved.
  :<- [::user-attributes]
  (fn [post _]
    (:is-saved post)))

(re-frame/reg-event-fx ::toggle-post-saved
  ;; :doc Change whether the current post is in the user's saved posts list.
  (fn-traced [{:keys [db]} [event _]]
    (let [{:keys [pid is-saved]} (single-post-user-attributes db)
          restore #(assoc-post-user-attributes % pid :is-saved is-saved)]
      {:db (assoc-post-user-attributes db pid :is-saved (not is-saved))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :save-post
                         :id (str :save-post pid)
                         :variables {:pid pid :save (not is-saved)}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-sub ::can-flair-post?
  ;; :doc Determine whether the user has permission to edit the post flair.
  :<- [::subs/viewed-sub-info]
  :<- [::subs/post-flairs]
  :<- [::single-post-status]
  :<- [::single-post-is-archived?]
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  (fn [[sub post-flairs status is-archived? is-author? is-mod-or-admin?] _]
    (and (= :ACTIVE status)
         (not is-archived?)
         (seq post-flairs)
         (or (and is-author?
                  (or (:user-can-flair sub)
                      (:user-must-flair sub)))
             is-mod-or-admin?))))

(re-frame/reg-event-db ::edit-single-post-flair
  ;; :doc Toggle the edit post flair modal.
  (fn-traced [db _]
    (-> db
        (update-in [:ui-state :post :show-edit-post-flair?] not)
        post-forms/initialize-select-flair-form)))

(re-frame/reg-sub ::show-single-post-flair-modal?
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-edit-post-flair?])))

(re-frame/reg-event-fx ::change-post-flair
  ;; :doc Change the post flair on the server.
  (fn-traced [_ [_ {:keys [id pid selected-id]} after-event]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql/subs-graphql
                       :name :set-post-flair
                       :id (str :set-post-flair pid)
                       :variables {:pid pid
                                   :flair_id selected-id}
                       :handler [::receive-post-flair-change
                                 {:form-id id
                                  :pid pid
                                  :update-form after-event}]}]]]}))

(re-frame/reg-event-fx ::receive-post-flair-change
  ;; :doc Receive a response to a post flair change from the server.
  ;; :doc On success, update the post's flair and hide the modal.
  ;; :doc On failure, record the errors and leave the form visible.
  (fn-traced [{:keys [db]} [_ {:keys [form-id pid update-form response]}]]
    (let [{:keys [data errors]} response
          new-flair (get-in data [:set-post-flair :text])]
      {:db (if errors
             (errors/assoc-errors db {:errors errors :event form-id})
             (-> db
                 (assoc-in [:ui-state :post :show-edit-post-flair?] false)
                 (assoc-post pid :flair new-flair)))
       :fx [[:dispatch update-form]]})))

(re-frame/reg-event-fx ::remove-post-flair
  ;; :doc Ask the server to remove a post flair.
  (fn-traced [_ [_ {:keys [id pid]} after-event]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql/subs-graphql
                       :name :remove-post-flair
                       :id (str :remove-post-flair pid)
                       :variables {:pid pid}
                       :handler [::receive-post-flair-removal
                                 {:form-id id
                                  :pid pid
                                  :update-form after-event}]}]]]}))

(re-frame/reg-event-fx ::receive-post-flair-removal
  ;; :doc Receive a response to a post flair removal from the server.
  ;; :doc On success, delete the post's flair and hide the modal.
  ;; :doc On failure, record the errors and leave the form visible.
  (fn-traced [{:keys [db]} [_ {:keys [form-id pid update-form response]}]]
    (let [errors (:errors response)]
      {:db (if errors
             (errors/assoc-errors db {:errors errors :event form-id})
             (-> db
                 (assoc-in [:ui-state :post :show-edit-post-flair?] false)
                 (assoc-post pid :flair nil)))
       :fx [[:dispatch update-form]]})))

(re-frame/reg-sub ::can-edit-post-content?
  ;; :doc Determine whether the user can edit the post content.
  :<- [::is-post-author?]
  :<- [::single-post]
  :<- [::single-post-status]
  (fn [[is-author? {:keys [type]} status] _]
    (and is-author? (not= :LINK type) (= :ACTIVE status))))

(re-frame/reg-event-fx ::toggle-edit-post-content
  ;; :doc Toggle display of the post content editor.
  ;; :doc Initialize the related form if that has not yet been done.
  ;; :doc If the markdown source is shown, and the edit form is toggled,
  ;; :doc switch content display back to html.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (update-in [:ui-state :post :show-content-editor?] not)
             post-edit-form/initialize-post-edit-form)
     :fx [[:dispatch [::hide-post-source]]]}))

(re-frame/reg-event-db ::hide-edit-post-content
  ;; :doc Hide the post content editor.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-content-editor?] false)))

(re-frame/reg-sub ::show-post-content-editor?
  ;; :doc Extract whether the edit post content form is shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-content-editor?])))

(re-frame/reg-event-fx ::send-post-edit
  ;; :doc Update post content on the server.
  (fn-traced [{:keys [db]}
              [_ {:keys [pid content] :as compose-form} update-form-event]]
    (let [existing-content (:content (single-post db))
          unchanged? (= (str/trim content) (str/trim existing-content))]
      {:fx (if unchanged?
             [[:dispatch [::hide-edit-post-content]]
              [:dispatch update-form-event]]
             [[:dispatch [::graphql/mutate
                          {:graphql graphql/subs-graphql
                           :name :update-post-content
                           :id (str :update-post-content pid)
                           :variables {:pid pid :content content}
                           :handler [::receive-post-edit-response
                                     {:form compose-form
                                      :update-form update-form-event}]}]]])})))

(defn update-post-content
  "Update the content and content history of a post."
  [{:keys [content] :as post} new-content now]
  (-> post
      (update :content-history concat [{:content content}])
      (assoc :content new-content)
      (assoc :edited now)))

(re-frame/reg-event-fx ::receive-post-edit-response
  ;; :doc Receive a response to a post content edit from the server.
  ;; :doc On success, update the post's content and edit history and
  ;; :doc hide the form. On failure, record the errors and leave the
  ;; :doc form visible.
  [(re-frame/inject-cofx ::window/now)]
  (fn-traced [{:keys [db now]} [event {:keys [form update-form response]}]]
    (let [{:keys [data errors]} response
          {:keys [content pid]} form
          changed? (:update-post-content data)
          db-with-errors (errors/assoc-errors db {:errors errors :event event})]
      {:db (if (or errors (not changed?))
             db-with-errors
             (update-post db pid #(update-post-content % content now)))
       :fx [(when (nil? errors)
              [:dispatch [::hide-edit-post-content]])
            [:dispatch update-form]]})))

(re-frame/reg-sub ::can-edit-post-title?
  ;; :doc Determine whether the user can edit the post title.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  :<- [::single-post-status]
  :<- [::settings/site-config]
  :<- [:throat-fe.ui.window/now]
  :<- [::show-post-title-editor?]
  (fn [[is-author? is-mod?
        {:keys [posted is-archived]} status site-config now is-visible?] _]
    (or is-visible?  ; Allow hiding it even if expired.
        ;; Mods should only be able to edit titles of posts not deleted by users
        (and is-mod? (not= :DELETED-BY-USER status) (not is-archived))
        (and is-author?
             (= :ACTIVE status)
             (< (/ (- now posted) 1000)
                (:title-edit-timeout site-config))))))

(re-frame/reg-event-db ::toggle-edit-post-title
  ;; :doc Show the edit post title form.
  (fn-traced [db [_ _]]
    (-> db
        (update-in [:ui-state :post :show-post-title-editor?] not)
        (post-edit-title-form/initialize-post-edit-title-form))))

(re-frame/reg-event-db ::hide-edit-post-title
  ;; :doc Hide the post title editor.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-post-title-editor?] false)))

(re-frame/reg-sub ::show-post-title-editor?
  ;; :doc Extract whether the edit post title form is shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-post-title-editor?])))

(re-frame/reg-event-fx ::send-post-title-edit
  ;; :doc Update a post title on the server.
  (fn-traced [{:keys [db]}
              [_ {:keys [pid title reason] :as edit-form} update-form-event]]
    (let [existing-title (:title (single-post db))
          unchanged? (= (str/trim title) (str/trim existing-title))]
      {:fx (if unchanged?
             [[:dispatch [::hide-edit-post-title]]
              [:dispatch update-form-event]]
             [[:dispatch [::graphql/mutate
                          {:graphql graphql/subs-graphql
                           :name :update-post-title
                           :id (str :update-post-title pid)
                           :variables {:pid pid :title title :reason reason}
                           :handler [::receive-post-title-edit-response
                                     {:form edit-form
                                      :update-form update-form-event}]}]]])})))

(defn update-post-title
  "Update the title and title edit history of a post."
  [{:keys [title] :as post} user new-title]
  (-> post
      (update :title-history concat [{:content title
                                      :user (select-keys user [:name :uid])}])
      (assoc :title new-title)))

(re-frame/reg-event-fx ::receive-post-title-edit-response
  ;; :doc Receive a response to a post title edit from the server.
  ;; :doc On success, update the post's title and edit history and
  ;; :doc hide the form. On failure, record the errors and leave the
  ;; :doc form visible.
  (fn-traced [{:keys [db]} [event {:keys [form update-form response]}]]
    (let [{:keys [data errors]} response
          user (:current-user db)
          changed? (:update-post-title data)
          {:keys [pid title]} form
          db-with-errors (errors/assoc-errors db {:errors errors :event event})]
      {:db (if (or errors (not changed?))
             db-with-errors
             (update-post db pid #(update-post-title % user title)))
       :fx [(when (nil? errors)
              [:dispatch [::hide-edit-post-title]])
            [:dispatch update-form]]})))

(re-frame/reg-sub ::can-delete-post?
  ;; :doc Determine whether the user can delete the post.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post-status]
  (fn [[is-author? is-mod-or-admin? status] _]
    (and (or is-author? is-mod-or-admin?) (= :ACTIVE status))))

(re-frame/reg-event-fx ::delete-post
  ;; :doc Delete a post.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid uid] :as post} (single-post db)
          fields (select-keys post [:status :link :content])
          restore (fn [db]
                    (update-post db pid #(merge % fields)))
          current-user-uid (get-in db [:current-user :uid])]
      (if (= current-user-uid uid)
        {:db (update-post db pid #(merge % {:status :DELETED-BY-USER
                                            :link nil
                                            :content nil}))
         :fx [[:dispatch [::util/ajax-form-post
                          [(route-util/url-for db :do/delete-post)
                           {:post pid}
                           nil
                           [::errors/receive-ajax-error
                            ::delete-post restore]]]]]}
        {:fx [[:dispatch [::toggle-mod-delete-post-form]]]}))))

(re-frame/reg-event-db ::toggle-mod-delete-post-form
  ;; :doc Toggle the display of the moderator delete post reason form.
  (fn-traced [db [_ _]]
    (-> db
        (mod-action-form/initialize-mod-action-form :delete-post)
        (update-in [:ui-state :post :show-mod-delete-post-form?] not))))

(re-frame/reg-event-db ::hide-mod-delete-post-form
  ;; :doc Hide the moderator delete post reason form.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-mod-delete-post-form?] false)))

(re-frame/reg-sub ::show-mod-delete-post-form?
  ;; :doc Extract whether the moderator delete post reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-mod-delete-post-form?])))

(re-frame/reg-event-fx ::mod-delete-post
  ;; :doc Send a mod post delete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [pid reason]} update-form-event]]
    (let [state {:pid pid
                 :update-form-event update-form-event
                 :restore #(assoc-post % pid :status :ACTIVE)}
          is-mod? (internal/is-mod-of-sub? db)]
      {:db (assoc-post db pid :status (if is-mod?
                                        :DELETED-BY-MOD
                                        :DELETED-BY-ADMIN))
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/delete-post)
               {:post pid
                :reason reason}
               [::receive-mod-delete-post-response state]
               [::receive-mod-delete-post-error-response state]]]]]})))

(re-frame/reg-event-fx ::receive-mod-delete-post-response
  ;; :doc Handle a server response to a mod delete post request.
  (fn-traced [_ [_ {:keys [update-form-event]} _]]
    {:fx [[:dispatch update-form-event]
          [:dispatch [::hide-mod-delete-post-form]]]}))

(re-frame/reg-event-fx ::receive-mod-delete-post-error-response
  ;; :doc Handle an error response to a mod delete post request.
  (fn-traced [{:keys [db]}
              [_ {:keys [restore update-form-event]} error-response]]
    {:db (-> db
             restore
             (errors/assoc-errors
              {:event ::receive-mod-delete-post-response
               :errors (errors/errors-from-ajax-error-response
                        error-response)}))
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-sub ::can-undelete-post?
  ;; :doc Determine whether the user can undelete the post.
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post-status]
  (fn [[is-mod? is-admin? status] _]
    (or (and is-admin? (#{:DELETED-BY-MOD
                          :DELETED-BY-ADMIN} status))
        (and is-mod? (= :DELETED-BY-MOD status)))))

(re-frame/reg-event-db ::toggle-mod-undelete-post-form
  ;; :doc Toggle the display of the moderator undelete post reason form.
  (fn-traced [db [_ _]]
    (-> db
        (mod-action-form/initialize-mod-action-form :undelete-post)
        (update-in [:ui-state :post :show-mod-undelete-post-form?] not))))

(re-frame/reg-event-db ::hide-mod-undelete-post-form
  ;; :doc Hide the moderator undelete post reason form.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-mod-undelete-post-form?] false)))

(re-frame/reg-sub ::show-mod-undelete-post-form?
  ;; :doc Extract whether the moderator undelete post reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-mod-undelete-post-form?])))

(re-frame/reg-event-fx ::mod-undelete-post
  ;; :doc Send a mod post undelete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [pid reason]} update-form-event]]
    (let [status (:status (single-post db))
          state {:pid pid
                 :update-form-event update-form-event
                 :restore #(assoc-post % pid :status status)}]
      {:db (assoc-post db pid :status :ACTIVE)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/undelete-post)
               {:post pid
                :reason reason}
               [::receive-mod-undelete-post-response state]
               [::receive-mod-undelete-post-error-response state]]]]]})))

(re-frame/reg-event-fx ::receive-mod-undelete-post-response
  ;; :doc Handle a server response to a mod undelete post request.
  (fn-traced [_ [_ {:keys [update-form-event]} _]]
    {:fx [[:dispatch update-form-event]
          [:dispatch [::hide-mod-undelete-post-form]]]}))

(re-frame/reg-event-fx ::receive-mod-undelete-post-error-response
  ;; :doc Handle an error response to a mod undelete post request.
  (fn-traced [{:keys [db]}
              [_ {:keys [restore update-form-event]} error-response]]
    {:db (-> db
             restore
             (errors/assoc-errors
              {:event ::receive-mod-undelete-post-response
               :errors (errors/errors-from-ajax-error-response
                        error-response)}))
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-sub ::can-tag-post-nsfw?
  ;; :doc Determine whether the user can change nsfw on the post
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post-status]
  :<- [::subs/viewed-sub-info]
  (fn [[is-author? is-mod-or-admin? status sub] _]
    (and (or is-author? is-mod-or-admin?)
         (= :ACTIVE status)
         (not (:nsfw sub)))))

(re-frame/reg-event-fx ::toggle-post-nsfw
  ;; :doc Send request to the server to toggle a post's NSFW flag.
  (fn-traced [{:keys [db]} [event _]]
    (let [{:keys [pid nsfw]} (single-post db)
          restore #(assoc-post % pid :nsfw nsfw)]
      {:db (assoc-post db pid :nsfw (not nsfw))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :set-post-nsfw
                         :id (str :set-post-nsfw pid)
                         :variables {:pid pid
                                     :nsfw (not nsfw)}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-sub ::can-close-poll?
  ;; :doc Determine whether the the user can close a poll.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod-or-admin? {:keys [status type poll-open]}] _]
    (and (= :ACTIVE status)
         (= :POLL type)
         poll-open
         (or is-author? is-mod-or-admin?))))

(re-frame/reg-event-fx ::close-poll
  ;; :doc Send a close poll request to the server
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid]} (single-post db)
          restore #(assoc-post % pid :poll-open true)]
      {:db (assoc-post db pid :poll-open false)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/close-poll)
               {:post pid}
               nil
               [::errors/receive-ajax-error ::close-poll restore]]]]]})))

(re-frame/reg-sub ::can-stick-or-unstick-post?
  ;; :doc Determine whether the user can mark the post sticky.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post-status]
  (fn [[is-mod? status] _]
    (and is-mod? (= :ACTIVE status))))

(re-frame/reg-event-fx ::toggle-post-sticky
  ;; :doc Stick or unstick a post.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid sticky]} (single-post db)
          restore #(assoc-post % :pid :sticky sticky)]
      {:db (assoc-post db pid :sticky (not sticky))
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-sticky :post pid)
               {:post pid}
               [::receive-toggle-sticky-response restore]
               [::errors/receive-ajax-error
                ::toggle-post-sticky restore]]]]]})))

(defn- describe-toggle-sticky-error
  "Pass through an already translated error message from Python."
  [{:keys [message]}]
  {:msg message})

(re-frame/reg-event-db ::receive-toggle-sticky-response
  ;; :doc Handle a response to a sticky toggle request.
  (fn-traced [db [event restore {:keys [status error]}]]
    (if (= "ok" status)
      db
      (-> db
          restore
          (errors/assoc-errors
           {:event event
            :errors (errors/errors-from-do-api error)
            :describe-error-fn describe-toggle-sticky-error})))))

(re-frame/reg-sub ::can-set-announcement-post?
  ;; :doc Determine whether the user can make this post the site announcement.
  :<- [::subs/normalized-sub-name]
  :<- [::user/is-admin?]
  :<- [:settings/current-language]
  :<- [::single-post]
  :<- [::settings/announcement-pid]
  (fn [[sub-name is-admin? lang {:keys [pid status]} announcement-pid] _]
    (and is-admin?
         (= (trx lang "announcements") (str/lower-case sub-name))
         (= :ACTIVE status)
         (not (= pid announcement-pid)))))

(re-frame/reg-event-fx ::set-announcement-post
  ;; :doc Set the current post to be the site-wide announcement.
  (fn-traced [{:keys [db]} [_ _]]
    (let [pid (get-in db [:view :options :pid])
          former (get-in db [:content :announcement-pid])
          restore #(assoc-in % [:content :announcement-pid] former)]
      {:db (assoc-in db [:content :announcement-pid] pid)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/make-announcement)
               {:post pid}
               nil
               [::errors/receive-ajax-error
                ::set-announcement-post restore]]]]]})))

(re-frame/reg-sub ::can-change-sticky-post-sort?
  ;; :doc Determine if the user can change the sticky sort.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post]
  :<- [::single-post-status]
  (fn [[is-mod? {:keys [sticky]} status] _]
    (and is-mod? sticky (= :ACTIVE status))))

(defn- next-sticky-post-sort
  [{:keys [default-sort best-sort-enabled]}]
  (cond
    (and (= :NEW default-sort)
         best-sort-enabled)    :BEST
    (= :NEW default-sort)      :TOP
    :else                      :NEW))

(re-frame/reg-sub ::next-sticky-post-sort
  ;; :doc Get the next sort type in the rotation.
  :<- [::single-post]
  (fn [post _]
    (next-sticky-post-sort post)))

(re-frame/reg-event-fx ::change-sticky-post-sort
  ;; :doc Send a request to set the post's default comment sort.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid default-sort] :as post} (single-post db)
          sort (next-sticky-post-sort post)
          restore #(assoc-post db pid :default-sort default-sort)]
      {:db (assoc-post db pid :default-sort sort)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-sort :post pid)
               {}
               [::receive-change-sticky-post-sort-response pid]
               [::errors/receive-ajax-error
                ::change-sticky-post-sort restore]]]]]})))

(re-frame/reg-event-fx ::receive-change-sticky-post-sort-response
  ;; :doc Receive the response to the comment sort change.
  ;; :doc Reload the post to get resorted comments.
  (fn-traced [{:keys [db]} [_ pid _]]
    (let [sub (get-in db [:view :options :sub])]
      {:fx [[:dispatch [::load-single-post {:sub sub
                                            :pid pid}]]]})))

(re-frame/reg-sub ::can-lock-comments?
  ;; :doc Determine if the user can lock or unlock comments on the post.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post-status]
  (fn [[is-mod? status] _]
    (and is-mod? (= :ACTIVE status))))

(re-frame/reg-event-fx ::toggle-post-comments-locked
  ;; :doc Send request to the server to toggle locking of comments.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid locked]} (single-post db)
          restore #(assoc-post db pid :locked locked)]
      {:db (assoc-post db pid :locked (not locked))
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-lock-comments :post pid)
               {}
               nil
               [::errors/receive-ajax-error
                ::toggle-post-comments-locked restore]]]]]})))

(re-frame/reg-sub ::can-report-post?
  ;; :doc Determine whether the user can report the current post.
  :<- [::is-post-author?]
  :<- [::single-post-status]
  (fn-traced [[is-author? status] _]
    (and (not is-author?) (= :ACTIVE status))))

(re-frame/reg-event-fx ::toggle-report-content-form
  ;; :doc Toggle the display of the report content form.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [rules]} (sub-info-by-name db sub)]
      {:db
       (-> db
           (report-form/initialize-report-form cid)
           (update-in [:ui-state :post :show-report-form?] not))
       :fx [(when (empty? rules)
              [:dispatch [::subs/load-sub-rules]])]})))

(re-frame/reg-sub ::show-report-content-form?
  ;; :doc Extract whether the report content form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-report-form?])))

(re-frame/reg-event-fx ::report-post
  ;; :doc Send a post report to the Python server.
  (fn-traced [{:keys [db]} [_ {:keys [pid reason rule explanation]}
                            update-form-event]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [rules]} (sub-info-by-name db sub)
          rule-text (->> rules
                         (filter #(= rule (:id %)))
                         first
                         :text)
          reason-text (case reason
                        :sub-rule (str (trx db "Circle Rule: ")
                                       (if (= :other rule)
                                         explanation
                                         rule-text))
                        :site-rule (trx db "Sitewide Rule violation")
                        :other explanation)]
      {:fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/report)
               {:post pid
                :send_to_admin (not= reason :sub-rule)
                :reason reason-text}
               [::receive-report-post-response update-form-event]
               [::receive-report-post-error-response update-form-event]]]]]})))

(defn- describe-report-post-error
  "Pass through an already translated error message from Python."
  [{:keys [message]}]
  {:msg message})

(re-frame/reg-event-fx ::receive-report-post-response
  ;; :doc Handle a server response to a report post request.
  (fn-traced [{:keys [db]}
              [event update-form-event {:keys [_status error]}]]
    {:db (errors/assoc-errors db
                              {:event event
                               :errors (errors/errors-from-do-api error)
                               :describe-error-fn describe-report-post-error})
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-event-fx ::receive-report-post-error-response
  ;; :doc Handle an error response to a report post request.
  (fn-traced [{:keys [db]} [_ update-form-event error-response]]
    {:db (-> db
             (errors/assoc-errors
              {:event ::receive-report-post-response
               :errors (errors/errors-from-ajax-error-response
                        error-response)}))
     :fx [[:dispatch update-form-event]]}))

(re-frame/reg-sub ::show-post-reports-link?
  ;; :doc Determine whether to show the link to open post reports.
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  (fn [[is-mod-or-admin? post] _]
    (and is-mod-or-admin? (seq (:open-reports post)))))

(re-frame/reg-sub ::open-post-report-count
  ;; :doc Get the count of open reports on the current post.
  :<- [::single-post]
  (fn [post _]
    (count (:open-reports post))))

(re-frame/reg-sub ::first-open-post-report-id
  ;; :doc Get the id of the first open post report.
  :<- [::single-post]
  (fn [post _]
    (-> post :open-reports first :id)))

(re-frame/reg-sub ::can-distinguish-post-author-oneshot?
  ;; :doc Determine whether the user can distinguish the post author.
  ;; :doc Admins get an option of how to distinguish if they also mod
  ;; :doc the sub.
  ;; :doc If the post is already distinguished, no need to give the
  ;; :doc admin the option.
  :<- [::is-post-author?]
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? (= :ACTIVE status)
         (or is-admin? is-mod?)
         (or (not is-admin?) (not is-mod?) (some? distinguish)))))

(re-frame/reg-sub ::can-distinguish-post-author-choice?
  ;; :doc Determine whether the user has a choice of how to distinguish.
  :<- [::is-post-author?]
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? is-mod? is-admin? (nil? distinguish) (= :ACTIVE status))))

(re-frame/reg-event-fx ::edit-post-author-distinguish
  ;; :doc Send a post author distinguish request to the server.
  ;; :doc If distinguish is set, it will be cleared, otherwise
  ;; :doc it will be set according to the user's mod or admin status.
  ;; :doc If the user is both mod and admin, supply
  ;; :doc `distinguish-as` with the user's choice.
  (fn-traced [{:keys [db]} [event distinguish-as]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          is-mod? (internal/is-mod-of-sub? db)
          distinguish (:distinguish (single-post db))
          new-distinguish (cond
                            (some? distinguish) nil
                            (= :admin distinguish-as) :ADMIN
                            (= :mod distinguish-as) :MOD
                            is-mod? :MOD
                            :else :ADMIN)
          restore #(assoc-post % pid :distinguish distinguish)]
      {:db (assoc-post db pid :distinguish new-distinguish)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql/subs-graphql
                         :name :distinguish-post
                         :id (str :distinguish-post pid)
                         :variables {:pid pid
                                     :sid (internal/sid-from-name db sub)
                                     :distinguish new-distinguish}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-event-db ::toggle-login-to-continue
  ;; :doc Toggle the login to continue modal.
  (fn-traced [db _]
    (-> db
        (update-in [:ui-state :post :show-login?] not))))

(re-frame/reg-sub ::show-login-to-continue?
  ;; :doc Extract whether the login modal should be shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-login?])))

(re-frame/reg-sub ::can-vote?
  ;; :doc Determine whether the user has active vote arrows on this post.
  ;; :doc Anons get active vote arrows that bring up the login modal.
  :<- [::is-post-author?]
  :<- [::subs/sub-banned?]
  :<- [::single-post-is-archived?]
  :<- [::single-post-author]
  :<- [::single-post-status]
  (fn [[is-author? banned? is-archived? author status
        {:keys [self-vote-posts]}] _]
    (and (or self-vote-posts (not is-author?))
         (not is-archived?)
         (not banned?)
         (= :ACTIVE status)
         (not= :DELETED (:status author)))))

(re-frame/reg-event-fx ::vote-post
  ;; :doc Send a vote request to the Python server.
  (fn-traced [{:keys [db]} [_ direction]]
    (let [{:keys [pid vote]} (single-post-user-attributes db)
          in-flight? (get-in db [:status :post-vote-in-flight?])
          new-vote (when (not= vote direction)
                     direction)
          vote-value (-> direction name str/lower-case)
          restore #(-> %
                       (assoc-post-user-attributes pid :vote vote)
                       (assoc-in [:status :post-vote-in-flight?] false))]
      (when-not in-flight?
        {:db (-> db
                 (assoc-post-user-attributes pid :vote new-vote)
                 (assoc-in [:status :post-vote-in-flight?] true))
         :fx [[:dispatch [::util/ajax-form-post
                          [(route-util/url-for db :do/upvote :pid pid
                                               :value vote-value)
                           {}
                           [::receive-vote-post-response pid direction]
                           [::errors/receive-ajax-error
                            ::vote-post restore]]]]]}))))

(re-frame/reg-event-db ::receive-vote-post-response
  ;; :doc Receive the response to a post vote request.
  (fn-traced [db [_ pid direction {:keys [rm score]}]]
    (-> db
        (assoc-in [:status :post-vote-in-flight?] false)
        (assoc-post pid :score score)
        (assoc-post-user-attributes pid :vote (when-not rm direction)))))

(re-frame/reg-sub ::poll-close-timeuntil
  ;; :doc Get the timeuntil when the post closes.
  :<- [::single-post]
  :<- [:throat-fe.ui.window/now]
  :<- [::tr/lang]
  (fn [[{:keys [poll-closes-time]} now lang] _]
    (when poll-closes-time
      (util/time-until now poll-closes-time lang))))

(re-frame/reg-sub ::see-poll-results?
  ;; :doc Extract whether the user has chosen to see the poll results.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :see-poll-results?])))

(re-frame/reg-event-db ::see-poll-results
  ;; :doc Choose to see the poll results.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :see-poll-results?] true)))

(re-frame/reg-sub ::show-see-results-option?
  ;; :doc Determine whether to show the "See Results" button.
  :<- [::single-post]
  :<- [::see-poll-results?]
  :<- [::can-vote-poll?]
  (fn [[{:keys [hide-results poll-votes]} see-results? can-vote?] _]
    (and (not hide-results) can-vote? (not see-results?)
         poll-votes (pos? poll-votes))))

(re-frame/reg-sub ::show-poll-results?
  ;; :doc Determine whether to show the poll results.
  :<- [::single-post]
  :<- [::can-vote-poll?]
  :<- [::see-poll-results?]
  (fn [[{:keys [poll-open poll-votes hide-results]} can-vote? see-results?] _]
    (and (or (not poll-open) (not hide-results))
         (or (not can-vote?) see-results?)
         (and poll-votes (pos? poll-votes)))))

(re-frame/reg-sub ::poll-option-data
  ;; :doc Extract the poll options and total votes.
  :<- [::single-post]
  (fn [post [_ _]]
    (select-keys post [:poll-options :poll-votes])))

(re-frame/reg-sub ::poll-options
  ;; :doc Get the poll options with percentages if available.
  :<- [::poll-option-data]
  (fn [{:keys [poll-options poll-votes]} [_ _]]
    (map (fn [{:keys [votes] :as option}]
           (assoc option :percent
                  (when (and votes poll-votes (pos? poll-votes))
                    (-> votes
                        (/ poll-votes)
                        (* 100)
                        (+ 0.5)
                        int))))
         poll-options)))

(re-frame/reg-sub ::can-vote-poll?
  ;; :doc Determine whether the user can vote in the poll.
  :<- [::single-post]
  :<- [::user-attributes]
  :<- [::user/is-authenticated?]
  :<- [::subs/sub-banned?]
  (fn [[{:keys [poll-open]} {:keys [poll-vote]} is-authenticated? sub-banned?]
       _]
    (and poll-open
         is-authenticated?
         (not sub-banned?)
         (nil? poll-vote))))

(defn- set-poll-vote [db id val]
  ;; TODO poll options need to be their own thing
  (let [pid (:pid (single-post db))
        option-vote-path [:poll-options s/ALL #(= (:id %) id) :votes]]
    (-> db
        (assoc-post-user-attributes pid :poll-vote val)
        (update-post pid #(-> %
                              (as-> $ (s/transform option-vote-path
                                                   (if val inc dec) $))
                              (update :poll-votes (if val inc dec)))))))

(re-frame/reg-event-fx ::vote-poll
  ;; :doc Vote on a poll.
  (fn-traced [{:keys [db]} [_ id]]
    (let [pid (:pid (single-post db))
          restore #(set-poll-vote % id nil)]
      {:db (set-poll-vote db id id)
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/cast-vote :pid pid :oid id)
                         {}
                         [::receive-poll-vote {:restore restore}]
                         [::errors/receive-ajax-error ::vote-poll
                          restore]]]]]})))

(re-frame/reg-event-fx ::withdraw-poll-vote
  ;; :doc Withdraw a poll vote.
  (fn-traced [{:keys [db]} [_ _]]
    (let [pid (:pid (single-post db))
          vote-id (:poll-vote (single-post-user-attributes db))
          see? (get-in db [:ui-state :post :see-poll-results])
          restore #(-> %
                       (assoc-in [:ui-state :post :see-poll-results?] see?)
                       (set-poll-vote vote-id vote-id))]
      {:db (-> db
               (assoc-in [:ui-state :post :see-poll-results?] true)
               (set-poll-vote vote-id nil))
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/remove-vote :pid pid)
                         {}
                         [::receive-poll-vote {:restore restore}]
                         [::errors/receive-ajax-error ::withdraw-poll-vote
                          restore]]]]]})))

(re-frame/reg-event-db ::receive-poll-vote
  ;; :doc Receive the server response to a poll vote or withdrawn vote.
  (fn [db [event {:keys [restore]} {:keys [status error]}]]
    (let [db-with-errors (errors/assoc-errors
                          db
                          {:event event
                           :errors (errors/errors-from-do-api error)
                           :describe-error-fn describe-report-post-error})]
      (cond
        (= status "ok") db
        ;; The poll closed message is translated so we really shouldn't
        ;; test it.
        (= error "Poll is closed") (restore db-with-errors)
        ;; If we got here it means the user already voted from another
        ;; browser tab.  When this route is converted to GraphQL we
        ;; should probably get the current state of the user's vote,
        ;; or use a subscription to be up to date.
        :else db-with-errors))))

(defn update-post-user-flair
  "Update the user's flair on all the posts in the sub in the db."
  ;; TODO get that author-flair field out of the post and just use the
  ;; sub object.
  [db sid flair]
  (let [current-user-uid (get-in db [:current-user :uid])]
    (-> db
        (update :reldb rel/transact
                [:update :Post
                 #(assoc % :author-flair flair)
                 [:and [= :sid sid] [= :uid current-user-uid]]]))))
