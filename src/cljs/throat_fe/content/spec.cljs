;; content/spec.cljs --  Content specs for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.spec
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.graphql-spec :as gspec]
   [weavejester.dependency :as dep])
  (:require
   [throat-be.content.spec.content-loader :as-alias content-loader]))

;; Due to GraphQL limitations on integer size, timestamps are
;; sent as strings containing milliseconds since the Unix epoch,
;; but in the db they are converted to integers.

(spec/def ::timestamp int?)

;; Specifications for the content map in the app db.

(spec/def :content/announcement-post
  (spec/keys :req-un [:type.Post/pid]))

(spec/def :content.pagination/more-items-available? (spec/nilable boolean?))
(spec/def :content.pagination/cursor (spec/nilable ::gspec/non-empty-string))

(spec/def :content/motto string?)

(spec/def :content.recent-error/msg
  (spec/or :message string?
           :html vector?))
(spec/def :content.recent-error/event keyword?)
(spec/def :content.recent-error/timestamp #(= (type %) js/Date))
(spec/def :content/recent-error (spec/keys :req-un
                                           [:content.recent-error/msg
                                            :content.recent-error/event
                                            :content.recent-error/timestamp]))
(spec/def :content/recent-errors
  (spec/and set? (spec/coll-of :content/recent-error)))

;; Mail and messages
(spec/def :content.message/userinfo (spec/keys :req-un
                                               [:type.User/uid
                                                :type.User/name]))
(spec/def :content.message/content ::gspec/non-empty-string)
(spec/def :content.message/mid ::gspec/non-empty-string)
(spec/def :content.message/mtype #{:USER_TO_MODS
                                   :MOD_TO_USER_AS_USER
                                   :MOD_TO_USER_AS_MOD
                                   :MOD_DISCUSSION
                                   :USER_NOTIFICATION})
(spec/def :content.message/receiver (spec/nilable :content.message/userinfo))
(spec/def :content.message/receiver-is-mod? boolean?)
(spec/def :content.message/sender :content.message/userinfo)
(spec/def :content.message/sender-is-mod? boolean?)
(spec/def :content.message/sent-with-name-hidden? boolean?)
(spec/def :content.message/time ::gspec/unix-timestamp)
(spec/def :content.message/unread? boolean?)

(spec/def :content.message/message
  (spec/and
   (spec/keys :req-un
              [:content.message/content
               :content.message/mid
               :content.message/mtype
               :content.message/receiver
               :content.message/receiver-is-mod?
               :content.message/sender
               :content.message/sender-is-mod?
               :content.message/sent-with-name-hidden?
               :content.message/time
               :content.message/unread?])
   (fn [{:keys [mtype sent-with-name-hidden? receiver]}]
     (case mtype
       :USER_TO_MODS (and (not sent-with-name-hidden?)
                          (nil? receiver))
       :MOD_TO_USER_AS_USER (and (not sent-with-name-hidden?)
                                 (some? receiver))
       :MOD_TO_USER_AS_MOD sent-with-name-hidden?
       :MOD_DISCUSSION (and (not sent-with-name-hidden?) (nil? receiver))
       :USER_NOTIFICATION (and sent-with-name-hidden? (some? receiver))))))

;; A thread of messages
(spec/def :content.message-thread/id ::gspec/non-empty-string)
(spec/def :content.message-thread/first-message :content.message/message)
(spec/def :content.message-thread/latest-message :content.message/message)
(spec/def :content.message-thread/mailbox #{:INBOX :ARCHIVED})
(spec/def :content.message-thread/reply-count nat-int?)
(spec/def :content.message-thread/sub (spec/keys :req-un [:type.Sub/sid
                                                          :type.Sub/name]))
(spec/def :content.message-thread/subject ::gspec/non-empty-string)

(spec/def :content.message-thread/message-thread
  (spec/keys :req-un [:content.message-thread/id
                      :content.message-thread/first-message
                      :content.message-thread/latest-message
                      :content.message-thread/mailbox
                      :content.message-thread/reply-count
                      :content.message-thread/sub
                      :content.message-thread/subject]))

;; A modmail mailbox is a list of message threads with
;; pagination information.
(spec/def :content.modmail.subs.mailbox/items
  (spec/* :content.message-thread/message-thread))
(spec/def :content.modmail.subs/mailbox
  (spec/keys :req-un [:content.pagination/cursor
                      :content.pagination/more-items-available?
                      :content.modmail.subs.mailbox/items]))

;; Each sub in the modmail content has a map from mailbox keyword
;; to mailbox content.
(spec/def :content.modmail.subs/mailbox-map
  (spec/map-of #{:all :archived :discussions :in-progress :new}
               :content.modmail.subs/mailbox))

;; Modmail contains a map from sub name (or the keyword :all-subs)
;; to the map of mailboxes for that sub (or for all the subs).
(spec/def :content.modmail.subs/all-subs #{:all-subs})
(spec/def :content.modmail.subs/sub-name-or-all-subs
  (spec/or :sub-name :type.Sub/name
           :all-subs :content.modmail.subs/all-subs))
(spec/def :content.modmail/subs
  (spec/nilable (spec/map-of :content.modmail.subs/sub-name-or-all-subs
                             :content.modmail.subs/mailbox-map)))

;; Modmail's message threads are a map from message id to
;; the thread info and the list of messages in the thread.
(spec/def :content.modmail.thread.thread-info/thread-id :content.message-thread/id)
(spec/def :content.modmail.thread.thread-info/parent :content.message/message)
(spec/def :content.modmail.thread.thread-info/items
  (spec/* :content.message/message))
(spec/def :content.modmail.thread/thread-info
  (spec/keys :req-un [:content.modmail.thread.thread-info/thread-id
                      :content.message-thread/subject
                      :content.message-thread/sub
                      :content.pagination/more-items-available?
                      :content.pagination/cursor
                      :content.modmail.thread.thread-info/parent]))
(spec/def :content.modmail/thread
  (spec/nilable (spec/map-of :content.message/mid
                             :content.modmail.thread/thread-info)))

(spec/def :content/modmail (spec/keys :opt-un [:content.modmail/subs
                                               :content.modmail/thread]))

;; Comments

(spec/def :content.comments.comment-data.tree/children
  (spec/*
   (spec/keys :req-un [:type.Comment/cid
                       :content.comments.comment-data.tree/children
                       :content.comments.comment-data.tree/level])))
(spec/def :content.comments.comment-data.tree/level nat-int?)
(spec/def :content.comments.comment-data/tree
  :content.comments.comment-data.tree/children)
(spec/def :content.comments.comment-data/levels
  (spec/nilable (spec/map-of :type.Comment/cid nat-int?)))
(spec/def :content.comments.comment-data/loaded
  (spec/and set?
            (spec/coll-of :type.Comment/cid)))
(spec/def ::content.comments.by-post.by-sort
  (spec/keys :req-un [:content.comments.comment-data/tree
                      :content.comments.comment-data/loaded]))
(spec/def :content.comments.by-post/BEST
  ::content.comments.by-post.by-sort)
(spec/def :content.comments.by-post/NEW
  ::content.comments.by-post.by-sort)
(spec/def :content.comments.by-post/TOP
  ::content.comments.by-post.by-sort)
(spec/def :content.comments.by-post/direct
  (spec/map-of :type.Comment/cid ::content.comments.by-post.by-sort))
(spec/def :content/comments
  (spec/map-of :type.Post/pid
               (spec/keys :opt-un [:content.comments.by-post/BEST
                                   :content.comments.by-post/NEW
                                   :content.comments.by-post/TOP
                                   :content.comments.by-post/direct
                                   :content.comments.comment-data/levels])))

;; UI state for content elements

;; UI state for comments
(spec/def :content.ui-state.comment.by-cid/checked-off? boolean?)
(spec/def :content.ui-state.comment.by-cid/content-history-index (spec/nilable nat-int?))
(spec/def :content.ui-state.comment.by-cid/content-dom-node
  (spec/nilable some?)) ;; how do you spec a dom element?
(spec/def :content.ui-state.comment.by-cid/height nat-int?)
(spec/def :content.ui-state.comment.by-cid/highight #{:new})
(spec/def :content.ui-state.comment.by-cid/hover-collapse (spec/nilable boolean?))
(spec/def :content.ui-state.comment.by-cid/marked-viewed? boolean?)
(spec/def :content.ui-state.comment.by-cid/show-reply-editor? (spec/nilable boolean?))
(spec/def :content.ui-state.comment.by-cid/toggle-collapse boolean?)
(spec/def :content.ui-state/comment
  (spec/map-of :type.Comment/cid
               (spec/nilable
                (spec/keys :opt-un [:content.ui-state.comment.by-cid/checked-off?
                                    :content.ui-state.comment.by-cid/content-dom-node
                                    :content.ui-state.comment.by-cid/height
                                    :content.ui-state.comment.by-cid/highlight
                                    :content.ui-state.comment.by-cid/hover-collapse
                                    :content.ui-state.comment.by-cid/marked-viewed?
                                    :content.ui-state.comment.by-cid/show-reply-editor?
                                    :content.ui-state.comment.by-cid/toggle-collapse]))))

;; A FSM per comment for changing the view of the content
(spec/def :content.ui-state.comment-content-view/_state #{:html :source :editor})
(spec/def :content.ui-state/comment-content-view
  (spec/map-of :type.Comment/cid
               (spec/keys :req-un [:content.ui-state.comment-content-view/_state])))

;; State that applies to the entire comment listing on the single-post page.
(spec/def :content.ui-state.comments/last-toggled-cid
  :type.Comment/cid)
(spec/def :content.ui-state/comments
  (spec/keys :opt-un [:content.ui-state.comments/last-toggled-cid]))

(spec/def :content.ui-state.post/content-height nat-int?)
(spec/def :content.ui-state.post/scroll-to-cid
  (spec/nilable :type.Comment/cid))
(spec/def :content.ui-state.post/show-content-editor? boolean?)
(spec/def :content.ui-state.post/show-edit-post-flair? boolean?)
(spec/def :content.ui-state.post/show-login? boolean?)
(spec/def :content.ui-state.post/show-mod-delete-post-form? boolean?)
(spec/def :content.ui-state.post/show-mod-undelete-post-form? boolean?)
(spec/def :content.ui-state.post/show-post-title-editor? boolean?)
(spec/def :content.ui-state.post/show-report-form? boolean?)
(spec/def :content.ui-state.post/show-source? boolean?)
(spec/def :content.ui-state.post/sort-path
  (spec/or :sort #{[:BEST] [:TOP] [:NEW]}
           :direct (spec/cat :key #{:direct} :val :type.Comment/cid)))
(spec/def :content.ui-state/post
  (spec/keys :opt-un [:content.ui-state.post/content-height
                      :content.ui-state.post/scroll-to-cid
                      :content.ui-state.post/show-content-editor?
                      :content.ui-state.post/show-edit-post-flair?
                      :content.ui-state.post/show-login?
                      :content.ui-state.post/show-mod-delete-post-form?
                      :content.ui-state.post/show-mod-undelete-post-form?
                      :content.ui-state.post/show-post-title-editor?
                      :content.ui-state.post/show-report-form?
                      :content.ui-state.post/show-source?
                      :content.ui-state.post/sort-path]))

(spec/def :content.ui-state.comment-modals/show-mod-delete-comment-form?
  boolean?)
(spec/def :content.ui-state.comment-modals/show-mod-undelete-comment-form?
  boolean?)
(spec/def :content.ui-state/comment-modals
  (spec/keys
   :opt-un
   [:content.ui-state.comment-modals/show-mod-delete-comment-form?
    :content.ui-state.comment-modals/show-mod-undelete-comment-form?]))

(spec/def ::ui-state
  (spec/keys :opt-un [:content.ui-state/post
                      :content.ui-state/comment
                      :content.ui-state/comments
                      :content.ui-state/comment-content-view
                      :content.ui-state/comment-modals]))

(spec/def ::content (spec/keys :req-un [:content/motto
                                        :content/recent-errors]
                               :opt-un [:content/announcement-post
                                        :content/comments
                                        :content/modmail]))

;; Content loader FSM state

(spec/def ::content-loader/_state #{:initial :loading :error :loaded})
(spec/def ::content-loader/epoch int?)
(spec/def ::content-loader/completed
  (spec/map-of some? #{:error :done}))
(spec/def ::content-loader/graph
  #(= (type %) dep/MapDependencyGraph))
(spec/def ::content-loader
  (spec/keys :req-un [::content-loader/_state
                      ::content-loader/epoch
                      ::content-loader/completed
                      ::content-loader/graph]))
