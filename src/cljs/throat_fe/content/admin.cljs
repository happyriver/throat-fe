;; content/admin.cljs -- Administrator pages for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.content.admin
  (:require
   [cljs-time.coerce :as coerce-time]
   [cljs-time.core :as time]
   [clojure.string :as str]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [graphql-builder.parser :refer-macros [defgraphql]]
   [re-frame.core :as re-frame]
   [throat-fe.content.load-more :as load-more]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.ui.forms.admin :as-alias admin-forms]
   [throat-fe.ui.forms.core :as-alias forms]
   [throat-fe.ui.window :as window]))

;; Import graphql queries.
(defgraphql graphql "src/cljs/throat_fe/content/admin.graphql")

;; Site Statistics

(re-frame/reg-event-fx ::load-stats
  ;; :doc Load site statistics.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query {:graphql graphql
                                       :name :stats-all-time
                                       :variables {}
                                       :handler [::set-stats]}]]]}))

(re-frame/reg-event-db ::set-stats
  ;; :doc Receive site statistics.
  (fn-traced [db [_ {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event ::set-stats :errors errors})
          (assoc-in [:content :stats] (:site-stats data))))))

(re-frame/reg-sub ::stats
  ;; :doc Extract site statistics.
  (fn [db _]
    (get-in db [:content :stats])))

(defn date-to-long
  "Return the start of the day as milliseconds since the Unix epoch."
  [date]
  (-> (time/date-time (:year date)
                      (:month date)
                      (:day date))
      coerce-time/to-long))

(defn long-to-date
  "Return a time in milliseconds since the epoch (in a string) as a date."
  [ms]
  (let [datetime (-> (js/parseInt ms)
                     coerce-time/from-long)]
    {:year (time/year datetime)
     :month (time/month datetime)
     :day (time/day datetime)}))

(re-frame/reg-event-fx ::load-stats-timespan
  ;; :doc Load site statistics for a timespan.
  (fn-traced [{:keys [db]} [_ _]]
    (let [until (-> (get-in db [:view :options ::stats-until])
                    date-to-long)
          since (-> (get-in db [:view :options ::stats-since])
                    date-to-long)]
      {:fx [[:dispatch [::graphql/query {:graphql graphql
                                         :name :stats-timespan
                                         :variables {:since (str since)
                                                     :until (str until)}
                                         :handler [::set-stats-timespan
                                                   {:since since
                                                    :until until}]}]]]})))

(re-frame/reg-event-db ::set-stats-timespan
  ;; :doc Receive site statistics for a timespan.
  (fn-traced [db [_ {:keys [since until response]}]]
    (let [{:keys [data errors]} response
          now-long (coerce-time/to-long (time/now))
          complete? (and (< until now-long)
                         (< since now-long))
          entries (get-in db [:content :stats-timespan])
          match? #(= (select-keys % [:since :until])
                     {:since since :until until})
          entry {:since since
                 :until until
                 :complete? complete?
                 :stats (:site-stats data)}
          new-entries (if (some match? entries)
                        (map #(if (match? %) entry %) entries)
                        (conj entries entry))]
      (-> db
          (errors/assoc-errors {:event ::set-stats-timespan :errors errors})
          (assoc-in [:content :stats-timespan] new-entries)))))

(re-frame/reg-sub ::stats-timespan-entries
  ;; :doc Get entries in list of recent statistics.
  (fn [db _]
    (get-in db [:content :stats-timespan])))

(re-frame/reg-sub ::stats-timespan-entry
  ;; :doc Extract current recent site statistics entry.
  :<- [::stats-since-date]
  :<- [::stats-until-date]
  :<- [::stats-timespan-entries]
  (fn [[since-date until-date entries] _]
    (let [since (date-to-long since-date)
          until (date-to-long until-date)]
      (some (fn [entry]
              (when (= (select-keys entry [:since :until])
                       {:since since :until until})
                entry))
            entries))))

(re-frame/reg-sub ::stats-timespan-complete?
  ;; :doc Get whether recent statistics are final (no partial last day).
  :<- [::stats-timespan-entry]
  (fn [entry _]
    (:complete? entry)))

(re-frame/reg-sub ::stats-timespan
  ;; :doc Extract recent site statistics.
  :<- [::stats-timespan-entry]
  (fn [entry _]
    (:stats entry)))

(re-frame/reg-event-fx ::load-visitor-counts
  ;; :doc Load visitor counts, daily and weekly.
  (fn-traced [{:keys [db]} [_ option]]
    (let [start-date (get-in db [:view :options ::visitor-count-date option])
          since (date-to-long start-date)
          interval (if (= option :daily)
                     (time/days 7)
                     (time/months 1))
          until-date (time/plus (time/date-time (:year start-date)
                                                (:month start-date)
                                                (:day start-date))
                                interval)
          until (coerce-time/to-long until-date)
          now-long (coerce-time/to-long (time/now))
          complete? (and (< until now-long) (< since now-long))]
      {:db (assoc-in db [:content :visitor-counts-complete? option] complete?)
       :fx [[:dispatch
             [::graphql/query
              {:graphql graphql
               :name :visitor-counts
               :id (str :visitor-counts option)
               :variables {:since (str since)
                           :until (str until)
                           :by (if (= option :daily) 1 7)}
               :handler [::set-visitor-counts {:option option}]}]]]})))

(re-frame/reg-event-db ::set-visitor-counts
  ;; :doc Receive visitor counts.
  (fn-traced [db [_ {:keys [option response]}]]
    (let [{:keys [data errors]} response
          entries (:site-visitor-counts data)
          dates (into {} (map (fn [rec]
                                [(js/parseInt (:start rec)) (:count rec)])
                              entries))]
      (-> db
          (errors/assoc-errors {:event ::set-visitor-counts :errors errors})
          (update-in [:content :visitor-counts option] merge dates)))))

(re-frame/reg-sub ::visitor-counts-complete?
  ;; :doc Extract whether visitor counts may include incomplete data.
  (fn [db [_ option]]
    (get-in db [:content :visitor-counts-complete? option])))

(re-frame/reg-sub ::visitor-count-data
  ;; :doc Extract visitor count data.
  (fn [db [_ option]]
    (get-in db [:content :visitor-counts option])))

(re-frame/reg-sub ::visitor-counts
  ;; :doc Get visitor count data for the currently selected date.
  ;; :doc Provide unpacked dates and an average.
  (fn [[_ option]]
    [(re-frame/subscribe [::visitor-count-data option])
     (re-frame/subscribe [::visitor-count-date option])])

  (fn [[data start-date] [_ option]]
    (let [num (if (= option :daily) 7 4)
          by (if (= option :daily) 1 7)
          start-datetime (time/date-time (:year start-date)
                                         (:month start-date)
                                         (:day start-date))
          dates (map (fn [day]
                       (-> start-datetime
                           (time/plus (time/days (* day by)))
                           coerce-time/to-long))
                     (range num))
          selected-values (map (fn [k] [k (or (get data k) 0)])
                               dates)
          entries (map (fn [[start count]]
                         (assoc (long-to-date start) :value count))
                       selected-values)
          sum (reduce + (map :value entries))
          average (-> (/ sum num)
                      (+ 0.5)
                      int)]
      {:entries entries
       :average average})))

(defn todays-date
  "Return the current date as a map."
  []
  (let [now (time/now)]
    {:year (time/year now)
     :month (time/month now)
     :day (time/day now)}))

(defn week-ago-date
  "Return the date from one week ago."
  []
  (let [then (time/minus (time/now) (time/days 7))]
    {:year (time/year then)
     :month (time/month then)
     :day (time/day then)}))

(defn month-ago-date
  "Return the date from one month ago."
  []
  (let [then (time/minus (time/now) (time/months 1))]
    {:year (time/year then)
     :month (time/month then)
     :day (time/day then)}))

(defn update-date
  "Update a date map with a new month, day or year.
  Fix the day if the number of days in the month changes."
  [previous key value]
  (let [date (assoc previous key (js/parseInt value))
        days-in-month (time/day (time/last-day-of-the-month (:year date)
                                                            (:month date)))]
    {:year (:year date)
     :month (:month date)
     :day (if (> (:day date) days-in-month) 1 (:day date))}))

(defn init-dates
  [db]
  (-> db
      (update-in [:view :options] assoc
                 ::stats-since (month-ago-date)
                 ::stats-until (todays-date))
      (update-in [:view :options ::visitor-count-date] assoc
                 :daily (week-ago-date)
                 :weekly (month-ago-date))))

(re-frame/reg-event-fx ::set-stats-since-date
  ;; :doc Set the month, day or year of the start date for recent statistics.
  (fn-traced [{:keys [db]} [_ key value]]
    (let [old (get-in db [:view :options ::stats-since])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::stats-since] new)
       :fx [(when (not= old new)
              [:dispatch [::load-stats-timespan]])]})))

(re-frame/reg-sub ::stats-since-date
  ;; :doc Extract the start date for recent statistics.
  (fn [db _]
    (get-in db [:view :options ::stats-since])))

(re-frame/reg-event-fx ::set-stats-until-date
  ;; :doc Set the month, day or year of the end date for recent statistics.
  (fn-traced [{:keys [db]} [_ key value]]
    (let [old (get-in db [:view :options ::stats-until])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::stats-until] new)
       :fx [(when (not= old new)
              [:dispatch [::load-stats-timespan]])]})))

(re-frame/reg-sub ::stats-until-date
  ;; :doc Extract the end date for recent statistics.
  (fn [db _]
    (get-in db [:view :options ::stats-until])))

(re-frame/reg-event-fx ::set-visitor-count-date
  ;; :doc Set the month, day or year of the start date for counting visitors.
  (fn-traced [{:keys [db]} [_ option key value]]
    (let [old (get-in db [:view :options ::visitor-count-date option])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::visitor-count-date option] new)
       :fx [(when (not= old new)
              [:dispatch [::load-visitor-counts option]])]})))

(re-frame/reg-sub ::visitor-count-date
  ;; :doc Extract the start date for visitor counts.
  (fn [db [_ option]]
    (get-in db [:view :options ::visitor-count-date option])))

;; Banned strings in user names.

(re-frame/reg-event-fx ::load-banned-username-strings
  ;; :doc Request the list of banned username strings and affected users.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql
                       :name :banned-username-strings
                       :variables {}
                       :handler [::receive-banned-username-strings]}]]]}))

(re-frame/reg-event-db ::receive-banned-username-strings
  ;; :doc Receive the list of banned username strings and affected users.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:content :banned-username-strings]
                    (:banned-username-strings data))))))

(re-frame/reg-event-fx ::ban-username-string
  ;; :doc Send a new string to ban in usernames.
  (fn-traced [_ [_ {:keys [content]} update-ban-form]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :ban-string-in-usernames
                       :id (str :ban-string-in-usernames content)
                       :variables {:banned content}
                       :handler [::receive-ban-username-string
                                 {:update-ban-form update-ban-form}]}]]]}))

(re-frame/reg-event-fx ::receive-ban-username-string
  ;; :doc Receive the response from the server after adding a new string to ban.
  (fn-traced [{:keys [db]} [_ {:keys [update-ban-form response]}]]
    (let [{:keys [data errors]} response
          existing (get-in db [:content :banned-username-strings])
          new-ban (:ban-string-in-usernames data)
          banned (:banned new-ban)
          present? (some #(= banned (:banned %)) existing)
          updated (cond
                    present? (map #(if (= banned (:banned %)) new-ban %)
                                  existing)
                    new-ban (conj existing new-ban)
                    :else existing)]
      {:db (-> db
               (errors/assoc-errors {:event ::ban-username-string :errors errors})
               (assoc-in [:content :banned-username-strings] updated))
       :fx [[:dispatch update-ban-form]]})))

(re-frame/reg-event-fx ::unban-username-string
  ;; :doc Unban a string in usernames.
  (fn-traced [_ [_ banned]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :unban-string-in-usernames
                       :id (str :unban-string-in-usernames banned)
                       :variables {:banned banned}
                       :handler [::receive-unban-username-string]}]]]}))

(re-frame/reg-event-db ::receive-unban-username-string
  ;; :doc Receive the response from the server after removing a banned string.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          banned (:unban-string-in-usernames data)
          existing (get-in db [:content :banned-username-strings])
          updated (filter #(not= (:banned %) banned) existing)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:content :banned-username-strings] updated)))))

(re-frame/reg-event-db ::set-ban-username-string-sort
  ;; :doc Change the sort type or direction in the banned strings table.
  ;; :doc Options are :string and :count.
  (fn-traced [db [_ typ]]
    (let [toggle? (= typ (get-in db [:view :options :sort-typ]))]
      (-> db
          (assoc-in [:view :options :sort-typ] typ)
          (update-in [:view :options :sort-asc?]
                     #(if toggle?
                        (not %)
                        (= typ :string)))))))

(re-frame/reg-event-db ::show-all-users
  ;; :doc Set the flag to show all users instead of just the first 5.
  ;; :doc Applies to one of the strings in the banned username strings table.
  (fn-traced [db [_ banned]]
    (assoc-in db [:view :options :show-all banned] true)))

(re-frame/reg-sub ::banned-username-data
  ;; :doc Extract the list of banned strings in usernames with user lists.
  (fn [db _]
    (get-in db [:content :banned-username-strings])))

(re-frame/reg-sub ::banned-username-sort-options
  ;; :doc Extract the sort options for banned strings in usernames.
  (fn [db _]
    (select-keys (get-in db [:view :options]) [:sort-typ :sort-asc?])))

(re-frame/reg-sub ::banned-username-show-all
  ;; :doc Extract the show all option for usernames in the banned strings table.
  (fn [db _]
    (get-in db [:view :options :show-all])))

(re-frame/reg-sub ::banned-username-strings
  ;; :doc Compute the sorted list of banned strings in usernames.
  :<- [::banned-username-data]
  :<- [::banned-username-show-all]
  :<- [::banned-username-sort-options]
  (fn [[data show-all {:keys [sort-typ sort-asc?]}] _]
    (let [num-to-show 5
          data-with-show (map
                          (fn [{:keys [banned users] :as elem}]
                            (let [show-all? (get show-all banned)]
                              (assoc elem
                                     :show (if show-all?
                                             (count users) num-to-show)
                                     :more? (and (not show-all?)
                                                 (> (count users) num-to-show)))))
                          data)
          extract (if (= sort-typ :string)
                    :banned
                    #(count (:users %)))
          sorted (sort-by extract data-with-show)]
      (if sort-asc?
        sorted
        (reverse sorted)))))

;;; Admin Invite Codes page

;; Fetch the data for the admin invite code page.

(def invite-code-table-request-size
  "Number of invite codes to get from the server on each request."
  100)

(re-frame/reg-event-fx ::load-invite-codes-and-settings
  ;; :doc Request the invite code settings and the first page of invite codes.
  (fn-traced [{:keys [db]} _]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql
                       :name :invite-codes-and-settings
                       :variables {:first invite-code-table-request-size}
                       :handler [::receive-invite-codes-and-settings]}]]]}))

(defn update-invite-codes
  "Update the list of invite codes to add new data at the end.
  Pass the results of the invite_codes query, after kebab-case."
  [existing-codes new-data]
  (let [new-codes (->> (:edges new-data)
                       (map :node))]
    (-> existing-codes
        (merge (:page-info new-data))
        (assoc :codes (concat (:codes existing-codes) new-codes)))))

(defn- settings-update-fx
  [{:keys [required visible minimum-level per-user]}]
  [[:dispatch [::forms/edit ::admin-forms/invite-code-settings
               :required? required]]
   [:dispatch [::forms/edit ::admin-forms/invite-code-settings
               :visible? visible]]
   [:dispatch [::forms/edit ::admin-forms/invite-code-settings
               :minimum-level (str minimum-level)]]
   [:dispatch [::forms/edit ::admin-forms/invite-code-settings
               :per-user (str per-user)]]])

(re-frame/reg-event-fx ::receive-invite-codes-and-settings
  ;; :doc Receive the invite code settings and the first page of invite codes.
  (fn-traced [{:keys [db]} [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          {:keys [invite-code-settings invite-codes]} data]
      {:db
       (-> db
           (errors/assoc-errors {:event event :errors errors})
           (assoc-in [:content :invite-code-settings] invite-code-settings)
           (assoc-in [:content :invite-codes :all]
                     (update-invite-codes {} invite-codes))
           (assoc-in [:status :content] :loaded))
       :fx (concat (settings-update-fx invite-code-settings)
                   [[:dispatch-later
                     [{:ms 100
                       :dispatch [::load-more/check-load-more]}]]])})))

(re-frame/reg-event-fx ::update-invite-code-settings
  ;; :doc Send modified invite code settings to the server.
  (fn-traced [_ [_ {:keys [required?
                           visible?
                           minimum-level
                           per-user]} update-ic-form]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :invite-code-settings
                       :id :invite-code-settings
                       :variables {:required required?
                                   :visible visible?
                                   :minimum_level (js/parseInt minimum-level)
                                   :per_user (js/parseInt per-user)}
                       :handler [::receive-invite-code-settings
                                 {:update-ic-form update-ic-form}]}]]]}))

(re-frame/reg-event-fx ::receive-invite-code-settings
  ;; :doc Receive the response after changing invite code settings.
  (fn-traced [{:keys [db]} [_ {:keys [update-ic-form response]}]]
    (let [ {:keys [data errors]} response
          existing (get-in db [:content :invite-code-settings])
          new (:set-invite-code-settings data)
          current (merge existing new)]
      {:db (-> db
               (errors/assoc-errors {:event ::update-invite-code-settings
                                     :errors errors})
               (assoc-in [:content :invite-code-settings] current))
       :fx (concat (settings-update-fx current)
                   [[:dispatch update-ic-form]])})))

(re-frame/reg-sub ::invite-code-tables
  ;; :doc Extract the invite code table.
  (fn [db _]
    (get-in db [:content :invite-codes])))

(re-frame/reg-sub ::invite-code-search-code
  (fn [db _]
    (get-in db [:view :options :search])))

(re-frame/reg-sub ::invite-codes-by-search-code
  :<- [::invite-code-tables]
  :<- [::invite-code-search-code]
  (fn [[tables search] _]
    (get-in tables [search :codes])))

(defn format-code-date
  "Format a date in an invite code record, given a java unix epoch
  value in a string, and the current time as a java unix epoch date
  in an integer.  If the date given is nil, return nil.  If `time?`
  is true and the date is within 48 hours of right now, include the
  time in the formatted date."
  [ms now time?]
  (when ms
    (let [ms-int (js/parseInt ms)
          diff (js/Math.abs (- now ms-int))
          datetime (js/Date. ms-int)]
      (if (and time? (< diff (* 1000 60 60 24 2)))
        (.toLocaleString datetime)
        (.toLocaleDateString datetime)))))

(defn format-code-dates
  "Format the dates in an invite code record from the server."
  [{:keys [expires uses max-uses] :as code} now]
  (let [expires-ms (when expires (js/parseInt expires))]
    (-> code
        (update :created-on format-code-date now false)
        (update :expires format-code-date now true)
        (assoc :expired? (or (>= uses max-uses)
                             (and expires-ms (> now expires-ms)))))))

(re-frame/reg-sub ::invite-codes
  ;; :doc Produce the invite code table with expirations.
  :<- [::invite-codes-by-search-code]
  :<- [::window/now]
  (fn [[codes now] _]
    (map #(format-code-dates % now) codes)))

(re-frame/reg-sub ::invite-codes-empty?
  :<- [::invite-codes]
  (fn [codes _]
    (zero? (count codes))))

(re-frame/reg-sub ::invite-code-ids
  ;; :doc Produce the list of invite code ids.
  :<- [::invite-codes]
  (fn [codes _]
    (map :id codes)))

(re-frame/reg-sub ::invite-code
  ;; :doc Get data for one invite code.
  :<- [::invite-codes]
  (fn [codes [_ id]]
    (first (filter #(= (:id %) id) codes))))

(re-frame/reg-event-fx ::load-invite-codes
  ;; :doc Request a page of invite codes from the server.
  (fn-traced [{:keys [db]} [_ search]]
    (let [invite-codes (get-in db [:content :invite-codes search])
          {:keys [has-next-page end-cursor]} invite-codes]
      (when (or (nil? invite-codes) has-next-page)
        {:db (assoc-in db [:status :content]
                       (if (nil? invite-codes)
                         :loading :loaded))
         :fx [[:dispatch [::graphql/query
                          {:graphql graphql
                           :name :invite-codes
                           :variables {:first invite-code-table-request-size
                                       :after end-cursor
                                       :search (when (not= search :all) search)}
                           :handler [::receive-invite-codes
                                     {:search search}]}]]]}))))

(re-frame/reg-event-fx ::receive-invite-codes
  ;; :doc Receive a page of invite codes from the server.
  (fn-traced [{:keys [db]} [event {:keys [search response]}]]
    (let [{:keys [data errors]} response
          {:keys [invite-codes]} data
          existing-codes (get-in db [:content :invite-codes search])]
      {:db
       (cond-> db
         true (errors/assoc-errors {:event event :errors errors})
         true (assoc-in [:status :content] :loaded)
         (nil? errors) (-> (assoc-in [:content :invite-codes search]
                                     (update-invite-codes existing-codes
                                                          invite-codes))))
       :fx [[:dispatch-later [{:ms 100
                               :dispatch [::load-more/check-load-more]}]]]})))

(re-frame/reg-event-db ::set-bottom-of-invite-code-table
  ;; :doc Keep track of the bottom element of the invite code table in the db.
  (fn-traced [db [_ elem]]
    (assoc-in db [:ui-state :invite-code-table :bottom] elem)))

(re-frame/reg-event-fx ::invite-code-table-check-load-more
  ;; :doc Check whether more invite codes need to be loaded.
  (fn-traced [{:keys [db]} [_ _]]
    (let [search (get-in db [:view :options :search])
          elem (get-in db [:ui-state :invite-code-table :bottom])
          y (when elem (.-offsetTop elem))
          more? (get-in db [:content :invite-codes search :has-next-page])
          {:keys [height scroll-y]} (get-in db [:ui-state :window])]
      {:fx [(when (and more?
                       elem
                       (< (- y 500) (+ height scroll-y)))
              [:dispatch [::load-invite-codes search]])]})))

(re-frame/reg-event-fx ::generate-invite-code
  ;; :doc Generate a new invite code.
  (fn-traced [_ [_ {:keys [code max-uses expiration]} update-ic-form]]
    (let [expires (when expiration (str (.getTime expiration)))]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :generate-invite-code
                         :variables {:code code
                                     :max_uses (js/parseInt max-uses)
                                     :expires expires}
                         :handler [::receive-generate-invite-code
                                   {:update-ic-form update-ic-form}]}]]]})))

(re-frame/reg-event-fx ::receive-generate-invite-code
  ;; :doc Receive the response after generating an invite code.
  (fn-traced [{:keys [db]} [event {:keys [update-ic-form response]}]]
    (let [{:keys [data errors]} response
          code (:generate-invite-code data)]
      {:db (-> db
               (errors/assoc-errors {:event event :errors errors})
               (update-in [:content :invite-codes :all :codes] #(cons code %))
               (update-in [:content :invite-codes (:code code)] #(cons code %)))
       :fx [[:dispatch update-ic-form]]})))

(re-frame/reg-event-fx ::expire-invite-codes
  ;; :doc Change the expiration of selected invite codes
  (fn-traced [_ [_ {:keys [selected option expiration]} update-form]]
    (let [expires (when expiration (str (.getTime expiration)))
          expire-time (condp = option
                        :at expires
                        :never nil
                        :now (str (.getTime (js/Date.))))]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :expire-invite-codes
                         :variables {:codes selected :expires expire-time}
                         :handler [::receive-expire-invite-codes
                                   {:expire-time expire-time
                                    :update-form update-form}]}]]]})))

(re-frame/reg-event-fx ::receive-expire-invite-codes
  ;; :doc Receive the response after changing invite code expirations.
  (fn-traced [{:keys [db]} [_ {:keys [expire-time update-form response]}]]
    (let [{:keys [data errors]} response
          search (get-in db [:view :options :search])
          ids (set (:expire-invite-codes data))
          update-expire #(if (contains? ids (:id %))
                           (assoc % :expires expire-time)
                           %)]
      {:db (-> db
               (errors/assoc-errors {:event ::expire-invite-codes
                                     :errors errors})
               (update-in [:content :invite-codes search :codes]
                          #(map update-expire %)))
       :fx [[:dispatch update-form]
            [:dispatch [::window/clock-update]]]})))

(def SearchInviteCodeForm
  [[:from :FormState]
   [:where [= [:_ ::admin-forms/search-invite-code] :id]]])

(re-frame/reg-event-fx ::search-invite-code
  ;; :doc Search for invite codes.
  (fn-traced [{:keys [db]} _]
    (let [trimmed-code (-> (:reldb db)
                           (rel/row SearchInviteCodeForm)
                           :code
                           str/trim)]
      {:db (assoc-in db [:view :options :search] trimmed-code)
       :fx [[:dispatch [::load-invite-codes trimmed-code]]
            [:dispatch [::forms/edit ::admin-forms/expire-invite-code
                        :selected #{}]]]})))

(def ExpireInviteCodeForm
  [[:from :FormState]
   [:where [= [:_ ::admin-forms/expire-invite-code] :id]]])

(re-frame/reg-event-fx ::select-invite-code
  ;; :doc Toggle whether an invite code id is selected
  (fn-traced [{:keys [db]} [_ id selected?]]
    (let [selections (-> (:reldb db)
                         (rel/row ExpireInviteCodeForm)
                         :selected)
          new-selections (if selected?
                           (conj selections id)
                           (disj selections id))]
      {:fx [[:dispatch [::forms/edit ::admin-forms/expire-invite-code
                        :selected new-selections]]]})))

(re-frame/reg-event-fx ::show-all-invite-codes
  ;; :doc Show all invite codes, and deselect any that are selected.
  (fn-traced [{:keys [db]} _]
    {:db (assoc-in db [:view :options :search] :all)
     :fx [[:dispatch [::forms/edit ::admin-forms/expire-invite-code
                      :selected #{}]]]}))
