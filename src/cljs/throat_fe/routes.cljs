;; routes.cljs -- Routing for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.routes
  (:require
   [bidi.bidi :as bidi]
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [pushy.core :as pushy]
   [re-frame.core :as re-frame]
   [throat-fe.content.admin :as admin]
   [throat-fe.content.auth :as auth]
   [throat-fe.content.comment :as comment]
   [throat-fe.content.donate :as donate]
   [throat-fe.content.loader :as loader]
   [throat-fe.content.messages :as messages]
   [throat-fe.content.post :as post]
   [throat-fe.content.posts :as posts]
   [throat-fe.content.settings :as settings]
   [throat-fe.content.spec]
   [throat-fe.content.subs :as subs]
   [throat-fe.errors :as errors]
   [throat-fe.log :as log]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.admin :as admin-forms]
   [throat-fe.ui.forms.auth :as auth-forms]
   [throat-fe.ui.forms.compose-message :as compose-message]
   [throat-fe.ui.forms.donate :as donate-form]
   [throat-fe.ui.forms.moderation :as mod-forms]
   [throat-fe.ui.forms.sub :as sub-forms]
   [throat-fe.ui.page :as page]
   [throat-fe.ui.page-title :as page-title]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.util :as util]
   [weavejester.dependency :as dep])
  (:import [goog Uri]))

;; Spec the :view key in the db.
(defmulti view-type :active-panel)
(spec/def ::view
  (spec/multi-spec view-type :active-panel))

(defmethod view-type :initializing [_]
  (spec/keys :req-un [::active-panel]))

(def handler-map
  "Map the keywords in the routing table to the events to dispatch."
  ;; First item in the returned vector is an event; the second element
  ;; is a map containing parameters which will be merged into the
  ;; route parameters from the URL and passed to the event.
  ;;
  ;; If a keyword maps to another keyword, look up the vector
  ;; associated with that keyword instead.
  {:admin/banned-user-names        [::show-admin-banned-user-names]
   :admin/invitecodes              [::show-admin-invite-codes]
   :admin/stats                    [::show-admin-stats]
   :auth/delete-account            [::show-delete-account]
   :auth/login                     [::show-login]
   :auth/register                  [::show-registration]
   :auth/confirm-registration      [::show-confirm-registration]
   :auth/verify-registration       [::show-verify-registration]
   :auth/resend-verification-email [::show-resend-verification-email]
   :auth/start-reset               [::show-start-reset-password]
   :auth/complete-reset            [::show-complete-reset-password]
   :auth/edit-account              [::show-edit-account]
   :auth/rename-account            [::show-rename-account]
   :auth/confirm-email             [::show-confirm-email]
   :donate/cancel                  [::show-donate-cancel]
   :donate/funding-progress        [::show-funding-progress]
   :donate/setup                   [::show-donate-setup]
   :donate/success                 [::show-donate-success]
   :mod/admin                      [::show-mod-dashboard {:show-other-circles? true}]
   :mod/index                      [::show-mod-dashboard {:show-other-circles? false}]
   :modmail/compose                [::show-modmail-compose {}]
   :modmail/mailbox                [::show-modmail {}]
   :modmail/thread                 [::show-modmail {}]
   :sub/contact-mods               [::show-contact-mods]
   :sub/edit-post-types            [::show-mod-edit-post-types]
   :sub/edit-sub-flairs            [::show-mod-edit-post-flairs]
   :sub/edit-sub-rules             [::show-mod-edit-sub-rules]
   :sub/view-direct-link           [::show-single-post]
   :sub/view-single-post           [::show-single-post]
   :sub/view-single-post-test      [::show-single-post-test]
   :sub/view-direct-link-test      [::show-single-post-test]
   :subs/submit-post               [::show-submit-post]
   :wiki/show-license              [::show-wiki-license {}]})

(defn lookup-event
  "Look up the event handler in 'handler-map'."
  [handler]
  (when handler
    (loop [h handler]
      (let [map-entry (h handler-map)]
        (if (keyword? map-entry)
          (recur map-entry)
          map-entry)))))

(defn- unpack-query-string
  "Return the URL query parameters as a map, given a goog.Uri."
  [uri]
  (let [query-data (.getQueryData ^Uri uri)
        params (zipmap ^js/Object (.getKeys query-data)
                       ^js/Object (.getValues query-data key))]
    ;; Keywordize the keys and ignore empty strings.
    (reduce-kv (fn [m k v]
                 (if (= "" v)
                   m
                   (assoc m (keyword k) v)))
               {} params)))

(defn parse-url
  "Parse a URL into handler keyword, route params and url params.
  Return a map."
  [routes url]
  (log/debug {:url url} "Matching url")
  (let [uri (Uri. url)
        uri-path (.getPath uri)
        ;; Remove the trailing "/" if there is one (except for the root path).
        path (cond
               (= uri-path "/") "/"
               (str/ends-with? uri-path "/") (apply str (butlast uri-path))
               :else uri-path)
        url-params (unpack-query-string uri)
        fragment (.getFragment uri)
        match (bidi/match-route routes path)]
    (log/debug {:url url :url-params url-params :path path
                :fragment fragment
                :match match
                :route-params (:route-params match)
                :handler (:handler match)}
               "Matched url")
    (assoc match :url-params url-params :fragment fragment)))

(defn- url-event
  "Parse a URL and construct the event to dispatch for it."
  [routes url]
  (try
    (let [parsed (parse-url routes url)
          {:keys [handler route-params url-params fragment]} parsed
          [event event-params] (lookup-event handler)]
      (cond
        event [event (-> url-params
                         (merge route-params event-params)
                         (assoc ::fragment fragment))]
        (nil? handler) [::show-error :unmatched-route]
        handler (log/warn {:url url :handler handler}
                          "Route not implemented by throat-fe")))
    (catch js/Error e
      (log/error {:exception e} "url-event matching-error"))))

(defn- dispatch-route [event]
  (log/info {:event event} "dispatch-route")
  (re-frame/dispatch [::errors/clear-errors])
  (re-frame/dispatch [::window/close-menu])
  (re-frame/dispatch [::window/set-force-day-mode false])
  (re-frame/dispatch [::settings/init-subscriptions (= ::show-funding-progress
                                                       (first event))])
  (re-frame/dispatch [::settings/stop-subscriptions-on-routing])
  (re-frame/dispatch [::dispatch-on-leave-events])
  (re-frame/dispatch [::loader/cleanup])
  (re-frame/dispatch [::inc-epoch])
  (re-frame/dispatch event)
  (re-frame/dispatch [::settings/choose-new-motto]))

(re-frame/reg-event-db ::inc-epoch
  (fn-traced [db [_ _]]
    (update db :epoch inc)))

(re-frame/reg-event-fx ::dispatch-on-leave-events
  (fn-traced [{:keys [db]} [_ _]]
    {:db (update db :view dissoc :on-leave)
     :fx (map (fn [ev]
                [:dispatch ev]) (get-in db [:view :on-leave]))}))

(def history
  "Application pushState instance."
  (atom nil))

(defn app-routes [routes]
  (reset! history
          (pushy/pushy dispatch-route (partial url-event routes)))
  (pushy/start! @history))

(re-frame/reg-fx ::redirect
  ;; :doc Do a redirect.
  ;; :doc Switching to another throat-fe page without reloading
  ;; :doc the index query can be done with ::set-url.
  (fn-traced [value]
    (log/debug {:value value} "redirecting")
    (set! (.. js/window -location -href) value)))

(re-frame/reg-fx ::set-url
  ;; :doc Change the page, and add it to the browser history.
  ;; :doc Only use this for throat-fe pages.
  (fn-traced [url]
    (log/debug {:url url} "set-url switching location")
    (pushy/set-token! @history url)))

(re-frame/reg-fx ::replace-url
  ;; :doc Change the page, and don't add it to the browser history.
  ;; :doc Only use this for throat-fe pages.
  (fn-traced [url]
    (log/debug {:url url} "replace-url updating location")
    (pushy/replace-token! @history url)))

(re-frame/reg-event-fx ::show-post-listing
  ;; :doc Show a post listing page.
  (fn-traced [{:keys [db]} [_ options]]
    {:db (assoc db :view (-> (:view db)
                             (assoc :active-panel :post-listing)
                             (assoc :options options)))
     :fx [[:dispatch [::posts/load-posts options]]
          [:dispatch [::page-title/update nil]]]}))

;; Spec the view options for the post listing page.
(spec/def :routes.post-listing/post-source #{:all :subscriptions})
(spec/def :routes.post-listing/sort #{:hot :top :new})
(spec/def :routes.post-listing/options
  (spec/keys :req-un [:routes.post-listing/post-source
                      :routes.post-listing/sort]))
(defmethod view-type :post-listing [_]
  (spec/keys :req-un [::active-panel
                      :routes.post-listing/options]))

(re-frame/reg-event-fx ::show-wiki-license
  ;; :doc Show the software license page.
  (fn-traced [{:keys [db]} [_ _]]
    (merge
     {:db (assoc db :view (-> (:view db)
                              (assoc :active-panel :wiki-page)
                              (assoc :options {:page :license})))
      :fx [[:dispatch [::settings/load-software-licenses]]
           [:dispatch [::page-title/update (trx db "Licenses")]]]})))

;; Spec the view options for wiki pages.
(spec/def :routes.wiki-page/page #{:license})
(spec/def :routes.wiki-page/options
  (spec/keys :req-un [:routes.wiki-page/page]))
(defmethod view-type :wiki-page [_]
  (spec/keys :req-un [::active-panel
                      :routes.wiki-page/options]))

(re-frame/reg-event-fx ::show-donate-setup
  ;; :doc Show the donate setup page.
  [user/authenticated?]
  (fn-traced [{:keys [db authenticated?]} [_ _]]
    {:db (-> db
             (assoc :view {:active-panel :donate-setup :options nil})
             donate-form/initialize-donate-form
             (assoc-in [:status :content] (if authenticated?
                                            :loading
                                            :loaded))
             (auth-forms/initialize-login-form
              (route-util/url-for db :donate/setup)))
     :fx [(when authenticated?
            [:dispatch [::donate/load-subscription]])
          (when authenticated?
            [:dispatch [::donate/subscribe-funding-progress]])]}))

;; Spec the view options for the donate page.
(defmethod view-type :donate-setup [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-donate-success
  ;; :doc Show the donate acknowledgement page.
  (fn-traced [{:keys [db]} [_ {:keys [session_id]}]]
    (log/debug {:session_id session_id} "show-donate-success")
    {:db (-> db
             (assoc :view {:active-panel :donate-success
                           :options nil})
             (assoc-in [:content :donate-session] nil))
     :fx [(when session_id
            [:dispatch [::donate/load-session session_id]])]}))

;; Spec the view options for the donate acknowledgement page.
(defmethod view-type :donate-success [_]
  (spec/keys :req-un [::active-panel]))

;; TODO this needs to get cancel info from the server.
(re-frame/reg-event-db ::show-donate-cancel
  ;; :doc Show the donate cancel page.
  (fn-traced [db [_ _]]
    (let [{:keys [amount recurring?]} (get-in db [:content :donation-intent])]
      (assoc db :view {:active-panel :donate-cancel
                       :options {:recurring? recurring?
                                 :amount amount}}))))

(defmethod view-type :donate-cancel [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-funding-progress
  ;; :doc Show the funding goal and progress (intended for an iframe).
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc db :view {:active-panel :donate-funding-progress})
     :fx [[:dispatch [::settings/subscribe
                      [::donate/subscribe-funding-progress]]]
          [:window/on-message {:id :listen-day-night
                               :dispatch window/listen-day-night}]]}))

(defmethod view-type :donate-funding-progress [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-mod-dashboard
  ;; :doc Show the moderator dashboard.
  (fn-traced [{:keys [db]} [_ options]]
    {:db (update db :view assoc
                 :active-panel :mod-dashboard
                 :options options)
     :fx [(if (:show-other-circles? options)
            [:dispatch [::subs/load-all-subs [::subs/load-admin-mod-stats]]]
            [:dispatch [::subs/load-current-user-mod-stats]])
          [:dispatch [::page-title/update (trx db "Dashboard")]]]}))

;; Spec the view options for the moderator dashboard.
(spec/def :routes.mod-dashboard/show-other-circles? boolean?)
(spec/def :routes.mod-dashboard/options
  (spec/keys :req-un [:routes.mod-dashboard/show-other-circles?]))
(defmethod view-type :mod-dashboard [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-dashboard/options]))

(re-frame/reg-event-fx ::show-modmail
  ;; :doc Show one of the modmail views.
  (fn-traced [{:keys [db]} [_ {:keys [msg mailbox sub]}]]
    (let [mbox (keyword mailbox)
          options {:mailbox mbox
                   :sub (or sub :all-subs)
                   :id msg}
          load-messages (if msg
                          [::messages/load-modmails options
                           [::initialize-modmail-reply-form]]
                          [::messages/load-modmail-category options])]
      (if (#{:all :archived :discussions :in-progress :new} mbox)
        {:db (assoc db :view (-> (:view db)
                                 (assoc :active-panel :modmail)
                                 (assoc :options options)))
         ;; If we're asked to route on sub name, load the list of subs
         ;; first, if we don't have it yet.
         :fx [(if (and (not= (:sub options) :all-subs)
                       (= :not-loaded (get-in db [:status :subs])))
                [:dispatch [::subs/load-all-subs load-messages]]
                [:dispatch load-messages])]}
        {:fx [[:dispatch [::show-error :unmatched-route]]]}))))

;; Spec the view options for the modmail views.
(spec/def :routes.modmail/mailbox #{:all :archived :discussions :in-progress
                                    :new})
(spec/def :routes.modmail/sub (spec/or :name string? :all #{:all-subs}))
(spec/def :routes.modmail/msg (spec/nilable string?))
(spec/def :routes.modmail/options
  (spec/keys :req-un [:routes.modmail/mailbox
                      :routes.modmail/sub]
             :opt-un [:routes.modmail/msg]))
(defmethod view-type :modmail [_]
  (spec/keys :req-un [::active-panel
                      :routes.modmail/options]))

(re-frame/reg-event-db ::initialize-modmail-reply-form
  ;; :doc After a modmail thread is loaded, set up the reply form.
  (fn-traced [db _]
    (compose-message/initialize-modmail-reply-form db)))

(re-frame/reg-event-db ::show-modmail-compose
  ;; :doc Show the form for mods to compose a new moderator mail.
  (fn-traced [db [_ url-params]]
    (let [options (-> (select-keys url-params [:sub :report_type :report_id :user
                                               :subject])
                      util/kebab-case-keys)]
      (-> db
          (update :view assoc
                  :active-panel :modmail-compose
                  :options options)
          compose-message/initialize-modmail-compose-form))))

;; Spec the view options for the moderator mail compose form.
(defmethod view-type :modmail-compose [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-contact-mods
  ;; :doc Show the contact mods page.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  (fn-traced [{:keys [db]} [_ {:keys [sub] :as options}]]
    {:db (-> db
             (assoc :view {:active-panel :contact-mods
                           :options options})
             (compose-message/initialize-contact-mods-form sub))
     :fx [(when (= :not-loaded (get-in db [:status :subs]))
            [:dispatch [::subs/load-all-subs]])]}))

;; Spec the view options for the contact mods page.
(spec/def :routes.contact-mods/sub string?)
(spec/def :routes.contact-mods/options
  (spec/keys :req-un [:routes.contact-mods/sub]))
(defmethod view-type :contact-mods [_]
  (spec/keys :req-un [::active-panel
                      :routes.contact-mods/options]))

(def mod-edit-sub-rules-deps
  "Initialization events for the mod edit sub rules panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-sub-rules] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])))

(re-frame/reg-event-fx ::show-mod-edit-sub-rules
  ;; :doc Show the edit sub rules panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  (fn-traced [{:keys [db]} [_ options]]
    {:db (-> db
             (assoc :view {:active-panel :mod-edit-sub-rules
                           :options (select-keys options [:sub])})
             (mod-forms/initialize-create-sub-rule-form))
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-sub-rules-deps}]]]}))

;; Spec the view options for the edit sub rules page.
(spec/def :routes.mod-edit-sub-rules/sub string?)
(spec/def :routes.mod-edit-sub-rules/options
  (spec/keys :req-un [:routes.mod-edit-sub-rules/sub]))
(defmethod view-type :mod-edit-sub-rules [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-sub-rules/options]))

(def mod-edit-post-flairs-deps
  "Initialization events for the mod edit post flairs panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-post-flairs] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])))

(re-frame/reg-event-fx ::show-mod-edit-post-flairs
  ;; :doc Show the edit sub flairs panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  (fn-traced [{:keys [db]} [_ options]]
    {:db (-> db
             (assoc :view {:active-panel :mod-edit-post-flairs
                           :options (select-keys options [:sub])})
             (update :reldb rel/transact [:delete :SubPostFlairState])
             (mod-forms/initialize-create-post-flair-form))
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-post-flairs-deps}]]]}))

;; Spec the view options for the edit sub flairs page.
(spec/def :routes.mod-edit-post-flairs/sub string?)
(spec/def :routes.mod-edit-post-flairs/options
  (spec/keys :req-un [:routes.mod-edit-post-flairs/sub]))
(defmethod view-type :mod-edit-post-flairs [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-post-flairs/options]))

(def mod-edit-post-types-deps
  "Initialization events for the mod edit post types panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-post-types] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])
      (dep/depend
       [::mod-forms/initialize-edit-post-types] [::subs/load-post-types])))

(re-frame/reg-event-fx ::show-mod-edit-post-types
  ;; :doc Show the mod edit post types panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  (fn-traced [{:keys [db]} [_ {:keys [sub]}]]
    {:db (assoc db :view {:active-panel :mod-edit-post-types
                          :options {:sub sub}})
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-post-types-deps}]]]}))

;; Spec the view options for the edit sub post types page.
(spec/def :routes.mod-edit-post-types/sub string?)
(spec/def :routes.mod-edit-post-types/options
  (spec/keys :req-un [:routes.mod-edit-post-types/sub]))
(defmethod view-type :mod-edit-post-types [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-post-types/options]))

(defn- cid-from-fragment
  "Parse the cid from the single post page url fragment, if there is one."
  [fragment]
  (let [prefix "comment-"]
    (when (and fragment
               (str/starts-with? fragment prefix)
               (> (count fragment) (count prefix)))
      (subs fragment (count prefix)))))

(re-frame/reg-event-fx ::show-single-post
  ;; :doc Show the single post panel.
  (fn-traced [{:keys [db]} [_ {:keys [cid ::fragment pid slug sort sub]}]]
    (let [target-cid (cid-from-fragment fragment)]
      {:db (-> db
               (assoc :view {:active-panel :single-post
                             :options {:sub sub
                                       :pid pid
                                       :cid cid
                                       :scroll-to-cid target-cid
                                       :slug slug
                                       :sort-choice sort}
                             :on-leave [[::comment/stop-scroll-watcher]
                                        [::comment/stop-resize-watcher]]})
               (assoc-in [:status :content] :loading)
               (assoc-in [:status :single-post] :loading)
               (assoc-in [:status :comment-tree] :loading)
               (assoc-in [:status :more-comments] {})
               (auth-forms/initialize-login-form
                (route-util/url-for db :home/index)))
       :fx [[:dispatch [::post/load-single-post {:sub sub
                                                 :pid pid
                                                 :cid cid}]]
            [:dispatch [::user/load-content-blocks]]
            [:dispatch [::comment/init-comment-compose-form :reply nil]]
            [:dispatch [::comment/start-scroll-watcher]]
            [:dispatch [::comment/start-resize-watcher]]
            [:dispatch [::donate/subscribe-funding-progress]]
            [:dispatch [::post/subscribe-post pid]]]})))

(re-frame/reg-event-fx ::show-single-post-test
  ;; :doc Redirect from the test URL to the normal URL
  (fn-traced [{:keys [db]} [_ {:keys [cid pid slug sub]}]]
    {:fx [[::replace-url (post/post-url db {:sub sub
                                            :pid pid
                                            :slug slug
                                            :cid cid})]]}))

;; Spec the view options for the view single post panel.
(spec/def :routes.single-post/sub string?)
(spec/def :routes.single-post/pid string?)
(spec/def :routes.single-post/slug (spec/nilable string?))
(spec/def :routes.single-post/cid (spec/nilable string?))
(spec/def :routes.single-post/scroll-to-cid (spec/nilable string?))
(spec/def :routes.single-post/sort-choice (spec/nilable string?))
(spec/def :routes.single-post/sort #{:BEST :NEW :TOP})
(spec/def :routes.single-post/options
  (spec/keys :req-un [:routes.single-post/sub
                      :routes.single-post/pid
                      :routes.single-post/slug
                      :routes.single-post/cid
                      :routes.single-post/scroll-to-cid
                      :routes.single-post/sort-choice]
             :opt-un [:routes.single-post/sort]))
(defmethod view-type :single-post [_]
  (spec/keys :req-un [::active-panel
                      :routes.single-post/options]))

(def submit-post-deps
  "Initialization events for the submit post panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])
      (dep/depend
       [::subs/load-info] [::subs/check-sub])
      (dep/depend
       [::subs/load-post-types] [::subs/check-sub])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-submit-post-settings])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-info])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-post-types])))

(re-frame/reg-event-fx ::show-submit-post
  [user/authenticated?]
  (fn-traced [{:keys [db authenticated?]} [_ {:keys [sub]}]]
    (let [nav? (not= :submit-post (get-in db [:view :active-panel]))]
      {:db (-> db
               (assoc :view {:active-panel :submit-post
                             :options {:sub sub}})
               ;; Changing the sub field in the form changes the URL and
               ;; lands us back here. So only clear the old form if we're
               ;; coming from a different page.
               (cond-> nav?
                 (update :reldb rel/transact
                         [:delete :FormState
                          [= [:_ ::sub-forms/submit-post] :id]])))
       :fx [(when authenticated?
              [:dispatch [::loader/init-content-loader
                          {:graph submit-post-deps}]])
            (when authenticated?
              [:dispatch [::page-title/update (trx db "New post")]])]})))

;; Spec the view options for the submit post panel.
(spec/def :routes.submit-post/sub (spec/nilable string?))
(spec/def :routes.submit-post/options
  (spec/keys :req-un [:routes.submit-post/sub]))
(defmethod view-type :submit-post [_]
  (spec/keys :req-un [::active-panel
                      :routes.submit-post/options]))

(re-frame/reg-event-fx ::show-error
  ;; :doc Show the error page when the route could not be matched.
  (fn-traced [{:keys [db]} [_ error]]
    {:db (assoc db :view (-> (:view db)
                             (assoc :active-panel :error-page)
                             (assoc :options {:error error})))
     :fx [[:dispatch [::page-title/update
                      (case error
                        :unmatched-route (trx db "Not found")
                        :server-error (trx db "Something went wrong"))]]
          [:dispatch [::page/stop-robots]]]}))

;; Spec the view options for the error page.
(spec/def :routes.error-page/error #{:unmatched-route :server-error})
(spec/def :routes.error-page/options
  (spec/keys :req-un [:routes.error-page/error]))
(defmethod view-type :error-page [_]
  (spec/keys :req-un [::active-panel
                      :routes.error-page/options]))

(re-frame/reg-event-fx ::show-admin-stats
  ;; :doc Show the admin stats page.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-stats)
             (admin/init-dates))
     :fx [[:dispatch [::admin/load-stats]]
          [:dispatch [::admin/load-stats-timespan]]
          [:dispatch [::admin/load-visitor-counts :daily]]
          [:dispatch [::admin/load-visitor-counts :weekly]]]}))

;; Spec the view options for the admin stats page.
(defmethod view-type :admin-stats [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-admin-banned-user-names
  ;; :doc Show the admin banned user names page.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-banned-user-names)
             (assoc-in [:view :options] {:sort-typ :string
                                         :sort-asc? true})
             admin-forms/initialize-ban-username-string)
     :fx [[:dispatch [::admin/load-banned-username-strings]]]}))

;; Spec the view options for the admin banned user names page.
(spec/def :routes.admin-banned-user-names/sort-asc? boolean?)
(spec/def :routes.admin-banned-user-names/sort-typ #{:string :count})
(spec/def :routes.admin-banned-user-names/options
  (spec/keys :req-un [:routes.admin-banned-user-names/sort-asc?
                      :routes.admin-banned-user-names/sort-typ]))
(defmethod view-type :admin-banned-user-names [_]
  (spec/keys :req-un [::active-panel
                      :routes.admin-banned-user-names/options]))

(re-frame/reg-event-fx ::show-admin-invite-codes
  ;; :doc Show the admin invite codes page.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-invite-codes)
             (assoc-in [:view :options] {:search :all})
             admin-forms/initialize-invite-code-forms)
     :fx [[:dispatch [::admin/load-invite-codes-and-settings]]]}))

;; Spec the view options for the admin invite codes page.
(spec/def :routes.admin-invite-codes/search (spec/or :name string?
                                                     :all #{:all}))
(spec/def :routes.admin-invite-codes/options
  (spec/keys :req-un [:routes.admin-invite-codes/search]))
(defmethod view-type :admin-invite-codes [_]
  (spec/keys :req-un [::active-panel
                      :routes.admin-invite-codes/options]))

;; TODO if they manage to navigate here when logged in,
;; redirect to the home page.
(re-frame/reg-event-fx ::show-registration
  ;; :doc Show the registration form.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :registration)
             (assoc-in [:view :options] {}))
     :fx [[:dispatch [::auth/prepare-auth-forms :registration]]
          [:dispatch [::page-title/update (trx db "Register")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the registration page.
(spec/def :routes.registration/options #(= % {}))
(defmethod view-type :registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.registration/options]))

(re-frame/reg-event-fx ::show-verify-registration
  ;; :doc Show a acknowledgement of a new registration.
  (fn-traced [{:keys [db]} [_ {:keys [delete_code uid]}]]
    (let [{:keys [email
                  username]} (get-in db [:content :verify-registration uid])]
      {:db (-> db
               (assoc-in [:view :active-panel] :verify-registration)
               (assoc-in [:view :options] {:delete-code delete_code
                                           :uid uid
                                           :email email
                                           :username username
                                           :already-verified? false}))
       :fx [[:dispatch [::auth/prepare-auth-forms :verify-registration]]
            [:dispatch [::page-title/update (trx db "Registered")]]
            [:dispatch [::window/set-force-day-mode true]]]})))

;; Spec the view options for the verify registration page.
(spec/def :routes.verify-registration.options/delete-code
  string?)
(spec/def :routes.verify-registration.options/username
  (spec/nilable string?))
(spec/def :routes.verify-registration.options/email
  (spec/nilable string?))
(spec/def :routes.verify-registration.options/already-verified?
  boolean?)
(spec/def :routes.verify-registration/options
  (spec/keys :req-un [:routes.verify-registration.options/delete-code
                      :routes.verify-registration.options/username
                      :routes.verify-registration.options/email
                      :routes.verify-registration.options/already-verified?]))
(defmethod view-type :verify-registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.verify-registration/options]))

(re-frame/reg-event-fx ::show-confirm-registration
  ;; :doc Confirm the user's registration, and show them the login form.
  ;; :doc Or if confirming their registration fails, show them an
  ;; :doc error page.
  (fn-traced [{:keys [db]} [_ {:keys [code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :confirm-registration)
             (assoc-in [:view :options] {:code (or code "")
                                         :state :loading}))
     :fx [[:dispatch [::auth/start-confirmation :registration]]
          [:dispatch [::page-title/update (trx db "Confirm registration")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the confirm registration page.
(spec/def :routes.confirm-registration.options/code string?)
(spec/def :routes.confirm-registration.options/state
  #{:loading :xmit-error :invalid-link})
(spec/def :routes.confirm-registration/options
  (spec/keys :req-un [:routes.confirm-registration.options/code
                      :routes.confirm-registration.options/state]))
(defmethod view-type :confirm-registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.confirm-registration/options]))

(re-frame/reg-event-fx ::show-login
  ;; :doc Show the login page.
  (fn-traced [{:keys [db]} [_ {:keys [next notify]}]]
    ;; See Django's url_has_allowed_host_and_scheme
    ;; before implmenting a redirect to arbitrary `next`.
    (let [home-url (route-util/url-for db :home/index)
          next-url (or (#{home-url
                          (route-util/url-for db :donate/setup)
                          (route-util/url-for db :wiki/welcome)} next)
                       home-url)]
      {:db (-> db
               (assoc-in [:view :active-panel] :login)
               (assoc-in [:view :options] {:notify (keyword notify)})
               (auth-forms/initialize-login-form next-url))
       :fx [[:dispatch [::page-title/update (trx db "Log in")]]
            [:dispatch [::window/set-force-day-mode true]]]})))

;; Spec the view options for the login page.
(spec/def :routes.login.options/notify (spec/nilable keyword?))
(spec/def :routes.login/options
  (spec/keys :req-un [:routes.login.options/notify]))
(defmethod view-type :login [_]
  (spec/keys :req-un [::active-panel
                      :routes.login/options]))

(re-frame/reg-event-fx ::show-resend-verification-email
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :resend-verification-email)
             (assoc-in [:view :options] {})
             (auth-forms/initialize-username-form))
     :fx [[:dispatch [::page-title/update (trx db "Resend email")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the resend verification email page.
(spec/def :routes.resend-verification-email/options #(= % {}))
(defmethod view-type :resend-verification-email [_]
  (spec/keys :req-un [::active-panel
                      :routes.resend-verification-email/options]))

(re-frame/reg-event-fx ::show-start-reset-password
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :start-reset-password)
             (assoc-in [:view :options] {})
             (auth-forms/initialize-start-reset-form))
     :fx [[:dispatch [::page-title/update (trx db "Reset password")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the start reset password page.
(spec/def :routes.start-reset-password/options #(= % {}))
(defmethod view-type :start-reset-password [_]
  (spec/keys :req-un [::active-panel
                      :routes.start-reset-password/options]))

(re-frame/reg-event-fx ::show-complete-reset-password
  (fn-traced [{:keys [db]} [_ {:keys [name code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :complete-reset-password)
             (assoc-in [:view :options] {:username name
                                         :code (or code "")
                                         :bad-code? false}))
     :fx [[:dispatch [::auth/prepare-auth-forms :complete-reset-password]]
          [:dispatch [::page-title/update (trx db "Reset password")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the complete reset password page.
(spec/def :routes.complete-reset-password.options/username
  (spec/nilable string?))
(spec/def :routes.complete-reset-password.options/code
  (spec/nilable string?))
(spec/def :routes.complete-reset-password.options/bad-code? boolean?)
(spec/def :routes.complete-reset-password/options
  (spec/keys :req-un [:routes.complete-reset-password.options/username
                      :routes.complete-reset-password.options/code
                      :routes.complete-reset-password.options/bad-code?]))
(defmethod view-type :complete-reset-password [_]
  (spec/keys :req-un [::active-panel
                      :routes.complete-reset-password/options]))

(re-frame/reg-event-fx ::show-edit-account
  [user/authenticated?]
  (fn-traced [{:keys [db authenticated?]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :edit-account)
             (assoc-in [:view :options] {}))
     :fx [(when authenticated?
            [:dispatch [::auth/prepare-auth-forms :edit-account]])
          (when authenticated?
            [:dispatch [::page-title/update (trx db "Edit account")]])
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the edit account page.
(spec/def :routes.edit-account/options map?)
(defmethod view-type :edit-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.edit-account/options]))

(def rename-account-deps
  "Initialization events for the rename account page."
  (-> (dep/graph)
      (dep/depend
       [::auth-forms/initialize-rename-form] [::auth/load-rename-settings])))

(re-frame/reg-event-fx ::show-rename-account
  [user/authenticated?]
  (fn-traced [{:keys [db authenticated?]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :rename-account)
             (assoc-in [:view :options] {}))
     :fx [(when authenticated?
            [:dispatch [::loader/init-content-loader
                        {:graph rename-account-deps}]])
          (when authenticated?
            [:dispatch [::page-title/update (trx db "Rename account")]])
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the rename account page.
(spec/def :routes.rename-account/options map?)
(defmethod view-type :rename-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.rename-account/options]))

(re-frame/reg-event-fx ::show-confirm-email
  ;; :doc Confirm a user's email and show them an acknowledgement page.
  (fn-traced [{:keys [db]} [_ {:keys [code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :confirm-email)
             (assoc-in [:view :options] {:code code
                                         :state :loading}))
     :fx [[:dispatch [::auth/start-confirmation :email]]
          [:dispatch [::page-title/update (trx db "Confirm email")]]
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the confirm email page.
(spec/def :routes.confirm-email.options/code string?)
(spec/def :routes.confirm-email.options/state
  #{:loading :confirmed :xmit-error :invalid-link})
(spec/def :routes.confirm-email/options
  (spec/keys :req-un [:routes.confirm-email.options/code
                      :routes.confirm-email.options/state]))
(defmethod view-type :confirm-email [_]
  (spec/keys :req-un [::active-panel
                      :routes.confirm-email/options]))

(re-frame/reg-event-fx ::show-delete-account
  ;;:doc Show the panel for deleting a user account.
  [user/authenticated?]
  (fn-traced [{:keys [db authenticated?]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :delete-account)
             (assoc-in [:view :options] {})
             (cond-> authenticated?
               (auth-forms/initialize-delete-account-form)))
     :fx [(when authenticated?
            [:dispatch [::page-title/update (trx db "Delete account")]])
          [:dispatch [::window/set-force-day-mode true]]]}))

;; Spec the view options for the delete account page..
(spec/def :routes.delete-account/options map?)
(defmethod view-type :delete-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.delete-account/options]))

;; Extractors.  No computation, just data access.

(re-frame/reg-sub :view/active-panel
  (fn [db _]
    (get-in db [:view :active-panel])))

(re-frame/reg-sub :view/post-source
  (fn [db _]
    (get-in db [:view :options :post-source])))

(re-frame/reg-sub :view/sort
  (fn [db _]
    (get-in db [:view :options :sort])))

(re-frame/reg-sub :view/mailbox
  (fn [db _]
    (get-in db [:view :options :mailbox])))

(re-frame/reg-sub :view/mid
  (fn [db _]
    (get-in db [:view :options :mid])))

(re-frame/reg-sub :view/options
  (fn [db _]
    (get-in db [:view :options])))

;; Computational subscriptions.  Each should depend on an extractor,
;; not the entire db.

(re-frame/reg-sub :view/post-source-sub
  :<- [:view/post-source]
  (fn [post-source _]
    (and (not (keyword? post-source)) post-source)))
