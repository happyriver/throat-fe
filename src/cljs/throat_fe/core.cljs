;; core.cljs -- Initialization for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.core
  (:require
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [lambdaisland.glogi :as glogi]
   [lambdaisland.glogi.console :as glogi-console]
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]
   [reagent.dom :as rdom]
   [throat-fe.config :as config]
   [throat-fe.content.posts :as posts]
   [throat-fe.content.settings :as settings]
   [throat-fe.db :as db]
   [throat-fe.graphql :as graphql]
   [throat-fe.log :as log]
   [throat-fe.routes :as routes]
   [throat-fe.ui.window :as window]
   [throat-fe.views :as views]))

(defn logging-setup
  "Configure logging."
  []
  (if config/debug?
    (do (glogi-console/install!)
        (glogi/add-handler log/db-handler)
        (glogi/add-handler log/backend-handler)
        (glogi/set-levels
         ;; Set a root logger level, this will be inherited by all loggers
         {:glogi/root :info
          ;; Customize levels here.
          'throat-fe.ui.forms.core :debug
          'throat-fe.content.post :debug}))
    (do (glogi/add-handler log/backend-handler)
        ;; See also the macros in log.clj which don't generate
        ;; code for log levels below :warn.
        (glogi/set-levels
         {:glogi/root :error
          ;; These two are set to shout because they could potentially
          ;; generate a lot of messages, and batching of log requests
          ;; is not yet implemented.
          'throat-fe.routes :shout
          'throat-fe.routes.util :shout
          ;; Log grab title results.
          'throat-fe.ui.forms.sub :warn}))))

(defn ^:dev/after-load mount-root
  "Mount reagent at the root node of the DOM."
  []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(re-frame/reg-fx ::route-and-mount
  ;; :doc Run routes and mount the page.
  (fn [routes]
    (routes/app-routes routes)
    (mount-root)))

(re-frame/reg-event-fx ::start
  ;; :doc Start routing.
  (fn-traced [{:keys [db]} _]
    {:fx [[::route-and-mount (:routes db)]
          [:dispatch [::window/after-load
                      {:width js/window.innerWidth
                       :height js/window.innerHeight
                       :scroll-y js/window.pageYOffset
                       :scroll-x js/window.pageXOffset}]]]}))

(defn init
  "Start all the moving parts."
  []
  (logging-setup)
  (re-frame/dispatch-sync [::db/initialize-db])
  (re-frame/dispatch-sync [::window/load-daynight-cookie])
  (re-frame/dispatch-sync [::re-graph/init (settings/re-graph-option-map)])
  (re-frame/dispatch-sync [::settings/load-html-head])
  (re-frame/dispatch-sync [::settings/load-index-query])
  (re-frame/dispatch-sync [::graphql/init-query-cache])
  (re-frame/dispatch [::settings/start-fathom])

  (when-not config/debug?  ; For now; makes debugging events harder.
    (re-frame/dispatch [::window/clock-tick]))
  (re-frame/dispatch [::window/start-resize-watcher])
  (re-frame/dispatch [::window/start-scroll-watcher])
  (re-frame/dispatch [::window/start-focus-watcher])
  (re-frame/dispatch [::start]))
