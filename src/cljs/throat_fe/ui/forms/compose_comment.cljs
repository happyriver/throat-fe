;; ui/forms/compose_comment.cljs -- Comment edit and reply forms for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.compose-comment
  (:require
   [clojure.spec.alpha :as spec]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [re-frame.interceptor :refer [get-coeffect]]
   [throat-fe.content.comment :as-alias comment]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.util :as util]))

;; Compose comment reply and comment edit forms

(defn form-path
  "Where to find the edit or reply form's state in the db."
  [form-type cid]
  [:ui-state :forms :comment form-type cid])

(def comment-common-values
  "Initial values common to the variations on the compose comment forms."
  {:preview        ""
   :preview-open?  false
   :errors         []
   :send-errors    []})

(defn initialize-comment-reply-form
  ([db cid] (initialize-comment-reply-form db cid false))
  ([db cid reset?]
   (let [path (form-path :reply cid)
         existing-content (get-in db (conj path :content))
         content (if reset?
                   ""
                   (or existing-content ""))]
     (->> {:content        content
           :parent-cid     cid
           :actions        {:send [::forms/dispatch-server-event path
                                   ::comment/send-reply
                                   ::comment/comment-reply-response]
                            :see-preview [::see-preview :reply cid]
                            :hide-preview [::hide-preview :reply cid]}
           :reset-function #(initialize-comment-reply-form % cid true)
           :form-state     (if (seq content) :READY :INCOMPLETE)
           :validators
           [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
             (trx db "Reply should be between 1 and 16,384 characters.")]]}
          (merge comment-common-values)
          (assoc-in db path)))))

(defn initialize-comment-edit-form
  [db cid]
  (let [pid (get-in db [:view :options :pid])
        path (form-path :edit cid)
        existing-content (get-in db (conj path :content))
        content (-> (rel/row (:reldb db) :Comment [= :cid cid])
                    :content)]
    (->> {:content        (or existing-content content)
          :cid            cid
          :pid            pid
          :actions        {:send [::forms/dispatch-server-event
                                  path
                                  ::comment/send-comment-edit
                                  ::comment/receive-comment-edit-response]
                           :see-preview [::see-preview :edit cid]
                           :hide-preview [::hide-preview :edit cid]}
          :reset-function #(initialize-comment-edit-form % cid)
          :form-state      :READY
          :validators
          [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Comment should be between 1 and 16,384 characters.")]]}
         (merge comment-common-values)
         (assoc-in db (form-path :edit cid)))))

;;; Interceptors

(defn form-data-path
  "Compute the path to a comment reply form from the re-frame context.
  Assumes the first two items after the event id in the event vector
  are the form type and the cid."
  [context]
  (let [[_ form-type cid] (get-coeffect context :event)]
    (form-path form-type cid)))

;;; Extractors.

(re-frame/reg-sub ::form
  ;; :doc Extract the comment form data.
  (fn [db [_ form-type cid]]
    (get-in db (form-path form-type cid))))

(re-frame/reg-event-db ::update-preview
  ;; :doc Show the html preview and update its content.
  [(util/path-fn form-data-path)]
  (fn-traced [{:keys [content] :as form} [_ _form-type _cid]]
    (-> form
        (assoc :preview (util/markdown-to-html content))
        (assoc :preview-open? true))))

(re-frame/reg-event-fx ::see-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ [_ form-type cid]]
    {:fx [[:dispatch-later
           {:ms 100
            :dispatch [::update-preview form-type cid]}]]}))

(re-frame/reg-event-db ::hide-preview
  ;; :doc Hide the html preview.
  [(util/path-fn form-data-path)]
  (fn-traced [form [_ _form-type _cid]]
    (assoc form :preview-open? false)))

;; Specs for the comment reply and edit forms.

(spec/def ::content string?)
(spec/def ::preview string?)
(spec/def ::preview-open? boolean?)

(spec/def ::fields
  (spec/keys :req-un [::content ::preview ::preview-open?]))
(spec/def ::compose (spec/merge ::fields ::forms/form))

(spec/def ::edit
  (spec/map-of string? ::compose))
;; The nil key in the reply map represents a top-level reply.
(spec/def ::reply
  (spec/map-of (spec/nilable string?) ::compose))
