;; ui/forms/post.cljs -- Post forms for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.post
  (:require
   [clojure.spec.alpha :as spec]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [throat-fe.content.graphql-spec]
   [throat-fe.content.internal :as content]
   [throat-fe.content.post :as-alias post]
   [throat-fe.content.post.db :as post-db]
   [throat-fe.content.subs :as subs]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.user :as user])
  (:require [throat-fe.ui.forms.post.select-flair
             :as-alias select-flair]))

;; Select a flair for a post form

(defn initialize-select-flair-form
  "Initial values and state for the select a post flair form."
  [db]
  (let [form-id ::select-flair
        sub-name (get-in db [:view :options :sub])
        {:keys [pid flair type]} (post-db/single-post db)
        sub-info (content/sub-info-by-name db sub-name)
        {:keys [name user-must-flair]} sub-info
        is-mod-or-admin? (user/is-mod-or-admin? db sub-name)
        post-flairs (subs/permitted-flairs (:reldb db)
                                           {:sub-name sub-name
                                            :is-mod-or-admin? is-mod-or-admin?
                                            :post-type type})
        flair-id (when flair
                   (->> post-flairs
                        (filter #(= flair (:text %)))
                        first
                        :id))]
    (->> {:id                   form-id
          :form-type            form-id
          :flair                flair
          :selected-id          (or flair-id :nothing-selected)
          :name                 name
          :pid                  pid
          :presets              post-flairs
          :user-must-flair      user-must-flair
          :disable-on-success?  false
          :reset-function       nil
          :validators           []
          :actions {:remove [::forms/dispatch-server-event form-id
                             ::post/remove-post-flair]}}
         (forms/initialize-generic-form db))))

;; Specs for the post flair form.
(spec/def ::select-flair/flair (spec/nilable string?))
(spec/def ::select-flair/selected-id
  (spec/or :not-found #(= :nothing-selected %)
           :found string?))
(spec/def ::select-flair/name string?)
(spec/def ::select-flair/pid string?)
(spec/def ::select-flair/presets
  (spec/nilable
   (spec/coll-of (spec/keys :req-un [:type.SubPostFlair/id
                                     :type.SubPostFlair/text]))))
(spec/def ::select-flair/user-must-flair boolean?)
(spec/def ::select-flair/fields (spec/keys :req-un [::select-flair/flair
                                                    ::select-flair/selected-id
                                                    ::select-flair/name
                                                    ::select-flair/pid
                                                    ::select-flair/presets
                                                    ::select-flair/user-must-flair]))
(defmethod forms/form-type ::select-flair [_]
  (spec/merge ::select-flair/fields
              ::forms/fsm-form))

(re-frame/reg-event-fx ::change-post-flair
  ;; :doc Set the flair selection in the form and send it to the server.
  (fn-traced [{:keys [db]} [_ new-flair-id]]
    {:db (update db :reldb rel/transact
             [:update :FormState {:selected-id new-flair-id}
              [= [:_ ::select-flair] :id]])
     :fx [[:dispatch [::forms/dispatch-server-event ::select-flair
                      ::post/change-post-flair ::select-flair]]]}))
