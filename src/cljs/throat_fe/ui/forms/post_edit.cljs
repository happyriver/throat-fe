;; ui/forms/post-edit.cljs -- Post edit form for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.post-edit
  (:require
   [clojure.spec.alpha :as spec]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [re-frame.std-interceptors :refer [path]]
   [throat-fe.content.post.db :as post-db]
   [throat-fe.content.post :as-alias post]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.util :as util]))

;; Post edit form.

(def form-path
  "Where to find the form's state in the db."
  [:ui-state :forms :post :edit])

(defn initialize-post-edit-form
  "Initialize the post edit form.
  When reinitializing, use previously edited content if present."
  [db]
  (let [{:keys [pid] :as post} (post-db/single-post db)
        content (or (get-in db (conj form-path :content))
                    (:content post))]
    (assoc-in db form-path
              {:content        content
               :pid            pid
               :preview        ""
               :preview-open?  false
               :errors         []
               :send-errors    []
               :form-state     :READY
               :disable-on-success? false
               :actions        {:send [::forms/dispatch-server-event form-path
                                       ::post/send-post-edit
                                       ::post/receive-post-edit-response]
                                :see-preview [::see-preview]
                                :hide-preview [::hide-preview]}
               :reset-function initialize-post-edit-form
               :validators
               [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
                 (trx db
                      "Post should be between 1 and 16,384 characters.")]]})))

(re-frame/reg-event-db ::update-preview
  ;; :doc Show the html preview and update its content.
  [(path form-path)]
  (fn-traced [{:keys [content] :as form} _]
    (-> form
        (assoc :preview (util/markdown-to-html content))
        (assoc :preview-open? true))))

(re-frame/reg-event-fx ::see-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ [_ _]]
    {:fx [[:dispatch-later
           {:ms 100
            :dispatch [::update-preview]}]]}))

(re-frame/reg-event-db ::hide-preview
  ;; :doc Hide the html preview.
  [(path form-path)]
  (fn-traced [form _]
    (assoc form :preview-open? false)))

;; Specs for the post edit form.

(spec/def ::content string?)
(spec/def ::preview string?)
(spec/def ::preview-open? boolean?)

(spec/def ::fields
  (spec/keys :req-un [::content ::preview ::preview-open?]))
(spec/def ::edit (spec/merge ::fields ::forms/form))
