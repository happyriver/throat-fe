;; ui/forms/post-edit-title.cljs -- Post title edit form for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.post-edit-title
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.post.db :as post-db]
   [throat-fe.content.post :as-alias post]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.util :as util :refer [trimmed-count]]))

;; Post title edit form.

(def form-path
  "Where to find the form's state in the db."
  [:ui-state :forms :post :edit-title])

(defn initialize-post-edit-title-form
  "Initialize the post edit form.
  When reinitializing, use previously edited content if present."
  [db]
  (let [{:keys [pid uid] :as post} (post-db/single-post db)
        title (or (get-in db (conj form-path :title))
                  (:title post))
        is-author? (= uid (get-in db [:current-user :uid]))]
    (assoc-in
     db form-path
     {:title          title
      :reason         ""
      :pid            pid
      :errors         []
      :send-errors    []
      :form-state     :READY
      :disable-on-success? false
      :actions        {:send [::forms/dispatch-server-event form-path
                              ::post/send-post-title-edit
                              ::post/receive-post-title-edit-response]}
      :reset-function initialize-post-edit-title-form
      :validators
      [[#(or (< (trimmed-count (:title %)) 3)
             (> (trimmed-count (:title %)) 255))
        (trx db
             "Title should be between 3 and 255 characters long.")]
       [#(and (not is-author?)
              (or (< (trimmed-count (:reason %)) 1)
                  (> (trimmed-count (:reason %)) 255)))
        (trx db
             "Reason should be between 1 and 255 characters.")]]})))

;; Specs for the post edit title form.

(spec/def ::title string?)
(spec/def ::pid string?)
(spec/def ::reason string?)

(spec/def ::fields
  (spec/keys :req-un [::title ::pid ::reason]))
(spec/def ::edit-title (spec/merge ::fields ::forms/form))
