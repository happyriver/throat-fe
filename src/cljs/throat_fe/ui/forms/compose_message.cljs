;; ui/forms/compose-message.cljs -- Compose message form for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.compose-message
  (:require
   [clojure.spec.alpha :as spec]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [re-frame.std-interceptors :refer [path]]
   [throat-fe.content.messages :as messages]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.util :as util]))

;; Compose message forms

(def form-path
  "Where to find this form's state in the db."
  [:ui-state :forms :messages :compose])

(def modmail-common-values
  "Initial values common to the variations on the compose form."
  {:content        ""
   :preview        ""
   :preview-open?  false
   :errors         []
   :send-errors    []
   :form-state     :INCOMPLETE
   :actions {:send [::forms/dispatch-server-event form-path
                    ::messages/send-message
                    ::messages/compose-response]}})

(defn truncate-string
  [s num]
  (subs s 0 (min (count s) num)))

(defn initialize-modmail-compose-form
  [db]
  (let [options (get-in db [:view :options])
        {:keys [user subject sub report-type report-id]} options
        subject' (when subject (truncate-string subject 255))]
    (->> {:type                   :modmail
          :sub                    (or sub :nothing-selected)
          :send-to-user?          true
          :show-username?         false
          :username               (or user "")
          :subject                (or subject' "")
          :disable-on-success?    false
          :report-type            report-type
          :report-id              report-id
          :reset-function         initialize-modmail-compose-form
          :validators
          [[#(= (:sub %) :nothing-selected)
            (trx db "Please select a circle.")]
           [#(and (:send-to-user? %) (or (< (count (:username %)) 1)
                                         (> (count (:username %)) 32)))
            (trx db "Username should be between 1 and 32 characters.")]
           [#(or (< (count (:subject %)) 1) (> (count (:subject %)) 255))
            (trx db "Subject should be between 1 and 255 characters.")]
           [#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Message should be between 1 and 16,384 characters.")]]}
         (merge modmail-common-values)
         (assoc-in db form-path))))

(defn initialize-modmail-reply-form
  [db]
  (let [{:keys [mid thread-id]} (get-in db [:view :options])
        message (get-in db [:content :modmail :thread mid :parent])
        {:keys [sender receiver mtype]} message
        sub (get-in db [:content :modmail :thread mid :sub])
        username (if (= :USER_TO_MODS mtype)
                   (:name sender)
                   (or (:name receiver) ""))]
    (assoc-in
     db form-path
     (merge
      modmail-common-values
      {:type                :modmail-reply
       :sub                 (:name sub)
       :mid                 (or (:reply-to message) mid)
       :thread-id           thread-id
       :send-to-user?       false
       :show-username?      false
       :username            username
       :disable-on-success? false
       :reset-function      initialize-modmail-reply-form
       :validators
       [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
         (trx db "Message should be between 1 and 16,384 characters.")]]}))))

(defn initialize-contact-mods-form
  [db sub]
  (->> {:type                :contact-mods
        :sub                 sub
        :subject             ""
        :disable-on-success? true
        :reset-function      #(initialize-contact-mods-form % sub)
        :validators
        [[#(or (< (count (:subject %)) 1) (> (count (:subject %)) 255))
          (trx db "Subject should be between 1 and 255 characters.")]

         [#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
          (trx db "Message should be between 1 and 16,384 characters.")]]}
       (merge modmail-common-values)
       (assoc-in db form-path)))

;;; Extractors.

(re-frame/reg-sub ::ready-to-compose-message-to-mod
  (fn [db _]
    (= :loaded (get-in db [:status :subs]))))

(re-frame/reg-sub ::form
  (fn [db _]
    (get-in db form-path)))

(re-frame/reg-sub ::linkable-to-report?
  ;; :doc Determine if the form is still linkable to the report, if any.
  :<- [::form]
  :<- [:view/options]
  (fn [[form options] _]
    (messages/linkable-to-report? form options)))

(re-frame/reg-event-db ::update-preview
  ;; :doc Show the html preview and update its content.
  [(path form-path)]
  (fn-traced [{:keys [content] :as compose} _]
    (-> compose
        (assoc :preview (util/markdown-to-html content))
        (assoc :preview-open? true))))

(re-frame/reg-event-fx ::see-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ _]
    {:fx [[:dispatch-later {:ms 100 :dispatch [::update-preview]}]]}))

(re-frame/reg-event-db ::hide-preview
  ;; :doc Hide the html preview.
  [(path form-path)]
  (fn-traced [compose _]
    (-> compose
        (assoc :preview-open? false))))

;; Specs for the message compose forms.

(spec/def ::sub (spec/or :nothing-selected #{:nothing-selected}
                         :sub string?))
(spec/def ::send-to-user? boolean?)
(spec/def ::show-username? boolean?)
(spec/def ::username string?)
(spec/def ::subject string?)
(spec/def ::mid string?)
(spec/def ::thread-id string?)
(spec/def ::content string?)
(spec/def ::preview string?)
(spec/def ::preview-open? boolean?)
(spec/def ::report-type (spec/nilable string?))
(spec/def ::report-id (spec/nilable string?))

(defmulti message-compose-type :type)

(defmethod message-compose-type :modmail [_]
  (spec/keys :req-un [::sub ::send-to-user? ::show-username? ::username
                      ::subject ::content ::preview ::preview-open?]))
(defmethod message-compose-type :modmail-reply [_]
  (spec/keys :req-un [::sub ::mid ::thread-id ::send-to-user? ::show-username?
                      ::username ::content ::preview ::preview-open?]))
(defmethod message-compose-type :contact-mods [_]
  (spec/keys :req-un [::sub ::subject ::content ::preview ::preview-open?]))

(spec/def ::fields (spec/multi-spec message-compose-type :type))
(spec/def ::compose (spec/merge ::fields ::forms/form))
