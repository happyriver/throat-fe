;; ui/forms/sub.cljs -- Sub forms for throat-fe
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.sub
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [goog.format :as gformat]
   [re-frame.core :refer [subscribe] :as re-frame]
   [re-graph.core :as re-graph]
   [statecharts.core :as fsm]
   [throat-fe.content.internal :as internal]
   [throat-fe.content.loader :as loader]
   [throat-fe.content.moderation :as-alias moderation]
   [throat-fe.content.subs :as subs]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.log :as log]
   [throat-fe.routes :as-alias routes]
   [throat-fe.routes.util :as route-util]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.user :as user]
   [throat-fe.util :as util]
   [weavejester.dependency :as dep])
  (:require
   [throat-fe.ui.forms.sub.submit-post :as-alias submit-post]))

;; Submit post form

(defn submit-post-state-machine
  "Create a statechart for the submit post form.
  Adds a region for the grab title button."
  []
  (-> forms/generic-form-machine
      (assoc :id :submit-post
             :type :parallel)
      (dissoc :states :initial)
      (assoc :regions
             {:main (select-keys forms/generic-form-machine [:initial :states])
              :grab {:initial :READY
                     :states {:SENDING
                              {:on {:grab-token [{:target :WAITING}]
                                    :grab-error [{:target :SHOW-FAILURE}]}}

                              :WAITING
                              {:on {:received-title [{:target :READY}]
                                    :edit [{:target :READY}]
                                    :no-title [{:target :SHOW-FAILURE}]}}

                              :READY
                              {:on {:send-grab [{:target :SENDING}]}}

                              :SHOW-FAILURE
                              {:on {:edit [{:target :READY}]}}}}})))

(let [mach (submit-post-state-machine)]
  (assert (= mach (fsm/machine mach))))

(defn- get-state [form]
  (let [{:keys [main grab]} (:_state form)]
    (if (= :SHOW-FAILURE grab)
      :SHOW-FAILURE
      main)))

(defn- submit-post-form
  [db]
  (rel/row (:reldb db) [[:from :FormState]
                        [:where [= [:_ ::submit-post] :id]]]))

(defn- post-type-configs
  [db sub]
  (-> (:reldb db)
      (rel/q [[:from :SubPostTypeConfig] [:where [= sub :name]]])))

(defn- set-permitted-post-types
  [{:keys [sub] :as form} db {:keys [restricted]}]
  (let [configs (post-type-configs db sub)
        mod-only-types (->> (if restricted
                              configs
                              (-> (filter #(:mods-only %) configs)))
                            (map :post-type)
                            set)
        types (->> (cond
                     (user/is-mod-or-admin? db sub) configs
                     restricted nil
                     :else (filter #(not (:mods-only %)) configs))
                   (map :post-type)
                   set)]
    (assoc form
           :permitted-post-types types
           :mod-only-types mod-only-types)))

(defn- set-selected-post-type
  [{:keys [selected-post-type permitted-post-types user-can-upload?] :as form}]
  (let [permitted? (if user-can-upload?
                     permitted-post-types
                     (disj permitted-post-types :UPLOAD))]
    (assoc form :selected-post-type
           (if (permitted? selected-post-type)
             selected-post-type
             ;; This is the same order as the radio buttons in the form.
             (some permitted? [:LINK :TEXT :UPLOAD :POLL])))))

(defn- reset-submit-post-form
  [db form]
  (let [sub (get-in db [:view :options :sub])
        sub-info (rel/row (:reldb db) [[:from :SubName]
                                       [:join :Sub {:sid :sid}]
                                       [:where [= sub :name]]])]
    (-> form
        (assoc :sub sub  ; content of the text input
               :loaded-sub sub  ; current or previous sub with all data loaded
               :banned? (boolean (get-in sub-info [:user-attributes :banned]))
               :sub-nsfw? (boolean (:nsfw sub-info))
               :nsfw? (boolean (:nsfw sub-info))
               :user-must-flair (boolean (:user-must-flair sub-info))
               :user-can-flair (boolean (:user-can-flair sub-info))
               :is-mod-or-admin? (boolean (when sub
                                            (user/is-mod-or-admin? db sub)))
               :selected-flair nil)
        (dissoc :token)
        (set-permitted-post-types db sub-info)
        set-selected-post-type)))

(defn- submit-post-form-initial-values
  [db]
  (let [names (->> (rel/q (:reldb db) [[:from :SubName]])
                   (map :name)
                   (sort-by str/lower-case))]
    {:id ::submit-post
     :fsm-id :submit-post
     :form-type ::submit-post
     :title ""
     :link ""
     :content ""
     :poll-options ["" ""]
     :hide-results? false
     :close-poll? false
     :close-date nil
     :sub-choices names
     :user-can-upload? (user/can-upload? db)
     :preview ""
     :preview-open? false
     :file nil
     :disable-on-success? true
     :reset-function nil
     :edit-hooks {:sub [::edit-sub]}
     :get-state get-state
     :errors []
     :send-errors []
     :validators
     ;; Validators that apply to all posts.
     [[#(or (< (count (:title %)) 3) (> (count (:title %)) 255))
       (trx db "Title should be between 3 and 255 characters.")]
      [#(nil? ((->> % :sub-choices (map str/lower-case) set)
               (-> % :sub str/lower-case)))
       (trx db "Please choose an existing circle.")]
      [#(and (:user-must-flair %) (not (:is-mod-or-admin? %))
             (nil? (:selected-flair %)))
       (trx db "Please choose a flair.")]

      ;; Validators for link posts
      [#(and (= :LINK (:selected-post-type %))
             (or (< (count (:link %)) 10) (> (count (:link %)) 255)))
       (trx db "Link should be between 10 and 255 characters.")]
      [#(and (= :LINK (:selected-post-type %))
             (not (util/valid-url? (:link %))))
       (trx db "Please enter a valid URL.")]

      ;; Validators for text posts
      [#(and (#{:POLL :TEXT} (:selected-post-type %))
             (> (count (:link %)) 65335))
       (trx db "Post content should be less than 65,335 characters.")]

      ;; Validators for poll posts
      [(fn [{:keys [selected-post-type poll-options]}]
         (and (= :POLL selected-post-type)
              (some #(> (count %) 127) poll-options)))
       (trx db "Poll options must be less than 128 characters.")]
      [(fn [{:keys [selected-post-type poll-options]}]
         (and (= :POLL selected-post-type)
              (> 1 (->> poll-options
                        (map count)
                        (filter pos?)
                        count))))
       (trx db "Polls must have at least two options.")]

      ;; Validators for upload posts
      [(fn [{:keys [selected-post-type file]}]
         (and (= :UPLOAD selected-post-type) (nil? file)))
       (trx db "Please select a file to upload.")]
      (let [max-size (get-in db [:settings :upload-max-size])]
        [(fn [{:keys [selected-post-type file]}]
           (and (= :UPLOAD selected-post-type)
                (some? file)
                (> (.-size file) max-size)))
         (trx db "File too large. Please choose a file smaller than %s."
              (gformat/numBytesToString max-size))])]

     :actions {:submit [::forms/dispatch-server-event ::submit-post
                        ::subs/submit-post]
               :hide-preview [::hide-preview]}}))

(defn initialize-submit-post
  "Initial values and state for the submit post form."
  [db]
  (let [form (->> (or (submit-post-form db)
                      (submit-post-form-initial-values db))
                  (reset-submit-post-form db))
        fsm (submit-post-state-machine)
        initialized-form (fsm/initialize fsm {:context form})]
    (-> db
        (assoc-in [:fsm :submit-post] fsm)
        (update :reldb rel/transact
                [:insert-or-replace :FormState initialized-form]))))

(re-frame/reg-event-fx ::initialize-submit-post
  ;; :doc Create state in the db for the submit post form.
  (fn-traced [{:keys [db]} event]
    {:db (initialize-submit-post db)
     :fx [[:dispatch [::loader/update-on-completion event :success]]]}))

(re-frame/reg-sub ::rules
  ;; :doc Get the rules for the selected post type.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [loaded-sub selected-post-type]} reldb] _]
    (-> reldb
        (rel/row [[:from :SubPostTypeConfig]
                  [:where
                   [= loaded-sub :name]
                   [= [:_ selected-post-type] :post-type]]])
        :rules)))

(re-frame/reg-sub ::flairs
  ;; :doc Get the flairs for the selected post type.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [is-mod-or-admin? loaded-sub selected-post-type]} reldb] _]
    (subs/permitted-flairs reldb {:sub-name loaded-sub
                                  :is-mod-or-admin? is-mod-or-admin?
                                  :post-type selected-post-type})))

(def new-sub-deps
  "Initialization events for a new sub selection."
  (-> (dep/graph)
      ;; ::subs/check-sub will fix capitalization
      (dep/depend [::subs/load-info] [::subs/check-sub])
      (dep/depend [::subs/load-post-types] [::subs/check-sub])
      (dep/depend [::update-sub] [::subs/load-info])
      (dep/depend [::update-sub] [::subs/load-post-types])))

(re-frame/reg-event-fx ::edit-sub
  ;; :doc Handle an edit to the sub field in the submit post form.
  ;; :doc If the new sub is valid, fetch post types and info for it.
  (fn-traced [{:keys [db]} [_ old-sub new-sub]]
    (let [form (submit-post-form db)
          choices (->> form :sub-choices (map str/lower-case) set)
          valid? (choices (str/lower-case new-sub))
          changed? (not= old-sub new-sub)]
      (when (and valid? changed?)
        {:db (assoc-in db [:view :options :sub] new-sub)
         :fx [[:dispatch [::loader/init-content-loader
                          {:graph new-sub-deps}]]]}))))

(re-frame/reg-event-fx ::update-sub
  ;; :doc Update the submit post form for a new sub selection.
  ;; :doc Update the browser location bar with the new sub name.
  (fn-traced [{:keys [db]} event]
    (let [sub (get-in db [:view :options :sub])]
      {:db (update db :reldb rel/transact
                   [:update :FormState #(reset-submit-post-form db %)
                    [= [:_ ::submit-post] :id]])
       :fx [[:dispatch [::loader/update-on-completion event :success]]
            [::routes/replace-url
             (route-util/url-for db :subs/submit-post
                                 :query-args {:sub sub})]]})))

(re-frame/reg-sub ::loaded-sub
  ;; :doc Get the details for the last sub for which details were loaded.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [loaded-sub]} reldb] _]
    (rel/row reldb [[:from :SubName]
                    [:join :Sub {:sid :sid}]
                    [:where [= loaded-sub :name]]])))

(re-frame/reg-sub ::html-rules
  ;; :doc Produce formatted rules for the selected post type.
  :<- [::rules]
  (fn [rules _]
    (util/markdown-to-html rules)))

(util/reg-ajax-form-event-chain ::grab-title
  {:call
   (fn-traced [{:keys [db]} _]
     (let [{:keys [link _state]} (submit-post-form db)]
       (when (and (seq link) (= :READY (:grab _state)))
         {:db (forms/transition db ::submit-post {:type :send-grab})
          :fx [[:dispatch [::util/ajax-csrf-form-post
                           {:url "/do/grabtitle"
                            :payload {:u link}}]]]})))
   :success
   (fn-traced [{:keys [db]} [_ {:keys [response]}]]
     (let [token (:token response)]
       {:db (-> db
                (update :reldb rel/transact
                        [:update :FormState {:grab-token token}
                         [= [:_ ::submit-post] :id]])
                (forms/transition ::submit-post {:type :grab-token}))
        :fx [[:dispatch [::graphql/subscribe
                         {:graphql graphql/subs-graphql
                          :name :grab-title
                          :id token
                          :variables {:token token}
                          :handler [::receive-grab-title-result
                                    {:token token}]}]]
             [:dispatch-later {:ms 10000
                               :dispatch [::grab-title-timeout token]}]]}))
   :failure
   (fn-traced [{:keys [db]} [_ {:keys [error-response]}]]
     {:db (-> db
              (errors/assoc-errors
               {:event ::grab-title
                :errors (errors/errors-from-ajax-error-response
                         error-response)})
              (forms/transition ::submit-post {:type :grab-error}))})})

(defn- assoc-grab-title-error
  [db]
  (update db :reldb rel/transact
          [:update :FormState #(update % :send-errors conj
                                       (trx db "Couldn't get title"))
           [= [:_ ::submit-post] :id]]))

(re-frame/reg-event-db ::receive-grab-title-result
  ;; :doc Receive the grabbed title from the server.
  (fn-traced [db [_ {:keys [token response]}]]
    ;; Log level warn makes it appear in production logs.
    (log/warn {:response response :token token} "grab title result")
    (let [form (submit-post-form db)]
      (if-not (= token (:grab-token form))
        ;; If the tokens don't match, the user managed to click the
        ;; button a second time, so ignore this result and wait for
        ;; that one.
        db
        (let [title (get-in response [:data :grab-title-result :title])
              db' (update db :reldb rel/transact
                          [:update :FormState #(dissoc % :grab-token)
                           [= [:_ ::submit-post] :id]])]
          (if title
            (-> db'
                (update :reldb rel/transact
                        [:update :FormState {:title title}
                         [= [:_ ::submit-post] :id]])
                (forms/transition ::submit-post {:type :received-title})
                (forms/transition ::submit-post {:type :edit :field :title}))
            (-> db'
                assoc-grab-title-error
                (forms/transition ::submit-post {:type :no-title}))))))))

(re-frame/reg-event-fx ::grab-title-timeout
  ;; :doc Notify the user that no title has been received yet.
  ;; :doc Unsubscribe from the graphql subscription.
  ;; :doc If this isn't the current grab title request, just cancel
  ;; :doc the subscription silently.
  (fn-traced [{:keys [db]} [_ token]]
    (let [active? (= token (:grab-token (submit-post-form db)))]
      ;; Log level warn makes it appear in production logs.
      (log/warn {:active? active? :token token} "grab title timed out")
      {:db (if (not active?)
             db
             (-> db
                 assoc-grab-title-error
                 (forms/transition ::submit-post {:type :no-title})))
       :fx [[:dispatch [::re-graph/unsubscribe {:id token}]]]})))

(re-frame/reg-event-db ::update-preview
  ;; :doc Show the html preview and update its content.
  (fn-traced [db _]
    (update db :reldb rel/transact
            [:update :FormState
             #(-> %
                  (assoc :preview (util/markdown-to-html (:content %)))
                  (assoc :preview-open? true))
             [= [:_ ::submit-post] :id]])))

(re-frame/reg-event-fx ::see-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ _]
    {:fx [[:dispatch-later
           {:ms 100
            :dispatch [::update-preview]}]]}))

(re-frame/reg-event-db ::hide-preview
  ;; :doc Hide the html preview.
  (fn-traced [db _]
    (update db :reldb rel/transact
            [:update :FormState {:preview-open? false}
             [= [:_ ::submit-post] :id]])))

(re-frame/reg-event-fx ::edit-poll-option
  ;; :doc Set a value for one of the poll options.
  (fn-traced [{:keys [db]} [_ num val]]
    (let [options (:poll-options (submit-post-form db))]
      {:fx [[:dispatch [::forms/edit ::submit-post :poll-options
                        (assoc options num val)]]]})))

(re-frame/reg-sub ::enable-add-poll-option?
  ;; :doc Whether to enable the add poll option button.
  (fn [_]
    (subscribe [::forms/field ::submit-post :poll-options]))
  (fn [options _]
    (< (count options) 6)))

(re-frame/reg-event-fx ::add-poll-option
  ;; :doc Add another poll option.
  (fn-traced [{:keys [db]} _]
    (let [options (:poll-options (submit-post-form db))]
      {:fx [[:dispatch [::forms/edit ::submit-post :poll-options
                        (conj options "")]]]})))

(re-frame/reg-sub ::upload-filename
  (fn []
    (subscribe [::forms/field ::submit-post :file]))
  (fn [file _]
    (when file
      (.-name file))))

;; Configuration fields
(spec/def ::submit-post/banned? boolean?)
(spec/def ::submit-post/is-mod-or-admin? boolean?)
(spec/def ::submit-post/loaded-sub (spec/nilable string?))
(spec/def ::submit-post/mod-only-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(spec/def ::submit-post/permitted-post-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(spec/def ::submit-post/sub-choices (spec/coll-of string?))
(spec/def ::submit-post/sub-nsfw? boolean?)
(spec/def ::submit-post/user-can-flair boolean?)
(spec/def ::submit-post/user-must-flair boolean?)

;; All types of posts
(spec/def ::submit-post/nsfw? boolean?)
(spec/def ::submit-post/selected-flair (spec/nilable string?))
(spec/def ::submit-post/selected-post-type (spec/nilable :reldb.Post/type))
(spec/def ::submit-post/sub (spec/nilable string?))
(spec/def ::submit-post/title string?)

;; Poll-specific fields
(spec/def ::submit-post/close-date
  (spec/nilable #(= (type %) js/Date)))
(spec/def ::submit-post/close-poll? boolean?)
(spec/def ::submit-post/hide-results? boolean?)

;; Text or poll fields
(spec/def ::submit-post/content string?)
(spec/def ::submit-post/preview string?)
(spec/def ::submit-post/preview-open? boolean?)

;; Link post fields
(spec/def ::submit-post/link string?)

;; Upload post fields
(spec/def ::submit-post/file (spec/nilable #(instance? js/File %)))

(spec/def ::submit-post/main ::forms/_state)
(spec/def ::submit-post/grab #{:READY :SENDING :WAITING :SHOW-FAILURE})
(spec/def ::submit-post/_state
  (spec/keys :req-un [::submit-post/main
                      ::submit-post/grab]))
(spec/def ::submit-post/fields
  (spec/keys :req-un [::submit-post/banned?
                      ::submit-post/close-date
                      ::submit-post/close-poll?
                      ::submit-post/content
                      ::submit-post/file
                      ::submit-post/hide-results?
                      ::submit-post/is-mod-or-admin?
                      ::submit-post/link
                      ::submit-post/loaded-sub
                      ::submit-post/mod-only-types
                      ::submit-post/nsfw?
                      ::submit-post/permitted-post-types
                      ::submit-post/preview
                      ::submit-post/preview-open?
                      ::submit-post/selected-flair
                      ::submit-post/selected-post-type
                      ::submit-post/sub
                      ::submit-post/sub-choices
                      ::submit-post/sub-nsfw?
                      ::submit-post/title
                      ::submit-post/user-can-flair
                      ::submit-post/user-must-flair
                      ::submit-post/_state]))
(defmethod forms/form-type ::submit-post [_]
  (spec/merge ::submit-post/fields
              ::forms/fsm-form-fields))
