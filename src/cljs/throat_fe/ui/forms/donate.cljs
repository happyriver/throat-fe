;; ui/forms/donate.cljs -- Create donate form for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.donate
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.donate :as donate :refer [parse-amount]]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]))

(def form-path
  "Where to find this form's state in the db."
  [:ui-state :forms :donate])

(defn initialize-donate-form
  "Initial values and state for the donate form."
  [db]
  (let [{:keys [donate-minimum donate-presets]} (:settings db)]
    (assoc-in
     db form-path
     {:custom-amount       ""
      :selection           nil
      :recurring?          true
      :errors              []
      :send-errors         []
      :form-state          :INCOMPLETE
      :disable-on-success? false
      :reset-function      initialize-donate-form
      :validators
      [[#(nil? (:selection %))
        (trx db "Please choose an amount.")]
       [#(and (= (:selection %) :custom)
              (zero? (parse-amount (:custom-amount %))))
        (trx db
             "Please enter an amount to donate in US dollars, without cents.")]
       [#(and (= (:selection %) :custom)
              (< (parse-amount (:custom-amount  %)) donate-minimum))
        (trx db "Please enter an amount greater than $1.")]
       [#(and (not= (:selection %) :custom)
              (not ((set donate-presets) (:selection %))))
        (trx db "Invalid selection.")]]
      :actions {:continue [::forms/dispatch-server-event form-path
                           ::donate/continue-to-checkout
                           ::donate/on-subscription-error]}})))

;; Specs for the donate form.
(spec/def ::custom-amount (spec/nilable integer?))
(spec/def ::selection (spec/nilable (spec/or :custom #(= % :custom)
                                             :value integer?)))
(spec/def ::recurring? boolean?)
(spec/def ::fields (spec/keys :req-un [::custom-amount
                                       ::selection
                                       ::recurring?]))
(spec/def ::donate (spec/merge ::fields ::forms/form))
