;; ui/forms/core.cljs -- Form state machine for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.core
  (:require
   [clojure.spec.alpha :as spec]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame :refer [subscribe]]
   [statecharts.core :as fsm]
   [throat-fe.errors :as errors]
   [throat-fe.log :as log]
   [throat-fe.util :as util]))

;; Generic form state machine

(def state-machine
  {:INCOMPLETE   {:valid-edit   :READY
                  :invalid-edit :INCOMPLETE
                  :send         :SHOW-ERROR}
   :READY        {:valid-edit   :READY
                  :invalid-edit :INCOMPLETE
                  :send         :SENDING}
   :SHOW-ERROR   {:valid-edit   :READY
                  :invalid-edit :INCOMPLETE
                  :send         :SHOW-ERROR}
   :SENDING      {:success      :SHOW-SUCCESS
                  :failure      :SHOW-FAILURE}
   :SHOW-FAILURE {:valid-edit   :READY
                  :invalid-edit :INCOMPLETE
                  :send         :SENDING}
   :SHOW-SUCCESS {:valid-edit   :READY
                  :invalid-edit :INCOMPLETE
                  :send         :SHOW-ERROR}})

(defn- check-form-fields
  "Check the contents of the fields in the form.
  Return the list of errors, or an empty list if all the
  fields are valid."
  [{:keys [validators] :as form}]
  (let [msgs (map (fn [[test msg]]
                    (when (test form) msg))
                  validators)]
    (filter some? msgs)))

(defn- all-fields-validate?
  "Return true if all the validators pass."
  [form _event]
  (empty? (check-form-fields form)))

(defn- validation-errors
  "Return the list of messages associated with failing validators."
  [form _event]
  (assoc form :errors (check-form-fields form)))

(defn- send-errors-present?
  "Return non-nil if any transmission errors are present."
  [form _event]
  (seq (:send-errors form)))

(defn- action-fx
  "Construct the re-frame fx for a form action."
  [{:keys [id] :as form} {:keys [event form-path]}]
  [:dispatch [event form [::receive-response form-path id]]])

(defn- reset
  "Run a form's reset function if it has one."
  [{:keys [reset-function] :as form} _]
  (if reset-function
    (reset-function form)
    form))

(def generic-form-machine
  "A statechart for forms."
  {:id :generic-form
   :initial :INCOMPLETE
   :context nil
   :states
   {:INCOMPLETE   {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}}
    :READY        {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}}
    :SHOW-ERROR   {:entry [(fsm/assign validation-errors)]
                   :on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]}}
    :SENDING      {:entry [(util/queue-fx action-fx)]
                   :on {:response [{:target :SHOW-FAILURE
                                    :guard send-errors-present?}
                                   {:target :SHOW-SUCCESS}]}}
    :SHOW-FAILURE {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}
                   :exit [(fsm/assign #(assoc %
                                              :send-errors []
                                              :errors []))]}
    :SHOW-SUCCESS {:entry [(fsm/assign reset)]
                   :on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]}}}})

(assert (= generic-form-machine
           (fsm/machine generic-form-machine)))

(defn initialize-generic-form
  [db context]
  (let [ctx (merge context {:fsm-id :generic-form
                            :errors []
                            :send-errors []})]
    (update db :reldb rel/transact
            [:insert-or-replace :FormState
             (fsm/initialize generic-form-machine {:context ctx})])))

(defn next-form-state
  "Update a form to the next state for an event."
  [db path event-types event]
  (let [form (get-in db path)
        errors (check-form-fields form)
        ;; Simplify life by lumping all the edits together.
        event-type (or (get event-types event) event)
        ;; If the event was an edit, determine the next state based
        ;; on whether errors were found in the validation check.
        exact-event (if (= event-type :edit)
                      (if (empty? errors) :valid-edit :invalid-edit)
                      event-type)
        current (:form-state form)
        next (get-in state-machine [current exact-event])]
    (when-not next
      (log/error {:path path :current current :event exact-event :next next}
                 "No next form state"))
    (-> (if (and (= next :SHOW-SUCCESS) (:reset-function form))
          ((:reset-function form) db)
          db)
        (update-in path assoc
                   :form-state (or next current)
                   :errors errors))))

(defn next-state
  [db form-path event]
  (next-form-state db form-path nil event))

(defn transition
  [db id event]
  (try
    (update db :reldb rel/transact
            [:update :FormState
             (fn [{:keys [fsm-id] :as form}]
               (let [result (fsm/transition (get-in db [:fsm fsm-id])
                                            form event
                                            {:ignore-unknown-event? true})]
                 (log/debug {:form result :event event} "after transition")
                 result))
             [= [:_ id] :id]])
    (catch js/Error e
      (log/error {:error e
                  :form-id id
                  :event event} "error in fsm transition")
      db)))

(re-frame/reg-event-fx ::edit
  ;; :doc Edit a form field.
  (fn-traced [{:keys [db]} [_ form-path field value]]
    (let [form (when (keyword? form-path)
                 (rel/row (:reldb db) [[:from :FormState]
                                       [:where [= [:_ form-path] :id]]]))
          old-value (get form field)
          {:keys [id fsm-id edit-hooks]} form
          hook (get edit-hooks field)]
      (log/trace {:form-path form-path :fsm-id fsm-id :field field :value value
                  :old-value old-value}
                 "edit a form field")
      (when (not= value old-value)
        {:db (if fsm-id
               (-> db
                   (update :reldb rel/transact
                           [:update :FormState {field [:_ value]}
                            [= [:_ form-path] :id]])
                   (transition id {:type :edit}))
               (-> db
                   (update-in form-path assoc field value)
                   (next-state form-path :edit)))
         :fx [(when hook
                [:dispatch (into hook [(get form field) value])])]}))))

(defn reset-input-on-success!
  "Reset a text value in an atom when the form reaches the success state.
  Use to make text inputs refetch their values from the db after those
  values are cleared on a successful send."
  [state-atom {:keys [value form-state] :as properties}]
  (if (= form-state :SHOW-SUCCESS)
    (when-not (:already-updated? @state-atom)
      (swap! state-atom assoc
             :value (value) :already-updated? true))
    (swap! state-atom assoc :already-updated? false))
  (dissoc properties :form-state))

(defn reset-input-on-field-change!
  "Reset the value field in an atom when another field changes.
  Use the value function in `props` to get the new value.
  Include the other field in the component properties."
  [field state-atom {:keys [value] :as props}]
  (when-not (= (get props field) (get @state-atom field))
    (swap! state-atom assoc
           :value (value)
           field (get props field)))
  (dissoc props field :form-state))

;; Generic form events and subscriptions

(re-frame/reg-sub ::form
  ;; :doc Get a form's state and context
  (fn [db [_ form-path]]
    (if (keyword? form-path)
      (rel/row (:reldb db) [[:from :FormState]
                            [:where [= [:_ form-path] :id]]])
      (get-in db form-path))))

(re-frame/reg-sub ::field
  ;; :doc Get the value of a form field.
  (fn [[_ form-path _]]
    (subscribe [::form form-path]))
  (fn [form [_ _ field]]
    (field form)))

(re-frame/reg-sub ::errors
  ;; :doc Retrieve the list of errors in a form.
  (fn [[_ form-path]]
    (subscribe [::form form-path]))
  (fn [form _]
    (:errors form)))

(re-frame/reg-sub ::send-errors
  ;; :doc Retrieve the list of errors from the last send.
  (fn [[_ form-path]]
    (subscribe [::form form-path]))
  (fn [form _]
    (:send-errors form)))

(re-frame/reg-sub ::form-state
  ;; :doc Get the state of a form.
  (fn [[_ form-path]]
    (subscribe [::form form-path]))
  (fn [{:keys [fsm-id get-state] :as form} _]
    (cond
      get-state (get-state form)
      fsm-id (:_state form)
      :else (:form-state form))))

(re-frame/reg-sub ::disable-on-success?
  ;; :doc Get whether the edit controls should be disabled on success.
  (fn [[_ form-path]]
    (subscribe [::form form-path]))
  (fn [form _]
    (:disable-on-success? form)))

(re-frame/reg-sub ::editing-disabled?
  ;; :doc Decide whether editing elements of the form should be disabled.
  (fn [[_ form-path]]
    [(subscribe [::form form-path])
     (subscribe [::form-state form-path])])
  (fn [[{:keys [disable-on-success?]} form-state] _]
    (or (= form-state :SENDING)
        (and disable-on-success? (= form-state :SHOW-SUCCESS)))))

(re-frame/reg-event-fx ::action
  ;; :doc Dispatch the event for an action keyword in the form.
  (fn-traced [{:keys [db]} [_ form-path action]]
    (let [form (if (keyword? form-path)
                 (rel/row (:reldb db) [[:from :FormState]
                                       [:where [= [:_ form-path] :id]]])
                 (get-in db form-path))
          event (get-in form [:actions action])]
      (log/debug {:form form :action action :event event}
                 "dispatching event for form action")
      {:db (cond-> db
             (keyword? form-path)
             (update :reldb rel/transact
                     [:update :FormState {:form-last-action [:_ action]}
                      [= [:_ form-path] :id]]))
       :fx [[:dispatch event]]})))

(re-frame/reg-event-fx ::dispatch-server-event
  ;; :doc Dispatch server requests for forms.
  ;; :doc Switches the form state to :SENDING and clears previous send
  ;; :doc errors, then dispatches the provided event, supplying it the form
  ;; :doc and an event to dispatch after the server response which will
  ;; :doc process errors associated with `receive-keyword`.
  (fn-traced [{:keys [db]} [_ form-path event receive-keyword]]
    (let [form (if (keyword? form-path)
                 (rel/row (:reldb db) [[:from :FormState]
                                       [:where [= [:_ form-path] :id]]])
                 (get-in db form-path))
          {:keys [fsm-id]} form]
      (log/debug {:form form :event event
                  :receive-keyword receive-keyword}
                 "dispatching form event to server")
      (if fsm-id
        (let [fsm (get-in db [:fsm fsm-id])
              new-state (fsm/transition fsm form
                                        {:type :send
                                         :event event
                                         :form-path form-path}
                                        {:ignore-unknown-event? true})]
          {:db (update db :reldb rel/transact
                       [:insert-or-replace :FormState (dissoc new-state :fx)])
           :fx (vec (:fx new-state))})
        {:db (-> db
                 ;; Clear any errors left over from recent requests before
                 ;; dispatching the new request to the server.
                 (update-in form-path assoc :send-errors [])
                 (next-state form-path :send))
         :fx [(when (#{:READY :SHOW-FAILURE} (:form-state form))
                [:dispatch [event form [::receive-response
                                        form-path receive-keyword]]])]}))))

(re-frame/reg-event-db ::receive-response
  ;; :doc Advance the form state on response from the server.
  ;; :doc Link error messages associated with `receive-keyword` to the form.
  (fn-traced [db [_ form-path receive-keyword]]
    (let [form (when (keyword? form-path)
                 (rel/row (:reldb db) [[:from :FormState]
                                       [:where [= [:_ form-path] :id]]]))
          error-descriptions (errors/get-errors db receive-keyword)
          error-messages (map :msg error-descriptions)
          {:keys [id fsm-id]} form]
      (when (seq error-messages)
        (log/debug {:receive-keyword receive-keyword
                    :form-path form-path
                    :error-messages error-messages} "form xmit error"))
      (if fsm-id
        (-> db
            (errors/clear-errors receive-keyword)
            (update :reldb rel/transact
                    [:update :FormState {:send-errors error-messages}
                     [= [:_ form-path] :id]])
            (transition id {:type :response}))
        (-> db
            (errors/clear-errors receive-keyword)
            (update-in form-path assoc :send-errors error-messages)
            (next-state form-path (if (empty? error-messages)
                                    :success :failure)))))))

(re-frame/reg-event-fx ::generic-graphql-handler
  ;; :doc Handle a response to a graphql query or mutation sent from a form.
  ;; :doc Associate any errors with `form-id`, and call `restore` if it is
  ;; :doc set and an error occurs.  Either way, dispatch `update-event`.
  (fn-traced [{:keys [db]} [_ {:keys [form-id restore response update-event]}]]
    (let [errors (:errors response)]
      {:db (-> db
               (errors/assoc-errors {:event form-id :errors errors})
               (cond-> (and errors restore) restore))
       :fx [[:dispatch update-event]]})))

;; Specs for the shared form fields.
(spec/def ::id keyword?)
(spec/def ::fsm-id keyword?)
(spec/def ::disable-on-success? boolean?)
(spec/def ::errors (spec/coll-of string?))
(spec/def ::send-errors
  (spec/coll-of
   (spec/or :message string?
            :html vector?)))
(spec/def ::reset-function (spec/nilable fn?))
(spec/def ::edit-hooks (spec/map-of keyword? vector?))
(spec/def ::actions (spec/map-of keyword? seq))
(spec/def ::validator (spec/cat :check-fn fn? :message string?))
(spec/def ::validators (spec/coll-of ::validator))
(spec/def ::form-state
  #{:INCOMPLETE :READY :SHOW-ERROR :SENDING :SHOW-SUCCESS :SHOW-FAILURE})
(spec/def ::_state
  #{:INCOMPLETE :READY :SHOW-ERROR :SENDING :SHOW-SUCCESS :SHOW-FAILURE})
(spec/def ::get-state fn?)

(spec/def ::form (spec/keys :req-un [::disable-on-success?
                                     ::reset-function
                                     ::errors
                                     ::send-errors
                                     ::reset-function
                                     ::validators
                                     ::actions
                                     ::form-state]
                            :opt-un [::edit-hooks]))

(spec/def ::fsm-form-fields (spec/keys :req-un [::disable-on-success?
                                                ::reset-function
                                                ::errors
                                                ::send-errors
                                                ::reset-function
                                                ::validators
                                                ::actions
                                                ::id
                                                ::fsm-id]
                                       :opt-un [::edit-hooks
                                                ::get-state]))

(spec/def ::fsm-form
  (spec/merge (spec/keys :req-un [::_state])
              ::fsm-form-fields))

(defmulti form-type :form-type)
(spec/def ::typed-form
  (spec/multi-spec form-type :form-type))
