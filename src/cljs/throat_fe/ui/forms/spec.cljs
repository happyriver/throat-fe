;; ui/forms/spec.cljs -- Form specs for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.spec
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.ui.forms.compose-comment :as compose-comment]
   [throat-fe.ui.forms.compose-message :as compose-message]
   [throat-fe.ui.forms.mod-action :as mod-action]
   [throat-fe.ui.forms.post-edit :as post-edit]
   [throat-fe.ui.forms.post-edit-title :as post-edit-title]
   [throat-fe.ui.forms.report :as report]
   [throat-fe.ui.forms.user-flair :as user-flair]))

(spec/def ::comment (spec/keys :opt-un [::compose-comment/edit
                                        ::compose-comment/reply]))
(spec/def ::messages (spec/keys :opt-un [::compose-message/compose]))
(spec/def ::moderation (spec/keys :opt-un
                                  [::mod-action/mod-action]))
(spec/def ::post (spec/keys :opt-un [::post-edit/edit
                                     ::post-edit-title/edit-title
                                     ::report/report]))
(spec/def ::user (spec/keys :opt-un [::user-flair/user-flair]))

(spec/def ::forms (spec/keys :opt-un [::messages
                                      ::moderation
                                      ::post
                                      ::user]))
