;; ui/forms/mod-action.cljs -- Perform a mod action with a reason.
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.mod-action
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.comment :as-alias comment]
   [throat-fe.content.post :as-alias post]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]))

;; Mod action with reason form.

(def form-path
  "Where to find the form's state in the db."
  [:ui-state :forms :post :mod-action])

(def send-events
  {:delete-post [::forms/dispatch-server-event form-path
                 ::post/mod-delete-post
                 ::post/receive-mod-delete-post-response]
   :undelete-post [::forms/dispatch-server-event form-path
                   ::post/mod-undelete-post
                   ::post/receive-mod-undelete-post-response]
   :delete-comment [::forms/dispatch-server-event form-path
                    ::comment/mod-delete-comment
                    ::comment/receive-mod-delete-comment-response]
   :undelete-comment [::forms/dispatch-server-event form-path
                      ::comment/mod-undelete-comment
                      ::comment/receive-mod-undelete-comment-response]})

(defn initialize-mod-action-form
  "Initialize the mod action form."
  ([db action]
   (initialize-mod-action-form db action nil))
  ([db action cid]
   (let [pid (get-in db [:view :options :pid])]
     (assoc-in db form-path
               {:reason         ""
                :pid            pid
                :cid            cid
                :errors         []
                :send-errors    []
                :form-state     :INCOMPLETE
                :actions        {:send (get send-events action)}
                :reset-function #(initialize-mod-action-form % action cid)
                :validators
                [[#(or (< (count (:reason %)) 1) (> (count (:reason %)) 255))
                  (trx db
                       "Reason should be between 1 and 255 characters.")]]}))))

;; Specs for the mod action form.

(spec/def ::reason string?)
(spec/def ::pid string?)
(spec/def ::cid (spec/nilable string?))

(spec/def ::fields
  (spec/keys :req-un [::reason ::pid ::cid]))
(spec/def ::mod-action (spec/merge ::fields ::forms/form))
