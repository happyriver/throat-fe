;; ui/forms/moderation.cljs -- Moderation forms for throat-fe
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.moderation
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.wotbrew.relic :as rel]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame :refer [subscribe]]
   [throat-fe.content.loader :as loader]
   [throat-fe.content.moderation :as-alias moderation]
   [throat-fe.content.subs :as-alias subs]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.util :as util])
  (:require
   [throat-fe.ui.forms.moderation.create-post-flair
    :as-alias create-post-flair]
   [throat-fe.ui.forms.moderation.create-sub-rule
    :as-alias create-sub-rule]
   [throat-fe.ui.forms.moderation.edit-post-flair
    :as-alias edit-post-flair]
   [throat-fe.ui.forms.moderation.edit-post-type
    :as-alias edit-post-type]))

;; Edit post type settings forms

(defn- get-current-namespace []
  (-> #'get-current-namespace meta :ns))

(defn- edit-post-type-form-id
  [sub-name post-type]
  (when (and sub-name post-type)
    (keyword (get-current-namespace)
             (str "edit-post-type." sub-name "."
                  (-> post-type name str/lower-case)))))

(re-frame/reg-sub ::edit-post-type-form-id
  :<- [:view/options]
  (fn [{:keys [sub]} [_ post-type]]
    (edit-post-type-form-id sub post-type)))

(defn- initialize-edit-post-type
  "Initialize one of the edit post type config forms."
  [db name post-type]
  (let [id (edit-post-type-form-id name post-type)
        conf (rel/row (:reldb db)
                      [[:from :SubPostTypeConfig]
                       [:where [:and
                                [= name :name]
                                [= [:_ post-type] :post-type]]]])
        {:keys [rules mods-only]} conf]
    (->> {:id id
          :form-type ::edit-post-type
          :name name
          :post-type post-type
          :mods-only? mods-only
          :rules rules
          :preview ""
          :preview-open? false
          :disable-on-success? false
          :reset-function nil
          :validators
          [[#(and (some? rules) (> (count (:rules %)) 16384))
            (trx db "Rules should be less than 16,384 characters long.")]]
          :actions {:see-preview [::see-rules-preview id]
                    :hide-preview [::hide-rules-preview id]
                    :save [::forms/dispatch-server-event id
                           ::moderation/update-post-type-config]}}
         (forms/initialize-generic-form db))))

(re-frame/reg-event-db ::update-rules-preview
  ;; :doc Show the html preview and update its content.
  (fn-traced [db [_ form-id]]
    (update db :reldb rel/transact
            [:update :FormState
             (fn [{:keys [rules] :as form-state}]
               (assoc form-state
                      :preview (util/markdown-to-html rules)
                      :preview-open? true))
             [= [:_ form-id] :id]])))

(re-frame/reg-event-fx ::see-rules-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ [_ form-id]]
    {:fx [[:dispatch-later
           {:ms 100
            :dispatch [::update-rules-preview form-id]}]]}))

(re-frame/reg-event-db ::hide-rules-preview
  ;; :doc Hide the html preview.
  (fn-traced [db [_ form-id]]
    (update db :reldb rel/transact
            [:update :FormState {:preview-open? false}
             [= [:_ form-id] :id]])))

(re-frame/reg-event-fx ::initialize-edit-post-types
  ;; :doc Create state in the db for the edit post type forms.
  (fn-traced [{:keys [db]} event]
    (let [sub-name (get-in db [:view :options :sub])]
      {:db (reduce #(initialize-edit-post-type %1 sub-name %2) db
                   [:LINK :POLL :TEXT :UPLOAD])
       :fx [[:dispatch [::loader/update-on-completion event :success]]]})))

;; Specs for the edit post types forms.
(spec/def ::edit-post-type/name :type.Sub/name)
(spec/def ::edit-post-type/post-type #{:LINK :TEXT :POLL :UPLOAD})
(spec/def ::edit-post-type/mods-only? boolean?)
(spec/def ::edit-post-type/preview string?)
(spec/def ::edit-post-type/preview-open? boolean?)
(spec/def ::edit-post-type/rules (spec/nilable string?))
(spec/def ::edit-post-type/fields
  (spec/keys :req-un [::edit-post-type/name
                      ::edit-post-type/post-type
                      ::edit-post-type/mods-only?
                      ::edit-post-type/preview
                      ::edit-post-type/preview-open?
                      ::edit-post-type/rules]))
(defmethod forms/form-type ::edit-post-type [_]
  (spec/merge ::edit-post-type/fields
              ::forms/fsm-form))

;; Form for creating a post flair.

(defn- reset-create-post-flair
  [state]
  (assoc state :flair ""))

(defn initialize-create-post-flair-form
  "Initial values and state for the moderator create post flair form."
  [db]
  (let [form-id ::create-post-flair]
    (->> {:id                  form-id
          :form-type           form-id
          :disable-on-success? false
          :reset-function      reset-create-post-flair
          :validators
          [[#(empty? (str/trim (:flair %)))
            (trx db "Please fill out this field.")]
           [#(> (count (str/trim (:flair %))) 25)
            (trx db "Flair is too long.")]]
          :actions {:create [::forms/dispatch-server-event form-id
                             ::moderation/create-sub-post-flair]}}
         reset-create-post-flair
         (forms/initialize-generic-form db))))

;; Specs for the create post flair form.
(spec/def ::create-post-flair/flair string?)
(spec/def ::create-post-flair/fields
  (spec/keys :req-un [::create-post-flair/flair]))
(spec/def ::fields (spec/keys :req-un [::create-post-flair/flair]))
(defmethod forms/form-type ::create-post-flair [_]
  (spec/merge ::create-post-flair/fields
              ::forms/fsm-form))

;; Form for editing options for a post flair

(defn edit-post-flair-form-id
  [flair-id]
  (when flair-id
    (keyword (get-current-namespace)
             (str "edit-post-flair." flair-id))))

(re-frame/reg-sub ::edit-post-flair-form-id
  (fn [_ [_ id]]
    (edit-post-flair-form-id id)))

(defn initialize-edit-post-flair
  "Initialize one of the edit post flair options forms."
  [db flair-id]
  (let [id (edit-post-flair-form-id flair-id)
        flair (rel/row (:reldb db)
                       [[:from :SubPostFlair]
                        [:where [= flair-id :id]]])
        {:keys [mods-only post-types]} flair]
    (->> {:id id
          :form-type ::edit-post-flair
          :flair-id (:id flair)
          :mods-only mods-only
          :post-types post-types
          :disable-on-success? false
          :reset-function nil
          :validators []
          :actions {:save [::forms/dispatch-server-event id
                           ::moderation/update-sub-post-flair id]
                    :delete [::forms/dispatch-server-event id
                             ::moderation/delete-sub-post-flair]}}
         (forms/initialize-generic-form db))))

(re-frame/reg-sub ::post-type-permitted?
  ;; :doc Determine whether a post type is permitted for a flair.
  (fn [[_ id _post-type]]
    (subscribe [::forms/field (edit-post-flair-form-id id) :post-types]))
  (fn [post-types [_ _id post-type]]
    (when post-types
      (some? (post-types post-type)))))

(re-frame/reg-event-db ::permit-flair-post-type
  ;; :doc Add or remove a post type from the post types set.
  (fn-traced [db [_ flair-id post-type val]]
    (let [id (edit-post-flair-form-id flair-id)]
      (update db :reldb rel/transact
              [:update :FormState #(if val
                                     (update % :post-types conj post-type)
                                     (update % :post-types disj post-type))
               [= [:_ id] :id]]))))

;; Specs for the edit post flair forms.
(spec/def ::edit-post-flair/flair-id :type.SubPostFlair/id)
(spec/def ::edit-post-flair/post-types
  (spec/and set? (spec/coll-of #{:LINK :TEXT :POLL :UPLOAD})))
(spec/def ::edit-post-flair/mods-only boolean?)
(spec/def ::edit-post-flair/fields
  (spec/keys :req-un [::edit-post-flair/flair-id
                      ::edit-post-flair/mods-only
                      ::edit-post-flair/post-types]))
(defmethod forms/form-type ::edit-post-flair [_]
  (spec/merge ::edit-post-flair/fields
              ::forms/fsm-form))

;; Form for creating a sub rule.

(defn- reset-create-sub-rule
  [state]
  (assoc state :rule ""))

(defn initialize-create-sub-rule-form
  "Initial values and state for the moderator create sub rule form."
  [db]
  (let [form-id ::create-sub-rule]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-create-sub-rule
          :validators
          [[#(empty? (str/trim (:rule %)))
            (trx db "Please fill out this field.")]
           [#(> (count (str/trim (:rule %))) 100)
            (trx db "Rule is too long.")]]
          :actions {:create [::forms/dispatch-server-event form-id
                             ::moderation/create-sub-rule]}}
         reset-create-sub-rule
         (forms/initialize-generic-form db))))

;; Specs for the create sub rule form.
(spec/def ::create-sub-rule/rule string?)
(spec/def ::create-sub-rule/fields
  (spec/keys :req-un [::create-sub-rule/rule]))
(spec/def ::fields (spec/keys :req-un [::create-sub-rule/rule]))
(defmethod forms/form-type ::create-sub-rule [_]
  (spec/merge ::create-sub-rule/fields
              ::forms/fsm-form))
