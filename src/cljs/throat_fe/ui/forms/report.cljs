;; ui/forms/report.cljs -- Report post or comment form for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.report
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.comment :as-alias comment]
   [throat-fe.content.post :as-alias post]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]))

;; Report post reason form.

(def form-path
  "Where to find the form's state in the db."
  [:ui-state :forms :post :report])

(defn initialize-report-form
  "Initialize the report form."
  [db cid]
  (let [pid (get-in db [:view :options :pid])]
    (assoc-in
     db form-path
     {:reason              :nothing-selected
      :rule                :nothing-selected
      :explanation         ""
      :pid                 pid
      :cid                 cid
      :errors              []
      :send-errors         []
      :disable-on-success? false
      :form-state          :INCOMPLETE
      :actions             {:send
                            (if cid
                              [::forms/dispatch-server-event form-path
                               ::comment/report-comment
                               ::comment/receive-report-comment-response]
                              [::forms/dispatch-server-event form-path
                               ::post/report-post
                               ::post/receive-report-post-response])}
      :reset-function      #(initialize-report-form % cid)
      :validators
      [[#(= :nothing-selected (:reason %))
        (trx db "Please select a reason.")]
       [#(and (= :sub-rule (:reason %))
              (= :nothing-selected (:rule %)))
        (trx db "Please select a rule.")]
       [#(and (or (= :other (:reason %))
                  (and (= :sub-rule (:reason %)) (= :other (:rule %))))
              (or (< (count (:explanation %)) 2)
                  (> (count (:explanation %)) 128)))
        (trx db
             "Your explanation should be between 2 and 128 characters.")]]})))

;; Specs for the report form.

(spec/def ::reason #{:nothing-selected :site-rule :sub-rule :other})
(spec/def ::rule (spec/or :nothing-selected #(= :nothing-selected %)
                          :other #(= :other %)
                          :rule-id string?))
(spec/def ::explanation string?)
(spec/def ::pid string?)
(spec/def ::cid (spec/nilable string?))

(spec/def ::fields
  (spec/keys :req-un [::reason ::rule ::explanation ::pid ::cid]))
(spec/def ::report (spec/merge ::fields ::forms/form))
