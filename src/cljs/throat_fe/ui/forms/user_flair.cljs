;; ui/forms/user-flair.cljs -- Change user flair form for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.forms.user-flair
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]
   [throat-fe.content.internal :as content]
   [throat-fe.tr :refer [trx]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.user :as user]))

(def form-path
  "Where to find this form's state in the db."
  (conj user/form-path :user-flair))

(defn initialize-user-flair-form
  "Initial values and state for the user flair form."
  [db path]
  (let [sub-name (get-in db [:view :options :sub])
        sub-info (content/sub-info-by-name db sub-name)
        {:keys [freeform-user-flairs user-flair-choices]} sub-info
        user-flair (content/current-user-sub-flair db)]
    (assoc-in db path
              {:flair                (or user-flair "")
               :name                 (:name sub-info)
               :sid                  (:sid sub-info)
               :presets              user-flair-choices
               :freeform-user-flairs freeform-user-flairs
               :errors               []
               :send-errors          []
               :form-state           :READY
               :disable-on-success?  false
               :reset-function       initialize-user-flair-form
               :validators
               [[#(empty? (str/trim (:flair %)))
                 (trx db "Please fill out this field.")]
                [#(> (count (str/trim (:flair %))) 25)
                 (trx db "Flair is too long.")]]
               :actions
               {:change [::forms/dispatch-server-event path
                         ::user/change-user-flair
                         ::user/receive-user-flair-change]
                :remove [::forms/dispatch-server-event path
                         ::user/remove-user-flair
                         ::user/receive-user-flair-change]}})))

;; Specs for the user flair form.
(spec/def ::flair string?)
(spec/def ::name string?)
(spec/def ::presets (spec/nilable (spec/* string?)))
(spec/def ::freeform-user-flairs boolean?)
(spec/def ::fields (spec/keys :req-un [::flair ::name ::presets
                                       ::freeform-user-flairs]))
(spec/def ::user-flair (spec/merge ::fields ::forms/form))

(re-frame/reg-event-db ::initialize-user-flair-form
  ;; :doc Initialize the user flair form.
  (fn-traced [db [_ _]]
    (initialize-user-flair-form db form-path)))
