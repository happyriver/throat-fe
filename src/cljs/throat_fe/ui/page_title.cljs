;; ui/page-title.cljs -- Set the page title for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.ui.page-title
  (:require [day8.re-frame.tracing :refer-macros [fn-traced]]
            [re-frame.core :as re-frame]
            [throat-fe.user :as user]))

(re-frame/reg-fx ::set
  ;; :doc Set the title in the JS DOM.
  (fn-traced [value]
    (set! (.. js/document -title) value)))

(defn titlemsg
  "Construct the title for the page."
  [db]
  (let [num (user/sum-all-notifications (:current-user db))
        count (if (pos? num) (str "(" num ") ") "")
        lema (or (get-in db [:settings :lema]) "")
        title (or (get-in db [:content :pagetitle]) "")
        sep (if (not (or (= lema "") (= title "")))
              " | " "")]
    (str count title sep lema)))

(re-frame/reg-event-fx ::refresh
  ;; :doc Refresh the page title (used after lema is fetched).
  (fn-traced [{:keys [db]} [_ _]]
    {:fx [[::set (titlemsg db)]]}))

(re-frame/reg-event-fx ::update
  ;; :doc Set a new page title message and refresh the page title.
  (fn-traced [{:keys [db]} [_ title]]
    (let [updated-db (assoc-in db [:content :pagetitle] title)]
      {:db updated-db
       ::set (titlemsg updated-db)})))
