;; content/spec.cljs -- Current user specs for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.user.spec
  (:require
   [clojure.spec.alpha :as spec]
   [throat-fe.content.graphql-spec]))

(spec/def :current-user.attributes/is-admin boolean?)
(spec/def :current-user.attributes/can-admin
  :type.UserAttributes/can_admin)
(spec/def :current-user.attributes/totp-expiration (spec/nilable int?))
(spec/def :current-user.attributes/can-upload
  :type.UserAttributes/can_upload)
(spec/def :current-user.attributes/nsfw-blur
  :type.UserAttributes/nsfw_blur)
(spec/def :current-user/attributes
  (spec/keys :req-un [:current-user.attributes/can-admin
                      :current-user.attributes/totp-expiration
                      :current-user.attributes/can-upload
                      :current-user.attributes/is-admin
                      :type.UserAttributes/nsfw
                      :current-user.attributes/nsfw-blur]))
(spec/def :current-user.subs-moderated/open-report-count
  :type.Sub/open_report_count)
(spec/def :current-user.subs-moderated/unread-modmail-count
  :type.Sub/unread_modmail_count)
(spec/def :current-user.subs-moderated/moderation-level
  #{:OWNER :MODERATOR :JANITOR})
(spec/def :current-user/subs-moderated
  (spec/*
   (spec/keys :req-un [:type.Sub/name
                       :type.Sub/sid
                       :current-user.subs-moderated/open-report-count
                       :current-user.subs-moderated/unread-modmail-count
                       :current-user.subs-moderated/moderation-level])))

(spec/def :current-user.subscriptions/status #{:SUBSCRIBED :BLOCKED})
(spec/def :current-user/subscriptions
  (spec/*
   (spec/keys :req-un [:type.Subscription/name
                       :current-user.subscriptions/status]
              :opt-un [:type.Subscription/order])))
(spec/def :current-user/unread-message-count
  :type.User/unread_message_count)
(spec/def :current-user/unread-notification-count
  :type.User/unread_notification_count)
(spec/def :current-user/content-block #{:HIDE :BLUR})
(spec/def :current-user/content-blocks
  (spec/map-of :type.User/uid :current-user/content-block))
(spec/def ::current-user
  (spec/nilable
   (spec/keys :req-un [:current-user/attributes
                       :type.User/language
                       :type.User/name
                       :type.User/score
                       :current-user/subs-moderated
                       :current-user/subscriptions
                       :type.User/uid
                       :current-user/unread-message-count
                       :current-user/unread-notification-count]
              :opt-un [:current-user/content-blocks])))
