;; log.cljs -- Logging macros for throat-fe
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.log
  (:require [ajax.core :as ajax]
            [clojure.walk :as walk]
            [lambdaisland.glogi :as glogi]
            [re-frame.core :as re-frame]
            [re-frame.db :as re-frame-db]
            [throat-fe.config :as config]
            [throat-fe.content.settings :as-alias settings])
  (:require-macros [throat-fe.log]))

(defn log
  "Output a log message to the given logger.
  Optionally provide an exception to be logged."
  ([name lvl message]
   (glogi/log name lvl message nil))
  ([name lvl message exception]
   (glogi/log name lvl message exception)))

(defn db-handler
  "Record errors and warnings in the db."
  [{:keys [level] :as log-record}]
  (when (>= (glogi/level-value level) (glogi/level-value :warning))
    (re-frame/dispatch [::settings/add-log-message log-record])))

(defn- mask-passwords
  "Strip possible passwords out of log records sent to the back end."
  [log-record]
  (walk/postwalk (fn [node]
                   (if (map? node)
                     (let [{:keys [password new_password]} node]
                       (cond-> node
                         password (assoc :password "***")
                         new_password (assoc :new_password "***")))
                     node))
                 log-record))

(defn backend-handler
  "Send log messages to the back end."
  [log-record]
  (try
    (let [origin (.. js/window -location -origin)
          handler (constantly nil)
          error-handler (constantly nil)
          csrf-token (get-in @re-frame-db/app-db [:settings :csrf-token])
          {:keys [logger-name message]} log-record
          ;; GraphQL results are useful in the browser console, but
          ;; avoid sending them back to the server.
          log-record' (if (and (= logger-name "throat-fe.graphql")
                               (map? message))
                        (update-in log-record [:message :response] dissoc :data)
                        log-record)]
      (ajax/POST (str origin "/do/record_log")
                 {:params (mask-passwords log-record')
                  :headers {"Request-Verification-Token" csrf-token}
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))
    (catch js/Error e
      (when config/debug?
        (js/console.log "log/backend-handler error" e)))))
