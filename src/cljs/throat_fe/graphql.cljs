;; graphql.cljs -- GraphQL definitions for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.graphql
  (:require [clojure.edn :as edn]
            [com.rpl.specter :as s]
            [day8.re-frame.tracing :refer-macros [fn-traced]]
            [goog.string]
            [graphql-builder.core :as core]
            [graphql-builder.parser :refer-macros [defgraphql]]
            [re-frame.core :as re-frame]
            [re-frame.std-interceptors :refer [path]]
            [re-graph.core :as re-graph]
            [shadow.resource :as rc]
            [throat-fe.config :as config]
            [throat-fe.errors :as errors]
            [throat-fe.log :as log]
            [throat-fe.spec.graphql :as-alias spec]
            [throat-fe.util :as util]))

;; Import graphql queries.
(defgraphql subs-graphql "src/cljs/throat_fe/content/subs.graphql")

(def query-hash-table
  (->> "query-hashes.edn"
       rc/inline
       edn/read-string
       (map (fn [elem] [(:query elem) elem]))
       (into {})))

(defn query-map [graphql]
  (core/query-map graphql {:inline-fragments true}))

(defn prepare-query
  "If the query is in 'query-hash-table', return the hash instead of the
  query.  Otherwise, in development mode, return the query (without
  the prefix, which disagrees with re-graph).  In production, return
  the keyword name of the query, which the server will log as an
  error."
  [query typ key]
  (let [{:keys [hash]} (get query-hash-table query)]
    (or hash
        (do
          (log/warn {:typ typ :key key} "Query not in query-hashes.edn")
          (if config/debug?
            query
            (str ":" (name key)))))))

(def query-name-regex #"\s*([a-z0-9]+) ?([a-z0-9_]*)")
(def query-regex #"\s*(mutation|query|subscription) ([a-z0-9_]+)")

(defn query-name-for-test
  "Given a query processed by `prepare-query`, return the name
  of the query.  Used by the tests."
  [hashed-query]
  (let [[_ word1 word2] (re-find query-name-regex hashed-query)
        unhashed (first (s/select [s/MAP-VALS #(= (:hash %) word1)]
                                  query-hash-table))
        [_ _ unhashed-name] (when unhashed
                              (->> (:query unhashed)
                                   (re-find query-regex)))]
    (or unhashed-name (if (#{"query" "mutation"} word1)
                        word2 word1))))

(defn get-graphql [graphql typ key]
  (let [query-fn (get-in (query-map graphql) [typ key])]
    (if query-fn
      (-> (query-fn)
          (get-in [:graphql :query])
          (prepare-query typ key))
      (log/error {:typ typ :key key} "GraphQL not found"))))

(defn get-query
  "Get the query named by 'key' from 'graphql'."
  [graphql key]
  (get-graphql graphql :query key))

(defn get-mutation
  "Get the mutation named by 'key' from 'graphql'."
  [graphql key]
  (get-graphql graphql :mutation key))

(defn get-subscription
  "Get the subscription named by 'key' from 'graphql'."
  [graphql key]
  (get-graphql graphql :subscription key))

(re-frame/reg-cofx ::html-query-results
  ;; :doc Extract query results sent by the server from the HTML.
  (fn [cofx _]
    (let [elem (.getElementById js/document "queries")]
      (assoc cofx :query-results
             (when elem
               (->> (.getAttribute elem "data-value")
                    util/parse-json
                    (s/transform [s/ALL :query] keyword)))))))

(re-frame/reg-event-fx ::init-query-cache
  ;; :doc Initialize the query cache with results from the HTML.
  [(re-frame/inject-cofx ::html-query-results)]
  (fn-traced [{:keys [db query-results]} [_ _]]
    {:db (assoc db :query-cache query-results)}))

(re-frame/reg-event-fx ::handle-query-response
  ;; :doc Handle the response to a graphql query or mutation.
  ;; :doc Applies kebab-case to the data before passing it onto the
  ;; :doc provided handler, by assoc'ing it into the map that is then
  ;; :doc handler's first argument  In test or debug mode, check the result
  ;; :doc against its spec.
  (fn-traced [_ [_ {:keys [query-key handler response cache-hit?] :as args}]]
    (when-not cache-hit?
      (log/info {:query-key query-key
                 :response response} "Received GraphQL response"))
    {:fx [(when (or config/debug? config/testing?)
            [:dispatch [::spec/check-graphql-spec args]])
          [:dispatch (->> (util/kebab-case-keys response)
                          (update handler 1 assoc :response))]]}))

(defn- find-in-cache
  [cache name variables]
  (some (fn [entry]
          (when (and (= name (:query entry))
                     (= variables (:variables entry)))
            entry))
        cache))

(re-frame/reg-event-fx ::query
  ;; :doc Perform and cache a graphql query.
  ;; :doc Similar to ::query, but takes arguments as a map.
  ;; :doc `name` is the name given to the query in the .graphql where
  ;; :doc it is defined. `id` is a unique id, used to prevent duplicate
  ;; :doc queries.
  [(path [:query-cache])]
  (fn-traced [{:keys [db]} [_ {:keys [graphql name id variables handler]}]]
    (let [existing (find-in-cache db name variables)]
      (if existing
        (log/info {:name name :existing existing} "GraphQL query cache hit")
        (log/info {:name name :variables variables} "Dispatching GraphQL query"))
      {:fx [(if existing
              [:dispatch [::handle-query-response
                          {:type :query
                           :cache-hit? true
                           :query-key name
                           :handler handler
                           :response (:result existing)}]]
              [:dispatch [::re-graph/query
                          {:id (or id name)
                           :query (get-query graphql name)
                           :variables variables
                           :callback [::handle-query-response
                                      {:type :query
                                       :query-key name
                                       :handler handler}]}]])]})))

(re-frame/reg-event-fx ::mutate
  ;; :doc Run a graphql mutation and check its results.
  ;; :doc Similar to ::mutate, but takes arguments as a map.
  ;; :doc `name` is the name given to the query in the .graphql where
  ;; :doc it is defined. `id` is a unique id, used to prevent duplicate
  ;; :doc queries, which may be nil in which case `name` will be used.
  (fn-traced [{:keys [db]} [_ {:keys [graphql name id variables handler]}]]
    (let [query (get-mutation graphql name)]
      (log/info {:id name :variables variables} "Dispatching GraphQL mutation")
      {:db (assoc db :query-cache [])
       :fx [[:dispatch [::re-graph/mutate
                        {:id (or id name)
                         :query query
                         :variables variables
                         :callback [::handle-query-response
                                    {:type :mutation
                                     :query-key name
                                     :handler handler}]}]]]})))

(re-frame/reg-event-db ::receive-mutation-handler
  ;; :doc Receive a mutation response.
  ;; :doc Record errors associated with the passed event value,
  ;; :doc and run a restore function if one is supplied and an
  ;; :doc error is received.
  (fn-traced [db [_ {:keys [event restore response]}]]
    (let [errors (:errors response)]
      (cond-> db
        errors (errors/assoc-errors {:event event :errors errors})
        (and errors restore) restore))))

(re-frame/reg-event-fx ::subscribe
  (fn-traced [_ [_ {:keys [graphql name id variables handler]}]]
    (log/info {:name name :id id :variables variables}
              "Dispatching GraphQL subscription")
    (let [query (get-subscription graphql name)]
      {:fx [[:dispatch [::re-graph/subscribe
                        {:id (or id name)
                         :query query
                         :variables variables
                         :callback [::handle-query-response
                                    {:type :subscription
                                     :query-key name
                                     :handler handler}]}]]]})))
