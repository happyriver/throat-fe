;; util.cljs -- Utility functions for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.util
  (:require
   ["markdown-it" :as markdown-it]
   ["markdown-it-link-attributes" :as markdown-it-link-attributes]
   ["markdown-it-reddit-spoiler" :as markdown-it-reddit-spoiler]
   ["markdown-it-sub" :as markdown-it-sub]
   ["markdown-it-sup" :as markdown-it-sup]
   [ajax.core :as ajax]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [goog.url]
   [re-frame.core :as re-frame]
   [re-frame.interceptor :refer [->interceptor
                                 get-coeffect assoc-coeffect
                                 get-effect assoc-effect]]
   [statecharts.core :as fsm]
   [throat-fe.log :as log]
   [throat-fe.tr :refer [trx trnx]]))

(defn parse-json
  "Parse JSON from a string and keywordize the keys."
  [str]
  (-> str
      js/JSON.parse
      (js->clj :keywordize-keys true)))

(def units [{:name "second" :limit 60 :in-second 1}
            {:name "minute" :limit 3600 :in-second 60}
            {:name "hour" :limit 86400 :in-second 3600}
            {:name "day" :limit 604800 :in-second 86400}
            {:name "week" :limit 2629743 :in-second 604800}
            {:name "month" :limit 31556926 :in-second 2629743}
            {:name "year" :limit nil :in-second 31556926}])

(defn zero-pad
  "Convert `num` to a string and pad it on the left with 0s to `width`."
  [num width]
  (. (str num) padStart width "0"))

(defn time-ago [then now lang]
  (let [diff (/ (- now then) 1000)]
    (if (< diff 60)
      (trx lang "just now")
      (let [unit (first (drop-while #(and (:limit %)
                                          (>= diff (:limit %)))
                                    units))
            num (-> (/ diff (:in-second unit))
                    Math/floor
                    int)]
        (condp = (:name unit)
          "second" (trnx lang ["a second ago" "%s seconds ago"] num)
          "minute" (trnx lang ["a minute ago" "%s minutes ago"] num)
          "hour" (trnx lang ["an hour ago" "%s hours ago"] num)
          "day" (trnx lang ["yesterday" "%s days ago"] num)
          "week" (trnx lang ["last week" "%s weeks ago"] num)
          "month" (trnx lang ["last month" "%s months ago"] num)
          "year" (trnx lang ["last year" "%s years ago"] num))))))

(defn time-until [now then lang]
  (let [diff (/ (- then now) 1000)]
    (if (< diff 60)
      (trx lang "in a moment")
      (let [unit (first (drop-while #(and (:limit %)
                                          (>= diff (:limit %)))
                                    units))
            num (-> (/ diff (:in-second unit))
                    Math/floor
                    int)]
        (condp = (:name unit)
          "second" (trnx lang ["in a second" "in %s seconds"] num)
          "minute" (trnx lang ["in a minute" "in %s minutes"] num)
          "hour" (trnx lang ["in an hour" "in %s hours"] num)
          "day" (trnx lang ["in a day" "in %s days"] num)
          "week" (trnx lang ["in a week" "in %s weeks"] num)
          "month" (trnx lang ["in a month" "in %s months"] num)
          "year" (trnx lang ["in a year" "in %s years"] num))))))

(defn kebab-case-keys
  "Translate the keys in m from snake_case to kebab-case."
  [data]
  (cske/transform-keys csk/->kebab-case data))

(defn screaming-kebab-case
  "Make something into an UPPER-CASE keyword."
  [item]
  (some-> item
          csk/->kebab-case
          str/upper-case
          keyword))

(re-frame/reg-fx ::ajax-post-internal
  ;; :doc Make an AJAX POST request.
  (fn-traced [[url data success-event error-event]]
    (let [origin (.. js/window -location -origin)
          handler (fn [resp]
                    (log/info {:url url
                               :data (dissoc data :csrf_token)
                               :resp resp}
                              "Received AJAX response")
                    (when success-event
                      (re-frame/dispatch (conj success-event resp))))
          error-handler (fn [{:keys [status failure original-text] :as resp}]
                          ;; ovarit-auth sends empty response bodies,
                          ;; and cljs-ajax does not approve.
                          (if (and (= status 202)
                                   (= failure :parse)
                                   (= original-text ""))
                            (handler resp)
                            (do
                              (log/warn {:url url
                                         :data (dissoc data :csrf_token)
                                         :resp resp}
                                        "Received AJAX error response")
                              (when error-event
                                (re-frame/dispatch (conj error-event resp))))))]
      (ajax/POST (str origin url)
                 {:params data
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(re-frame/reg-event-fx ::ajax-form-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  ;; :doc This is the older API, use ::ajax-csrf-form-post instead.
  (fn-traced [{:keys [db]} [_ [url data success-event error-event]]]
    (let [csrf-token (get-in db [:settings :csrf-token])
          payload (assoc data :csrf_token csrf-token)]
      {::ajax-post-internal [url payload success-event error-event]})))

(re-frame/reg-fx ::ajax-delete
  ;; :doc Make an AJAX DELETE request.
  (fn-traced [[url success-event error-event]]
    (ajax/DELETE url
                 {:handler #(re-frame/dispatch (conj success-event %))
                  :error-handler #(re-frame/dispatch (conj error-event %))
                  :format :json
                  :response-format :json
                  :keywords? true})))

;; Multistage ajax get and post events

(defn- ajax-success-handler [{:keys [url payload success-event] :as state}]
  (fn [resp]
    (log/info {:url url :payload payload :resp resp}
              "Received AJAX response")
    (when success-event
      (re-frame/dispatch (->> (assoc state :response resp)
                              (conj success-event))))))

(defn- ajax-failure-handler [{:keys [url payload error-event] :as state}
                             success-handler]
  (fn [{:keys [status failure original-text] :as resp}]
    ;; ovarit-auth sends empty response bodies, and cljs-ajax does not
    ;; approve.
    (if (and (< status 300) (= failure :parse) (= original-text ""))
      (success-handler resp)
      (do
        (log/warn {:url url :payload payload :resp resp}
                  "Received AJAX error response")
        (when error-event
          (re-frame/dispatch (->> (assoc state :error-response resp)
                                  (conj error-event))))))))

(re-frame/reg-fx ::ajax-get
  ;; :doc Make an AJAX GET request.
  (fn-traced [{:keys [url] :as state}]
    (let [handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/GET url
                {:handler handler
                 :error-handler error-handler
                 :response-format :json
                 :keywords? true}))))

(re-frame/reg-fx ::ajax-post
  ;; :doc Make an AJAX POST request.
  (fn-traced [{:keys [url payload] :as state}]
    (let [origin (.. js/window -location -origin)
          handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/POST (str origin url)
                 {:params payload
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(re-frame/reg-fx ::ajax-post-formdata
  ;; :doc Make an AJAX POST request with a js/FormData object.
  (fn-traced [{:keys [url payload] :as state}]
    (let [origin (.. js/window -location -origin)
          handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/POST (str origin url)
                 {:body payload
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(defn add-prefix
  "Add a prefix to a keyword, preserving the namespace if any."
  [id extra]
  (let [with-prefix (str (name extra) "-" (name id))]
    (if (namespace id)
      (keyword (namespace id) with-prefix)
      (keyword with-prefix))))

(defn add-suffix
  "Add a suffix to a keyword, preserving the namespace if any."
  [id extra]
  (let [with-suffix (str (name id) "-" (name extra))]
    (if (namespace id)
      (keyword (namespace id) with-suffix)
      (keyword with-suffix))))

(defn- wrap-ajax-caller
  "Wrap an reg-event-fx handler to add handler events.
  Call the handler, and then modify the map it returns."
  [caller success-id failure-id]
  (fn [ctx event]
    (let [result (caller ctx event)]
      (s/transform [:fx s/ALL
                    #(#{::ajax-post ::ajax-get} (first %))
                    (s/nthpath 1)]
                   #(assoc %
                           :success-event [success-id]
                           :error-event [failure-id])
                   result))))

(defn reg-ajax-event-chain
  "Register a set of three re-frame events to handle an AJAX Post request.
  Arguments are a keyword `id` for naming the events, and the three
  functions in a map with keys :call, :success and :failure.  Any use
  of ::ajax-post in the :call function's :fx value will be modified to
  set up the :success function as the event handler for a successful
  response and the :failure function as the event handler for an error
  response."
  [id {:keys [call success failure]}]
  (let [success-id (add-suffix id :success)
        failure-id (add-suffix id :failure)
        wrapped-call (wrap-ajax-caller call success-id failure-id)]
    (re-frame/reg-event-fx id wrapped-call)
    (re-frame/reg-event-fx success-id success)
    (re-frame/reg-event-fx failure-id failure)))

(re-frame/reg-event-fx ::ajax-csrf-form-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  (fn-traced [{:keys [db]} [_ state]]
    (let [csrf-token (get-in db [:settings :csrf-token])]
      {::ajax-post (update state :payload assoc :csrf_token csrf-token)})))

(re-frame/reg-event-fx ::ajax-csrf-formdata-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  ;; :doc The :payload field of `state` should be a js FormData object
  ;; :doc and the form will be sent as multipart/form-data.
  (fn-traced [{:keys [db]} [_ state]]
    (let [csrf-token (get-in db [:settings :csrf-token])]
      (.append (:payload state) "csrf_token" csrf-token)
      {::ajax-post-formdata state})))

(defn- wrap-ajax-form-caller
  [caller success-id failure-id]
  (fn [ctx event]
    (let [result (caller ctx event)]
      (s/transform [:fx s/ALL #(= (first %) :dispatch) (s/nthpath 1)
                    #(#{::ajax-csrf-formdata-post
                        ::ajax-csrf-form-post} (first %)) (s/nthpath 1)]
                   #(assoc %
                           :success-event [success-id]
                           :error-event [failure-id])
                   result))))

(defn reg-ajax-form-event-chain
  [id {:keys [call success failure]}]
  (let [success-id (add-suffix id :success)
        failure-id (add-suffix id :failure)
        wrapped-call (wrap-ajax-form-caller call success-id failure-id)]
    (re-frame/reg-event-fx id wrapped-call)
    (re-frame/reg-event-fx success-id success)
    (re-frame/reg-event-fx failure-id failure)))

;; Translate markdown to html.

(set! (.. markdown-it-reddit-spoiler -openTag)
      "<spoiler>")
(set! (.. markdown-it-reddit-spoiler -closeTag) "</spoiler>")

(def re-mention #"^([a-zA-Z0-9_-]+)")

(defn make-mentions-handler
  "Define a handler for markdown-it's linkify to recognize mentions."
  [prefix link-prefix]
  (clj->js
   {:validate (fn [text pos _self]
                (let [tail (subs text pos)
                      match (re-find re-mention tail)]
                  (if match
                    (count (first match))
                    0)))
    :normalize (fn [match]
                 (let [url (. match -url)]
                   (set! (.. match -url)
                         (str link-prefix (subs url (count prefix))))))}))

(defn create-markdown-renderer
  "Create a customized markdown-it object."
  [{:keys [sub-prefix]}]
  (let [sub-mention (str "/" sub-prefix "/")
        obj (-> (markdown-it (clj->js {:linkify true}))
                ;; Disable the code blocks that start with leading spaces,
                ;; but not the backtick or tilde-fenced ones.
                (.disable (clj->js ["code"]))
                (.use markdown-it-reddit-spoiler/spoiler)
                (.use markdown-it-reddit-spoiler/blockquote)
                (.use markdown-it-sub)
                (.use markdown-it-sup)
                (.use markdown-it-link-attributes
                      (clj->js {:matcher (fn [href _]
                                           (not (str/starts-with? href "/")))
                                :attrs {:rel "noopener nofollow ugc"
                                        :target "_blank"}})))]
    (. (. obj -linkify) set (clj->js {:fuzzyLink false}))
    (. (. obj -linkify) add "@" (make-mentions-handler "@" "/u/"))
    (. (. obj -linkify) add "/u/" (make-mentions-handler "/u/" "/u/"))
    (. (. obj -linkify) add sub-mention (make-mentions-handler sub-mention
                                                               sub-mention))
    (log/info {:sub-prefix sub-prefix} "Initialized markdown renderer")
    obj))

(def md (atom nil))

(re-frame/reg-fx ::set-markdown-renderer
  ;; :doc Create the markdown renderer.
  (fn-traced [renderer]
    (reset! md renderer)))

(re-frame/reg-event-fx ::start-markdown-renderer
  ;; :doc Create the markdown renderer object.
  (fn-traced [_ [_ options]]
    {:fx [[::set-markdown-renderer (create-markdown-renderer options)]]}))

(defn markdown-to-html
  "Convert markdown to sanitized HTML."
  [text]
  (if (empty? text)
    ""
    (try
      (.render @md text)
      (catch js/Error e
        (log/error e "Error rendering markdown")
        "<p class=\"p i gray\">Error showing content.
         Refresh the page, and if the problem persists, please
         report it to the admins.</p>"))))

(defn case-insensitive-compare
  "Compare two strings, disregarding case."
  [a b]
  (compare (str/lower-case a) (str/lower-case b)))

(defn reorder-list
  "Return a new list containing the same elements as `elems`, with the
  item at `move-index` moved to `dest-index`."
  [elems move-index dest-index]
  (let [moved-elem (nth elems move-index)
        elems-before (take move-index elems)
        elems-after (take-last (- (count elems) move-index 1) elems )]
    (if (< dest-index move-index)
      (concat (take dest-index elems-before)
              [moved-elem]
              (take-last (- (count elems-before) dest-index) elems-before)
              elems-after)
      (concat  elems-before
               (take (- dest-index (count elems-before)) elems-after)
               [moved-elem]
               (take-last (- (count elems) dest-index 1) elems-after)))))

(defn update-or-add
  "Either replace or add a map to a sequence.
  If an item or items in `coll` matching `item` (using the `match?`
  funtion) are found, replace them with `item`, otherwise append
  `item` to the end of `coll`."
  [coll item match?]
  (let [existing (s/select [s/ALL match?] coll)]
    (if (empty? existing)
      (s/setval [s/END] [item] coll)
      (s/setval [s/ALL match?] item coll))))

;;; Re-frame Interceptors

(defn path-fn
  "Interceptor to substitute a path in the db for the db.
  Like the re-frame interceptor `path`, except it uses a function
  to calculate the path from the context."
  [calc-path]
  (->interceptor
   :id :comment-data
   :before
   (fn [context]
     (let [original-db (get-coeffect context :db)
           path (calc-path context)]
       (-> context
           (update ::db-store-key conj original-db)
           (update ::path conj path)
           (assoc-coeffect :db (get-in original-db path)))))

   :after
   (fn [context]
     (let [db-store (::db-store-key context)
           original-db (peek db-store)
           new-db-store (pop db-store)
           path (peek (::path context))
           context' (-> context
                        (assoc ::db-store-key new-db-store)
                        (update ::path pop)
                        (assoc-coeffect :db original-db))
           db (get-effect context :db ::not-found)]
       (if (= db ::not-found)
         context'
         (->> (assoc-in original-db path db)
              (assoc-effect context' :db)))))))

(defn trimmed-count
  "Return the number of characters in a string after trimming whitespace."
  [s]
  (-> s str/trim count))

(defn parse-int
  "Parse a string into a number.
  Return nil if the argument is nil."
  [s]
  (when s (js/parseInt s)))

(defn valid-url?
  "Return true if a URL can be parsed."
  [link]
  (try
    (goog.url/resolveUrl link)
    true
    (catch js/Error _ false)))

(defn set-of-keywords
  "Convert a collection of strings into a set of keywords."
  [items]
  (->> items
       (map keyword)
       (set)))

;;; Statecharts helpers

(defn queue-fx
  "Wrap a function that returns a re-frame event.
  The wrapped function will add the event to the :fx key in the state map,
  and use fsm/assign to ensure it is persisted."
  [event-fn]
  (fsm/assign (fn [state event]
                (->> (event-fn state event)
                     (update state :fx conj)))))

(defn queue-multi-fx
  "Wrap a function that returns multiple re-frame events.
  The wrapped function will add the events to the :fx key in the state map,
  and use fsm/assign to ensure it is persisted."
  [event-fn]
  (fsm/assign (fn [state event]
                (->> (event-fn state event)
                     (update state :fx concat)))))
