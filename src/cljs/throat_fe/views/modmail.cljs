;; views/modmail.cljs -- Modmail for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.views.modmail
  (:require
   [clojure.string :as str]
   [re-frame.core :refer [subscribe dispatch]]
   [throat-fe.content.messages :as messages]
   [throat-fe.content.settings :as settings]
   [throat-fe.content.subs :as subs]
   [throat-fe.tr :refer [tr trm-html trn]]
   [throat-fe.ui.forms.compose-message :as compose]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.views.common :refer [dangerous-html helper-text userlink]]
   [throat-fe.views.forms :refer [transmission-error-if-present
                                  validation-error-if-present]]
   [throat-fe.views.icon :refer [icon]]
   [throat-fe.views.inputs :refer [button checkbox dropdown-menu radio-button
                                   markdown-link-modal text-input]]
   [throat-fe.views.messages :as view-messages]
   [throat-fe.views.misc-panels :as misc-panels]
   [throat-fe.views.moderation :as moderation]
   [throat-fe.views.panels :refer [panel]]
   [throat-fe.views.sidebar :refer [modmail-sidebar]]
   [throat-fe.views.util :refer [cls url-for]]))

(def compose-form compose/form-path)
(def icon-class (cls :dib.mh1.w16px.h16px.pointer.relative.z-1))

(defn modmail-menu-icon
  "Create an icon for the icon menu on a modmail message."
  [{:keys [flag class title-on title-off icon-name event]}]
  (let [icon-fill @(subscribe [::window/icon-fill])]
    [icon
     {:class (cls icon-class (or class icon-fill))
      :title (if flag title-on title-off)
      :on-click #(dispatch event)}
     icon-name]))

(defn sender-and-receiver
  [{:keys [receiver receiver-is-mod? sender sender-is-mod?
           sent-with-name-hidden?]}]
  [:<>
   (if (nil? (:name sender))
     (tr "[Deleted]")
     [userlink {:class (when sender-is-mod? (cls :custom.green))
                :prefix "/u/"}
      (:name sender)
      (when sent-with-name-hidden? " [as mod]")])
   (when receiver
     (if (nil? (:name receiver))
       (tr " ➜ [Deleted]")
       [userlink {:class (when receiver-is-mod? (cls :custom.green))
                  :prefix " ➜ /u/"}
        (:name receiver)]))])

(defn- message-class
  [unread? mod-discussion?]
  (let [day? @(subscribe [::window/day?])]
    (cls :pa3.mv2.br3.bw3.br--left.shadow-7
         (when unread?
           [:bl (if day?
                  :b--orangey-brown :b--reddish-brown)])
         (if mod-discussion?
           (if day? :bg-washed-purple :bg-grim-purple)
           (if day? :alternating-near-white :alternating-near-black)))))

(defn modmail-message-in-thread-view
  "Create a modmail message for a message thread."
  [{:keys [mid html-content timeago receiver unread? mtype] :as message}]
  (let [supervising-admin? @(subscribe [::messages/supervising-admin?])
        mod-discussion? (or (= mtype :MOD_DISCUSSION)
                            (and (= mtype :MOD_TO_USER_AS_MOD)
                                 (nil? receiver)))]
    [:article {:class (message-class unread? mod-discussion?)}
     [:div.mid-gray.f6.mb2
      [sender-and-receiver message]
      (str " - " timeago)]
     [dangerous-html {} html-content]
     [:div.flex.mt1
      [:div.w-100.w-80-ns]
      [:div.w-100.w-20-ns.mid-gray
       [:div.fr
        (when-not supervising-admin?
          [modmail-menu-icon
           {:class (when unread? :fill-light-green)
            :icon-name "ccheck"
            :title-on (tr "Mark read")
            :title-off (tr "Mark unread")
            :event [::messages/set-unread mid (not unread?)]}])]]]]))

(defn modmail-thread-url
  "Calculate the URL for a modmail thread, given the mid of the parent
  message, and the current mailbox and sub selections."
  [mailbox mid]
  (let [sub-selection @(subscribe [::messages/modmail-sub-selection])]
    (if (= :all-subs sub-selection)
      (url-for :modmail/thread :mailbox mailbox :msg mid)
      (url-for :modmail/thread :mailbox mailbox :msg mid
               :query-args {:sub sub-selection}))))

(defn modmail-message-in-mailbox-list
  "Create the latest message in the thread for the mailbox list panel."
  [{:keys [id latest-message subject sub sub-color-class reply-count] :as thread}
   mailbox]
  (let [{:keys [mid html-content timeago receiver unread? mtype]
         :as message} latest-message
        archived? (= (:mailbox thread) :ARCHIVED)
        supervising-admin? @(subscribe [::messages/supervising-admin?])
        icon-fill @(subscribe [::window/icon-fill])
        mod-discussion? (or (= mtype :MOD_DISCUSSION)
                            (and (= mtype :MOD_TO_USER_AS_MOD)
                                 (nil? receiver)))]
    ^{:key id}
    [:article {:class (message-class unread? mod-discussion?)}
     [:div.relative
      [:div.dib.b
       [icon {:class (cls icon-class sub-color-class)} "mail"]
       [:a.dib.b.ml2 {:href (url-for :sub/view-sub :sub (:name sub))}
        " " (:name sub)]]
      [:p.bb.bw1.b--moon-gray.ml4 subject]
      [:div.mid-gray.f6.mb2
       [sender-and-receiver message]]
      [dangerous-html {} html-content]
      [:div.mid-gray.f6.flex.mt1
       [:div.w-100.w-80-ns
        (str (trn ["%s reply" "%s replies"] reply-count) " - " timeago)]
       [:div.w-100.w-20-ns
        [:div.fr
         (when-not supervising-admin?
           [:<>
            [modmail-menu-icon
             {:class (when unread? :fill-light-green)
              :icon-name "ccheck"
              :title-on (tr "Mark read")
              :title-off (tr "Mark unread")
              :event [::messages/set-unread mid (not unread?)]}]
            (when (not= mailbox :discussions)
              [modmail-menu-icon
               {:class (when archived? :fill-blue)
                :icon-name "filebox"
                :title-on (tr "Unarchive")
                :title-off (tr "Archive")
                :event [::messages/set-archived id (not archived?)]}])
            [:a {:title (tr "See thread and reply")
                 :href (modmail-thread-url mailbox id)}
             [icon {:class (cls icon-class icon-fill)} "reply"]]])
         [:a.absolute.absolute--fill
          {:href (modmail-thread-url mailbox id)}]]]]]]))

(defn show-username-checkbox
  "Create the show username checkbox for the modmail compose forms."
  []
  (let [show-username? (fn [] @(subscribe
                                [::forms/field compose-form :show-username?]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe
                                [::forms/editing-disabled? compose-form])]
        [:div.inline-flex
         [checkbox {:disabled editing-disabled?
                    :label (tr "Show recipient my username")
                    :label-class :mv2
                    :value show-username?
                    :event [::forms/edit compose-form :show-username?]
                    :form-state form-state}]]))))

(defn send-to-user-or-mods
  "Create a pair of radio buttons to choose the destination of a modmail."
  [to-mods-label to-user-label]
  (let [send-to-user? @(subscribe [::forms/field compose-form :send-to-user?])
        form-state @(subscribe [::forms/form-state compose-form])
        editing-disabled? @(subscribe [::forms/editing-disabled? compose-form])]
    [:<>
     [radio-button
      {:name "modmail-recipient" :id "modmail-recipient-mod"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label to-mods-label
       :value "mod"
       :form-value (if send-to-user? "user" "mod")
       :event [::forms/edit compose-form :send-to-user? false]
       :form-state form-state}]
     [radio-button
      {:name "modmail-recipient" :id "modmail-recipient-user"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label to-user-label
       :value "user"
       :form-value (if send-to-user? "user" "mod")
       :event [::forms/edit compose-form :send-to-user? true]
       :form-state form-state}]]))

(defn modmail-reply-form
  "Create the form elements for the modmail reply panel."
  []
  (let [mailbox @(subscribe [:view/mailbox])
        send-to-user? @(subscribe [::forms/field compose-form :send-to-user?])]
    [:div
     [view-messages/content-editor]
     [validation-error-if-present compose-form]
     (when (not= :discussions mailbox)
       [:<>
        [:div.pa2
         [send-to-user-or-mods
          (tr "Add a private moderator note")
          (tr "Reply to user")]]
        (when send-to-user?
          [show-username-checkbox])])
     [transmission-error-if-present compose-form]
     [:div.pb2
      [view-messages/send-button
       (if (and send-to-user? (not= :discussions mailbox))
         (tr "Reply")
         (tr "Add note"))] " "
      [view-messages/preview-button]
      [view-messages/success-message-on-success]]
     [view-messages/preview]]))

(defn modmail-sub-menu
  "Create a sub picker for the modmail mailbox list view."
  []
  (let [supervising-admin? @(subscribe [::messages/supervising-admin?])
        subs @(subscribe (if supervising-admin?
                           [::user/subs-not-moderated-names]
                           [::user/subs-moderated-names]))
        sub @(subscribe [::messages/modmail-sub-selection])]
    [:div.fr-ns.mt3-ns.mb1
     [dropdown-menu {:value sub
                     :choices (into {:all-subs (if supervising-admin?
                                                 (tr "My circles")
                                                 (tr "All circles"))}
                                    (zipmap subs subs))
                     :event [::messages/set-modmail-sub-selection]}]]))

(defn modmail-list
  "Create the content for the modmail list panel."
  []
  (let [options @(subscribe [:view/options])
        threads @(subscribe [::messages/modmail-mailbox-threads])
        mailbox @(subscribe [:view/mailbox])
        more? @(subscribe [::messages/more-modmail-messages-available?])
        cursor @(subscribe [::messages/modmail-mailbox-cursor])
        status @(subscribe [::window/status])
        page-size @(subscribe [::settings/page-size])
        supervising-admin? @(subscribe [::messages/supervising-admin?])]
    [:<>
     [:div
      [:h2.w-100.w-50-ns.dib
       (tr "Moderator Mail")]
      [:div.w-100.w-50-ns.dib
       [modmail-sub-menu]]]
     (when supervising-admin?
       [helper-text "You are viewing this circle's messages as the admin."])
     (if (and (= :loaded status) (empty? threads))
       [:h4 (tr "Nothing here, yet...")]
       (into [:div]
             (map (fn [thread]
                    [modmail-message-in-mailbox-list thread mailbox])
                  (if (= :loaded status)
                    threads
                    (take page-size threads)))))
     (when more?
       [button
        {:button-class :secondary
         :on-click #(dispatch [::messages/load-modmail-category
                               (assoc options :cursor cursor)])}
        (tr "Load more")])]))

(defn modmail-list-panel
  "Create the modmail list panel."
  []
  [panel {:sidebar modmail-sidebar
          :footer :<>}
   [modmail-list]])

(defn modmail-thread
  "Create the content for the single-thread modmail panel."
  []
  (let [options @(subscribe [:view/options])
        mailbox @(subscribe [:view/mailbox])
        thread @(subscribe [::messages/modmail-thread-info])
        {:keys [thread-id more-items-available? cursor sub subject
                report]} thread
        archived? (= (:mailbox thread) :ARCHIVED)
        sub-color-class @(subscribe [::messages/modmail-thread-sub-color-class])
        page-size @(subscribe [::settings/page-size])
        parent-message @(subscribe [::messages/modmail-thread-parent])
        messages @(subscribe [::messages/modmail-thread])
        any-unread? @(subscribe [::messages/modmail-thread-any-unread])
        status @(subscribe [::window/status])
        supervising-admin? @(subscribe [::messages/supervising-admin?])
        icon-fill @(subscribe [::window/icon-fill])]
    [:<>
     (when (= :loaded status)
       (if (and (nil? parent-message) (empty? messages))
         [:h4 (tr "Nothing here, yet...")]
         [:div
          (when supervising-admin?
            [helper-text
             "You are viewing this circle's messages as the admin."])
          [:div.pb2
           [:div.w-100.w-50-ns.dib
            [icon {:class (cls :w16px.h16px.dib sub-color-class)} "mail"]
            [:a.dib.ml2.b.f4
             {:href (url-for :sub/view-sub :sub (:name sub))}
             (:name sub)]]
           (when-not supervising-admin?
             [:div.w-100.w-50-ns.pv2.pv1-ns.dib
              (when (not= :discussions mailbox)
                [:a.mr3.fr-ns
                 {:on-click #(dispatch [::messages/set-archived
                                        thread-id (not archived?)])}
                 [icon {:class (cls icon-class (if archived?
                                                 "fill-blue"
                                                 icon-fill))}
                  "filebox"]
                 (if archived?
                   (tr "Un-archive thread")
                   (tr "Archive thread"))])
              [:a.mr3.fr-ns
               {:on-click #(dispatch [::messages/set-thread-unread
                                      thread-id (not any-unread?)])}
               [icon {:class (cls icon-class (if any-unread?
                                               "fill-light-green"
                                               icon-fill))}
                "ccheck"]
               (if any-unread?
                 (tr "Mark all read")
                 (tr "Mark all unread"))]])]
          [:div {:class (cls :ml3.b.bb.b--moon-gray
                             (if report :mb2 :mb3))} subject]
          (when report
            [:a.db.ml3 {:href (url-for :mod/report-details
                                       :sub (:name sub)
                                       :report-type (-> (:rtype report)
                                                        name
                                                        str/lower-case)
                                       :report-id (:id report))}
             [icon {:class (cls icon-class :fill-purple)} "shield"]
             [:span.fw7.f7.mv2
              (if (= (:rtype report) :POST)
                (tr "Linked post report")
                (tr "Linked comment report"))]])
          (into [:div
                 (modmail-message-in-thread-view parent-message)
                 (when more-items-available?
                   [:a
                    {:on-click #(dispatch [::messages/load-modmails
                                           (assoc options :cursor cursor)])}
                    "▹ "
                    [:span.i (tr "Show older replies")]])]
                (map modmail-message-in-thread-view
                     (if (= :loaded status)
                       messages
                       (take-last page-size messages))))
          (when-not supervising-admin?
            [:<>
             [modmail-reply-form]
             [markdown-link-modal]])]))]))

(defn modmail-thread-panel
  "Create the modmail thread panel."
  []
  [panel {:sidebar modmail-sidebar
          :footer :<>}
   [modmail-thread]])

(defn mod-of-sub-or-admin-only
  "Create the appropriate panel for modmail depending on the selected sub."
  [panel]
  (let [sub @(subscribe[::messages/modmail-sub-selection])
        subs-moderated (set @(subscribe [::user/subs-moderated-names]))
        is-admin? @(subscribe [::user/is-admin?])
        sub-load-status @(subscribe [::subs/sub-load-status])
        content-status @(subscribe [::window/status])
        sub-names (set @(subscribe [::subs/all-sub-names]))]
    (cond
      (= sub :all-subs)
      panel

      (subs-moderated sub)
      panel

      (not is-admin?)
      [misc-panels/unauthorized-panel]

      (or (= sub-load-status :loading) (= content-status :loading))
      [misc-panels/waiting-panel]

      (sub-names sub)
      panel

      :else
      [misc-panels/page-not-found-panel])))

(defn modmail-panel
  "Create a modmail panel listing messages in a mailbox or in a thread."
  []
  (let [options @(subscribe [:view/options])]
    [moderation/mods-only
     [mod-of-sub-or-admin-only
      (if (:mid options)
        [modmail-thread-panel]
        [modmail-list-panel])]]))

(defn modmail-compose-report-link
  "Create the link to a report referenced by a modmail, if any."
  []
  (when @(subscribe [::compose/linkable-to-report?])
    (let [sub @(subscribe [::forms/field compose-form :sub])
          report-type @(subscribe [::forms/field compose-form :report-type])
          report-id @(subscribe [::forms/field compose-form :report-id])
          form-state @(subscribe [::forms/form-state compose-form])
          reflink [:a {:href (url-for :mod/report-details
                                      :sub sub
                                      :report-type report-type
                                      :report-id report-id)}
                   (if (= report-type "comment")
                     (tr "Comment Report")
                     (tr "Post Report"))]]
      [:p.i (if (not= :SUCCESS form-state )
              (trm-html "Message will be linked to: %{reflink}"
                        {:reflink reflink})
              (trm-html "Message was linked to: %{reflink}"
                        {:reflink reflink}))])))

(defn modmail-compose-form
  "Create the form elements for the modmail compose new message panel."
  []
  (let [username (fn [] @(subscribe [::forms/field compose-form :username]))]
    (fn []
      (let [send-to-user? @(subscribe
                            [::forms/field compose-form :send-to-user?])
            subs-moderated @(subscribe [::user/subs-moderated-names])
            sub @(subscribe [::forms/field compose-form :sub])
            form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe
                                [::forms/editing-disabled? compose-form])]
        [:div
         [dropdown-menu {:id "modmail-compose-circle"
                         :label "Send from: "
                         :disabled editing-disabled?
                         :choices
                         (into {:nothing-selected (tr "Choose a circle")}
                               (zipmap subs-moderated subs-moderated))
                         :value sub
                         :event [::forms/edit compose-form :sub]}]
         [:div
          [send-to-user-or-mods
           (tr "Mod Discussion")
           (tr "To User")]
          (when send-to-user?
            [:div.inline-flex.pv1
             [text-input {:id "modmail-compose-name"
                          :placeholder (tr "username")
                          :value username
                          :required true
                          :update-value! forms/reset-input-on-success!
                          :form-state form-state
                          :disabled editing-disabled?
                          :event [::forms/edit compose-form :username]}]
             [show-username-checkbox]])]
         [view-messages/subject-editor]
         [view-messages/content-editor]
         [markdown-link-modal]
         [validation-error-if-present compose-form]
         [transmission-error-if-present compose-form]
         [modmail-compose-report-link]
         [:div.pv2
          [view-messages/send-button (tr "Send")] " "
          [view-messages/preview-button]
          [view-messages/success-message-on-success]
          [view-messages/preview]]]))))

(defn modmail-compose-panel
  "Create the modmail compose new message panel."
  []
  [moderation/mods-only
   [panel {:sidebar modmail-sidebar
           :footer :<>}
    [:h2 (tr "New Modmail Message")]
    [modmail-compose-form]]])
