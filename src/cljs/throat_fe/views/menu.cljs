;; views/menu.cljs -- Menu for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
(ns throat-fe.views.menu
  (:require
   [re-frame.core :refer [subscribe dispatch]]
   [throat-fe.content.settings :as settings]
   [throat-fe.tr :refer [tr]]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.views.common :refer [userlink]]
   [throat-fe.views.icon :refer [icon]]
   [throat-fe.views.util :refer [cls url-for]]))

(defn toggle-dark-mode
  "Create the sun/moon icon which toggles light and dark mode."
  []
  (let [day-night @(subscribe [::window/day-night])
        is-authenticated? @(subscribe [::user/is-authenticated?])]
    (when is-authenticated?
      [:div.glyphbutton {:id "toggledark"
                         :on-click #(dispatch [::window/toggle-daynight])}
       [icon {:class "p-icon"} (if (= day-night :light) "moon" "sun")]])))

(defn menu-site-info
  "Create the site info for the left side of the menu."
  [with-toggle?]
  (let [motto @(subscribe [::settings/motto])]
    [:div.cw-brand.pure-u-1.pure-u-md-3-24
     [:div.pure-menu
      [:a.logocont {:href (url-for :home/index)}
       [icon {:class "logo"} "logo"]]
      [:span.motto motto]
      (when with-toggle?
        [:a.th-toggle {:on-click #(dispatch [::window/toggle-menu])}
         [:s.bar]
         [:s.bar]])]]))

(defn menu-user-controls
  "Create the user controls for the right side of the menu."
  [menu-state]
  (let [current-user @(subscribe [::user/current-user])
        is-authenticated? @(subscribe [::user/is-authenticated?])
        subs-moderated-names @(subscribe [::user/subs-moderated-names])
        notifications @(subscribe [::user/notification-count])
        mod-notifications @(subscribe [::user/mod-notification-count])]
    [:div.pure-u-1.pure-u-md-12-24 {:id "th-uinfo"}
     (if (not is-authenticated?)
       ;; Anonymous user
       [:div {:class ["cw-items"
                      (when (= menu-state :open) "pure-menu-horizontal")]}
        [toggle-dark-mode]
        [:span " "]
        [:a.pure-button.pure-button-primary
         {:href (url-for :auth/login
                         :query-args {:next (.. js/window -location -origin)})}
         (tr "Log in")]
        [:span " "]
        [:a.pure-button.pure-button.button-secondary
         {:href (url-for :auth/register)} (tr "Register")]]

       ;; Not anonymous
       [:div {:class ["cw-items"
                      (when (= menu-state :open) "pure-menu-horizontal")]}
        [userlink {:class (cls :custom.smallcaps.white) :id "unameb"}
         (:name current-user)]
        [:span " "] [:span.separator] [:span " "]
        [:abbr.bold {:title (tr "Score")
                     :id "postscore"} (:score current-user)]
        [:span " "] [:span.separator] [:span " "]
        [:div.glyphbutton
         [:a {:href (url-for :user/edit-user)}
          [icon {:class "p-icon"} "cog"]]]
        [:span " "]
        [:div.glyphbutton.sep
         [:a {:href (url-for :messages/inbox-sort)}
          [:span.hasmail
           [icon {:class ["p-icon" (when-not (zero? notifications)
                                     "hasmail")]} "mail"]
           (when-not (zero? notifications)
             [:span.mailcount.white.hover-black {:id "mailcount"} (str notifications)])]]]
        (when (or (get-in current-user [:attributes :can-admin])
                  (seq subs-moderated-names))
          [:<>
           [:span " "]
           [:div.glyphbutton
            [:a {:href (url-for :mod/index)}
             [:span.hasmail
              [icon {:class ["p-icon"]} "shield"]
              (when-not (zero? mod-notifications)
                [:span.mailcount.white.hover-black {:id "modcount"} (str mod-notifications)])]]]])
        [:span " "]
        [toggle-dark-mode]
        [:span " "]
        [:span {:id "logout"}
         [:button.pure-button.button-secondary
          {:on-click #(dispatch [::user/logout])}
          (tr "Log out")]]])]))

(defn menu
  "Create the menu bar."
  []
  (let [menu-state @(subscribe [::window/menu-state])]
    [:div {:id "menu"
           :class ["th-navbar pure-g"
                   (when (= menu-state :open) "open")]}
     [menu-site-info :with-toggle]
     [menu-user-controls menu-state]]))

(defn simplified-menu
  "Create a simplified menu bar for error pages."
  []
  [:div.th-navbar.pure-g {:id "menu"}
   [menu-site-info nil]])
