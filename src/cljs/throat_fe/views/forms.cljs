;; views/forms.cljs -- Form bibs and bobs for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.views.forms
  (:require
   [re-frame.core :refer [dispatch subscribe]]
   [throat-fe.views.common :refer [dangerous-html]]
   [throat-fe.views.util :refer [cls]]
   [throat-fe.ui.forms.core :as forms]))

(defn form-error
  [content]
  [:div.mv2.br2.pa2.bg-red.white
   content])

(defn validation-error-if-present
  "Create a validation error message, if there is one to show."
  [form]
  (let [form-state @(subscribe [::forms/form-state form])
        errors @(subscribe [::forms/errors form])]
    (when (= form-state :SHOW-ERROR)
      [form-error (first errors)])))

(defn transmission-error-if-present
  "Create a transmission error message, if there is one to show."
  [form]
  (let [form-state @(subscribe [::forms/form-state form])
        send-errors @(subscribe [::forms/send-errors form])]
    (when (= form-state :SHOW-FAILURE)
      [form-error (first send-errors)])))

(defn preview
  "Create the content preview for a form."
  [{:keys [form]} text]
  (let [preview-open? @(subscribe [::forms/field form :preview-open?])
        preview-html @(subscribe [::forms/field form :preview])]
    (when preview-open?
      [:div.mt3.mb2.relative
       [:h4 text]
       [:span
        {:class (cls :absolute.top-0.right-0.fr.f3.pointer)
         :on-click #(dispatch [::forms/action form :hide-preview])} "×"]
       [dangerous-html {} preview-html]])))
