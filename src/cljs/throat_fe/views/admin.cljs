;; views/admin.cljs -- Administration ui for throat-fe
;; Copyright (C) 2021-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
(ns throat-fe.views.admin
  (:require
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]
   [throat-fe.content.admin :as admin]
   [throat-fe.tr :refer [tr]]
   [throat-fe.ui.forms.admin :as admin-forms]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.util :refer [zero-pad]]
   [throat-fe.views.common :refer [helper-text userlink table-cell
                                   table-label-cell]]
   [throat-fe.views.forms :refer [transmission-error-if-present
                                  validation-error-if-present]]
   [throat-fe.views.inputs :refer [button checkbox datepicker radio-button
                                   text-input date-selector]]
   [throat-fe.views.misc-panels :as misc-panels]
   [throat-fe.views.panels :refer [panel]]
   [throat-fe.views.sidebar :refer [admin-sidebar]]
   [throat-fe.views.util :refer [cls] :as util]))

(defn admins-only
  "Create a panel that is only for admins."
  [panel]
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        is-admin? @(subscribe [::user/is-admin?])]
    (cond
      (not is-authenticated?)
      [misc-panels/unauthenticated-panel]

      (not is-admin?)
      [misc-panels/unauthorized-panel]

      :else
      panel)))

(defn note-if-incomplete
  "Create some helper text if the date range includes today."
  [complete?]
  (when-not complete?
    [helper-text
     (tr "Time span includes the current day (GMT), so data may be incomplete.")]))

(defn stats-table-row
  [label data]
  [:tr
   [table-label-cell label]
   [table-cell data]])

(defn stats-table
  "Create a table for the site statistics."
  [{:keys [users subs posts comments upvotes downvotes]}]
  (let [border-color @(subscribe [::window/table-border-colors])]
    [:table {:class (cls :collapse.ba.bw1 border-color)}
     [:tbody
      [stats-table-row (tr "Users") users]
      [stats-table-row (tr "Circles") subs]
      [stats-table-row (tr "Posts") posts]
      [stats-table-row (tr "Comments") comments]
      [stats-table-row (tr "Votes")
       [:span "+" upvotes " | " "-" downvotes]]]]))

(defn recent-stats-table
  "Create a table for the recent site statistics."
  [{:keys [users subs posts comments upvotes downvotes
           users-who-posted users-who-commented users-who-voted]}]
  (let [border-color @(subscribe [::window/table-border-colors])]
    [:table {:class (cls :collapse.ba.bw1 border-color)}
     [:tbody
      [stats-table-row (tr "New users") users]
      [stats-table-row (tr "New circles") subs]
      [stats-table-row (tr "New posts") posts]
      [stats-table-row (tr "New comments") comments]
      [stats-table-row (tr "New votes") [:span "+" upvotes " | " "-" downvotes]]
      [stats-table-row (tr "Users who posted") users-who-posted]
      [stats-table-row (tr "Users who commented") users-who-commented]
      [stats-table-row (tr "Users who voted") users-who-voted]]]))

(defn visitor-stats-table
  "Create a daily or weekly visitor stats table."
  [option]
  (let [border-color @(subscribe [::window/table-border-colors])
        stats @(subscribe  [::admin/visitor-counts option])
        complete? @(subscribe [::admin/visitor-counts-complete? option])
        {:keys [entries average]} stats]
    [:<>
     [:table {:class (cls :collapse.ba.bw1 border-color)}
      [:tbody
       [:tr
        [table-label-cell (tr "Date")]
        [table-label-cell (tr "Visitors")]]
       (into [:<>]
             (map (fn [{:keys [month day year value]}]
                    [:tr
                     [table-cell
                      [:span
                       year "/"
                       (zero-pad month 2) "/"
                       (zero-pad day 2)]]
                     [table-cell (. value toLocaleString)]])
                  entries))
       [:tr
        [table-cell (tr "Average")]
        [table-cell (. average toLocaleString)]]]]
     (note-if-incomplete complete?)]))

(defn lifetime-stats
  "Create the site lifetime stats section."
  []
  (let [stats @(subscribe [::admin/stats])]
    [:div.pb3
     [:h2 (tr "Site lifetime")]
     (when stats
       [stats-table stats])]))

(defn recent-stats
  "Create the site stats recent activity section. "
  []
  (let [stats-timespan @(subscribe [::admin/stats-timespan])
        complete? @(subscribe [::admin/stats-timespan-complete?])
        this-year @(subscribe [::window/this-year])]
    [:div.pb3
     [:h2 (tr "Recent activity")]
     [:table.bw0.mb2
      [:tbody
       [:tr
        [:td.pa0.bw0 (tr "From: ")]
        [:td.bw0 [datepicker {:start-year 2016
                              :end-year this-year
                              :subscription [::admin/stats-since-date]
                              :setter [::admin/set-stats-since-date]}]]]
       [:tr
        [:td.pa0.bw0 (tr "Until: ")]
        [:td.bw0 [datepicker {:start-year 2016
                              :end-year this-year
                              :subscription [::admin/stats-until-date]
                              :setter [::admin/set-stats-until-date]}]]]]]
     (when stats-timespan
       [:div
        [recent-stats-table stats-timespan]
        [note-if-incomplete complete?]])]))

(defn daily-visitors
  "Create the site stats daily visitors section."
  []
  (let [this-year @(subscribe [::window/this-year])]
    [:div.pb3
     [:h2 (tr "Daily visitors")]
     [:div.mb3
      [:div.di-ns.mr2-ns.pb1 (tr "Week beginning: ")]
      [datepicker {:class (cls :di-ns)
                   :start-year 2020
                   :end-year this-year
                   :subscription [::admin/visitor-count-date :daily]
                   :setter [::admin/set-visitor-count-date :daily]}]]
     [visitor-stats-table :daily]]))

(defn weekly-visitors
  "Create the site stats weekly visitors section."
  []
  (let [this-year @(subscribe [::window/this-year])]
    [:div
     [:h2 (tr "Weekly visitors")]
     [:div.mb3
      [:div.di-ns.mr2-ns.pb1 (tr "Month beginning: ")]
      [datepicker {:class (cls :di-ns)
                   :start-year 2020
                   :end-year this-year
                   :subscription [::admin/visitor-count-date :weekly]
                   :setter [::admin/set-visitor-count-date :weekly]}]]
     [visitor-stats-table :weekly]]))

(defn stats-panel
  "Create a page to show the the admin stats dashboard, to admins only."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [lifetime-stats]  [:hr]
    [recent-stats]    [:hr]
    [daily-visitors]  [:hr]
    [weekly-visitors]]])

(defn add-button
  "Create the add button for the banned user name form."
  []
  (let [form ::admin-forms/ban-username-string
        dispatch-send #(dispatch [::forms/action form :send])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [button {:class [util/reset-padding :pv2.ph4.mv1]
                 :disabled editing-disabled?
                 :on-click dispatch-send}
         (if (= form-state :SENDING)
           (tr "Adding...")
           (tr "Add"))]))))

(defn banned-username-string-form
  "Create a form to add a new string to ban in usernames."
  []
  (let [form ::admin-forms/ban-username-string
        new-string (fn [] @(subscribe [::forms/field form :content]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])]
        [:<>
         [:p (tr "Enter a string to ban in usernames:")]
         [:div
          [text-input {:placeholder ""
                       :value new-string
                       :required true
                       :update-value! forms/reset-input-on-success!
                       :form-state form-state
                       :disabled false
                       :event [::forms/edit form :content]}]
          [validation-error-if-present form]
          [transmission-error-if-present form]
          [:div
           [add-button]]]]))))

(defn banned-username-string-table-row
  "Create a row in the banned strings in usernames table."
  [{:keys [banned users show more?]}]
  (let [unban #(dispatch [::admin/unban-username-string banned])
        body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100.pa2.ba.bw1.data-name-ns
                          (str "w-" percent "-ns")
                          body-colors border-color))]
    ^{:key banned}
    [:li.flex-ns
     [:div {:class (cell-class 25)
            :data-name (tr "String: ")}
      banned]
     [:div {:class (cls (cell-class 10) :tr-ns)
            :data-name (tr "# Users: ")}
      (count users)]
     [:div {:class (cell-class 50)
            :data-name (tr "Users: ")}
      (doall (interpose (tr ", ")
                        (map (fn [name]
                               ^{:key (str banned name)}
                               [userlink name])
                             (take show users))))
      (when more?
        [button
         {:class [util/reset-padding :dib.f6.pv1.mh2]
          :button-class :modal
          :on-click #(dispatch [::admin/show-all-users banned])}
         (tr "Show all")])]
     [:div {:class (cls (cell-class 15) :dn.db-ns)}
      [:a {:on-click unban}
       (tr "[x]")]]
     [:div.mb3
      [button {:class :dn-ns.db.mt1
               :button-class :secondary
               :on-click unban}
       "Remove"]]]))

(defn banned-user-name-list
  "Create the list of strings banned in user names."
  []
  (let [string-sort #(dispatch [::admin/set-ban-username-string-sort :string])
        count-sort #(dispatch [::admin/set-ban-username-string-sort :count])]
    (fn []
      (let [banned-strings @(subscribe [::admin/banned-username-strings])
            options @(subscribe [::admin/banned-username-sort-options])
            {:keys [sort-typ sort-asc?]} options
            sort-indicator (if sort-asc? " ▾" " ▴")
            header-colors @(subscribe [::window/table-header-colors])
            border-colors @(subscribe [::window/table-border-colors])
            header-class (cls :dn.dib-ns.b.pa2.ba.bw1
                              header-colors border-colors)]
        [:<>
         [:h2 (tr "Banned User Names")]
         [banned-username-string-form]
         (if (zero? (count banned-strings))
           [helper-text (tr "No banned strings, yet.")]
           [:ol.ph0.pt3.w-100.bn.list
            [:li.flex
             [:div
              {:class (cls :w-100.w-25-ns header-class)
               :on-click string-sort}
              (tr "String")
              (when (= sort-typ :string)
                sort-indicator)]
             [:div
              {:class (cls :w-100.w-10-ns header-class)
               :on-click count-sort}
              (tr "# Users")
              (when (= sort-typ :count)
                sort-indicator)]
             [:div {:class (cls :w-100.w-50-ns header-class)}
              (tr "Users")]
             [:div {:class (cls :w-100.w-15-ns header-class)}
              (tr "Remove")]]
            (doall (map banned-username-string-table-row
                        banned-strings))])]))))

(defn banned-user-names
  "Create the admin banned user name list."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [banned-user-name-list]]])

(defn reset-input-on-ready!
  "Reset a field when the form reaches the ready state for the first time."
  [state-atom {:keys [value form-state] :as properties}]
  (if (= form-state :READY)
    (when-not (:already-updated? @state-atom)
      (swap! state-atom assoc
             :value (value) :already-updated? true))
    (swap! state-atom assoc :already-updated? false))
  (dissoc properties :form-state))

(defn invite-code-settings-form
  "Create the form to modify admin invite code settings."
  []
  (let [form ::admin-forms/invite-code-settings
        required? (fn [] @(subscribe [::forms/field form :required?]))
        visible? (fn [] @(subscribe [::forms/field form :visible?]))
        minimum-level (fn [] @(subscribe [::forms/field form :minimum-level]))
        per-user (fn [] @(subscribe [::forms/field form :per-user]))
        submit #(dispatch [::forms/action form :save-changes])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [:h2 (tr "Invite code settings")]
         [:div
          [:div.lh-copy
           [checkbox
            {:label (tr "Enable invite code to register")
             :label-class (cls :db)
             :value required?
             :event [::forms/edit form :required?]
             :form-state form-state}]
           [checkbox
            {:label
             (tr "Allow users to see who they invited and who invited them")
             :label-class (cls :db)
             :value visible?
             :event [::forms/edit form :visible?]
             :form-state form-state}]]
          [:div.pv1
           [:label
            {:class (cls :dib-ns.w-20.tr.v-mid.pr3)}
            (tr "Minimum level to create invite codes")]
           [text-input {:value minimum-level
                        :required true
                        :form-state form-state
                        :disabled editing-disabled?
                        :update-value! reset-input-on-ready!
                        :event [::forms/edit form :minimum-level]}]]
          [:div
           [:label
            {:class (cls :dib-ns.w-20.tr.v-mid.pr3)}
            (tr "Max amount of invites per user")]
           [text-input {:value per-user
                        :required true
                        :form-state form-state
                        :disabled editing-disabled?
                        :update-value! reset-input-on-ready!
                        :event [::forms/edit form :per-user]}]]
          [validation-error-if-present form]
          [transmission-error-if-present form]
          [:div.pv2
           [button
            {:button-class :primary
             :disabled editing-disabled?
             :on-click submit}
            (case form-state
              :SENDING (tr "Saving...")
              :SHOW-SUCCESS (tr "Saved!")
              (tr "Save"))]]]]))))

(defn generate-invite-code-form
  []
  (let [form ::admin-forms/generate-invite-code
        code (fn [] @(subscribe [::forms/field form :code]))
        max-uses (fn [] @(subscribe [::forms/field form :uses]))
        submit #(dispatch [::forms/action form :generate])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe
                                [::forms/editing-disabled? form])
            input-colors @(subscribe [::window/input-colors editing-disabled?])]
        [:div.pb3
         [:h2 (tr "Generate a new invite code")]
         [:div.pb2
          [text-input {:placeholder
                       (tr "Code (empty to generate random)")
                       :value code
                       :class :w-100
                       :required true
                       :form-state form-state
                       :disabled editing-disabled?
                       :update-value! forms/reset-input-on-success!
                       :event [::forms/edit form :code]}]] " "
         [:div
          [text-input {:placeholder (tr "Uses")
                       :value max-uses
                       :required true
                       :form-state form-state
                       :disabled editing-disabled?
                       :update-value! forms/reset-input-on-success!
                       :event [::forms/edit form :max-uses]}] " "
          [date-selector
           {:date-atom (subscribe [::forms/field form :expiration])
            :on-select #(dispatch [::forms/edit form :expiration %])
            :pikaday-attrs {:min-date (js/Date.)
                            :format "MM/DD/YYYY"}
            :input-attrs {:placeholder (tr "Expiration")
                          :auto-complete "off"
                          :class (cls :dib.lh-title.pa2.mv1.ba.bw1.br2.v-mid
                                      :shadow-6
                                      input-colors)}}] " "
          [button
           {:button-class :primary
            :class :dib-ns.db.mt1
            :disabled editing-disabled?
            :on-click submit}
           (if (= form-state :SENDING)
             (tr "Generating...")
             (tr "Generate"))]]
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-search-form
  "Create a form to search for invite codes."
  []
  (let [form ::admin-forms/search-invite-code
        code (fn [] @(subscribe [::forms/field form :code]))
        submit #(dispatch [::forms/action form :search])
        show-all #(dispatch [::forms/action form :show-all])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [:label
          (tr "Search for a code: ")]
         [text-input {:value code
                      :required true
                      :form-state form-state
                      :disabled editing-disabled?
                      :update-value! (fn [_state-atom props]
                                       (dissoc props :form-state))
                      :event [::forms/edit form :code]}] " "
         [:div.dib-ns.pv2
          [button {:button-class :primary
                   :class :dib
                   :disabled editing-disabled?
                   :on-click submit}
           (if (= form-state :SENDING)
             (tr "Searching...")
             (tr "Search"))] " "
          [button {:button-class :secondary
                   :class :dib
                   :disabled editing-disabled?
                   :on-click show-all}
           (tr "Show all codes")]]
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-expiration-form
  "Create a form to change the expiration of selected codes."
  []
  (let [form ::admin-forms/expire-invite-code
        submit #(dispatch [::forms/action form :change])]
    (fn []
      (let [option @(subscribe [::forms/field form :option])
            form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])
            input-colors @(subscribe [::window/input-colors editing-disabled?])]
        [:div
         [:p
          (tr "Change selected codes to expire: ")]
         [radio-button
          {:name "expire-option" :id "expire-option-never"
           :disabled editing-disabled?
           :label (tr "Never")
           :value "never"
           :form-value (name option)
           :event [::forms/edit form :option :never]
           :form-state form-state}] " "
         [radio-button
          {:name "expire-option" :id "expire-option-now"
           :disabled editing-disabled?
           :label (tr "Now")
           :value "now"
           :form-value (name option)
           :event [::forms/edit form :option :now]
           :form-state form-state}] " "
         [radio-button
          {:name "expire-option" :id "expire-option-at"
           :disabled editing-disabled?
           :label (tr "At: ")
           :value "at"
           :form-value (name option)
           :event [::forms/edit form :option :at]
           :form-state form-state}]
         [date-selector
          {:date-atom (subscribe [::forms/field form :expiration])
           :on-select #(dispatch [::forms/edit form :expiration %])
           :pikaday-attrs {:min-date (js/Date.)
                           :format "MM/DD/YYYY"}
           :input-attrs {:placeholder (tr "Expiration")
                         :class (cls :dib.lh-title.pa2.mv1.ba.bw1.br2.v-mid
                                     :shadow-6
                                     input-colors)
                         :auto-complete "off"}}] " "
         [button
          {:button-class :primary
           :class :dib
           :disabled editing-disabled?
           :on-click submit}
          (if (= form-state :SENDING)
            (tr "Changing...")
            (tr "Change Expiration"))] " "
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-user
  "Create a user link for the invite codes table."
  [{:keys [name status]}]
  ^{:key name}
  [userlink name
   (case status
     "DELETED" (tr " (Deleted)")
     "PROBATION" (tr " (Pending)")
     "BANNED" (tr " (Banned)")
     "")])

(defn invite-code-table-row
  "Create a row in the invite codes table."
  [id]
  (let [ic @(subscribe [::admin/invite-code id])
        {:keys [code created-by created-on expires used-by uses
                max-uses expired?]} ic
        form ::admin-forms/expire-invite-code
        selected @(subscribe [::forms/field form :selected])
        editing-disabled? @(subscribe [::forms/editing-disabled? form])
        form-state @(subscribe [::forms/form-state form])
        body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100-s.w-100-m.pa2.ba.bw1.data-name-sm
                          (str "w-" percent "-l")
                          body-colors border-color))]
    ^{:key id}
    [:li.flex-l.pv2-s.pv2-m
     [:div {:class (cell-class 5)
            :data-name (tr " ")}
      [checkbox {:disabled editing-disabled?
                 :label ""
                 :value #(contains? selected id)
                 :event [::admin/select-invite-code id]
                 :form-state form-state}]]
     [:div {:class (cell-class 30)
            :data-name (tr "Code: ")}
      [:span {:class (cls (when (or expired? (not= (:status created-by) "ACTIVE"))
                            :gray.strike))} code]]
     [:div {:class (cell-class 15)
            :data-name (tr "Created by: ")}
      [invite-code-user created-by]]
     [:div {:class (cell-class 15)
            :data-name (tr "Created on: ")} created-on]
     [:div {:class (cell-class 15)
            :data-name (tr "Expires: ")} expires]
     [:div {:class (cls (cell-class 5) :tr-l)
            :data-name (tr "Uses: ")} uses]
     [:div {:class (cls (cell-class 5) :tr-l)
            :data-name (tr "Max uses: ")} max-uses]
     [:div {:class (cell-class 10)
            :data-name (tr "Used by: ")}
      (doall (interpose (tr ", ")
                        (map invite-code-user used-by)))]]))

(defn invite-code-table-body
  "Create the body of the invite code table."
  []
  (let [invite-code-ids @(subscribe [::admin/invite-code-ids])]
    (into [:span]
          (doall (map invite-code-table-row invite-code-ids)))))

(defn invite-code-table
  "Create a table of invite codes."
  []
  (let [on-load (fn [elem]
                  (reagent/after-render
                   #(dispatch [::admin/set-bottom-of-invite-code-table elem])))]
    (fn []
      (let [header-colors @(subscribe [::window/table-header-colors])
            border-colors @(subscribe [::window/table-border-colors])
            header-class (fn [percent]
                           (cls :dn.w-100.dib-l.b.ph1.pv2.ba.bw1
                                (str "w-" percent "-l")
                                header-colors border-colors))]
        [:div
         (into [:ol.ph0.pt3.w-100.bn.list
                [:li.flex
                 [:div {:class (header-class 5)}]
                 [:div {:class (header-class 30)} (tr "Code")]
                 [:div {:class (header-class 15)} (tr "Created by")]
                 [:div {:class (header-class 15)} (tr "Created on")]
                 [:div {:class (header-class 15)} (tr "Expires")]
                 [:div {:class (header-class 5)} (tr "Uses")]
                 [:div {:class (header-class 5)} (tr "Max uses")]
                 [:div {:class (header-class 10)} (tr "Used by")]]
                [invite-code-table-body]])
         [:p {:ref on-load}]]))))

(defn invite-codes-listing
  "Create the search, actions and invite code table."
  []
  (let [codes-empty? @(subscribe [::admin/invite-codes-empty?])
        search-code @(subscribe [::admin/invite-code-search-code])
        loading? (= @(subscribe [::window/status]) :loading)]
    [:div
     [:h2 (tr "Invite codes")]
     [invite-code-search-form]
     (when-not loading?
       (if codes-empty?
         (if (= search-code :all)
           [:h3 (tr "No invite codes have been created yet")]
           [:h3 (tr "No invite codes found matching '%s'" search-code)])
         [:<>
          [invite-code-expiration-form]
          [invite-code-table]]))]))

(defn invite-codes-panel
  "Create the admin invite code page."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [:div
     [invite-code-settings-form]
     [:hr]
     [generate-invite-code-form]
     [:hr]
     [invite-codes-listing]]]])
