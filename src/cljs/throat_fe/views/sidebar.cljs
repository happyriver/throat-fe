;; sidebar.cljs -- Sidebar rendering for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.views.sidebar
  (:require
   [clojure.string :as str]
   [goog.string :as gstring]
   [goog.string.format]
   [re-frame.core :refer [dispatch subscribe]]
   [throat-fe.content.messages :as messages]
   [throat-fe.content.post :as post]
   [throat-fe.content.settings :as settings]
   [throat-fe.content.subs :as subs]
   [throat-fe.tr :refer [tr trm-html]]
   [throat-fe.ui.forms.core :as forms]
   [throat-fe.ui.forms.user-flair :as user-flair-form]
   [throat-fe.ui.window :as window]
   [throat-fe.user :as user]
   [throat-fe.views.common :refer [dangerous-html
                                   helper-text
                                   modal
                                   nsfw-tag
                                   user-flair userlink]]
   [throat-fe.views.forms :refer [transmission-error-if-present
                                  validation-error-if-present]]
   [throat-fe.views.funding-progress :refer [funding-progress-content]]
   [throat-fe.views.icon :refer [icon]]
   [throat-fe.views.inputs :refer [button text-input]]
   [throat-fe.views.util :refer [cls url-for] :as util]))

(defn sortbutton-class
  [button-type]
  (let [sort-type @(subscribe [:view/sort])]
    ["sbm-post" "pure-button" "button-xsmall" "pure-u-md-7-24"
     (when (= button-type sort-type)
       "pure-button-primary")]))

(defn sortbutton-link
  [button-type]
  (let [post-source @(subscribe [:view/post-source])
        post-source-sub @(subscribe [:view/post-source-sub])]
    (case button-type
      :hot (case post-source
             :all (url-for :home/all-hot)
             :single-sub (url-for :sub/view-sub-hot
                                  :sub (:name post-source-sub))
             (url-for :home/hot))
      :top (case post-source
             :all (url-for :home/all-top)
             :single-sub (url-for :sub/view-sub-top
                                  :sub (:name post-source-sub))
             (url-for :home/top))
      :new (case post-source
             :all (url-for :home/all-new)
             :single-sub (url-for :sub/view-sub-new
                                  :sub (:name post-source-sub))
             (url-for :home/new)))))

(defn sortbuttons
  "Create the sort buttons on the sidebar."
  []
  [:div.pure-button-group  {:id "sortbuttons"
                            :role "group"}
   [:div.pure-g
    [:a {:class (sortbutton-class :hot) :href (sortbutton-link :hot)}
     (tr "Hot")]
    [:a {:class (sortbutton-class :top) :href (sortbutton-link :top)}
     (tr "Top")]
    [:a {:class (sortbutton-class :new) :href (sortbutton-link :new)}
     (tr "New")]]])

(defn search
  "Create the search box on the sidebar."
  []
  [:form.relative.ph2.pt2.pb1
   {:method :POST
    :action (url-for :do/search)}
   [:div [icon {:class (cls :absolute.h24px.w24px.top-1.left-1.fill-silver)}
          "search"]]
   [:input.w-100.border-box.mt1.pv2.pr2.pl4.ba.bw1.br2.f6.b--silver
    {:aria-label (tr "Search")
     :name "term"
     :placeholder (tr "Title search...")
     :type "text"} ]])

(defn button-link
  "Create one of the button-styled links for the sidebar."
  [props text]
  (let [{:keys [button-class]} props
        colors @(subscribe [::window/button-colors (or button-class
                                                       :secondary)])]
    [:a (-> props
            (dissoc :button-class)
            (assoc :class (cls :mh2.mv1.db.v-mid.tc.pv2.ph2.br1.bn
                               :custom.pointer.noselect.active colors)))
     text]))

(defn sub-buttons
  "Create the sidebar buttons for logged in users to navigate and post."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        sub-creation-permitted? @(subscribe [::user/sub-creation-permitted?])]
    (when is-authenticated?
      [:span
       [button-link {:href (url-for :subs/submit :ptype "link")}
        (tr "Submit a post")]
       [:hr]
       [button-link {:href (url-for :home/view-subs)}
        (tr "View all subs")]
       [button-link {:href (url-for :subs/random-sub)}
        (tr "Go to random sub")]
       (when sub-creation-permitted?
         [button-link {:href (url-for :subs/create-sub)}
          (tr "Create a sub")])])))

(defn todays-top-posts
  "Create the top recent posts list in the sidebar."
  []
  [:span
   [:hr]
   [:div.sidebarlists
    [:h4.center (tr "Top posts in the last 24 hours")]]])

(defn sub-of-the-day
  "Create the sub of the day in the sidebar."
  []
  [:span
   [:hr]
   [:div.sidebarlists
    [:h4.center (tr "Sub of the day")]]])

(defn recent-activity
  "Create the recent activity in the sidebar."
  []
  [:span
   [:hr]
   [:div.sidebarlists
    [:h4.center (tr "Recent activity")]]])

(defn changelog
  "Create the changelog in the sidebar."
  []
  [:span])

(defn sitelog
  "Create the site log in the sidebar."
  []
  (let [show-sitelog? (subscribe [::user/show-sitelog?])]
    (when @show-sitelog?
      [button-link {:href (url-for :site/view-sitelog)}
       (tr "Site logs")])))

(defn footer-donate
  "Create the funding goal and the donate link for the footer.
  These are only shown on mobile."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        colors @(subscribe [::window/button-colors :primary])]
    (when is-authenticated?
      [:div.dn-ns.mt3.w-100
       [:h3.mh3.tc (tr "Funding Goal")]
       [funding-progress-content]
       [:a {:href (url-for :donate/setup)
            :class (cls :mh3.mv1.db.tc.pv2.ph2.br1.bn
                        :custom.pointer colors)}
        (tr "Donate")]])))

(defn footer-without-donate
  "Create a page or sidebar footer without the donation link."
  []
  (let [copyright @(subscribe [::settings/copyright])
        footer-links @(subscribe [::settings/footer-links])
        last-word @(subscribe [::settings/last-word])]
    [:div.footer
     (str (gstring/unescapeEntities "&copy; ") copyright)
     [:br]
     (into [:span]
           (->> footer-links
                (map (fn [{:keys [name link]}]
                       [:a {:href link} name]))
                (interpose " | ")))
     (when-not (str/blank? last-word)
       [:span [:br] last-word])]) )

(defn footer
  "Create the page or sidebar footer."
  []
  [:<>
   [footer-donate]
   [footer-without-donate]])

(defn sidebar
  "Create the sidebar."
  []
  (let [menu-state @(subscribe [::window/menu-state])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     [sortbuttons]
     [search]
     [sub-buttons]
     [todays-top-posts]
     [sub-of-the-day]
     [recent-activity]
     [changelog]
     [sitelog]
     [footer]]))

(defn author-link
  "Create the author link for a single post."
  []
  (let [author @(subscribe [::post/single-post-author])
        username (:name author)
        deleted-author? (= :DELETED (:status author))
        is-admin? @(subscribe [::user/is-admin?])]
    (if deleted-author?
      (if is-admin?
        [userlink username " " (tr "[Deleted]")]
        [:a "[Deleted]"])
      [userlink username])))

(defn author
  "Create the author section for a single post sidebar."
  []
  (let [post @(subscribe [::post/single-post])
        {:keys [score upvotes downvotes]} post
        author-flair @(subscribe [::post/single-post-author-flair])
        posted @(subscribe [::post/single-post-when-posted])
        edited @(subscribe [::post/single-post-when-edited])]
    [:div#author.pa2
     [:div
      [:span.pr1
       (trm-html "Posted %{timeago} by %{user}" {:timeago posted
                                                 :user [author-link]})]
      (when author-flair
        [user-flair author-flair])]
     (when edited
       [:div (tr "Edited %s" edited)])
     [:div (tr "Score: ") score
      (when (and upvotes downvotes)
        [:span.b (tr " (+%s|-%s)" upvotes downvotes)])]]))

(defn sub-submit-post-button
  "Create the submit a post button for a specific sub."
  []
  (let [options @(subscribe [:view/options])
        sub @(subscribe [::subs/viewed-sub-info])
        authenticated? @(subscribe [::user/is-authenticated?])
        is-mod? @(subscribe [::user/is-mod-of-sub?])]
    (when authenticated?
      (cond
        (and (:restricted sub) (not is-mod?))
        [:div.ph2 (tr "Only mods can post.")]

        (:banned sub)
        [:div.ph2 (tr "You are currently banned from posting.")]

        :else
        [button-link {:href (url-for :subs/submit-post
                                     :query-args {:sub (:sub options)})}
         (tr "Submit a post")]))))

(defn subscribe-and-block
  "Create the subscribe and block buttons."
  []
  (let [subscribed? @(subscribe [::user/subscribed?])
        blocked? @(subscribe [::user/blocked?])
        icon-class (cls :absolute.left--05.fill-white.h16px.w16px)
        sub @(subscribe [::subs/viewed-sub-info])
        sb-button (fn [button-class icon-name button-event text]
                    [button {:class :f6.br1.mr1.dib
                             :button-class button-class
                             :on-click #(dispatch button-event)}
                     [:span.relative.pl2 [icon {:class icon-class} icon-name]
                      text]])]
    [:div.pb2
     (if subscribed?
       [sb-button :subscribed "check" [::user/unsubscribe sub]
        (tr "Subscribed")]
       [sb-button :subscribe "add" [::user/subscribe sub]
        (tr "Subscribe")])
     (if blocked?
       [sb-button :blocked "check" [::user/unblock sub]
        (tr "Blocked")]
       [sb-button :block "close" [::user/block sub]
        (tr "Block")])]))

(defn reset-input-on-selection-change
  [state-atom {:keys [value selected-value] :as properties}]
  (when-not (= selected-value (:last-selection @state-atom))
    (swap! state-atom assoc
           :value (value)
           :last-selection selected-value))
  (dissoc properties :form-state :selected-value))

(defn user-flair-form-content
  "Show the content of the user change flair form."
  [{:keys [form]}]
  (let [flair-fn (fn [] @(subscribe [::forms/field form :flair]))
        change-fn (fn [] (dispatch [::forms/action form :change]))
        remove-fn (fn [] (dispatch [::forms/action form :remove]))]
    (fn [{:keys [form]}]
      (let [form-state @(subscribe [::forms/form-state form])
            sub-name @(subscribe [::forms/field form :name])
            flair @(subscribe [::forms/field form :flair])
            presets @(subscribe [::forms/field form :presets])
            freeform? @(subscribe [::forms/field form :freeform-user-flairs])

            current-user @(subscribe [::user/current-user])
            is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
            custom? (or freeform? is-mod-or-admin?)]
        [:div
         [:h3
          (if (seq presets)
            (tr "Choose your flair for %s:" sub-name)
            (tr "Set your flair for %s:" sub-name))]
         [helper-text
          (tr "Your flair will be shown after your username on your posts and comments.")]
         (when (seq presets)
           (into [:div.flex.flex-wrap.pb3]
                 (map (fn [elem]
                        [:span.pa1
                         {:on-click #(dispatch
                                      [::forms/edit form :flair elem])}
                         [user-flair {:selectable? true
                                      :selected? (= elem flair)}
                          elem]]))
                 presets))
         (when custom?
           [text-input {:placeholder (tr "enter a custom flair")
                        :class :w-100.w-80-ns.mb3
                        :value flair-fn
                        :update-value! reset-input-on-selection-change
                        :form-state form-state
                        :selected-value flair
                        :event [::forms/edit form :flair]}])
         [:div
          [:span "Preview:"]
          [:div.ph3.pv2
           [:a {:class :user} (:name current-user)]
           (when (seq flair)
             [:span.pa1
              [user-flair flair]])]]
         [validation-error-if-present form]
         [transmission-error-if-present form]
         [:div.pt3
          [button {:button-class :primary
                   :class :dib.mb2.mb0-ns.mr3
                   :on-click change-fn}
           (tr "Change flair")]
          [button {:button-class :modal
                   :class :dib
                   :on-click remove-fn}
           (tr "Remove flair")]]]))))

(defn modal-user-flair-form
  "Create the form that allows the user to change their flair."
  []
  (let [toggle-modal #(dispatch [::window/toggle-user-flair-modal])
        show-modal? (fn [] @(subscribe [::window/show-user-flair-modal?]))]
    (fn []
      (let [day? @(subscribe [::window/day?])
            form-colors (if day? :bg-white :bg-black)]
        [modal {:show show-modal?
                :cancel toggle-modal
                :foreground form-colors}
         [user-flair-form-content {:cancel toggle-modal
                                   :form user-flair-form/form-path}]]))))

(defn user-name-and-flair
  "Show the username, user flair, and the link to the change user flair form."
  []
  (let [current-user @(subscribe [::user/current-user])
        sub @(subscribe [::subs/viewed-sub-info])
        flair @(subscribe [::subs/sub-user-flair
                           (:sid sub) (:uid current-user)])
        show-form #(do
                     (dispatch [::user-flair-form/initialize-user-flair-form])
                     (dispatch [::window/toggle-user-flair-modal]))
        editable-flair? @(subscribe [::user/can-edit-flair?]) ]
    (when (or (seq flair) editable-flair?)
      [:div.pt1.ph2
       (if (seq flair)
         [:span.lh-copy
          (tr "Your flair: ")
          [userlink {:class :user} (:name current-user)]
          (when (seq flair)
            [:span.pa1
             [user-flair flair]])
          ;; TODO this wraps weirdly if the sidebar is too narrow
          (when editable-flair?
            [:a.mv2.small.pl1 {:on-click show-form}
             (tr "(change)")])]
         (when editable-flair?
           [button {:on-click show-form
                    :button-class :secondary
                    :class [:small.center util/reset-padding :ph2.pv1]}
            (tr "Set your user flair")]))])))

(defn- redirect-to-flair
  "Navigate when a sub flair is selected."
  [sub-name event]
  (let [flair (.. event -target -value)]
    (when (seq flair)
      (set! (.. js/window -location -href)
            (url-for :sub/view-sub-hot :sub sub-name
                     :query-args {:flair flair})))))

(defn created-by
  "Create a sub's creation date for the sidebar."
  []
  (let [timeago @(subscribe [::subs/creation-timeago])]
    [:div#createdby.pb2
     (tr "Created %s" timeago)]))

(defn sub-navigation
  "Create the sub navigation controls for the single post page."
  []
  (let [sub @(subscribe [::subs/viewed-sub-info])
        sub-name (:name sub)
        redirect (partial redirect-to-flair sub-name)
        prefix @(subscribe [::settings/sub-prefix])
        flairs @(subscribe [::subs/post-flairs])
        authenticated? @(subscribe [::user/is-authenticated?])]
    [:div.tc
     [:h3.f3
      [:a.custom.purple.hover-light-purple
       {:href (url-for :sub/view-sub :sub sub-name)}
       "/" prefix "/" sub-name]
      (when (:nsfw sub)
        [nsfw-tag])]
     [:div.pb1 (tr "%s subscribers" (:subscriber-count sub))]
     [created-by]
     (when authenticated?
       [:<>
        [subscribe-and-block]
        [user-name-and-flair]])
     (when (seq flairs)
       [:div.pa2
        (into
         [:select
          {:default-value ""
           :class (cls :ph1.pv2.w-100.f6.lh-title.ba.br2.
                       :bg-white.b--moon-gray.shadow-6)
           :on-change redirect}
          [:option {:value ""
                    :disabled true}
           (tr "Show posts with flair...")]
          (doall (map (fn [{:keys [id text]}]
                        ^{:key id} [:option {:value text} text])
                      flairs))])])]))

(defn sub-sidebar-text
  "Create the sub sidebar text."
  []
  (when-let [sidebar-text @(subscribe [::subs/sidebar])]
    [:<>
     [:div#sidebar-text.ph2
      [dangerous-html {} sidebar-text]]
     [:hr]]))

(defn contact-the-mods
  "Create the contact the mods button."
  []
  (let [authenticated? @(subscribe [::user/is-authenticated?])
        is-mod? @(subscribe [::user/is-mod-of-sub?])
        options @(subscribe [:view/options])]
    (when (and authenticated? (not is-mod?))
      [button-link
       {:href (url-for :sub/contact-mods :sub (:sub options))}
       "Message the Mods"])))

(defn sub-moderators
  "Create the list of sub moderators."
  []
  (let [day? @(subscribe [::window/day?])
        owners @(subscribe [::subs/mod-names #{:OWNER}])
        mods @(subscribe [::subs/mod-names #{:MODERATOR :JANITOR}])]
    [:<>
     [:div#moderators.ph2
      (tr "Moderators")
      (into [:ul.list]
            (concat
             (map (fn [owner]
                    ^{:key owner}
                    [:li
                     (icon {:class (cls :h16px.w16px.dib.mr1
                                        (when-not day? :fill-gray))
                            :title (tr "Owner")} "owner")
                     [userlink owner]])
                  owners)
             (map (fn [mod]
                    ^{:key mod}
                    [:li [userlink mod]])
                  mods)))]
     [contact-the-mods]]))

(defn moderator-buttons
  "Create buttons for sub mods and admins."
  []
  (let [moderation-level @(subscribe [::user/moderation-level])
        is-admin? @(subscribe [::user/is-admin?])
        options @(subscribe [:view/options])
        sub-name (:sub options)]
    [:<>
     (when (or is-admin? moderation-level)
       [:hr])
     (when (or is-admin? (#{:OWNER :MODERATOR} moderation-level))
       [:<>
        [button-link {:href (url-for :sub/edit-sub :sub sub-name)}
         (tr "Settings")]
        [button-link {:href (url-for :sub/edit-sub-flairs :sub sub-name)}
         (tr "Edit flairs")]
        [button-link {:href (url-for :sub/edit-sub-rules :sub sub-name)}
         (tr "Edit rules")]
        [button-link {:href (url-for :sub/edit-sub-css :sub sub-name)}
         (tr "Edit stylesheet")]
        [button-link {:href (url-for :mod/reports-sub :sub sub-name)}
         (tr "Circle Moderation")]])
     (when (or is-admin? moderation-level)
       [button-link {:href (url-for :sub/edit-sub-mods :sub sub-name)}
        (tr "Moderators")])]))

(defn sublog-buttons
  "Create the sublog and banned user buttons, if configured."
  []
  (let [sub @(subscribe [::subs/viewed-sub-info])
        {:keys [sub-banned-users-private sublog-private]} sub
        ;; TODO There's a force sublogs public site config option
        ;; which is not yet respected.
        public-sublog? (not sublog-private)
        public-banned-users? (not sub-banned-users-private)
        is-mod? @(subscribe [::user/is-mod-of-sub?])
        can-admin? @(subscribe [::user/can-admin?])
        show-sublog? (or public-sublog? is-mod? can-admin?)
        show-banned? (or public-banned-users? is-mod? can-admin?)
        sub-name @(subscribe [::subs/normalized-sub-name])]
    [:<>
     (when (or show-sublog? show-banned?)
       [:hr])
     (when show-sublog?
       [button-link {:href (url-for :sub/view-sublog :sub sub-name)}
        (tr "Circle Log")])
     (when show-banned?
       [button-link {:href (url-for :sub/view-sub-bans :sub sub-name)}
        (tr "Banned Users")])]))

(defn donate
  "Create the funding goal and the donate link."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])]
    (when is-authenticated?
      [:<>
       [:h4.center (tr "Funding Goal")]
       [funding-progress-content]
       [button-link {:href (url-for :donate/setup)}
        (tr "Donate")]])))

(defn single-post-sidebar
  "Create a sidebar for a single post page"
  []
  (let [menu-state @(subscribe [::window/menu-state])
        loaded? @(subscribe [::post/loaded?])]
    [:<>
     [:div {:id "sidebar"
            :class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                    (when (= menu-state :open) "open")]}
      (when loaded?
        [author])
      [:hr]
      [search]
      (when loaded?
        [:<>
         [sub-submit-post-button]
         [sub-navigation]])
      [:hr]
      (when loaded?
        [:<>
         [sub-sidebar-text]
         [sub-moderators]
         [moderator-buttons]
         [sublog-buttons]])
      [:hr]
      [donate]]
     [modal-user-flair-form]]))

(defn mod-sidebar
  "Create the sidebar for the mod dashboard."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        is-admin? @(subscribe [::user/is-admin?])
        {:keys [show-other-circles?]} @(subscribe [:view/options])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     (when is-admin?
       [:<>
        [button-link {:href (url-for :mod/index)
                      :button-class (when-not show-other-circles?
                                      :sidebar)}
         (tr "My Circles")]
        [button-link {:href (url-for :mod/admin)
                      :button-class (when show-other-circles?
                                      :sidebar)}
         (tr "Other Circles")]
        [:hr]])
     [button-link {:href (url-for :mod/reports)}
      (tr "All Open Reports")]
     [button-link {:href (url-for :mod/closed)}
      (tr "All Closed Reports")]
     [button-link {:href (url-for :modmail/mailbox :mailbox :all)}
      (tr "All Mod Mail")]]))

(defn sidebar-button
  "Create one of the buttons on the sidebar menu.  If `panel` matches the current
  view, highlight the button."
  [panel url text]
  (let [active-panel @(subscribe [:view/active-panel])
        class ["sbm-post" "pure-button" (when (= panel active-panel)
                                          "button-secondary")]]
    [:a {:href url :class class} text]))

(defn mod-sub-sidebar
  "Create the sidebar for mod pages within a sub."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        sub @(subscribe [::subs/normalized-sub-name])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     [sidebar-button false (url-for :sub/edit-sub :sub sub)
      (tr "Settings")]
     [sidebar-button :mod-edit-post-flairs (url-for :sub/edit-sub-flairs :sub sub)
      (tr "Post flairs")]
     [sidebar-button false (url-for :sub/edit-sub-user-flairs :sub sub)
      (tr "User flairs")]
     [sidebar-button :mod-edit-sub-rules (url-for :sub/edit-sub-rules :sub sub)
      (tr "Rules")]
     [sidebar-button :mod-edit-post-types (url-for :sub/edit-post-types :sub sub)
      (tr "Post types")]
     [sidebar-button false (url-for :sub/edit-sub-css :sub sub)
      (tr "Stylesheet")]
     [:hr]
     [sidebar-button false (url-for :sub/edit-sub-mods :sub sub)
      (tr "Moderators")]
     [sidebar-button false (url-for :sub/view-sub-bans :sub sub)
      (tr "Bans")]
     [:hr]
     [sidebar-button false (url-for :mod/reports-sub :sub sub)
      (tr "Open Reports")]
     [sidebar-button false (url-for :mod/reports-sub-closed :sub sub)
      (tr "Closed Reports")]]))

(defn modmail-sortbutton-class
  [button-type]
  (let [sort-type @(subscribe [:view/sort])]
    ["sbm-post" "pure-button" "button-xsmall" "pure-u-md-6-24"
     (when (= button-type sort-type)
       "pure-button-primary")]))

(defn modmail-sortbutton-link
  [_button-type]
  (url-for :modmail/mailbox :mailbox :all))

(defn modmail-sortbutton
  "Create a single sort button for the modmail sidebar"
  [button-type text]
  [:a {:class (modmail-sortbutton-class button-type)
       :href (modmail-sortbutton-link button-type)}
   text])

(defn modmail-sortbuttons
  "Create the sort buttons on the modmail sidebar."
  []
  [:div.pure-button-group  {:id "sortbuttons" :role "group"}
   [:div.pure-g
    [modmail-sortbutton :recent (tr "Recent")]
    [modmail-sortbutton :unread (tr "Unread")]
    [modmail-sortbutton :mod (tr "Mod")]
    [modmail-sortbutton :user (tr "User")]]])

(defn modmail-mailbox-link
  [mailbox-type text]
  (let [sub-selection @(subscribe [::messages/modmail-sub-selection])
        mailbox @(subscribe [:view/mailbox])
        url (if (= sub-selection :all-subs)
              (url-for :modmail/mailbox :mailbox mailbox-type)
              (url-for :modmail/mailbox :mailbox mailbox-type
                       :query-args {:sub sub-selection}))]
    [button-link {:href url
                  :button-class (if (= mailbox-type mailbox)
                                  :sidebar
                                  :secondary)}
     text]))

(defn modmail-sidebar
  "Create the sidebar for modmail."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        active-panel @(subscribe [:view/active-panel])
        supervising-admin? @(subscribe [::messages/supervising-admin?])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     #_(when (= active-panel :modmail)
         [modmail-sortbuttons])
     #_[search "Search Modmail"]
     (when-not supervising-admin?
       [:<>
        [button-link {:button-class (when (= active-panel :modmail-compose)
                                      :sidebar)
                      :href (url-for :modmail/compose)}
         (tr "Compose a message")]
        [:hr]])
     [modmail-mailbox-link :all (tr "All")]
     #_[modmail-mailbox-link :inbox (tr "Inbox")]
     [modmail-mailbox-link :new (tr "New")]
     [modmail-mailbox-link :in-progress (tr "In progress")]
     [:hr]
     [modmail-mailbox-link :archived (tr "Archived")]
     #_[modmail-mailbox-link :appeals (tr "Ban appeals")]
     #_[modmail-mailbox-link :highlighted (tr "Highlighted")]
     [modmail-mailbox-link :discussions (tr "Mod discussions")]
     #_[modmail-mailbox-link :notifications (tr "Notifications")]
     [:hr]
     [button-link
      {:href (url-for :wiki/view :slug "modmail-help")}
      (tr "Modmail help")]
     [footer]]))

(defn admin-sidebar
  "Create the sidebar for the admin pages."
  []
  (let [menu-state @(subscribe [::window/menu-state])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     [button-link {:href (url-for :admin/index)}
      (tr "Admin")]
     [:hr]
     [button-link {:href (url-for :admin/view)}
      (tr "Admins")]
     [button-link {:href (url-for :admin/users)}
      (tr "Users")]
     [button-link {:href (url-for :admin/userbadges)}
      (tr "User Badges")]
     [button-link {:href (url-for :admin/invitecodes)}
      (tr "Invite Codes")]
     [:hr]
     [button-link {:href (url-for :admin/subs)}
      (tr "Circles")]
     [button-link {:href (url-for :admin/posts)}
      (tr "Posts")]
     [button-link {:href (url-for :admin/reports)}
      (tr "Reports")]
     [:hr]
     [button-link {:href (url-for :admin/user-uploads)}
      (tr "Uploads")]
     [button-link {:href (url-for :admin/wiki)}
      (tr "Wiki")]
     [button-link {:href (url-for :admin/domains :domain-type "link")}
      (tr "Banned Domains")]
     [button-link {:href (url-for :admin/domains :domain-type "email")}
      (tr "Banned Email Domains")]
     [button-link {:href (url-for :admin/banned-user-names)}
      (tr "Banned User Names")]
     [:hr]
     [button-link {:href (url-for :admin/configure)}
      (tr "Site Configuration")]
     [button-link {:href (url-for :admin/stats)}
      (tr "Site Statistics")]
     [button-link {:href (url-for :site/view-sitelog)}
      (tr "Sitelog")]]))
