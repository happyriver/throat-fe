;; views/errors.cljs -- Display errors for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.views.errors
  (:require
   [re-frame.core :refer [dispatch subscribe]]
   [throat-fe.config :as config]
   [throat-fe.content.settings :as settings]
   [throat-fe.errors :as errors]
   [throat-fe.ui.window :as window]
   [throat-fe.views.icon :refer [icon]]
   [throat-fe.views.util :refer [cls]]))

(defn error-bar
  "Show network or server errors."
  []
  (let [errors @(subscribe [::errors/errors])
        site-name @(subscribe [::settings/name])
        icons @(subscribe [::settings/icons])
        icon-href (get icons "16x16")
        colors @(subscribe [::window/toast-colors])
        border-color @(subscribe [::window/toast-border-color])
        close-color @(subscribe [::window/toast-close-color])]
    (when (seq errors)
      (into [:div.di.fixed.top-2.z-999
             ;; Put toasts on the left in dev mode so devtools can
             ;; have the right side.
             {:class (cls (if config/debug? :left-2 :right-2))}]
            (map (fn [{:keys [msg timeago] :as err}]
                   [:div.mw5.mw6-ns.mb3.bw1.br2.b--black.shadow-1
                    {:class (cls colors)}
                    [:div.pa2.bb.flex.items-center
                     {:class (cls border-color)}
                     (when icon-href
                       [:img {:src icon-href
                              :class (cls :mr2)}])
                     [:div.w-100.flex.mt2
                      [:span.f5.b.mr-auto site-name]
                      [:span.f7.gray.ml3.mt1.pr2 timeago]]
                     [icon {:class (cls :h1.w1.pointer.dib.self-start
                                        close-color)
                            :on-click #(dispatch [::errors/clear-error err])}
                      "close"]]
                    [:div.ph3.pv2
                     msg]])
                 errors)))))
