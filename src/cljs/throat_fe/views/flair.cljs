(ns throat-fe.views.flair
  (:require
   [reagent.core :as reagent]
   [re-frame.core :refer [subscribe]]
   [throat-fe.ui.window :as window]
   [throat-fe.views.util :refer [cls] :as util]))

(defn post-flair
  "Create a post flair without the href.
  In properties, use :selected? to make it change color."
  []
  (let [this (reagent/current-component)
        props (reagent/props this)
        children (reagent/children this)
        flair-colors @(subscribe [::window/post-flair-colors props])]
    (into
     [:a {:class (cls :custom.ph2.pv1.mr2.lh-title.br2
                      :f5.pointer flair-colors)}]
     children)))
