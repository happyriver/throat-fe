;; routes.cljs -- Routing for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.routes.util
  (:require
   [bidi.bidi :as bidi]
   [re-frame.core :as re-frame]
   [throat-fe.log :as log])
  (:import
   [goog Uri]
   [goog.Uri QueryData]))

(defn pack-query-string
  "Return a query string (including the leading '?')."
  [params]
  (if (empty? params)
    ""
    (str "?" (.toString (QueryData/createFromMap
                         (clj->js params))))))

(defn- url
  [routes handler args]
  (let [{:keys [query-args]} (apply hash-map args)
        query-string (pack-query-string query-args)]
    (try
      (str
       (apply bidi/path-for (into [routes handler] args))
       query-string)
      (catch js/Error e
        (log/error {:handler handler :exception e} "url-for error")
        "/"))))

(defn url-for
  "Construct the URI for a route handler.
  If the key :query-string is in args use its value (a map) to
  construct a query string."
  [{:keys [routes] :as _db} handler & args]
  (url routes handler args))

(re-frame/reg-sub ::routes
  ;; :doc Extract the routing table.
  (fn [db _]
    (:routes db)))

(re-frame/reg-sub ::url-for
  ;; :doc Construct the URI for a route handler.
  ;; :doc If the key :query-string is in args use its value (a map) to
  ;; :doc construct a query string.
  :<- [::routes]
  (fn [routes [_ handler & args]]
    (url routes handler args)))
