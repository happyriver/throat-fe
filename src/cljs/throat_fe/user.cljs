;; user.cljs -- Logged-in user for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.user
  (:require
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [graphql-builder.parser :refer-macros [defgraphql]]
   [re-frame.core :as re-frame]
   [throat-fe.config :as config]
   [throat-fe.content.internal :as internal]
   [throat-fe.content.settings :as-alias settings]
   [throat-fe.content.subs :as subs]
   [throat-fe.errors :as errors]
   [throat-fe.graphql :as graphql]
   [throat-fe.log :as log]
   [throat-fe.routes :as-alias routes]
   [throat-fe.ui.page-title :as-alias page-title]
   [throat-fe.util :as util]))

(defgraphql graphql
  "src/cljs/throat_fe/content/user.graphql")

(def form-path
  "Path to user forms in the db."
  [:ui-state :forms :user])

(re-frame/reg-sub ::current-user
  ;; :doc Extract the current user from the db.
  (fn [db _]
    (:current-user db)))

(re-frame/reg-sub ::load-status
  (fn [db _]
    (get-in db [:status :current-user])))

(re-frame/reg-sub ::is-authenticated?
  :<- [::current-user]
  (fn [user _]
    (some? (:uid user))))

(def authenticated?
  "Add whether the user is authenticated to the coeffects."
  (re-frame/->interceptor
   :id ::authenticated?
   :before  (fn [{:keys [coeffects] :as ctx}]
              (let [uid (-> (:db coeffects)
                            (get-in [:current-user :uid]))]
                (assoc-in ctx [:coeffects :authenticated?] (some? uid))))))

(re-frame/reg-sub ::is-admin?
  :<- [::current-user]
  (fn [user _]
    (get-in user [:attributes :is-admin])))

(re-frame/reg-sub ::can-admin?
  :<- [::current-user]
  (fn [user _]
    (get-in user [:attributes :can-admin])))

(re-frame/reg-sub ::subs-moderated
  ;; :doc Extract the subs moderated by the current user.
  :<- [::current-user]
  (fn [user _]
    (:subs-moderated user)))

;; Computational subscriptions.  Each should depend on an extractor,
;; not the entire db.

(defn sum-mod-notifications
  "Calculate the sum of open reports and new modmails for all subs
  the user moderates."
  [subs-moderated]
  (reduce + (map #(+ (:open-report-count %) (:unread-modmail-count %))
                 subs-moderated)))

(defn sum-all-notifications
  "From the current-user value in the db, calculate a count of
  notifications for the title."
  [{:keys [unread-message-count unread-notification-count subs-moderated]}]
  (+ unread-message-count unread-notification-count
     (sum-mod-notifications subs-moderated)))

(re-frame/reg-sub ::notification-count
  ;; :doc Notifications for current user.
  :<- [::current-user]
  (fn [{:keys [unread-message-count unread-notification-count]} _]
    (+ unread-message-count unread-notification-count)))

(re-frame/reg-sub ::subs-moderated-names
  ;; :doc Get the names of the subs moderated by the current user.
  :<- [::subs-moderated]
  (fn [subs _]
    (->> subs
         (map :name)
         (sort-by str/lower-case))))

(re-frame/reg-sub ::is-mod-of-sub?
  ;; :doc Get whether the user is a mod of the currently viewed sub.
  :<- [::subs-moderated-names]
  :<- [:view/options]
  (fn [[names {:keys [sub]}] _]
    (some? ((set names) sub))))

(re-frame/reg-sub ::is-mod-or-admin?
  ;; :doc Determine if the user is an admin, or mod of the current sub.
  :<- [::is-admin?]
  :<- [::is-mod-of-sub?]
  (fn [[is-admin? is-mod?] _]
    (or is-admin? is-mod?)))

(defn is-mod-or-admin?
  "Determine if the current user is either an admin or mod."
  [db sub]
  (->
   (or (get-in db [:current-user :attributes :is-admin])
       ((set (internal/subs-moderated-names db)) sub))
   boolean))

(re-frame/reg-sub ::moderation-level
  ;; :doc Get the user's moderation level if they are a mod of the current sub.
  :<- [::subs-moderated]
  :<- [:view/options]
  (fn [[subs-modded {:keys [sub]}] _]
    (-> (s/select [s/ALL #(= (:name %) sub) :moderation-level] subs-modded)
        first)))

(re-frame/reg-sub ::can-edit-flair?
  ;; :doc Determine whether the user can change their own flair.
  :<- [::is-mod-or-admin?]
  :<- [::subs/sub-banned?]
  :<- [::subs/viewed-sub-info]
  (fn [[is-mod-or-admin? sub-banned? {:keys [freeform-user-flairs
                                             user-can-flair-self
                                             user-flair-choices]}] _]
    (and (not sub-banned?)
         (or freeform-user-flairs
             is-mod-or-admin?
             (and user-can-flair-self
                  (seq user-flair-choices))))))

(re-frame/reg-sub ::subs-not-moderated-names
  ;; :doc Get the names of the subs not moderated by the current user.
  :<- [::subs-moderated]
  :<- [::subs/all-sub-names]
  (fn [[subs all-subs] _]
    (let [names (set (map :name subs))]
      (->> all-subs
           (filter #(not (names %)))
           (sort-by str/lower-case)))))

(re-frame/reg-sub ::subs-moderated-sids
  ;; :doc Get the sids of the subs moderated by the current user.
  :<- [::subs-moderated]
  (fn [subs _]
    (map :sid subs)))

(re-frame/reg-sub ::mod-notification-count
  ;; :doc Get the count of moderator notifications for the user.
  :<- [::subs-moderated]
  (fn [subs _]
    (sum-mod-notifications subs)))

(re-frame/reg-sub ::sub-creation-permitted?
  ;; :doc Whether user can create subs.
  :<- [::current-user]
  (fn [user _]
    (get-in user [:attributes :is-admin])))

(re-frame/reg-sub ::subscriptions
  ;; :doc Get the user's subscriptions.
  :<- [::current-user]
  (fn [user _]
    (get user :subscriptions)))

(re-frame/reg-sub ::subscribed?
  ;; :doc Determine whether the user subscribes to the currently viewed sub.
  :<- [::subscriptions]
  :<- [::subs/viewed-sub-info]
  (fn [[subscriptions {:keys [name]}] _]
    (let [result (some #(and (= name (:name %))
                             (= :SUBSCRIBED (:status %))) subscriptions)]
      result)))

(re-frame/reg-sub ::blocked?
  ;; :doc Determine whether the user has blocked the currently viewed sub.
  :<- [::subscriptions]
  :<- [::subs/viewed-sub-info]
  (fn [[blocks {:keys [name]}] _]
    (some #(and (= name (:name %))
                (= :BLOCKED (:status %))) blocks)))

(re-frame/reg-sub ::default-subs
  ;; :doc Get the site's default subs.
  (fn [db _]
    (get-in db [:settings :default-subs])))

(re-frame/reg-sub ::sorted-subscriptions
  :<- [::subscriptions]
  ;; :doc Get the user's subscriptions in sorted order.
  (fn [subscriptions _]
    (let [max-order (->> (map :order subscriptions)
                         (remove nil?)
                         (reduce max))
          idx #(or (:order %) (+ max-order 1 (:index %)))]
      (->> subscriptions
           (filter #(= :SUBSCRIBED (:status %)))
           (map #(assoc %2 :index %1) (range))
           (sort-by idx)
           (map #(dissoc % :order :index :status))))))

(re-frame/reg-sub ::topbar-sub-names
  ;; :doc Compute the top bar sub names.
  :<- [::is-authenticated?]
  :<- [::sorted-subscriptions]
  :<- [::default-subs]
  (fn [[is-authenticated? subs default-subs] _]
    (map :name (if is-authenticated?
                 subs
                 default-subs))))

(defn change-subscription-event
  "Construct a change-subscription mutation event."
  [db sid change]
  (let [revert (get-in db [:current-user :subscriptions])]
    [::graphql/mutate {:graphql graphql
                       :name :change-subscription
                       :id (str :change-subscription change)
                       :variables {:sid sid :change change}
                       :handler [::receive-change-subscription-response
                                 {:revert revert}]}]))

(defn update-subscription
  "Update or add a new subscription for the user."
  [db {:keys [name] :as update}]
  (update-in db [:current-user :subscriptions]
             util/update-or-add update #(= name (:name %))))

(defn delete-subscription
  "Delete one of the user's subscriptions or blocks."
  [db name status]
  (let [match? #(and (= name (:name %)) (= status (:status %)))]
    (s/setval [:current-user :subscriptions s/ALL match?] s/NONE db)))

(re-frame/reg-event-fx ::subscribe
  ;; :doc Subscribe to a sub.
  (fn-traced [{:keys [db]} [_ {:keys [sid name]}]]
    {:db (update-subscription db {:name name
                                  :status :SUBSCRIBED})
     :fx [[:dispatch (change-subscription-event db sid :SUBSCRIBE)]]}))

(re-frame/reg-event-fx ::unsubscribe
  ;; :doc Unsubscribe from a sub.
  (fn-traced [{:keys [db]} [_ {:keys [sid name]}]]
    {:db (delete-subscription db name :SUBSCRIBED)
     :fx [[:dispatch (change-subscription-event db sid :UNSUBSCRIBE)]]}))

(re-frame/reg-event-fx ::block
  ;; :doc Block a sub.
  (fn-traced [{:keys [db]} [_ {:keys [sid name]}]]
    {:db (update-subscription db {:name name
                                  :status :BLOCKED})
     :fx [[:dispatch (change-subscription-event db sid :BLOCK)]]}))

(re-frame/reg-event-fx ::unblock
  ;; :doc Unblock a sub.
  (fn-traced [{:keys [db]} [_ {:keys [sid name]}]]
    {:db (delete-subscription db name :BLOCKED)
     :fx [[:dispatch (change-subscription-event db sid :UNBLOCK)]]}))

(re-frame/reg-event-db ::receive-change-subscription-response
  ;; :doc Receive the response from the change-subscription mutation.
  (fn-traced [db [event {:keys [revert response]}]]
    (if-let [errors (:errors response)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:current-user :subscriptions] revert))
      db)))

(re-frame/reg-event-fx ::subscribe-subscription-updates
  ;; :doc Subscribe to subscription updates.
  (fn-traced [_ [_ _]]
    {:fx [[:dispatch [::graphql/subscribe
                      {:graphql graphql
                       :name :subscription-updates
                       :variables {}
                       :handler [::receive-subscription-update]}]]]}))

(re-frame/reg-event-db ::receive-subscription-update
  ;; :doc Update the db to reflect a subscription update.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          update (-> (:subscription-update data)
                     (update :status keyword))
          {:keys [name status]} update]
      (cond
        errors (errors/assoc-errors db {:event event :errors errors})
        status (update-subscription db update)
        ;; If status is nil, that means the sub is neither subscribed
        ;; nor blocked.
        :else (-> db
                  (delete-subscription name :SUBSCRIBED)
                  (delete-subscription name :BLOCKED))))))

(re-frame/reg-sub ::show-sitelog?
  ;; :doc User is able to look at the sitelog.
  :<- [::current-user]
  (fn [user _]
    (get-in user [:attributes :can-admin])))

(re-frame/reg-event-fx ::logout
  ;; :doc Log out current user.
  (fn-traced [_ [_ _]]
    {:dispatch [::util/ajax-form-post ["/do/logout"
                                       {}
                                       [::logout-handler]
                                       [::logout-error-handler]]]}))

(re-frame/reg-event-fx ::logout-handler
  ;; :doc On a successful /do/logout, redirect home.
  (fn-traced [{:keys [db]} [_ _]]
    (if config/debug?
      {:db (assoc db :current-user nil)
       :fx [[::routes/redirect "/"]]}
      {:fx [[::routes/redirect "/"]]})))

(re-frame/reg-event-fx ::logout-error-handler
  (fn-traced [_ [_ {:keys [status status-text]}]]
    (log/error {:status status :status-text status-text} "Error on logout")
    {:fx [[::routes/redirect "/"]]}))

(re-frame/reg-event-fx ::admin-logout
  ;; :doc Disable admin mode.
  (fn-traced [_ [_ _]]
    {:dispatch [::util/ajax-form-post ["/admin/logout"
                                       {}
                                       [::admin-logout-handler]
                                       [::admin-logout-error-handler]]]}))

(re-frame/reg-event-fx ::admin-logout-handler
  ;; :doc On a successful /admin/logout, redirect to the admin page.
  (fn-traced [{:keys [db]} [_ _]]
    (if config/debug?
      {:db (assoc db :current-user nil)
       :fx [[::routes/redirect "/admin"]]}
      {:fx [[::routes/redirect "/admin"]]})))

(re-frame/reg-event-fx ::admin-logout-error-handler
  (fn-traced [_ [_ {:keys [status status-text]}]]
    (log/error {:status status :status-text status-text}
               "Error on admin logout")
    {:fx [[::routes/redirect "/admin"]]}))

(re-frame/reg-event-fx ::subscribe-notification-counts
  ;; :doc Subscribe to notification counts.
  (fn-traced [_ [_ _]]
    {:fx [[:dispatch [::graphql/subscribe
                      {:graphql graphql
                       :name :notification-counts
                       :variables {}
                       :handler [::receive-notification-counts]}]]]}))

(defn update-mod-counts
  "Update the notification counts in the list of subs the user moderates
  with new values."
  [subs-moderated sub-notifications]
  (let [by-sid (->> sub-notifications
                    (map (fn [{:keys [sid] :as elem}]
                           [sid (select-keys elem [:open-report-count
                                                   :unread-modmail-count])]))
                    (into {}))
        update-key (fn [{:keys [sid] :as sub} k]
                     (update sub k
                             #(or (get-in by-sid [sid k]) %)))]
    (map #(-> %
              (update-key :open-report-count)
              (update-key :unread-modmail-count))
         subs-moderated)))

(re-frame/reg-event-fx ::receive-notification-counts
  ;; :doc Receive a notification count update.
  ;; :doc Incorporate any non-nil values in it into the db.  If the user is
  ;; :doc viewing their mod dashboard, refresh those numbers.
  (fn-traced [{:keys [db]} [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          update (:notification-counts data)
          {:keys [unread-message-count
                  unread-notification-count
                  sub-notifications]} update
          {:keys [active-panel options]} (:view db)]
      {:db (-> db
               (errors/assoc-errors {:event event :errors errors})
               (update-in [:current-user :unread-message-count]
                          #(or unread-message-count %))
               (update-in [:current-user :unread-notification-count]
                          #(or unread-notification-count %))
               (update-in [:current-user :subs-moderated]
                          #(update-mod-counts % sub-notifications)))
       :fx [[:dispatch [::page-title/refresh]]
            (when (and (= active-panel :mod-dashboard)
                       (not (:show-other-circles? options)))
              [:dispatch [::subs/load-current-user-mod-stats]])]})))

(re-frame/reg-event-fx ::change-user-flair
  ;; :doc Change the user's flair on the server.
  ;; :doc No optimistic update, because the user will be looking
  ;; :doc at the modal until the response gets back.
  (fn-traced [{:keys [db]} [_ {:keys [flair sid]} update-form-event]]
    {:fx [[:dispatch
           [::graphql/mutate
            {:graphql graphql
             :name :change-user-flair
             :id (str :change-user-flair flair)
             :variables {:user_flair flair
                         :uid (get-in db [:current-user :uid])
                         :sid sid}
             :handler  [::receive-user-flair-change
                        {:sid sid
                         :update-form-event update-form-event}]}]]]}))

(re-frame/reg-event-fx ::remove-user-flair
  ;; :doc Remove the user's flair in a sub on the server.
  ;; :doc No optimistic update, because the user will be looking
  ;; :doc at the modal until the response gets back.
  (fn-traced [{:keys [db]} [_ {:keys [sid]} update-form-event]]
    {:fx [[:dispatch
           [::graphql/mutate
            {:graphql graphql
             :name :change-user-flair
             :id :change-user-flair
             :variables {:user_flair nil
                         :uid (get-in db [:current-user :uid])
                         :sid sid}
             :handler [::receive-user-flair-change
                       {:sid sid
                        :update-form-event update-form-event}]}]]]}))

(re-frame/reg-event-fx ::receive-user-flair-change
  ;; :doc Receive the response from the server to updating user flair.
  ;; :doc Update the flair in the db and hide the modal on success.
  (fn-traced [{:keys [db]} [event {:keys [sid update-form-event response]}]]
    (let [{:keys [data errors]} response
          db-with-errors (errors/assoc-errors db {:errors errors :event event})]
      {:db (if errors
             db-with-errors
             (-> db
                 (subs/update-sub-user-flair sid
                                             (get-in db [:current-user :uid])
                                             (:change-user-flair data))
                 (assoc-in [:ui-state :user-flair-modal] false)))
       :fx [[:dispatch update-form-event]]})))

(re-frame/reg-sub ::nsfw-settings
  ;; :doc Return the nsfw settings for the current user or for anons.
  :<- [::is-authenticated?]
  :<- [::current-user]
  :<- [::settings/nsfw-anon-show?]
  :<- [::settings/nsfw-anon-blur?]
  (fn [[is-authenticated? current-user nsfw-anon-show? nsfw-anon-blur?] _]
    (let [{:keys [nsfw nsfw-blur]} (:attributes current-user)]
      (if is-authenticated?
        {:show? nsfw
         :blur? nsfw-blur}
        {:show? nsfw-anon-show?
         :blur? nsfw-anon-blur?}))))

(re-frame/reg-event-fx ::load-content-blocks
  ;; :doc Fetch the user's list of content blocked users from the server.
  ;; :doc Only necessary for authenticated users who are not admins.
  (fn-traced [{:keys [db]} [_ _]]
    (if (and (nil? (get-in db [:current-user :uid]))
             (not (get-in db [:current-user :attributes :can-admin])))
      {:db (assoc-in db [:status :content-blocks] :loaded)}
      (when (= :not-loaded (get-in db [:status :content-blocks]))
        {:db (assoc-in db [:status :content-blocks] :loading)
         :fx [[:dispatch [::graphql/query
                          {:graphql graphql
                           :name :content-blocks
                           :variables {}
                           :handler [::receive-content-blocks]}]]]}))))

(defn- flatten-content-blocks
  "Convert content blocks from the server into a map.
  The keys will be uids and the values will be block types."
  [blocks]
  (->> blocks
       (map (fn [{:keys [user content-block]}]
              [(:uid user) (keyword content-block)]))
       (into {})))

(re-frame/reg-event-db ::receive-content-blocks
  ;; :doc Receive content blocks from the server and add them to the db.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (assoc-in [:status :content-blocks] :loaded)
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:current-user :content-blocks]
                    (-> data
                        :current-user :content-blocks
                        flatten-content-blocks))))))

(re-frame/reg-sub ::content-blocks
  ;; :doc Extract the user's content blocks of other users.
  (fn [db _]
    (get-in db [:current-user :content-blocks])))

(re-frame/reg-sub ::content-block
  ;; :doc Extract the user's content block of another user, if any.
  :<- [::content-blocks]
  (fn [blocks [_ uid]]
    (get blocks uid)))

(defn level
  "Calculate a user's level from their score."
  [{:keys [score]}]
  (if (or (nil? score) (neg? score))
    0
    (Math/sqrt (/ score 10))))

(defn can-upload?
  "Determine whether the user can upload files."
  [{:keys [current-user settings]}]
  (or (:is-admin current-user)
      (>= (level current-user) (:upload-min-level settings))))

(re-frame/reg-sub ::can-upload?
  ;; :doc Determine whether the user can upload files.
  :<- [::current-user]
  :<- [::settings/site-config]
  (fn [[user settings] _]
    (or (:is-admin user)
        (>= (level user) (:upload-min-level settings)))))
