;; views.cljs -- Page rendering for throat-fe
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-fe.views
  (:require
   [re-frame.core :refer [subscribe]]
   [throat-fe.views.admin :as admin]
   [throat-fe.views.auth :as auth]
   [throat-fe.views.donate :as donate]
   [throat-fe.views.funding-progress :as funding-progress]
   [throat-fe.views.messages :as messages]
   [throat-fe.views.misc-panels :as misc-panels]
   [throat-fe.views.moderation :as moderation]
   [throat-fe.views.modmail :as modmail]
   [throat-fe.views.post-listing :as post-listing]
   [throat-fe.views.single-post :as single-post]
   [throat-fe.views.submit-post :as submit-post]
   [throat-fe.views.wiki :as wiki]))

(defn show-panel [panel-name]
  (case panel-name
    :login                     [auth/login-panel]
    :registration              [auth/registration-panel]
    :verify-registration       [auth/verify-registration-panel]
    :confirm-registration      [auth/confirm-registration-panel]
    :resend-verification-email [auth/resend-verification-email-panel]
    :start-reset-password      [auth/start-reset-password-panel]
    :complete-reset-password   [auth/complete-reset-password-panel]
    :edit-account              [auth/edit-account-panel]
    :rename-account            [auth/rename-account-panel]
    :confirm-email             [auth/confirm-email-panel]
    :delete-account            [auth/delete-account-panel]

    :initializing              [misc-panels/initializing-panel]
    :post-listing              [post-listing/post-listing-panel]
    :single-post               [single-post/single-post-panel]
    :submit-post               [submit-post/submit-post-panel]
    :wiki-page                 [wiki/wiki-panel]
    :error-page                [misc-panels/error-panel]
    :mod-dashboard             [moderation/mod-dashboard-panel]
    :mod-edit-sub-rules        [moderation/mod-edit-rules-panel]
    :mod-edit-post-flairs      [moderation/mod-edit-post-flairs-panel]
    :mod-edit-post-types       [moderation/mod-edit-post-types-panel]
    :modmail                   [modmail/modmail-panel]
    :modmail-compose           [modmail/modmail-compose-panel]
    :contact-mods              [messages/contact-mods-panel]
    :admin-stats               [admin/stats-panel]
    :admin-banned-user-names   [admin/banned-user-names]
    :admin-invite-codes        [admin/invite-codes-panel]
    :donate-setup              [donate/setup-panel]
    :donate-cancel             [donate/cancel-panel]
    :donate-success            [donate/success-panel]
    :donate-funding-progress   [funding-progress/funding-progress-content]

    [misc-panels/page-not-found-panel]))

(defn main-panel []
  (let [active-panel @(subscribe [:view/active-panel])]
    [show-panel active-panel]))
