;; https://github.com/bhauman/devcards/issues/168
(ns cljsjs.highlight
  (:require ["highlight.js/lib/core.js" :as hljs]))

(js/goog.exportSymbol "hljs" hljs)
(js/goog.exportSymbol "DevcardsSyntaxHighlighter" hljs)
