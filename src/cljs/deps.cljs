{:npm-dev-deps {"shadow-cljs"           "2.18.0"
                "karma"                 "6.3.19"
                "karma-chrome-launcher" "3.1.1"
                "karma-cljs-test"       "0.1.0"
                "karma-junit-reporter"  "2.0.1"
                "marked"                "1.2.5"
                "highlight.js"          "11.1.0"}

 :npm-deps {"markdown-it"                 "12.3.2"
            "markdown-it-link-attributes" "4.0.0"
            "markdown-it-reddit-spoiler"  "1.0.1"
            "markdown-it-sub"             "1.0.0"
            "markdown-it-sup"             "1.0.0"
            "pikaday"                     "1.8.2"
            "moment"                      "2.29.3"}}
